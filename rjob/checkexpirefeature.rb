#m h  dom mon dow   command
#00 03 * * * cd /var/sites/rethai.com; ./script/runner ./rjob/checkexpirefeature.rb



chkusers = Product.find(:all,:conditions=>"(expiredate <='#{DateTime.now.strftime('%Y-%m-%d')}' or magazinedate <='#{DateTime.now.strftime('%Y-%m-%d')}') and Deleted = '1' ",:group=>"id_user",:order=>"id_user")
for chkuser in chkusers
    user = User.find(chkuser.id_user)
  
     listings = Product.find(:all,
                              :select=>"options.Name as typename,products.City,products.Price,products.RPrice,products.Bed,products.Bath,products.salerent,products.updated_on,products.expiredate,products.magazinedate ",
                              :joins=>"INNER JOIN options ON products.Type = options.Value ",
                              :conditions=>"(products.expiredate <='#{DateTime.now.strftime('%Y-%m-%d')}' or products.magazinedate <='#{DateTime.now.strftime('%Y-%m-%d')}') and products.Deleted = '1' and products.id_user = '#{user.id}' and options.option_type = 'Listing'")
     str_listing = ""
     
       str_listing = str_listing + "<table>  "
         str_listing = str_listing + "<tr>  "
         str_listing = str_listing + "    <td><b>Property Type</b></td>"
         str_listing = str_listing + "    <td><b>Type</b></td>"
         str_listing = str_listing + "    <td><b>Location</b></td>"
         str_listing = str_listing + "    <td><b>Price</b></td>"
         str_listing = str_listing + "    <td><b>Bedroom</b></td>"
         str_listing = str_listing + "    <td><b>Bathroom</b></td>"
         str_listing = str_listing + "    <td><b>Last Update</b></td>" 
         str_listing = str_listing + "    <td><b>Link</b></td> "
         str_listing = str_listing + "</tr>  "
       for listing in listings
         feature={}
         if listing.expiredate.strftime("%Y%m%d") <= DateTime.now.strftime('%Y-%m-%d')
           # ---- feature expire  ----
           feature["SearchFeatured"] ="0"
           feature["homefeatured"] ="0"
           feature["condofeatured"] ="0"
         end
         if listing.magazinedate.strftime("%Y%m%d") <= DateTime.now.strftime('%Y-%m-%d')
           # ---- feature expire  ----
           feature["magazine"] ="0"
         end
        price = "" 
        salerent = "Sale"
        if listing.salerent.to_s =="1"
          price = listing.Price
          if price.to_f >= 1000
             price = price.to_s.reverse
             price = price.gsub!(/([0-9]{3})/,"\\1,")
             price = price.gsub(/,$/,"").reverse
         end
          
        end
        if listing.salerent.to_s =="0"
          salerent = "Rent"          
          price = listing.RPrice
          if price.to_f >= 1000
             price = price.to_s.reverse
             price = price.gsub!(/([0-9]{3})/,"\\1,")
             price = price.gsub(/,$/,"").reverse
         end
        elsif listing.salerent.to_s =="2"
          salerent = "Sale or Rent"
          strprice = listing.Price
          if strprice.to_f >= 1000
             strprice = strprice.to_s.reverse
             strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
             strprice = strprice.gsub(/,$/,"").reverse
         end
         price = strprice
          strprice = listing.RPrice
          if strprice.to_f >= 1000
             strprice = strprice.to_s.reverse
             strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
             strprice = strprice.gsub(/,$/,"").reverse
         end
         
         price = price + ", #{strprice}/month"
         
        end    
         
         strdate = listing.updated_on.strftime("%d/%m/%Y")
         bedroom = ""
         if listing.Bed.to_s =="0"
           bedroom = "Studio"
         elsif listing.Bed.to_s =="6"           
           bedroom = "6 up"
         else           
           bedroom = "#{listing.Bed}"
         end
         
         str_listing = str_listing + "<tr>  "
         str_listing = str_listing + "    <td>#{listing.typename}</td>"
         str_listing = str_listing + "    <td>#{salerent}</td>"
         str_listing = str_listing + "    <td>#{listing.City}</td>"
         str_listing = str_listing + "    <td>#{price}</td>"
         str_listing = str_listing + "    <td>#{bedroom}</td>"
         str_listing = str_listing + "    <td>#{listing.Bath}</td>"
         str_listing = str_listing + "    <td>#{strdate}</td>" 
         str_listing = str_listing + "     <td><a href='http://www.rethai.com/rethai/#{listing.id}'>#{listing.typename} for #{salerent} #{price} </a></td> "
         str_listing = str_listing + "</tr>  " 
         listing.update_attributes(feature)  
         
         
       end
       str_listing = str_listing + "</table>  "
  
          message = ""
          message = message + "<img src='http://www.rethai.com/images/logo.png' /><br><br>"
          message = message + "Hello,<br>\n<br>\n "
          message = message + "Your featured listing has expired.<br>\n<br>\n "
          message = message + "Please login to rethai.com and visit the control panel to upgrade your listing.<br>\n<br>\n"
          message = message + "#{str_listing}\n<br /><br /><br />\n"
          message = message + "If you have any problems, please contact billing@rethai.com<br>\n<br>\n"
          message = message + "Thanks !<br>\n"
          message = message + "REThai Team<br>\nhttp://www.rethai.com"
          message = message + ""
#          Notifier.deliver_inforation(user.Email,message)
          Notifier.deliver_standardemail("REThai.com <renewals@REThai.com>",user.Email,"Listing Renewal - upgrade your listing",message)   
    
end  #end for listing in @listings





#update templistings table
    Templisting.delete_all("`created_at` <= '#{(DateTime.now - 1).to_date}'")
    
    
    
    