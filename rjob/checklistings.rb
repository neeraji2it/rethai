#run this file on background (crontab) in every day

#check for offline (not update 2 month)
sql = "update products set Deleted = 'p',`Status`='f' where Deleted = '1' and updated_on < '#{(DateTime.now - 14.month).strftime('%Y-%m-%d')}'"
Product.connection.execute(sql)

@listings = Product.find(:all,
                          :select=>"products.*,users.Email as useremail",
                          :joins=>"inner join users on products.id_user = users.id",
                          :conditions=>" products.Deleted = '1' and products.updated_on between ('#{(DateTime.now - 14.month).strftime('%Y-%m-%d')}' and '#{(DateTime.now - 13.month).strftime('%Y-%m-%d')}') ",
                          :group=>"products.id_user",:order=>"products.id_user")

for listing in @listings
  #send  email to users
  
          message = ""
          message = message + "<img src='http://www.rethai.com/images/logo.png' /><br><br>"
          message = message + "Hello,<br>\n<br>\n "
          message = message + "You have listings on REThai.com that are about to expire. To ensure quality, we auto-expire any listings that have not been updated for 12 months. If your listings are still active, please login to the REThai.com Control Panel and update your listings.<br>\n<br>\n<br>\n"
          alllistings = Product.find(:all,:conditions=>" Deleted = '1' and id_user = '#{listing.id_user}' and updated_on between ('#{(DateTime.now - 14.month).strftime('%Y-%m-%d')}' and '#{(DateTime.now - 13.month).strftime('%Y-%m-%d')}') ")
          if alllistings.length > 20
            # this is  aggency user
              message = message + "Visit tihs link to view your expiring listings: http://www.rethai.com/controlpanel/expirelistings<br />\n<br />\n"
          else
            message = message + "<table>\n"
            for al in alllistings
              search_link=""
#----------------------------------------------------------------------------------------------------------------
                listingprice = ""
                   if al.salerent.to_s == '1'  # sale
                  lsalerent = "sale"
                    elsif al.salerent.to_s == '0' # rent    
                  lsalerent = "rent"
                    else  # sale / rent   
                  lsalerent = "sale or rent"
                    end 
      
                        
                  #get listing type
                showlistingtype = ""
                    if al.Type.to_s != '0' and al.Type.to_s != "" and al.Type !=nil
                      listingtypes = Option.find(:all,:conditions=>"`option_type` = 'Listing' and `Value` = '#{al.Type}'")
                      if listingtypes.length > 0
                        for lt in listingtypes
                          showlistingtype = "#{lt.Name}"  
                            if showlistingtype[" / "]       
                              showlistingtype[" / "] ="-"   
                            end 
                          break
                        end
                      end
                  end
                  
                
                
                    showcity = ""
                  if al.City.to_s !="" and al.City.to_s !="0" and al.City != nil
                    showcity = al.City.downcase
                      while showcity[" "]       
                        showcity[" "] ="_"    
                      end
                  end
                if al.salerent.to_s =="0"
                  search_link="http://www.rethai.com/rethai/#{showcity}-#{showlistingtype}-for-#{lsalerent}-#{al.RPrice}-#{al.id}"
                else
                  search_link="http://www.rethai.com/rethai/#{showcity}-#{showlistingtype}-for-#{lsalerent}-#{al.Price}-#{al.id}"
                end
                begin
                  if al.titlelink.to_s !=""
                    search_link = "#{al.titlelink}-#{al.id}"
                  end
                rescue
                end
#----------------------------------------------------------------------------------------------------------------
              
              message = message + "<tr><td width='350'>#{search_link}</td><td><a href='http://www.rethai.com/controlpanel/reupdate/#{al.id}?delete=0'>Click here to keep this listing active</a></td></tr>\n"
            end  #end for al in alllistings
            message = message + "</table>\n"
          end  #end if alllistings.length > 20
          
          
          
          message = message + "<br>\n<br>\n<br>\n<br>\nIf you have any problems, please contact info@rethai.com<br>\n<br>\n"
          message = message + "Thanks!<br>\n"
          message = message + "REThai Team<br>\nhttp://www.rethai.com"
          message = message + ""
          Notifier.deliver_standardemail("REThai.com <renewals@REThai.com>",listing.useremail,"REThai.com Listing Expiration",message)   
        
  
end  #end for listing in @listings