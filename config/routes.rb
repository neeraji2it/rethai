ActionController::Routing::Routes.draw do |map|

  #domain routing
  map.connect '', :controller => 'listings', :action => 'partnermaps', :id => '18', :conditions => { :host => 'map.bkkcondos.com' }
  map.connect '', :controller => 'listings', :action => 'search', :partner => 'thailandrealestateforum', :code => 'bangkok', :conditions => { :host => 'property.thailandrealestateforum.com' }
  map.connect '', :controller => 'listings', :action => 'search', :partner => 'aacondo', :code => 'bangkok', :conditions => { :host => 'property.aacondo.com' }
  map.connect '', :controller => 'listings', :action => 'search', :partner => 'pattayaone', :code => 'pattaya', :conditions => { :host => 'realestate.pattayaone.net' }
  map.connect '', :controller => 'listings', :action => 'search', :partner => 'thaivisa', :code => 'bangkok', :conditions => { :host => 'realestate.thaivisa.com' }
  map.connect '', :controller => 'virtual', :action => 'index', :conditions => { :host => 'virtual.rethai.com' }

#  map.connect '', :controller => 'listings', :action => 'search', :partner => 'property', :conditions => { :host => 'property.phuket-post.com' }

  map.root :controller => 'index', :action => 'index'

  map.connect 'listings/search', :controller => 'listings', :action => 'search', :partner => 'rethai', :seo => "Property Search "
  map.connect 'listings/show', :controller => 'listings', :action => 'show', :partner => 'rethai'  
  
  map.location 'maps/pattaya-real-estate',   :controller => 'listings', :action => 'maps', :location => 'pattaya', :seo => "Pattaya Property "
  map.location 'maps/bangkok-real-estate',   :controller => 'listings', :action => 'maps', :location => 'bangkok', :seo => "Bangkok Property "
  map.location 'maps/hua-hin-real-estate',    :controller => 'listings', :action => 'maps', :location => 'hua hin', :seo => "Hua Hin Property "
  map.location 'maps/chiang-mai-real-estate',  :controller => 'listings', :action => 'maps', :location => 'chiang mai', :seo => "Chiang Mai Property "
  map.location 'maps/phuket-real-estate',    :controller => 'listings', :action => 'maps', :location => 'phuket', :seo => "Phuket Property "
  map.location 'maps/rayong-real-estate',    :controller => 'listings', :action => 'maps', :location => 'rayong', :seo => "Rayong Property "
  map.location 'maps/sattahip-real-estate',    :controller => 'listings', :action => 'maps', :location => 'sattahip', :seo => "Sattahip Property "

  #rethai ## PARTNERS - NEED TO ALSO EDIT THE APPLICATION CONTROLLER
  map.partner 'rethai',                :controller => 'index',    :action => 'index',          :partner => 'rethai'
  map.partner 'rethai/search',         :controller => 'listings', :action => 'search',         :partner => 'rethai'
  map.partner 'listings/rethai/search',:controller => 'listings', :action => 'search',         :partner => 'rethai'
  map.partner 'rethai/compare',        :controller => 'listings', :action => 'compare',        :partner => 'rethai'
  map.partner 'rethai/new',            :controller => 'listings', :action => 'new',            :partner => 'rethai'
  map.partner 'rethai/register',       :controller => 'listings', :action => 'register',       :partner => 'rethai'
  map.partner 'rethai/searchlistings', :controller => 'listings', :action => 'searchlistings', :partner => 'rethai'
  map.partner 'rethai/show',           :controller => 'listings', :action => 'show',           :partner => 'rethai'
  map.partner 'rethai/login',          :controller => 'listings', :action => 'login',          :partner => 'rethai'
  map.partner 'rethai/addlisting',     :controller => 'listings', :action => 'addlisting',     :partner => 'rethai'

  #pattayaone
  map.partner 'pattayaone',                :controller => 'listings', :action => 'search',          :partner => 'pattayaone'
  map.partner 'pattayaone/search',         :controller => 'listings', :action => 'search',         :partner => 'pattayaone'
  map.partner 'pattayaone/compare',        :controller => 'listings', :action => 'compare',        :partner => 'pattayaone'
  map.partner 'pattayaone/new',            :controller => 'listings', :action => 'new',            :partner => 'pattayaone'
  map.partner 'pattayaone/register',       :controller => 'listings', :action => 'register',       :partner => 'pattayaone'
  map.partner 'pattayaone/searchlistings', :controller => 'listings', :action => 'searchlistings', :partner => 'pattayaone'
  map.partner 'pattayaone/show',           :controller => 'listings', :action => 'show',           :partner => 'pattayaone'
  map.partner 'pattayaone/login',          :controller => 'listings', :action => 'login',          :partner => 'pattayaone'
  map.partner 'pattayaone/addlisting',     :controller => 'listings', :action => 'addlisting',     :partner => 'pattayaone'


  #cities
  map.location 'pattaya-real-estate', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya "
  map.location 'bangkok-real-estate', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok Prerty "
  map.location 'chiang-mai-real-estate', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai "
  map.location 'phuket-real-estate', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket "
  map.location 'rayong-real-estate', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong "
  map.location 'sattahip-real-estate', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip "
  map.location 'hua-hin-real-estate', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin "
  map.location 'koh-samui-real-estate', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui "
  map.location 'sattahip-real-estate',    :controller => 'listings', :action => 'search', :code => 'sattahip',      :partner => 'rethai', :seo => "Sattahip "

  #rentals
  map.location 'pattaya-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya ", :salerent => '0'
  map.location 'bangkok-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok ", :salerent => '0'
  map.location 'chiang-mai-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai ", :salerent => '0'
  map.location 'phuket-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket ", :salerent => '0'
  map.location 'rayong-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong ", :salerent => '0'
  map.location 'sattahip-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip ", :salerent => '0'
  map.location 'hua-hin-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin ", :salerent => '0'
  map.location 'koh-samui-real-estate-for-rent', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui ", :salerent => '0'

  #condos for sale
  map.location 'pattaya-condos-apartments', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya ", :salerent => '1', :propertytype => '1'
  map.location 'bangkok-condos-apartments', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok ", :salerent => '1', :propertytype => '1'
  map.location 'chiang-mai-condos-apartments', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai ", :salerent => '1', :propertytype => '1'
  map.location 'phuket-condos-apartments', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket ", :salerent => '1', :propertytype => '1'
  map.location 'rayong-condos-apartments', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong ", :salerent => '1', :propertytype => '1'
  map.location 'sattahip-condos-apartments', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip ", :salerent => '1', :propertytype => '1'
  map.location 'hua-hin-condos-apartments', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin ", :salerent => '1', :propertytype => '1'
  map.location 'koh-samui-condos-apartments', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui ", :salerent => '1', :propertytype => '1'

  #condos for rent
  map.location 'pattaya-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya ", :salerent => '0', :propertytype => '1'
  map.location 'bangkok-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok ", :salerent => '0', :propertytype => '1'
  map.location 'chiang-mai-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai ", :salerent => '0', :propertytype => '1'
  map.location 'phuket-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket ", :salerent => '0', :propertytype => '1'
  map.location 'rayong-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong ", :salerent => '0', :propertytype => '1'
  map.location 'sattahip-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip ", :salerent => '0', :propertytype => '1'
  map.location 'hua-hin-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin ", :salerent => '0', :propertytype => '1'
  map.location 'koh-samui-condos-apartments-for-rent', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui ", :salerent => '0', :propertytype => '1'

  #property for sale ## LEGACY - try to phase out
  map.location 'property-for-sale-pattaya', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya ", :salerent => '1'
  map.location 'property-for-sale-bangkok', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok ", :salerent => '1'
  map.location 'property-for-sale-chiang-mai', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai ", :salerent => '1'
  map.location 'property-for-sale-phuket', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket ", :salerent => '1'
  map.location 'property-for-sale-rayong', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong ", :salerent => '1'
  map.location 'property-for-sale-sattahip', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip ", :salerent => '1'
  map.location 'property-for-sale-hua-hin', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin ", :salerent => '1'
  map.location 'property-for-sale-koh-samui', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui ", :salerent => '1'
  
  #property for sale
  map.location 'pattaya-property-for-sale', :controller => 'listings', :action => 'search', :code => 'pattaya', :partner => 'rethai', :seo => "Pattaya ", :salerent => '1'
  map.location 'bangkok-property-for-sale', :controller => 'listings', :action => 'search', :code => 'bangkok', :partner => 'rethai', :seo => "Bangkok ", :salerent => '1'
  map.location 'chiang-mai-property-for-sale', :controller => 'listings', :action => 'search', :code => 'chiang mai', :partner => 'rethai', :seo => "Chiang Mai ", :salerent => '1'
  map.location 'phuket-property-for-sale', :controller => 'listings', :action => 'search', :code => 'phuket', :partner => 'rethai', :seo => "Phuket ", :salerent => '1'
  map.location 'rayong-property-for-sale', :controller => 'listings', :action => 'search', :code => 'rayong', :partner => 'rethai', :seo => "Rayong ", :salerent => '1'
  map.location 'sattahip-property-for-sale', :controller => 'listings', :action => 'search', :code => 'sattahip', :partner => 'rethai', :seo => "Sattahip ", :salerent => '1'
  map.location 'hua-hin-property-for-sale', :controller => 'listings', :action => 'search', :code => 'hua hin', :partner => 'rethai', :seo => "Hua Hin ", :salerent => '1'
  map.location 'koh-samui-property-for-sale', :controller => 'listings', :action => 'search', :code => 'koh samui', :partner => 'rethai', :seo => "Koh Samui ", :salerent => '1'


	#navigation items
  map.location 'thailand-condos', :controller => 'condo', :action => 'home', :code => 'thailand', :seo => "Thailand "
  map.location 'pattaya-condos', :controller => 'condo', :action => 'home', :code => 'pattaya', :seo => "Pattaya "
  map.location 'bangkok-condos', :controller => 'condo', :action => 'home', :code => 'bangkok', :seo => "Bangkok "
  map.location 'phuket-condos', :controller => 'condo', :action => 'home', :code => 'phuket', :seo => "Phuket "

  map.location 'thailand-houses', :controller => 'house', :action => 'home', :code => 'thailand', :seo => "Thailand "
  map.location 'pattaya-houses', :controller => 'house', :action => 'home', :code => 'pattaya', :seo => "Pattaya "
  map.location 'bangkok-houses', :controller => 'house', :action => 'home', :code => 'bangkok', :seo => "Bangkok "
  map.location 'phuket-houses', :controller => 'house', :action => 'home', :code => 'phuket', :seo => "Phuket "
  map.location 'chiangmai-houses', :controller => 'house', :action => 'home', :code => 'chiangmai', :seo => "Chiang Mai "




  map.location 'developer/alldevelopers', :controller => 'development', :action => 'alldevelopments'
  map.location 'developer/:id', :controller => 'development', :action => 'index'
  
  map.location 'condo/allcondos', :controller => 'condo', :action => 'allcondos'
  map.location 'condo/getcondo', :controller => 'condo', :action => 'getcondo'
  map.location 'condo/:id', :controller => 'condo', :action => 'index'
  
  #houses  
#  map.location 'house/gethouse', :controller => 'house', :action => 'gethouse'
  
  map.location 'agency/allagencies', :controller => 'agency', :action => 'allagencies'
  map.location 'agency/:id', :controller => 'agency', :action => 'index'

  map.location 'listings/rss/:id.xml', :controller => 'listings', :action => 'rss'
  map.connect ':controller/:action/:id'
  map.show ':partner/:id', :controller => 'listings', :action => 'show'












  # See how all your routes layout with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.
#  map.connect ':controller/:action/:id'
#  map.connect ':controller/:action/:id.:format'
end
