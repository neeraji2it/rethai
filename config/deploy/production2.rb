default_run_options[:pty] = true
#ssh_options[:paranoid] = false 

set :application, "www.rethai.com"
set :rails_env, "production"
set :rails_port, "8000"

set :user, "foxxx"
set :domain, "www.rethai.com"
set :password, "kSiS6Nd9N"

set :deploy_to, "/var/sites/rethai.com/production"
set :port, "6446"
#set :mongrel_conf, "#{deploy_to}/config/mongrel_cluster.yml"

#122.155.18.20
#122.155.18.102

role :web, "122.155.18.102"
role :app, "122.155.18.102"
role :db,  "122.155.18.102", :primary => true

set :repository, "svn+rethai-svn://files.rethai.com/var/sites/rethai.com/svn/rethai/trunk"


namespace(:deploy) do
  desc "Server Restart"
  task :restart, :roles => :app do
    run "cd #{deploy_to}/current/"
#    sudo "mongrel_rails cluster::restart -C #{deploy_to}/current/config/mongrel_cluster.yml"
    sudo "mongrel_rails cluster::stop -C #{deploy_to}/current/config/mongrel_cluster.yml"
#    sudo "thin stop -C #{deploy_to}/current/config/thin-production.yml"
    sleep 10
    sudo "mongrel_rails cluster::start -C #{deploy_to}/current/config/mongrel_cluster.yml"
#    sudo "thin start -C #{deploy_to}/current/config/thin-production.yml"
    sudo "rm -f /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    sudo "ln -s /var/sites/rethai.com/files/current/public/#{rails_env}/listingdata/ /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    sudo "ln -s /var/www/xmlsitemap/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-ru/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/ru-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-th/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/th-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-fr/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/fr-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-de/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/de-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-nl/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/nl-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-kr/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/kr-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-jp/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/jp-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-it/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/it-sitemap.xml"
  end
end

namespace(:deploy) do
  desc "Server Stop"
  task :stop, :roles => :app do
    run "cd #{deploy_to}/current/"
    sudo "mongrel_rails cluster::stop -C #{deploy_to}/current/config/mongrel_cluster.yml"
#    sudo "thin stop -C #{deploy_to}/current/config/thin-production.yml"
  end
end

namespace(:deploy) do
  desc "Server Start"
  task :start, :roles => :app do
    run "cd #{deploy_to}/current/"
    sudo "mongrel_rails cluster::start -C #{deploy_to}/current/config/mongrel_cluster.yml"
#    sudo "thin stop -C #{deploy_to}/current/config/thin-production.yml"
    sudo "rm -f /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    sudo "ln -s /var/sites/rethai.com/files/current/public/#{rails_env}/listingdata/ /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    sudo "ln -s /var/www/xmlsitemap/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-ru/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/ru-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-th/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/th-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-fr/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/fr-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-de/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/de-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-nl/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/nl-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-kr/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/kr-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-jp/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/jp-sitemap.xml"
    sudo "ln -s /var/www/xmlsitemap-it/ror.xml /var/sites/rethai.com/#{rails_env}/current/public/it-sitemap.xml"
  end
end

namespace(:migrate) do
  desc "Migrate the Database"
  task :migrate, :roles => :db do
    run  "cd #{deploy_to}/current/"
    sudo "rake db:migrate RAILS_ENV=#{rails_env}"
  end
end

desc "Write current revision to app/layouts/_revision.rhtml"
task :publish_revision do
  run "svn info #{release_path} | grep ^Revision > #{release_path}/app/views/layouts/_revision.erb"
end

desc "Run this after update_code"
task :after_update_code do
  publish_revision
end
