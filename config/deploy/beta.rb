default_run_options[:pty] = true
#ssh_options[:paranoid] = false 

set :application, "beta.rethai.com"
set :rails_env, "beta"
set :rails_port, "9000"

set :user, "foxxx"
set :domain, "beta.rethai.com"
set :password, "kSiS3Nd9N"

set :deploy_to, "/var/sites/rethai.com/beta"
set :port, "22"

#122.155.18.20
#122.155.18.102

role :web, "122.155.18.102"
role :app, "122.155.18.102"
role :db,  "122.155.18.102", :primary => true


set :repository, "svn+rethai-svn://foxxx@rethai.com/var/sites/rethai.com/svn/repository/trunk"


namespace(:deploy) do
  desc "Server Restart"
  task :restart, :roles => :app do
    sudo "touch /var/sites/rethai.com/beta/current/tmp/restart.txt"
    sudo "ln -s /var/sites/rethai.com/files/current/public/production/listingdata /var/sites/rethai.com/beta/current/public/listingdata"
  end
end

#namespace(:deploy) do
#  desc "Server Stop"
#  task :stop, :roles => :app do
#    run "cd #{deploy_to}/current/"
#    sudo "mongrel_rails cluster::stop -C #{deploy_to}/current/config/mongrel_cluster.yml"
#  end
#end

#namespace(:deploy) do
#  desc "Server Start"
#  task :start, :roles => :app do
#    run "cd #{deploy_to}/current/"
#    sudo "mongrel_rails cluster::start -C #{deploy_to}/current/config/mongrel_cluster.yml"
#  end
#end


namespace(:migrate) do
  desc "Migrate the Database"
  task :migrate, :roles => :db do
    run  "cd #{deploy_to}/current/"
    sudo "rake db:migrate RAILS_ENV=#{rails_env}"
  end
end

desc "Write current revision to app/layouts/_revision.rhtml"
task :publish_revision do
  run "svn info #{release_path} | grep ^Revision > #{release_path}/app/views/layouts/_revision.erb"
end

desc "Run this after update_code"
task :after_update_code do
  publish_revision
end
