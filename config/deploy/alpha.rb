
default_run_options[:pty] = true
#ssh_options[:paranoid] = false 

set :application, "alpha.rethai.com"
set :rails_env, "alpha"
set :rails_port, "3100"

set :user, "foxxx"
set :domain, "alpha.rethai.com"
set :password, "kSiS3Nd9N"

set :deploy_to, "/var/sites/rethai.com/alpha"
set :port, "22"

role :web, "122.155.18.102"
role :app, "122.155.18.102"
role :db,  "122.155.18.102", :primary => true

set :repository, "svn+rethai-svn://files.rethai.com/var/sites/rethai.com/svn/rethai/trunk"


set :repository, "svn+rethai-svn://foxxx@svn.rethai.com/var/sites/rethai.com/svn/repository/trunk"


namespace(:deploy) do
  desc "Server Restart"
  task :restart, :roles => :app do
    sudo "touch /var/sites/rethai.com/alpha/current/tmp/restart.txt"
  end
end



# namespace(:deploy) do
  # desc "Server Restart"
  # task :restart, :roles => :app do
    # run "cd #{deploy_to}/current/"
    # sudo "mongrel_rails stop -P #{deploy_to}/current/tmp/pids/mongrel#{rails_port}.pid -c #{deploy_to}/current/; mongrel_rails start -e #{rails_env} -p #{rails_port} -l #{deploy_to}/current/log/mongrel#{rails_port}.log -P #{deploy_to}/current/tmp/pids/mongrel#{rails_port}.pid -c #{deploy_to}/current -r #{deploy_to}/current/public -d"
    # sudo "rm -f /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    # sudo "ln -s /var/sites/rethai.com/files/current/public/#{rails_env}/listingdata/ /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
  # end
# end
# 
# namespace(:deploy) do
  # desc "Server Stop"
  # task :stop, :roles => :app do
    # run "cd #{deploy_to}/current/"
    # sudo "mongrel_rails stop -P #{deploy_to}/current/tmp/pids/mongrel#{rails_port}.pid -c #{deploy_to}/current/"
  # end
# end
# 
# namespace(:deploy) do
  # desc "Server Start"
  # task :start, :roles => :app do
    # run "cd #{deploy_to}/current/"
    # sudo "mongrel_rails start -e #{rails_env} -p #{rails_port} -l #{deploy_to}/current/log/mongrel#{rails_port}.log -P #{deploy_to}/current/tmp/pids/mongrel#{rails_port}.pid -c #{deploy_to}/current -r #{deploy_to}/current/public -d"
    # sudo "rm -f /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
    # sudo "ln -s /var/sites/rethai.com/files/current/public/#{rails_env}/listingdata/ /var/sites/rethai.com/#{rails_env}/current/public/listingdata"
  # end
# end


namespace(:migrate) do
  desc "Migrate the Database"
  task :migrate, :roles => :db do
    sudo "cd #{deploy_to}/current/;  rake db:migrate RAILS_ENV=#{rails_env}"
  end
end



desc "Write current revision to app/layouts/_revision.rhtml"
task :publish_revision do
  run "svn info #{release_path} | grep ^Revision > #{release_path}/app/views/layouts/_revision.erb"
end

desc "Run this after update_code"
task :after_update_code do
  publish_revision
end
