"jp":
  date:
    formats:
      default: "%Y-%m-%d"
      short: "%e %b"
      long: "%B %e, %Y"
      only_day: "%e"

    day_names: [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
    abbr_day_names: [Sun, Mon, Tue, Wed, Thu, Fri, Sat]
    month_names: [~, January, February, March, April, May, June, July, August, September, October, November, December]
    abbr_month_names: [~, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]
    order: [ :year, :month, :day ]
  
  time:
    formats:
      default: "%a %b %d %H:%M:%S %Z %Y"
      time: "%H:%M"
      short: "%d %b %H:%M"
      long: "%B %d, %Y %H:%M"
      only_second: "%S"
      
      datetime:
        formats:
          default: "%Y-%m-%dT%H:%M:%S%Z"
          
    am: 'am'
    pm: 'pm'
      
  datetime:
    distance_in_words:
      half_a_minute: 'half a minute'
      less_than_x_seconds:
        zero: 'less than 1 second'
        one: '1 second'
        other: '%{count} seconds'
      x_seconds:
        one: '1 second'
        other: '%{count} seconds'
      less_than_x_minutes:
        zero: 'less than a minute'
        one: '1 minute'
        other: '%{count} minutes'
      x_minutes:
        one: '1 minute'
        other: '%{count} minutes'
      about_x_hours:
        one: 'about 1 hour'
        other: '%{count} hours'
      x_days:
        one: '1 day'
        other: '%{count} days'
      about_x_months:
        one: 'about 1 month'
        other: '%{count} months'
      x_months:
        one: '1 month'
        other: '%{count} months'
      about_x_years:
        one: 'about 1 year'
        other: '%{count} years'
      over_x_years:
        one: 'over 1 year'
        other: '%{count} years'
      
  number:
    format:
      precision: 3
      separator: '.'
      delimiter: ','
    currency:
      format:
        unit: '$'
        precision: 2
        format: '%u %n'
        
  active_record:
    error:
      header_message: ["Couldn't save this %{object_name}: 1 error", "Couldn't save this %{object_name}: %{count} errors."]
      message: "Please check the following fields:"
    error_messages:
      inclusion: "is not included in the list"
      exclusion: "is not available"
      invalid: "is not valid"
      confirmation: "does not match its confirmation"
      accepted: "must be accepted"
      empty: "must be given"
      blank: "must be given"
      too_long: "is too long (no more than %{count} characters)"
      too_short: "is too short (no less than %{count} characters)"
      wrong_length: "is not the right length (must be %{count} characters)"
      taken: "is not available"
      not_a_number: "is not a number"
      greater_than: "must be greater than %{count}"
      greater_than_or_equal_to: "must be greater than or equal to %{count}"
      equal_to: "must be equal to %{count}"
      less_than: "must be less than %{count}"
      less_than_or_equal_to: "must be less than or equal to %{count}"
      odd: "must be odd"
      even: "must be even"
      
  txt:
    # for login/test =========================================================================================================================================
    main_title: "Localizing Rails"
    app_name: "Demo Application"
    sub_title: "how to localize your app with Rails' new i18n features"
    contents: "Contents"
    menu:
      introduction: "Introduction"
      about: "About"
      setup: "Setup"
      date_formats: "Date formats"
      time_formats: "Time formats"
    about:
      title: "About this demo app"
      author: "This demo app was written by %{mail_1}."
      feedback: "If you have any feedback, please feel free to drop me a line. Also visit %{blog_href} where I regularly blog about Rails and other stuff."
      licence: "This demo app and all its contents are licensed under the %{licence_href}. If you want to use it in ways prohibited by this license, please contact me and ask my permission."
    active_record:
      too_lazy: "No examples here since I'm too lazy to think of attributes to show <strong>all</strong> custom error messages. ;-)"
      easy_to_understand: "It's quite easy to understand, though."
    date_formats:
      rails_standards_work: "Rails standard formats (Date::DATE_FORMATS) still work:"
    date_helper:
      date_time_title: "Date/Time distance"
      forms_title: "Forms"
    index:
      others: "others"
      introduction: "Lately, a lot of work has been done by %{sven_blog} and %{sven_github} to facilitate future internationalization and localization of Rails."
      story_so_far: "This demo app tries to show you how you can use the features that have been implemented so far to localize big parts of your Rails application."
    number_helper:
      note_one: "Note: <code>number_to_phone</code> hasn't been localized yet and probably never will be - at least not in core. Look out for new internationalization/localization plugins like a new version of %{globalize} as they will probably support stuff like that."
      note_two: "Another note: <code>number_to_currency</code>, <code>number_to_percentage</code> and <code>number_to_human_size</code> all use <code>number_with_precision</code> internally and <code>number_with_precision</code> uses <code>number_with_delimiter</code> internally."
    setup:
      freezing_edge_and_adding: "Freezing Edge and installing the localized_dates plugin"
      you_need_to_be_on_edge: "You need to be on Edge Rails in order to use the Rails i18n features:"
      date_time_formats: "For date and time formats, you also need to install the %{localized_dates_link}:"
      config_locale: "Configuring the locale"
      best_place: "The best place to put your locale configuration, in my opinion, is <code>config/locales</code>. The localized_dates plugin will copy two locales, en-US and de-AT, in this directory. You can extend or modify them and also create new locales."
      locale: "Here's the demo locale that was used for this demo application:"
      defaults: "You also need to set up the default locale and/or locale in your <code>environment.rb</code> or an initializer."
      locale_structure_title: "A word on the structure of locales"
      locale_structure_number: "You may have noticed that inside the <code>:number</code> part of the locale, we defined <code>:format</code> and <code>:currency</code>. In general, locales are structured hierarchically - i.e. a currencies are numbers, percentages are numbers, etc. <code>:currency</code> can either override the basic <code>:format</code> settings (in our case, we set <code>:precision</code> to 2 instead of 3) or extend them (we add two new options, <code>:unit</code> and <code>:format</code>)."
      locale_structure_date_time: "The same holds true for dates and times: If needed, <code>:datetime</code> and <code>:time_with_zone</code> can be used to specifically address formatting of their respective types instead of just relying on the settings for <code>:time</code>. Note, however, that usually you want to use the same formats as <code>:time</code>."
    time_formats:
      rails_standards_work: "Rails standard formats (Time::DATE_FORMATS) still work:"
    
    
    
    # provinces ================================================================================================================================================
    province:    
      amnat_charoen: "Amnat Charoen"
      ang_thong: "Ang Thong"
      buri_ram: "Buri Ram"
      cha_choeng_sao: "Cha Choeng Sao"
      chai_nat: "Chai Nat"
      chaiyaphum: "Chaiyaphum"
      chanthaburi: "Chanthaburi"
      chiang_mai: "Chiang Mai"
      chiang_rai: "Chiang Rai"
      chon_buri: "Chon Buri"
      chumphon: "Chumphon"
      kalasin: "Kalasin"
      kamphaeng_phet: "Kamphaeng phet"
      kanchanaburi: "Kanchanaburi"
      khon_kaen: "Khon Kaen"
      krabi: "Krabi"
      lampang: "Lampang"
      lamphun: "Lamphun"
      loei: "Loei"
      lop_buri: "Lop Buri"
      mae_hong_son: "Mae Hong Son"
      maha_sarakham: "Maha Sarakham"
      mukdahan: "Mukdahan"
      nakhon_nayok: "Nakhon Nayok"
      nakhon_pathom: "Nakhon Pathom"
      nakhon_phanom: "Nakhon Phanom"
      nakhon_ratchasima: "Nakhon Ratchasima"
      nakhon_sawan: "Nakhon Sawan"
      nakhon_si_thammarat: "Nakhon Si Thammarat"
      nan: "Nan"
      narathiwat: "Narathiwat"
      nongbua_lamphu: "Nong Bua Lam Phu"
      nong_khai: "Nong Khai"
      nonthaburi: "Nonthaburi"
      pathum_thani: "Pathum Thani"
      pattani: "Pattani"
      phang_nga: "Phangnga"
      phatthalung: "Phatthalung"
      phayao: "Phayao"
      phetchabun: "Phetchabun"
      phetchaburi: "Phetchaburi"
      phichit: "Phichit"
      phitsanulok: "Phitsanulok"
      phra_nakhon_si_ayutthaya: "Phra Nakhon Si Ayutthaya"
      phrae: "Phrae"
      phuket: "Phuket"
      prachin_buri: "Prachin Buri"
      prachuap_khiri_khan: "Prachuap Khiri Khan"
      ranong: "Ranong"
      ratchaburi: "Ratchaburi"
      rayong: "Rayong"
      roi_et: "Roi Et"
      sa_kaew: "Sa Kaew"
      sakon_nakhon: "Sakon Nakhon"
      samut_prakan: "Samut Prakan"
      samut_sakhon: "Samut Sakhon"
      samut_songkhram: "Samut Songkhram"
      saraburi: "Saraburi"
      satun: "Satun"
      si_sa_ket: "Si Sa Ket"
      sing_buri: "Sing Buri"
      song_khla: "Songkhla"
      sukhothai: "Sukhothai"
      suphan_buri: "Suphan Buri"
      surat_thani: "Surat Thani"
      surin: "Surin"
      tak: "Tak"
      trang: "Trang"
      trat: "Trat"
      ubon_ratchathani: "Ubon Ratchathani"
      udon_thani: "Udon Thani"
      umnad_chareun: "Umnad Chareun"
      uthai_thani: "Uthai Thani"
      uttaradit: "Uttaradit"
      yala: "Yala"
      yasothon: "Yasothon"  
      bangkok: "Bangkok"
      pattaya: "Pattaya"
      hua_hin: "Hua Hin"
      sattahip: "Sattahip"
      koh_samui: "Koh Samui"
      koh_chang: "Koh Chang"
      bang_saen: "Bang Saen"
      laem_mae_phim: "Laem Mae Phim"
      saen_suk: "Saen Suk"
      koh_phangan: "Koh Phangan"

    
      #search and result
    listingtype:
      listingtype: "タイプリスト"
      condo-apartment: "マンション / アパート"
      condo--apartment: "マンション / アパート"
      house: "ハウス"
      townhouse: "タウンハウス"
      shophouse: "ショップハウス"
      land: "ランド"
      detached-house: "Detached House"
      semi-detached-house: "Semi Detached House"
      terrace-house: "Terrace House"
      flatapartment: "Flat/Apartment"
      bungalow: "Bungalow"
      park-home: "Park Home"
      garage: "Garage"
      land-with-planning: "Land with planning"
      land-without-planning: "Land without planning"
      
      #parking
      garage: "Garage"
      unsecured-parking-space: "無担保駐車場スペース"
      secured-parking-space: "セキュリティで保護さ駐車スペース"
      street-parking: "ストリート駐車場"
      bike-space: "自転車空間"
      no-parking: "駐車場なし"
      unlocked-carport: "ロック解除カーポート"
      locked-carport: "ロックカーポート"
      driveway: "私道"
      allocated-parking-space: "割り当てられた駐車場スペース"
      private-car-park: "プライベート駐車場"
      road-side---permit: "ロードサイドは - 許可"
      road-side-parking: "ロードサイド駐車場"
      na: "N/A"
    
      
      head: "検索タイの不動産の"
      sale: "セール"
      rent: "家賃"
      sale-or-rent: "販売またはレンタル"
      buy-rent: "購入するまたはレンタル"
      buy: "購入する"
      bedroom: "ベッドルーム"
      bathroom: "バスルーム"
      studio: "スタジオ"
      for: "のために"
      txtany: "任意の"
      price: "価格"
      from: "から"
      to: "に"
      i-want-to: "私がしたい"
      propertytype: "物件のタイプ"
      city: "シティ"
      sqm: "平方メートル"
      
    # for index/index ===========================================================================================================================================
    k_index:
      page_title: "不動産タイ、販売やレンタルパタヤのための、プーケット、バンコク、サムイ島とチェンマイチェンマイプロパティを探す"
      page_desc: "販売とレンタルパタヤ、バンコク、プーケット、チェンマイ、タイ各地での不動産をして下さい。タイのプロパティのすべての種類 - 住宅、コンドミニアム、アパート、ショップハウスと販売または賃貸のための土地。我々は、不動産業者ではない！我々は、不動産ポータルしている、誰でもリストを追加できます。"
      page_keywords: "不動産タイ、不動産、どんどん狭くタイ、タイのプロパティ、タイのプロパティ、販売タイ、家賃タイ、賃貸物件タイ、バンコクの不動産、パタヤ、不動産、プーケット、不動産売却、土地のための住宅用不動産の不動産販売"
      
    
      real_estate: "不動産"  
      real_estate_for_rent: "賃貸不動産"
      real_estate_condos_&_apartments: "コンドミニアム＆アパート"
      property_for_sale: "販売物件の"  
      
      #Find Real Estate By Map
      viewmap: "地図を見るの"
      viewlist: "一覧を表示するの"
      findrealestate: "タイの不動産を探す"
      fr_link: "%{frlink}"
      
      #find your perfect house
      perfect_house: "お客様にぴったりの家を探す"
      ph_text: "<p>不動産タイを使用することにより、完璧な家、マンションやアパートを見つけることができます %{maplink} または %{listling}.</p><p>あなたはお互いに一緒に5物件までと比較することができます。</p><p>あなたが所有者に無料で直接リストお問い合わせください。</p>"
      
      
      #List your Property Free
      addlistinglink: "リスティングを追加"
      agentslink: "エージェント"
      developmentlink: "開発者"
      property_free: "リストをプロパティ無料"
      pf_text: "<p>我々は、手数料を取ることはない、または任意のプロパティ当サイトで販売さからお金を稼ぐ。誰もが %{addlistinglink}.</p><p>あなたが所有権とリスティングやお問い合わせのコントロールを保持します。</p><p>不動産タイは特別に設計されて %{agentslink} と %{developmentlink}.</p>"
      
      # for index/_footer
      popsearch: "検索人気都市"
      poprent: "人気のレンタル場所"
      popcondosale: "販売のための人気のコンドミニアム"
      popsale: "販売物件の"
      
      #what's rethai.com?
      agencies: "機関"
      map: "地図"
      list: "リスト"
      whatrethai1: "我々は代理店されていません &mdash; 我々のシステム用に設計さ %{agentslink} 個人"
      whatrethai2: "我々は、誰のための不動産ポータルのプロパティのリストを宣伝している"
      whatrethai3: "やすい場所タイのプロパティを見つけるために"
      whatrethai4: "そこは販売手数料や隠れた料金は"
      whatrethai5: "誰もがリスティングを追加することができます"
      whatrethai6: "代理店を追加することができますリスティングの数を管理し、高度な統計データを表示する"
      whatrethai7: "内の検索プロパティ %{listling} または %{maplink}"
      
      #search pages ================================================================================================================================
    search:
      page_title: "不動産 %{seo}- 財産 %{seo}- 検索コンドスの, アパート、住宅および販売やレンタルの土地"
      page_desc: "%{seo}不動産タイ、プロパティ %{seo}. タイのプロパティのすべてのタイプ - 住宅は、コンドミニアム、アパート、ショップハウス販売のための土地やタイ各地を借りる。我々は、不動産業者ではない！我々は、不動産ポータルしている、誰でもリストを追加できます。"
      page_keywords: "財産  %{seo}, 不動産タイ、不動産、どんどん狭くタイ、タイのプロパティ、タイのプロパティ、販売タイ、家賃タイ、賃貸物件タイ、バンコクの不動産、パタヤ、不動産、プーケット、不動産売却、土地のための住宅用不動産の不動産販売"
      
      listings-found: "リスティングが見つかりました"
      sort-price: "並べ替え価格"
      view-on-map: "プロフィールを地図上で"
      hight-to-low: "HighからLow"
      low-to-hight: "低高に"
      add-now: "現在のリスティングを追加"
      text1: "<p><strong>リスティングを追加する</strong></p><p>いいえ委員会<br>いいえリストは、手数料<br>いいえ販売手数料</p><p>誰もが15のリストを追加できます。</p><p>政府機関は、1000 +のリストを追加することができます！</p><p>あなたは代理店ですか？<br><a href=/index/foragents>ここをクリック</a> どのように生成することが確認する <strong>より多くのリード</strong> リスティングの！</p>"
     
      #show pages ================================================================================================================================
    show:
      page_title: "%{city} %{listingtype} のために %{salerent} のために %{price} - 不動産 %{city}"
      page_desc: "%{city} プロパティの %{salerent}. %{city} 不動産, %{txtdetail}"
      page_keywords: "%{city} 財産, %{city} %{salerent}, 不動産タイ、不動産、どんどん狭くタイ、タイのプロパティ、タイのプロパティ、販売タイ、家賃タイ、賃貸物件タイ、バンコクの不動産、パタヤ、不動産、プーケット、不動産売却、土地のための住宅用不動産の不動産販売"
      
      sale-price: "販売価格"
      rent-price: "レンタル料金"
      parking: "駐車場"
      price-per-sqm: "฿<strong>/平方メートル</strong>"
      description: "説明"
      contact-the-owner: "所有者に連絡"
      name: "あなたのお名前"
      email: "あなたのEmail"
      password: "パスワード"
      message: "メッセージ"
      viewonmap: "プロフィールを地図上で"
      
      business-details: "ビジネス詳細"
      broker-number: "ブローカー数"
      years-in-business: "操業年数"
      website: "ウェブサイトのアドレス"
      aboutus: "私たちについて"
      services: "サービス"
      
      address: "住所"
      listingsonline: "リストオンライン"
      phone: "電話番号"

      related-listings: "関連リスト"
      in: "で"
      editlisting: "[編集]リスト"
      
      #new pages ================================================================================================================================
    new:
      page_title: "無料タイプロパティのリストは、 - 新しいリストを無料追加"
      page_desc: "新しい不動産物件をREThai.comに自由のためのリストを追加します。不動産タイ。タイのプロパティのすべてのタイプ - 住宅は、コンドミニアム、アパート、ショップハウス販売のための土地やタイ各地を借りる。我々は、不動産業者ではない！我々は、不動産ポータルしている、誰でもリストを追加できます。"
      page_keywords: "不動産タイ、不動産、どんどん狭くタイ、タイのプロパティ、タイのプロパティ、販売タイ、家賃タイ、賃貸物件タイ、バンコクの不動産、パタヤ、不動産、プーケット、不動産売却、土地のための住宅用不動産の不動産販売"
      
      image: "イメージ"
      select-city: "都市を選択"
      property-for: "プロパティリスト"
      drag_drop: "ドラッグ＆マップに掲載するリスティングの場所を設定するには赤いマーカーをドロップすると、 <br><span style=margin-left:27px></span>をクリックして場所を保存する確認"
      
      #top menu bar ================================================================================================================================
    nav: 
      mm1: "<a href=\"/\">検索</a><p>タイのプロパティについて</p>"   
      mm2: "<a href=\"/rethai/new\">リストを追加</a><p>のリストあなたの財産を無料で</p>"  
      mm3: "<a href=\"/index/advertise\">広告</a><p>当社の幅広いメディアネットワーク間で</p>"  
      mm4: "<a href=\"/index/foragents\">のエージェント</a><p>プレミアムのアップグレードとADV。機能</p>"  
      mm5: "<a href=\"/index/stats\">統計＆トレンド</a><p>専属タイプロパティのデータ</p>"  
        