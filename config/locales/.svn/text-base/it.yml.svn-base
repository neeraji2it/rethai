"it":
  date:
    formats:
      default: "%Y-%m-%d"
      short: "%e %b"
      long: "%B %e, %Y"
      only_day: "%e"

    day_names: [Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday]
    abbr_day_names: [Sun, Mon, Tue, Wed, Thu, Fri, Sat]
    month_names: [~, January, February, March, April, May, June, July, August, September, October, November, December]
    abbr_month_names: [~, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]
    order: [ :year, :month, :day ]
  
  time:
    formats:
      default: "%a %b %d %H:%M:%S %Z %Y"
      time: "%H:%M"
      short: "%d %b %H:%M"
      long: "%B %d, %Y %H:%M"
      only_second: "%S"
      
      datetime:
        formats:
          default: "%Y-%m-%dT%H:%M:%S%Z"
          
    am: 'am'
    pm: 'pm'
      
  datetime:
    distance_in_words:
      half_a_minute: 'half a minute'
      less_than_x_seconds:
        zero: 'less than 1 second'
        one: '1 second'
        other: '%{count} seconds'
      x_seconds:
        one: '1 second'
        other: '%{count} seconds'
      less_than_x_minutes:
        zero: 'less than a minute'
        one: '1 minute'
        other: '%{count} minutes'
      x_minutes:
        one: '1 minute'
        other: '%{count} minutes'
      about_x_hours:
        one: 'about 1 hour'
        other: '%{count} hours'
      x_days:
        one: '1 day'
        other: '%{count} days'
      about_x_months:
        one: 'about 1 month'
        other: '%{count} months'
      x_months:
        one: '1 month'
        other: '%{count} months'
      about_x_years:
        one: 'about 1 year'
        other: '%{count} years'
      over_x_years:
        one: 'over 1 year'
        other: '%{count} years'
      
  number:
    format:
      precision: 3
      separator: '.'
      delimiter: ','
    currency:
      format:
        unit: '$'
        precision: 2
        format: '%u %n'
        
  active_record:
    error:
      header_message: ["Couldn't save this %{object_name}: 1 error", "Couldn't save this %{object_name}: %{count} errors."]
      message: "Please check the following fields:"
    error_messages:
      inclusion: "is not included in the list"
      exclusion: "is not available"
      invalid: "is not valid"
      confirmation: "does not match its confirmation"
      accepted: "must be accepted"
      empty: "must be given"
      blank: "must be given"
      too_long: "is too long (no more than %{count} characters)"
      too_short: "is too short (no less than %{count} characters)"
      wrong_length: "is not the right length (must be %{count} characters)"
      taken: "is not available"
      not_a_number: "is not a number"
      greater_than: "must be greater than %{count}"
      greater_than_or_equal_to: "must be greater than or equal to %{count}"
      equal_to: "must be equal to %{count}"
      less_than: "must be less than %{count}"
      less_than_or_equal_to: "must be less than or equal to %{count}"
      odd: "must be odd"
      even: "must be even"
      
  txt:
    # for login/test =========================================================================================================================================
    main_title: "Localizing Rails"
    app_name: "Demo Application"
    sub_title: "how to localize your app with Rails' new i18n features"
    contents: "Contents"
    menu:
      introduction: "Introduction"
      about: "About"
      setup: "Setup"
      date_formats: "Date formats"
      time_formats: "Time formats"
    about:
      title: "About this demo app"
      author: "This demo app was written by %{mail_1}."
      feedback: "If you have any feedback, please feel free to drop me a line. Also visit %{blog_href} where I regularly blog about Rails and other stuff."
      licence: "This demo app and all its contents are licensed under the %{licence_href}. If you want to use it in ways prohibited by this license, please contact me and ask my permission."
    active_record:
      too_lazy: "No examples here since I'm too lazy to think of attributes to show <strong>all</strong> custom error messages. ;-)"
      easy_to_understand: "It's quite easy to understand, though."
    date_formats:
      rails_standards_work: "Rails standard formats (Date::DATE_FORMATS) still work:"
    date_helper:
      date_time_title: "Date/Time distance"
      forms_title: "Forms"
    index:
      others: "others"
      introduction: "Lately, a lot of work has been done by %{sven_blog} and %{sven_github} to facilitate future internationalization and localization of Rails."
      story_so_far: "This demo app tries to show you how you can use the features that have been implemented so far to localize big parts of your Rails application."
    number_helper:
      note_one: "Note: <code>number_to_phone</code> hasn't been localized yet and probably never will be - at least not in core. Look out for new internationalization/localization plugins like a new version of %{globalize} as they will probably support stuff like that."
      note_two: "Another note: <code>number_to_currency</code>, <code>number_to_percentage</code> and <code>number_to_human_size</code> all use <code>number_with_precision</code> internally and <code>number_with_precision</code> uses <code>number_with_delimiter</code> internally."
    setup:
      freezing_edge_and_adding: "Freezing Edge and installing the localized_dates plugin"
      you_need_to_be_on_edge: "You need to be on Edge Rails in order to use the Rails i18n features:"
      date_time_formats: "For date and time formats, you also need to install the %{localized_dates_link}:"
      config_locale: "Configuring the locale"
      best_place: "The best place to put your locale configuration, in my opinion, is <code>config/locales</code>. The localized_dates plugin will copy two locales, en-US and de-AT, in this directory. You can extend or modify them and also create new locales."
      locale: "Here's the demo locale that was used for this demo application:"
      defaults: "You also need to set up the default locale and/or locale in your <code>environment.rb</code> or an initializer."
      locale_structure_title: "A word on the structure of locales"
      locale_structure_number: "You may have noticed that inside the <code>:number</code> part of the locale, we defined <code>:format</code> and <code>:currency</code>. In general, locales are structured hierarchically - i.e. a currencies are numbers, percentages are numbers, etc. <code>:currency</code> can either override the basic <code>:format</code> settings (in our case, we set <code>:precision</code> to 2 instead of 3) or extend them (we add two new options, <code>:unit</code> and <code>:format</code>)."
      locale_structure_date_time: "The same holds true for dates and times: If needed, <code>:datetime</code> and <code>:time_with_zone</code> can be used to specifically address formatting of their respective types instead of just relying on the settings for <code>:time</code>. Note, however, that usually you want to use the same formats as <code>:time</code>."
    time_formats:
      rails_standards_work: "Rails standard formats (Time::DATE_FORMATS) still work:"
    
    
    
    # provinces ================================================================================================================================================
    province:    
      amnat_charoen: "Amnat Charoen"
      ang_thong: "Ang Thong"
      buri_ram: "Buri Ram"
      cha_choeng_sao: "Cha Choeng Sao"
      chai_nat: "Chai Nat"
      chaiyaphum: "Chaiyaphum"
      chanthaburi: "Chanthaburi"
      chiang_mai: "Chiang Mai"
      chiang_rai: "Chiang Rai"
      chon_buri: "Chon Buri"
      chumphon: "Chumphon"
      kalasin: "Kalasin"
      kamphaeng_phet: "Kamphaeng phet"
      kanchanaburi: "Kanchanaburi"
      khon_kaen: "Khon Kaen"
      krabi: "Krabi"
      lampang: "Lampang"
      lamphun: "Lamphun"
      loei: "Loei"
      lop_buri: "Lop Buri"
      mae_hong_son: "Mae Hong Son"
      maha_sarakham: "Maha Sarakham"
      mukdahan: "Mukdahan"
      nakhon_nayok: "Nakhon Nayok"
      nakhon_pathom: "Nakhon Pathom"
      nakhon_phanom: "Nakhon Phanom"
      nakhon_ratchasima: "Nakhon Ratchasima"
      nakhon_sawan: "Nakhon Sawan"
      nakhon_si_thammarat: "Nakhon Si Thammarat"
      nan: "Nan"
      narathiwat: "Narathiwat"
      nongbua_lamphu: "Nong Bua Lam Phu"
      nong_khai: "Nong Khai"
      nonthaburi: "Nonthaburi"
      pathum_thani: "Pathum Thani"
      pattani: "Pattani"
      phang_nga: "Phangnga"
      phatthalung: "Phatthalung"
      phayao: "Phayao"
      phetchabun: "Phetchabun"
      phetchaburi: "Phetchaburi"
      phichit: "Phichit"
      phitsanulok: "Phitsanulok"
      phra_nakhon_si_ayutthaya: "Phra Nakhon Si Ayutthaya"
      phrae: "Phrae"
      phuket: "Phuket"
      prachin_buri: "Prachin Buri"
      prachuap_khiri_khan: "Prachuap Khiri Khan"
      ranong: "Ranong"
      ratchaburi: "Ratchaburi"
      rayong: "Rayong"
      roi_et: "Roi Et"
      sa_kaew: "Sa Kaew"
      sakon_nakhon: "Sakon Nakhon"
      samut_prakan: "Samut Prakan"
      samut_sakhon: "Samut Sakhon"
      samut_songkhram: "Samut Songkhram"
      saraburi: "Saraburi"
      satun: "Satun"
      si_sa_ket: "Si Sa Ket"
      sing_buri: "Sing Buri"
      song_khla: "Songkhla"
      sukhothai: "Sukhothai"
      suphan_buri: "Suphan Buri"
      surat_thani: "Surat Thani"
      surin: "Surin"
      tak: "Tak"
      trang: "Trang"
      trat: "Trat"
      ubon_ratchathani: "Ubon Ratchathani"
      udon_thani: "Udon Thani"
      umnad_chareun: "Umnad Chareun"
      uthai_thani: "Uthai Thani"
      uttaradit: "Uttaradit"
      yala: "Yala"
      yasothon: "Yasothon"  
      bangkok: "Bangkok"
      pattaya: "Pattaya"
      hua_hin: "Hua Hin"
      sattahip: "Sattahip"
      koh_samui: "Koh Samui"
      koh_chang: "Koh Chang"
      bang_saen: "Bang Saen"
      laem_mae_phim: "Laem Mae Phim"
      saen_suk: "Saen Suk"
      koh_phangan: "Koh Phangan"

    
      #search and result
    listingtype:
      listingtype: "Tipo di annuncio"
      condo-apartment: "Condo / Apartment"
      condo--apartment: "Condo / Apartment"
      house: "Casa"
      townhouse: "Townhouse"
      shophouse: "Shophouse"
      land: "Terra"
      detached-house: "Casa indipendente"
      semi-detached-house: "Semi indipendente Casa"
      terrace-house: "Terrace House"
      flatapartment: "Flat/Apartment"
      bungalow: "Bungalow"
      park-home: "Park Home"
      garage: "Garage"
      land-with-planning: "Land with planning"
      land-without-planning: "Land without planning"
      
      #parking
      garage: "Garage"
      unsecured-parking-space: "Parcheggio non garantiti"
      secured-parking-space: "Parcheggio custodito"
      street-parking: "Via Parcheggio"
      bike-space: "Bike Space"
      no-parking: "Divieto di sosta"
      unlocked-carport: "Sbloccato Carport"
      locked-carport: "Bloccato Carport"
      driveway: "Driveway"
      allocated-parking-space: "Allocated Parking Space"
      private-car-park: "Private Car Park"
      road-side---permit: "Road Side - Permit"
      road-side-parking: "Road Side Parking"
      na: "N/A"
    
      
      head: "Cerca Immobiliari in Thailandia"
      sale: "Vendita"
      rent: "Affitto"
      sale-or-rent: "Vendita / affitto"
      buy-rent: "Comprare / affittare"
      buy: "Comprare"
      bedroom: "Camere da letto"
      bathroom: "Bagni"
      studio: "Studio"
      for: "Per"
      txtany: "Qualsiasi"
      price: "Prezzo"
      from: "da"
      to: "a"
      i-want-to: "Voglio"
      propertytype: "Tipo di proprietà"
      city: "Città"
      sqm: "SQM"
      
    # for index/index ===========================================================================================================================================
    k_index:
      page_title: "Thailandia Real Estate, Trova Case in Vendita e Affitto a Pattaya, Phuket, Bangkok, Koh Samui e Chiang Mai"
      page_desc: "Cerca Immobili in vendita e in affitto a Pattaya, Bangkok, Phuket, Chiang Mai e tutto intorno Thailandia. Tutti i tipi di proprietà in Thailandia - appartamenti, ville, appartamenti, un'eleganza e terreni in vendita o in affitto. Non siamo un'agenzia immobiliare! Siamo un portale immobiliare, chiunque può inserire un annuncio"
      page_keywords: "Thailandia real estate, immobiliare, Thailandia immobiliare, di proprietà thai, thailandia proprietà, beni immobili per la Thailandia in vendita, immobili in affitto per la Thailandia, Thailandia proprietà in affitto, Bangkok immobiliari, real Pattaya, Phuket immobiliari, case in vendita, terreni per vendita"
      
    
      real_estate: "Real Estate"  
      real_estate_for_rent: "Immobili in affitto"
      real_estate_condos_&_apartments: "Condos & Apartments"
      property_for_sale: "Immobili in Vendita"  
      
      #Find Real Estate By Map
      viewmap: "Visualizza una mappa di"
      viewlist: "Visualizzare un elenco di"
      findrealestate: "Trova Immobiliari in Thailandia"
      fr_link: "%{frlink}"
      
      #find your perfect house
      perfect_house: "Trova la tua casa ideale"
      ph_text: "<p>Con Real Estate Thailandia si può trovare la casa perfetta, condominio o appartamento da %{maplink} o su un %{listling}.</p><p>È possibile confrontare fino a 5 liste uni accanto agli altri.</p><p>È possibile contattare direttamente i proprietari annuncio gratuitamente.</p>"
      
      
      #List your Property Free
      addlistinglink: "inserire il tuo annuncio"
      agentslink: "Agenti"
      developmentlink: "Sviluppatori"
      property_free: "Proponi la tua casa gratis"
      pf_text: "<p>Non prendiamo commissioni o fare soldi da qualsiasi bene venduto sul nostro sito. Chiunque può %{addlistinglink}.</p><p>Si mantengono la proprietà e il controllo delle inserzioni e le indagini.</p><p>Immobiliare La Thailandia è appositamente progettato per %{agentslink} e %{developmentlink}.</p>"
      
      # for index/_footer
      popsearch: "Città di ricerca più diffusi"
      poprent: "Noleggio località famose"
      popcondosale: "Condomini popolari in vendita"
      popsale: "Immobili in Vendita"
      
      #what's rethai.com?
      agencies: "agenzie"
      map: "mappa"
      list: "elenco"
      whatrethai1: "Non siamo un'agenzia &mdash; Il nostro sistema è progettato per %{agentslink} e gli individui"
      whatrethai2: "Siamo un portale immobiliare per chiunque di pubblicizzare i propri annunci proprietà"
      whatrethai3: "Un luogo facile da trovare proprietà in Thailandia"
      whatrethai4: "Non ci sono commissioni di vendita o spese nascoste"
      whatrethai5: "Chiunque può inserire il tuo annuncio"
      whatrethai6: "Le agenzie possono aggiungere e gestire centinaia di annunci e visualizzare le statistiche avanzate e dati"
      whatrethai7: "proprietà trovano in una %{listling} o da %{maplink}"
      
      #search pages ================================================================================================================================
    search:
      page_title: "Real Estate %{seo}- Proprietà %{seo}- Cerca Condos, appartamenti, case e terreni in vendita e affitto"
      page_desc: "%{seo}Real Estate Thailandia, di proprietà %{seo}. Tutti i tipi di proprietà in Thailandia - appartamenti, ville, appartamenti, un'eleganza e terreni in vendita o in affitto in tutta la Thailandia. Non siamo un'agenzia immobiliare! Siamo un portale immobiliare, chiunque può inserire un annuncio"
      page_keywords: "proprietà %{seo}, Thailandia real estate, immobiliare, Thailandia immobiliare, di proprietà thai, thailandia proprietà, beni immobili per la Thailandia in vendita, immobili in affitto per la Thailandia, Thailandia proprietà in affitto, Bangkok immobiliari, real Pattaya, Phuket immobiliari, case in vendita, terreni per vendita"
      
      listings-found: "Elenchi trovati"
      sort-price: "Ordina per Prezzo"
      view-on-map: "Guarda sulla mappa"
      hight-to-low: "Alto al più basso"
      low-to-hight: "Basso al più alto"
      add-now: "Aggiungi le inserzioni ora"
      text1: "<p><strong>Aggiungi il tuo annuncio</strong></p><p>Nessuna commissione<br>Nessun tasse di elenco<br>Nessun tasse di vendita</p><p>Chiunque può aggiungere 15 liste</p><p>Le agenzie possono aggiungere 1.000 + inserzioni!</p><p>Sei un agenzia?<br><a href=/index/foragents>Clicca qui</a> per scoprire come siamo in grado di generare <strong>maggior numero di lead</strong> per le tue inserzioni!</p>"
     
      #show pages ================================================================================================================================
    show:
      page_title: "%{city} %{listingtype} in %{salerent} in %{price} - Real Estate %{city}"
      page_desc: "%{city} Immobili in %{salerent}. %{city} Real Estate, %{txtdetail}"
      page_keywords: "%{city} proprietà, %{city} %{salerent}, Thailandia real estate, immobiliare, Thailandia immobiliare, di proprietà thai, thailandia proprietà, beni immobili per la Thailandia in vendita, immobili in affitto per la Thailandia, Thailandia proprietà in affitto, Bangkok immobiliari, real Pattaya, Phuket immobiliari, case in vendita, terreni per vendita"
      
      sale-price: "Prezzo di vendita"
      rent-price: "Affitto"
      parking: "Parcheggio"
      price-per-sqm: "฿<strong>/SQM</strong>"
      description: "Descrizione"
      contact-the-owner: "Contattare il proprietario"
      name: "Il tuo nome"
      email: "La tua email"
      password: "La tua password"
      message: "Messaggio"
      viewonmap: "Guarda sulla mappa"
      
      business-details: "Business Details"
      broker-number: "Numero Broker"
      years-in-business: "Anni di attività"
      website: "Sito web Indirizzo"
      aboutus: "Chi siamo"
      services: "Servizi"
      
      address: "Indirizzo"
      listingsonline: "Liste Online"
      phone: "Telefono"

      related-listings: "Inserzioni correlate"
      in: "in"
      editlisting: "Modifica Listing"
      
      #new pages ================================================================================================================================
    new:
      page_title: "FREE annunci immobiliari Thailandia - Aggiungi una nuova inserzione gratuita"
      page_desc: "Aggiungere una nuova proprietà immobiliare annuncio ad REThai.com per LIBERO. Real Estate Thailandia. Tutti i tipi di proprietà in Thailandia - appartamenti, ville, appartamenti, un'eleganza e terreni in vendita o in affitto in tutta la Thailandia. Non siamo un'agenzia immobiliare! Siamo un portale immobiliare, chiunque può inserire un annuncio"
      page_keywords: "Thailandia real estate, immobiliare, Thailandia immobiliare, di proprietà thai, thailandia proprietà, beni immobili per la Thailandia in vendita, immobili in affitto per la Thailandia, Thailandia proprietà in affitto, Bangkok immobiliari, real Pattaya, Phuket immobiliari, case in vendita, terreni per vendita"
      
      image: "Immagine"
      select-city: "Seleziona la città"
      property-for: "Immobili in vendita"
      drag_drop: "Drag & drop l'indicatore rosso sulla mappa per impostare la posizione annuncio, <br><span style=margin-left:27px></span>Fate clic su Conferma percorso per salvare"
      
      #top menu bar ================================================================================================================================
    nav: 
      mm1: "<a href=\"/\">Cerca</a><p>For property in Thailand</p>"   
      mm2: "<a href=\"/rethai/new\">Aggiungi lista</a><p>Proponi la tua casa gratuitamente</p>"  
      mm3: "<a href=\"/index/advertise\">Pubblicizzare</a><p>Attraverso la nostra vasta rete di media</p>"  
      mm4: "<a href=\"/index/foragents\">Agenti per</a><p>aggiornamenti Premium e adv. caratteristiche</p>"  
      mm5: "<a href=\"/index/stats\">Tendenze</a><p>Esclusivo Thailandia dati immobile</p>"  
        