# Be sure to restart your server when you modify this file

# Uncomment below to force Rails into production mode when
# you don't control web/app server and can't set it the proper way
# ENV['RAILS_ENV'] ||= 'production'

# Specifies gem version of Rails to use when vendor/rails is not present
RAILS_GEM_VERSION = '2.3.5' unless defined? RAILS_GEM_VERSION
#RAILS_GEM_VERSION = '3.0.3' unless defined? RAILS_GEM_VERSION
#RAILS_GEM_VERSION = '3.0.5' unless defined? RAILS_GEM_VERSION


# Bootstrap the Rails environment, frameworks, and default configuration
require File.join(File.dirname(__FILE__), 'boot')

if Gem::VERSION >= "1.3.6"
    module Rails
        class GemDependency
            def requirement
                r = super
                (r == Gem::Requirement.default) ? nil : r
            end
        end
    end
end





Rails::Initializer.run do |config|
  # Settings in config/environments/* take precedence over those specified here.
  # Application configuration should go into files in config/initializers
  # -- all .rb files in that directory are automatically loaded.
  # See Rails::Configuration for more options.

  # Skip frameworks you're not going to use. To use Rails without a database
  # you must remove the Active Record framework.
  # config.frameworks -= [ :active_record, :active_resource, :action_mailer ]

  # Specify gems that this application depends on. 
  # They can then be installed with "rake gems:install" on new installations.
  # config.gem "bj"
  # config.gem "hpricot", :version => '0.6', :source => "http://code.whytheluckystiff.net"
  # config.gem "aws-s3", :lib => "aws/s3"

  # Only load the plugins named here, in the order given. By default, all plugins 
  # in vendor/plugins are loaded in alphabetical order.
  # :all can be used as a placeholder for all plugins not explicitly named
  # config.plugins = [ :exception_notification, :ssl_requirement, :all ]

  # Add additional load paths for your own custom dirs
  # config.load_paths += %W( #{RAILS_ROOT}/extras )

  # Force all environments to use the same logger level
  # (by default production uses :info, the others :debug)
  # config.log_level = :debug

  # Make Time.zone default to the specified zone, and make Active Record store time values
  # in the database in UTC, and return them converted to the specified local zone.
  # Run "rake -D time" for a list of tasks for finding time zone names. Comment line to use default local time.
#  config.time_zone = 'UTC'

  # Your secret key for verifying cookie session data integrity.
  # If you change this key, all old sessions will become invalid!
  # Make sure the secret is at least 30 characters and all random, 
  # no regular words or you'll be exposed to dictionary attacks.
  
  config.gem 'exceptional'


  
 config.action_controller.session = {
    :session_key => '_webmap_session',
    :secret      => 'bd3998c82812d2e4b422f6f3d9d9c6f863d58f9e4e251ff0ca4aff0e997d87a6f18861d74832089e6fdb5bf11cf524923bd5660a5922c9550ddc87866708c8f4'
  }


  config.plugins = :sanitize_params, :all


  # Use the database for sessions instead of the cookie-based default,
  # which shouldn't be used to store highly confidential information
  # (create the session table with "rake db:sessions:create")
  # config.action_controller.session_store = :active_record_store
  


  # Use SQL instead of Active Record's schema dumper when creating the test database.
  # This is necessary if your schema can't be completely dumped by the schema dumper,
  # like if you have constraints or database-specific column types
  # config.active_record.schema_format = :sql

  # Activate observers that should always be running
  # config.active_record.observers = :cacher, :garbage_collector
  
#config.gem 'fleximage'
#config.gem 'ruby-recaptcha'





  end
  
   
  
ActionMailer::Base.default_content_type = "text/html"
#require 'paypal'
#require 'money'




require 'ipaddr'
require 'RMagick'  
require 'gchart'
require 'recaptcha'

#require 'email_veracity'
#   require 'currency/exchange/rate/source/base'
#require 'currency'
#   require 'money'
##require "rcurrency" 
#   require 'currency/exchange/rate/deriver'
#   require 'currency/exchange/rate/source/xe'
#   require 'currency/exchange/rate/source/timed_cache'
#   require 'currency/exchange/rate/source/base'
   
   




#require 'bj'
#  require 'socket'






  
  
  # Include your application configuration below
# These defaults are used in GeoKit::Mappable.distance_to and in acts_as_mappable
GeoKit::default_units = :miles
GeoKit::default_formula = :sphere

# This is the timeout value in seconds to be used for calls to the geocoder web
# services.  For no timeout at all, comment out the setting.  The timeout unit
# is in seconds. 
GeoKit::Geocoders::timeout = 3

# These settings are used if web service calls must be routed through a proxy.
# These setting can be nil if not needed, otherwise, addr and port must be 
# filled in at a minimum.  If the proxy requires authentication, the username
# and password can be provided as well.
GeoKit::Geocoders::proxy_addr = nil
GeoKit::Geocoders::proxy_port = nil
GeoKit::Geocoders::proxy_user = nil
GeoKit::Geocoders::proxy_pass = nil

# This is your yahoo application key for the Yahoo Geocoder.
# See http://developer.yahoo.com/faq/index.html#appid
# and http://developer.yahoo.com/maps/rest/V1/geocode.html
GeoKit::Geocoders::yahoo = 'REPLACE_WITH_YOUR_YAHOO_KEY'
    
# This is your Google Maps geocoder key. 
# See http://www.google.com/apis/maps/signup.html
# and http://www.google.com/apis/maps/documentation/#Geocoding_Examples


#GeoKit::Geocoders::google = 'ABQIAAAAmJz8xm-Ja_ZK1XQwhiJzDBQZkXe_EpvnrmZpXiq6nSo1ISoWFhQ531AAdomVJEComYTNoI683GmTDQ'
GeoKit::Geocoders::google = 'REPLACE_WITH_YOUR_GOOGLE_KEY'


    
# This is your username and password for geocoder.us.
# To use the free service, the value can be set to nil or false.  For 
# usage tied to an account, the value should be set to username:password.
# See http://geocoder.us
# and http://geocoder.us/user/signup
GeoKit::Geocoders::geocoder_us = false 

# This is your authorization key for geocoder.ca.
# To use the free service, the value can be set to nil or false.  For 
# usage tied to an account, set the value to the key obtained from
# Geocoder.ca.
# See http://geocoder.ca
# and http://geocoder.ca/?register=1
GeoKit::Geocoders::geocoder_ca = false

# This is the order in which the geocoders are called in a failover scenario
# If you only want to use a single geocoder, put a single symbol in the array.
# Valid symbols are :google, :yahoo, :us, and :ca.
# Be aware that there are Terms of Use restrictions on how you can use the 
# various geocoders.  Make sure you read up on relevant Terms of Use for each
# geocoder you are going to use.
GeoKit::Geocoders::provider_order = [:google,:us]


  SubdomainFu.tld_size = 1 # sets for current environment
  SubdomainFu.tld_sizes = {:development => 0,
                           :alpha => 1,
                           :beta => 1,
                           :production => 1} # set all at once (also the defaults)



I18n.default_locale = 'en'

LOCALES_DIRECTORY = "#{RAILS_ROOT}/config/locales"
LOCALES_AVAILABLE = Dir["#{LOCALES_DIRECTORY}/*.{rb,yml}"].collect do |locale_file|
  I18n.load_path << locale_file
  File.basename(File.basename(locale_file, ".rb"), ".yml")
end.uniq.sort

#Bj.config["#{RAILS_ENV}.no_tickle"] = true 
#Bj.config["production.no_tickle"] = true  
#class String
##  require 'iconv' #this line is not needed in rails !
#  def to_utf8
#    Iconv.conv('utf-8','iso-8859-1', self)
#  end
#end










#RCC_PUB='foo' #normal constant decl
#RCC_PUB=ENV['6Le37boSAAAAAGpPE5zXg2WOPH1miNcjnxHxEV__'] #from an environment variable
#RCC_PUB, RCC_PRIV = '6Le37boSAAAAAH9drrvY5UCcg3aXiTxWydA5opiH'

ENV['RECAPTCHA_PUBLIC_KEY']  = '6Le37boSAAAAAGpPE5zXg2WOPH1miNcjnxHxEV__'
ENV['RECAPTCHA_PRIVATE_KEY'] = '6Le37boSAAAAAH9drrvY5UCcg3aXiTxWydA5opiH'

#RCC_PUB, RCC_PRIV = (*File.open('/etc/keys', 'r').read_lines).map(&:reverse) #stored backwards, in a file.  muaahah!


    unless RAILS_ENV == 'production'
      # for test
      PAYPAL_ACCOUNT = 'p_khan_1237377250_biz@hotmail.com'
    else
      # for production
      PAYPAL_ACCOUNT = 'production@account.com'
  end
  
  



# require 'dalli'

#require 'memcache'
#require 'cached_model'

#memcache_options = {
#  :c_threshold => 10_000,
#  :compression => true,
#  :debug => false,
#  :namespace => 'rethai',
#  :readonly => false,
#  :urlencode => false
#}

#CACHE = MemCache.new memcache_options
#CACHE.servers = '192.168.1.8:17898'




