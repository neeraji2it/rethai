var _IE = false;
if(navigator.appName.indexOf('Microsoft Internet Explorer') != -1) _IE = true;
document.observe("dom:loaded", function(){
	initMainGall();
});

var _changeEl;

/*---- main gallery function ---*/
function initMainGall(){
	var nav_h = $('navigation');
	if(nav_h){
		var _a = 0;		
		var _btn = nav_h.select('a');
		_btn.each(function(_el, _ind){
			_el._box = $(_el.href.substr(_el.href.indexOf("#") + 1));
			if(_el._box){
				if(_el.parentNode.className.indexOf('active') != -1){
					_a = _ind;
					_el._box.addClassName('active').setStyle({display:'block', 'opacity': 1});
				}
				else{
					_el._box.setStyle({display:'block', 'opacity': 0});
				}				
			}			
		});
	}
	
	$$('.visual div.tab').each(function(gall_hold){
		var _list = gall_hold.select('.fade-gall > li');
		var btn_prev = gall_hold.select('.paging .prev')[0];
		var btn_next = gall_hold.select('.paging .next')[0];
		var _info = gall_hold.select('.paging span')[0];
		var _a = 0;
		for(var i = 0; i < _list.length; i++){
			if(_list[i].hasClassName('active')) _a = i;
			_list[i].removeClassName('active').setStyle({opacity: 0});
		}
		_list[_a].addClassName('active').setStyle({opacity: 1});
		if(_info) _info.innerHTML = (_a+1)+'/'+_list.length;
		if(btn_prev){
			btn_prev.onclick = function(){
				if(_a == 0) changeEl(_list.length -1);
				else changeEl(_a - 1);
				return false;
			}
		}
		if(btn_next){
			btn_next.onclick = function(){
				if(_a == _list.length - 1) changeEl(0);
				else changeEl(_a + 1);
				return false;
			}
		}
		
		$$('#tabs>li').each(function(current_li)
  	{
  	  var cur_index = $$('#tabs>li').indexOf(current_li);
  	  current_li.onclick = function(){
        changeEl(cur_index);
        return false;        
  	  }
  	});
		
		var _f = true;
		function changeEl(_ind){
			if(_f && _ind != _a){
				_f = false;
				_list[_a].removeClassName('active');
				_list[_ind].addClassName('active');
				
				// take the active class off the tabs
        $$('#tabs>li.active').each(function(li){li.removeClassName('active');})
        // set the selected tab active
        $$('#tabs>li')[_ind].addClassName('active');
        
				new Effect.Opacity(_list[_a], { to: 0, duration: 0.6});
				new Effect.Opacity(_list[_ind], { to: 1, duration: 0.6, afterFinish:function(){ _f = true;}});
				if(_info) _info.innerHTML = (_ind+1)+'/'+_list.length;
				_a = _ind;
			}
		}
		_changeEl = changeEl;
	});	
}