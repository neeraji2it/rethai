function load2(location){
	
	//alert('location:' + document.getElementById(location).value);
	if (GBrowserIsCompatible()) {
		var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var i = 0;
		var mylisting = 0;
		
		
		// A function to create the marker and set up the event window
		function createMarker(point, name, html){
			var marker = new GMarker(point);
			// === store the name so that the tooltip function can use it ===
			marker.tooltip = '<div class="tooltip">' + name + '<\/div>';
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(html);
			});
			gmarkers[i] = marker;
			htmls[i] = html;
			side_bar_html += '<a href="javascript:myclick(' + i + ')" onmouseover="mymouseover(' + i + ')" onmouseout="mymouseout()">' + name + '<\/a><br>';
			i++;
			map.addOverlay(marker);
			
			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
			GEvent.addListener(marker, "mouseover", function(){
				showTooltip(marker);
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden"
			});
			 
		}
		
		// ====== This function displays the tooltip ======
		// it can be called from an icon mousover or a side_bar mouseover
		function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
		// It launches the tooltip on the icon      
		function mymouseover(i){
			showTooltip(gmarkers[i])
		}
		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
		// It hides the tooltip      
		function mymouseout(){
			tooltip.style.visibility = "hidden";
		}
		
		// This function picks up the side_bar click and opens the corresponding info window
		function myclick(i){
			gmarkers[i].openInfoWindowHtml(htmls[i]);
		}
		
		
		
		// create the map
		
		
		
////		var p_address = new GLatLng(address);
		var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		
		

		
		
		
		geocoder = new GClientGeocoder();
		if ((document.getElementById(location).value).search(/thailand|Thailand/)>0){
			strlocation = document.getElementById(location).value;
		}else{
			strlocation = document.getElementById(location).value + ", thailand";
		}
			//alert("location in search :"+strlocation);
		geocoder.getLatLng(strlocation, function(point){
			if (point) {
				map.setCenter(point, 14);
			}else{
//				alert('Please check your Location');
				map.setCenter(new GLatLng(12.9357, 100.889), 14);
			}
		});
// 			GEvent.addListener(map,'zoomend',function(){
////                alert("zoomed: " + map.getZoom());
//				if (map.getZoom() > 18){
//					alert("Please not zoom out more here(set me at indexpro.js line 99)");
//					map.setZoom(18);
//					
//				}else if(map.getZoom() < 8){
//					alert("Please not zoom out more here(set me at indexpro.js line 103)");
//					map.setZoom(8);
//					
//				}
//        });
		// ====== set up marker mouseover tooltip div ======
		var tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		
		// Read the data from example.xml
		var request = GXmlHttp.create();
		request.open("GET", "/xml/offer/index.xml", true);
		request.onreadystatechange = function(){
			if (request.readyState == 4) {
				var xmlDoc = GXml.parse(request.responseText);
				// obtain the array of markers and loop through it
				var markers = xmlDoc.documentElement.getElementsByTagName("marker");
				
				for (var i = 0; i < markers.length; i++) {
					// obtain the attribues of each marker
					var lat = parseFloat(markers[i].getAttribute("lat"));
					var lng = parseFloat(markers[i].getAttribute("lng"));
					var point = new GLatLng(lat, lng);
					var html = markers[i].getAttribute("html");
					var label = markers[i].getAttribute("label");
					//Check first popup
					var firstpopup="src=/products/gdetail/"+document.getElementById('firstpopup').value;
					if (document.getElementById('firstpopup').value !='0'){
						if (html.search(firstpopup)>0){
//							alert("yes I can see first popup:D   "+i);
							mylisting = i;
						}
					}
					// create the marker
					var marker = createMarker(point, label, html);
				}
				// put the assembled side_bar_html contents into the side_bar div
				document.getElementById("side_bar").innerHTML = side_bar_html;
			}
		}
		request.send(null);
		
		
//		alert("Zoom level:"+map.getZoom());
		//show mylisting
		if(document.getElementById('firstpopup').value!='0'){
//		alert(mylisting);
//		myclick(mylisting);
		gmarkers[mylisting].openInfoWindowHtml(htmls[mylisting]);
//		document.getElementById('temppopup').value = mylisting;
		}
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
	
	// This Javascript is based on code provided by the
	// Blackpool Community Church Javascript Team
	// http://www.commchurch.freeserve.co.uk/   
	// http://econym.googlepages.com/index.htm

	//]]>
}
var htmloption2; 
var y;
var mytypes;
var map;
var mylisting;
var popuplisting;
//var detailform = "";

//var kendiotext;
//var kendiopoint;
function load2index2(location,id_listing){
	popuplisting = id_listing;
				
				
//				document.getElementById('firstpopup').value = ""	
	//alert('location:' + document.getElementById(location).value);
	if (GBrowserIsCompatible()) {
		var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var i = 0;
		var mylisting = 0;
		
		
		// A function to create the marker and set up the event window
		function createMarker(point, number, id_product, alllistings,popuplisting){
			var marker = new GMarker(point);
			marker.value = number;
			marker.id_product = id_product;
			marker.point = point;
			var myhtml = "";
			  if(popuplisting == id_product){
			  	// Hidden shadow
				document.getElementById('shadow').style.display='none';
				document.getElementById('quickeventmodal').style.display='none';
			  			var myAjax = new Ajax.Request('/controlpanel/getimages2', {
						method: 'get',
						parameters: 'id=' + marker.id_product,
						onComplete: function(request){
							myhtml = myhtml + request.responseText;
							var detailform = "";
							detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
							detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							marker.htmlshow = myhtml + "<br>" + detailform;
							map.openInfoWindowHtml(marker.point, marker.htmlshow);
						} // end of anonymous onComplete function
					}); // end of the new Ajax.Request() call
					popuplisting = 0;
//			 alert(popuplisting);
			 }	
			 	 
//			 } // end of anonymous onComplete function
//			 }); // end of the new Ajax.Request() call
//			
			 GEvent.addListener(marker,"click", function() {
			 				  var myhtml="";
//				alert(id_product);
				if (!marker.htmlshow) {
					var myAjax = new Ajax.Request('/controlpanel/getimages2', {
						method: 'get',
						parameters: 'id=' + marker.id_product,
						onComplete: function(request){
							myhtml = myhtml + request.responseText;
							var detailform = "";
							detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
							detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							marker.htmlshow = myhtml + "<br>" + detailform;
							map.openInfoWindowHtml(marker.point, marker.htmlshow);
						} // end of anonymous onComplete function
					}); // end of the new Ajax.Request() call
				}else{
					map.openInfoWindowHtml(marker.point, marker.htmlshow);
				}
				
		      });
			  
			  
//			gmarkers[number] = marker;
//			markers.push(marker);
//			var markerCluster = new MarkerClusterer(map, markers);

			
			map.addOverlay(marker);
			 if(number == (alllistings -1)){
			 	progressBar.updateLoader(1);
				setTimeout('progressBar.remove();',1000)	
			}else{progressBar.updateLoader(1);}
			
			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
			GEvent.addListener(marker, "mouseover", function(){
//				alert(marker.id_product);			
					if (!marker.tooltip) {
						var mylabel = "loadding...";
						var myAjax2 = new Ajax.Request('/products/getlabel', {
							method: 'get',
							parameters: 'id=' + marker.id_product,
							onComplete: function(request){
								mylabel = request.responseText;
								marker.tooltip = '<div class="tooltip">' + mylabel + '<\/div>';
								showTooltip(marker);
								
							} // end of anonymous onComplete function
						}); // end of the new Ajax.Request() call			 
					}else{showTooltip(marker);}
				
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden"
			});
			 
		}
		
		// ====== This function displays the tooltip ======
		// it can be called from an icon mousover or a side_bar mouseover
		function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
		// It launches the tooltip on the icon      
		function mymouseover(i){
			showTooltip(gmarkers[i]);
		}
		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
		// It hides the tooltip      
		function mymouseout(){
			tooltip.style.visibility = "hidden";
		}
		
		// This function picks up the side_bar click and opens the corresponding info window
		function myclick(i){
			gmarkers[i].openInfoWindowHtml(htmls[i]);
		}
		
		
		
		// create the map
		
		
		
////		var p_address = new GLatLng(address);
		 map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		progressBar = new ProgressbarControl(map, {width:150}); 
		

		

		
		
		
		geocoder = new GClientGeocoder();
		if ((document.getElementById(location).value).search(/thailand|Thailand/)>0){
			strlocation = document.getElementById(location).value;
		}else{
			strlocation = document.getElementById(location).value + ", thailand";
		}
			//alert("location in search :"+strlocation);
		 if (document.getElementById('firstpopup').value =="") {
		 	geocoder.getLatLng(strlocation, function(point){
		 		if (point) {
		 			map.setCenter(point, 14);
		 		}
		 		else {
		 			//				alert('Please check your Location');
						map.setCenter(new GLatLng(12.9357, 100.889), 14);
					}
				});
			}else{
				map.setCenter(new GLatLng(document.getElementById('mylat').value,document.getElementById('mylng').value), 14);
			}
		
//		var mcOptions = { gridSize: 100, maxZoom: 18};
//		var markers = []; // Create the markers you want to add and collect them into a array.
//		var mc = new MarkerClusterer(map, markers, mcOptions);
		
		


		
		// ====== set up marker mouseover tooltip div ======
		var tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		//ajax for create marker
		var html="";
		htmloption2 = new Array();
		 if (document.getElementById('firstpopup').value != "") {
		 	var bounds = map.getBounds();
		 	var southWest = bounds.getSouthWest();
		 	var northEast = bounds.getNorthEast();
		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
		 	var clearlisting = document.getElementById('clearlisting').value;
			if(clearlisting == ""){clearlisting = 1;}
			if(clearlisting ==1){
				if(document.getElementById('hiddenshadow').value == '0'){
					document.getElementById('quickeventmodal').style.display='block';
					document.getElementById('shadow').style.display='block';	
				}else{
					document.getElementById('quickeventmodal').style.display='none';
					document.getElementById('shadow').style.display='none';
					document.getElementById('hiddenshadow').value = '0';
				}
				
			}else{
				document.getElementById('quickeventmodal').style.display='none';
				document.getElementById('shadow').style.display='none';
			}
		 	var myAjax2 = new Ajax.Request("/products/getmarker?clearlisting="+clearlisting+"&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value + "", {
		 	
		 		method: 'get',
		 		parameters: mapBoundary,
		 		onComplete: function(request){
		 			mytypes = eval("(" + request.responseText + ")");
		 			if (mytypes.length > 0) {
		 				progressBar.start(mytypes.length);
		 			}
		 			
		 			for (var y = 0; y < mytypes.length; y++) {
		 				var lat = mytypes[y].lat;
		 				var lng = mytypes[y].lng;
		 				var id_product = mytypes[y].id;
		 				var point = new GLatLng(lat, lng);
		 				
		 				createMarker(point, y, id_product, mytypes.length,popuplisting);
		 				
		 			}
		 			//setTimeOut("alert('Welcome to www.rethai.Com');",5000);
					//gmarkers[0].openInfoWindowHtml(htmls[0]);
					//alert("wait wait wait!!"+htmls[0]);
					
					} // end of anonymous onComplete function  
				}); // end of the new Ajax.Request() call 	
				document.getElementById('clearlisting').value = "0";						 
				clearlisting = '0';									 
			}							 
			GEvent.addListener(map,'moveend',function() {
			//	alert('Mooooove');
//			 markers = [];
			 if (document.getElementById('firstpopup').value =="") {
				var bounds = map.getBounds();
			var southWest = bounds.getSouthWest();
			var northEast = bounds.getNorthEast();
			var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();			
			var clearlisting = document.getElementById('clearlisting').value;
			if(clearlisting == ""){clearlisting = 1;}
			if(clearlisting ==1){
				if(document.getElementById('hiddenshadow').value == '0'){
					document.getElementById('quickeventmodal').style.display='block';
					document.getElementById('shadow').style.display='block';	
				}else{
					document.getElementById('quickeventmodal').style.display='none';
					document.getElementById('shadow').style.display='none';
					document.getElementById('hiddenshadow').value = '0';
				}
				
			}else{
				document.getElementById('quickeventmodal').style.display='none';
				document.getElementById('shadow').style.display='none';
			}
										var myAjax2 = new Ajax.Request( "/products/getmarker?clearlisting="+clearlisting+"&salerent="+document.getElementById('salerent').value+"&propertytype="+document.getElementById('propertytype').value+"&code="+document.getElementById('code').value+"&fprice="+document.getElementById('fprice').value+"&tprice="+document.getElementById('tprice').value+"&bed="+document.getElementById('bed').value+"&bed2="+document.getElementById('bed2').value+"&bath="+document.getElementById('bath').value +"", {  
										 method: 'get',  
										 parameters: mapBoundary,
										 onComplete: function(request){  
										 mytypes = eval( "(" + request.responseText + ")" );  
										if(mytypes.length > 0){progressBar.start(mytypes.length);} 
										 for (var y = 0; y < mytypes.length; y++) {
										 			var lat = mytypes[y].lat;
												 	var lng = mytypes[y].lng;
													var id_product = mytypes[y].id;
													var point = new GLatLng(lat, lng);	
													
													createMarker(point,y,id_product,mytypes.length,popuplisting);
													if(popuplisting == id_product){ popuplisting=0;}
										 }
											
										 } // end of anonymous onComplete function  
										 }); // end of the new Ajax.Request() call 
				document.getElementById('clearlisting').value = "0";						 
				clearlisting = '0';
										 
				}								 
				tooltip.style.visibility = "hidden";						 
			});

	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
	
	// This Javascript is based on code provided by the
	// Blackpool Community Church Javascript Team
	// http://www.commchurch.freeserve.co.uk/   
	// http://econym.googlepages.com/index.htm

	//]]>
}


function salerentprice(id){
	//alert(document.getElementById(id).value);
	 if(document.getElementById(id).value == '1'){
	 	document.getElementById('saleprice').style.display='block';
		document.getElementById('rentprice').style.display='none';
		
		document.getElementById('saleprice2').style.display='block';
		document.getElementById('rentprice2').style.display='none';
	}else if(document.getElementById(id).value == '0'){
		document.getElementById('saleprice').style.display='none';
		document.getElementById('rentprice').style.display='block';
		
		
		document.getElementById('saleprice2').style.display='none';
		document.getElementById('rentprice2').style.display='block';
	}else if(document.getElementById(id).value == '2'){
		document.getElementById('saleprice').style.display='block';
		document.getElementById('rentprice').style.display='block';
		
		document.getElementById('saleprice2').style.display='block';
		document.getElementById('rentprice2').style.display='block';
	}
return false;
}

function listingtype(id){
	//alert(document.getElementById(id).value);
	 if(document.getElementById(id).value == '5'){
	 	// Hidden bedrooms and bathrooms
	 	document.getElementById('showbed').style.display='none';
		document.getElementById('showbath').style.display='none';
		
		document.getElementById('showbed2').style.display='none';
		document.getElementById('showbath2').style.display='none';
		
		document.getElementById('showparking').style.display='none';
		document.getElementById('showparking2').style.display='none';
		
		//hidden condoname
	 	document.getElementById('condoname1').style.display='none';
	 	document.getElementById('condoname2').style.display='none';
	}else if(document.getElementById(id).value == '1'){		
	 	document.getElementById('condoname1').style.display='block';
	 	document.getElementById('condoname2').style.display='block';
		// Show bedrooms and bathrooms
	 	document.getElementById('showbed').style.display='block';
		document.getElementById('showbath').style.display='block';
		
		document.getElementById('showbed2').style.display='block';
		document.getElementById('showbath2').style.display='block';
		
		document.getElementById('showparking').style.display='block';
		document.getElementById('showparking2').style.display='block';
	}else{
		// Show bedrooms and bathrooms
	 	document.getElementById('showbed').style.display='block';
		document.getElementById('showbath').style.display='block';
		
		document.getElementById('showbed2').style.display='block';
		document.getElementById('showbath2').style.display='block';
		
		document.getElementById('showparking').style.display='block';
		document.getElementById('showparking2').style.display='block';
		//hidden condoname
	 	document.getElementById('condoname1').style.display='none';
	 	document.getElementById('condoname2').style.display='none';
	}
return false;
}
var htmloption1 ="";
var htmloption2 ="";
function load3(id,address) {
//	alert('yes me');
	document.getElementById('shadow').style.display='none';
		
		if (address == ''){
			if(id=='0'){document.getElementById('user2_Suburb').value = "pattaya";}
			document.getElementById('quickeventmodal').style.display='none';
			address = document.getElementById('user2_Suburb').value;
		}else{
			address = address + ", thailand";//'pattaya';//document.getElementById('product_City').value;
			document.getElementById('mapshow').style.display='none';
			document.getElementById('map').style.display='block';
		}
		
		map = new GMap2(document.getElementById("map"));
						map.setCenter(new GLatLng(12.9357, 100.889), 14);
						map.enableDoubleClickZoom();
						map.setMapType(G_HYBRID_MAP);
						map.addControl(new GMapTypeControl());
						map.addControl(new GLargeMapControl());
						geocoder = new GClientGeocoder();	
		//geocoder = new GClientGeocoder();
			//if (geocoder) {
//				alert('not');
				geocoder.getLatLng(address, function(point){
					if (!point) {
//						alert(address + " not found\nPlease try again.");
						
						var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
						map.setCenter(new GLatLng(12.9357, 100.889), 14);
						map.enableDoubleClickZoom();
						map.setMapType(G_HYBRID_MAP);
						map.addControl(new GMapTypeControl());
						map.addControl(new GLargeMapControl());
						 geocoder = new GClientGeocoder();
						 
						 htmloption1 = "";
						 var myAjax = new Ajax.Request( '/products/getlisting', {  
										 method: 'get',  
										 onComplete: function(request){  
										 mytypes = eval( "(" + request.responseText + ")" );  
//										 alert(mytypes);
										 for (var i = 0 ; i < mytypes.length ; i++) {  
										 var ioption = mytypes[i]
										 var listingvalue = ioption.Value;
										 var listingname = ioption.Name;
//										 alert(ioption);
										   htmloption1 = htmloption1 + "<option value=\""+listingvalue+"\">"+listingname +"</option>\n";
										 } // end of for loop  
										 } // end of anonymous onComplete function  
										 }); // end of the new Ajax.Request() call  
										 
						htmloption2 = ""; 
						var myAjax2 = new Ajax.Request( '/products/getparking', {  
										 method: 'get',  
										 onComplete: function(request){  
										 mytypes = eval( "(" + request.responseText + ")" );  
//										 alert(mytypes);
										 for (var x = 0 ; x < mytypes.length ; x++) {  
										 var ioption = mytypes[x]
										 var listingvalue = ioption.Value;
										 var listingname = ioption.Name;
//										 alert(ioption);
										   htmloption2 = htmloption2 + "<option value=\""+listingvalue+"\">"+listingname +"</option>\n";
										 } // end of for loop  
										 } // end of anonymous onComplete function  
										 }); // end of the new Ajax.Request() call 	
						GEvent.addListener(map, "click", function(overlay, latlng){	
						var htmlform = "<form method=\"post\" enctype=\"multipart/form-data\"  action=\"/products/create\" >";
						htmlform = htmlform + "<table border=0 class=\"addlistingtable\"><tr><td width=\"350\" height=\"320\"  valign=\"top\">\n";
						htmlform = htmlform + "<div id=\"gaddproductform1\" style=\"display:block;\">\n<!--lat:"+latlng.lat()+"\n<br>lng:"+latlng.lng()+"-->\n   <input type=\"hidden\" name=\"product2[lat]\" id=\"product2_lat\" value=\""+latlng.lat()+"\"/>\n";
						htmlform = htmlform + "<input type=\"hidden\" name=\"product2[lng]\" id=\"product2_lng\" value=\""+latlng.lng()+"\"/>\n";
						
						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /></div>";
						
						// set redirect after created
						htmlform = htmlform + "<input type=\"hidden\" name=\"myredirect\" id=\"myredirect\" value=\""+document.getElementById('myredirect').value+"\"/>"
						
						//For sale etc
						htmlform = htmlform + "<table border=0 cellpadding=0 cellspacing=5><tr><td>Property Listing: </td><td><select name=\"product2[salerent]\" id=\"product2_salerent\" onchange=\"return salerentprice(\'product2_salerent\');\">\n";
						htmlform = htmlform + "<option value=\"1\">For Sale</option>\n";
						htmlform = htmlform + "<option value=\"0\">For Rent</option>\n";
						htmlform = htmlform + "<option value=\"2\">For Sale or Rent</option>\n";						
						htmlform = htmlform + "</select></td></tr>\n";
						
						//type of listings
						htmlform = htmlform + "<tr><td>Property Type: </td><td><select name=\"product2[Type]\" id=\"product2_Type\" onchange=\"return listingtype(\'product2_Type\');\">\n";
						htmlform = htmlform + htmloption1;
						htmlform = htmlform + "</select></td></tr>";
						
						//SQM
						htmlform = htmlform + "<tr>\n<td>SQM:</td>\n";
						htmlform = htmlform + "<td><input type='text' name='tmpsqm' id='tmpsqm' onblur=\"this.value=formatCurrency(this.value,'product2_SQM');\"/><input type='hidden' name='product2[SQM]' id='product2_SQM'/></td>\n";
						htmlform = htmlform + "</tr>\n";
						
						
						//price						
		   				htmlform = htmlform + "<tr><td><span id=\"saleprice\" stylel=\"display:block;\">Sale Price: </span></td><td><span id=\"saleprice2\" stylel=\"display:block;\">   	<input type=\"text\" name=\"tempprice1\" id=\"tempprice1\"  	onblur=\"this.value=formatCurrency(this.value,'product2_Price');\"/>    	<input type=\"hidden\" name=\"product2[Price]\" id=\"product2_Price\"  />      	</span></td></tr>\n";
						htmlform = htmlform + "<tr><td><span id=\"rentprice\" style=\"display:none;\">Rent Price: </span></td><td><span id=\"rentprice2\" style=\"display:none;\">			<input type=\"text\" name=\"tempprice2\" id=\"tempprice2\"  	onblur=\"this.value=formatCurrency(this.value,'product2_RPrice');\"/>		<input type=\"hidden\" name=\"product2[RPrice]\" id=\"product2_RPrice\"  />		</span></td></tr>\n";
						
						//bedroom
						htmlform = htmlform + "<tr><td><div id=\"showbed\">Bedrooms: </div></td><td><div id=\"showbed2\"><select name=\"product2[Bed]\" id=\"product2_Bed\">\n";
						htmlform = htmlform + "<option value=\"0\">Studio</option>\n";
						htmlform = htmlform + "<option value=\"1\">1</option>\n";
						htmlform = htmlform + "<option value=\"2\">2</option>\n";
						htmlform = htmlform + "<option value=\"3\">3</option>\n";
						htmlform = htmlform + "<option value=\"4\">4</option>\n";
						htmlform = htmlform + "<option value=\"5\">5</option>\n";
						htmlform = htmlform + "<option value=\"6\">6+</option>\n";
						htmlform = htmlform + "</select></div></td></tr>";
						
						//bathroom
						htmlform = htmlform + "<tr><td><div id=\"showbath\">Bathroom: </div></td><td><div id=\"showbath2\"><select name=\"product2[Bath]\" id=\"product2_Bath\">\n";
						htmlform = htmlform + "<option value=\"1\">1</option>\n";
						htmlform = htmlform + "<option value=\"2\">2</option>\n";
						htmlform = htmlform + "<option value=\"3\">3</option>\n";
						htmlform = htmlform + "<option value=\"4\">4</option>\n";
						htmlform = htmlform + "<option value=\"5\">5+</option>\n";
						htmlform = htmlform + "</select></div></td></tr>";

						//parking
						htmlform = htmlform + "<tr><td>Parking: </td><td><select name=\"product2[Parking]\" id=\"product2_Parking\">\n";						
						htmlform = htmlform + htmloption2;
						htmlform = htmlform + "</select></td></tr>\n"

						
						htmlform = htmlform + "</table>\n";
						// next button
						htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'block\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\">Next</a>\n";
						htmlform = htmlform + "</div>\n";
						

						htmlform = htmlform + "<div id=\"gaddproductform2\" style=\" display:none;\">\n";
						// Detail

						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /><br /></div>";

		   				htmlform = htmlform + "<table border=0 cellpadding=0 cellspacing=5 style=margin-top:-10px><tr><td valign=top>Property Details: </td><td><textarea name=\"product2[Detail]\" id=\"product2_Detail\" cols=\"30\" rows=\"3\"></textarea></td></tr>\n";						
						
						//picture
						//htmlform = htmlform + "<tr><td><strong>Images</strong> </td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 1: </td><td><input type=\"file\" name=\"image2[pic1]\" id=\"image2_pic1\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 2: </td><td><input type=\"file\" name=\"image2[pic2]\" id=\"image2_pic2\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 3: </td><td><input type=\"file\" name=\"image2[pic3]\" id=\"image2_pic3\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 4: </td><td><input type=\"file\" name=\"image2[pic4]\" id=\"image2_pic4\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 5: </td><td><input type=\"file\" name=\"image2[pic5]\" id=\"image2_pic5\" cols=\"30\" ></td></tr></table>\n";
						
		   				htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:					document.getElementById(\'gaddproductform1\').style.display=\'block\';		document.getElementById(\'gaddproductform2\').style.display=\'none\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\" style=margin-right:15px>Back</a>\n";
						htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'none\';		document.getElementById(\'gaddproductform3\').style.display=\'block\'; genpage3();\">Next</a>\n";
						htmlform = htmlform + "</div>\n";	
						
						htmlform = htmlform + "<div id=\"gaddproductform3\" style=\" display:none;\">\n";


						// confirm page
						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /></div>";

						htmlform = htmlform + "<p></p>\n";
						htmlform = htmlform + "<p></p>\n";
						htmlform = htmlform+ "<div id=\"confirmdetail\"></div>\n";
						htmlform = htmlform + "<!--Edit me at javascripts/indexpro.js --> ";


						
						//auto-renew this listing every month
						htmlform = htmlform + "<input type=\"checkbox\"name=\"product2[renew]\" id=\"product2_renew\" value=\"1\"  checked=\"checked\" /><label for=\"product2_renew\" style=padding-left:10px>auto-renew this listing every month</label><br><br>";						

						
												
						
		   				htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'block\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\" style=margin-right:15px>Back</a>\n";
						htmlform = htmlform + "<input type=\"Submit\" value=\"Add My Listing!\" onclick=\"return validateform(\'product2_Price\',\'product2_Bed\',\'product2_Bath\',\'product2_Detail\',\'product2_RPrice\',\'product2_salerent\');\" />	\n";  
						htmlform = htmlform + "</div>\n";										
		  				htmlform = htmlform + "</td></tr></table></form>\n";
							if (latlng) {   
					            var myHtml = "The GLatLng value is: " + latlng.lat + " at zoom level " + map.getZoom();
//					            map.openInfoWindow(latlng, myHtml);
								map.openInfoWindow(latlng, htmlform);
					          }

						});
						
						
						
					//        geocoder = new GClientGeocoder();				
					}
					else {
//						alert('yes');
//						alert(address + " yes, have");
						map = new GMap2(document.getElementById("map"));
						var mt = map.getMapTypes();
					      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
					      for (var i=0; i<mt.length; i++) {
					        mt[i].getMinimumResolution = function() {return 8;}
					        mt[i].getMaximumResolution = function() {return 18;}
					      }
						map.enableDoubleClickZoom();
						//        		map.setMapType(G_SATELLITE_MAP);
						map.setMapType(G_HYBRID_MAP);
						map.addControl(new GMapTypeControl());
						map.addControl(new GLargeMapControl());
//						map.addControl(new GOverviewMapControl());
						//geocoder = new GClientGeocoder();
						
						map.setCenter(point, 13);
						
						htmloption1 = "";
						 var myAjax = new Ajax.Request( '/products/getlisting', {  
										 method: 'get',  
										 onComplete: function(request){  
										 mytypes = eval( "(" + request.responseText + ")" );  
//										 alert(mytypes);
										 for (var i = 0 ; i < mytypes.length ; i++) {  
										 var ioption = mytypes[i]
										 var listingvalue = ioption.Value;
										 var listingname = ioption.Name;
//										 alert(ioption);
										   htmloption1 = htmloption1 + "<option value=\""+listingvalue+"\">"+listingname +"</option>\n";
										 } // end of for loop  
										 } // end of anonymous onComplete function  
										 }); // end of the new Ajax.Request() call 
										 
						htmloption2 = "";
						 var myAjax2 = new Ajax.Request( '/products/getparking', {  
										 method: 'get',  
										 onComplete: function(request){  
										 mytypes = eval( "(" + request.responseText + ")" );  
//										 alert(mytypes);
										 for (var x = 0 ; x < mytypes.length ; x++) {  
										 var ioption = mytypes[x]
										 var listingvalue = ioption.Value;
										 var listingname = ioption.Name;
//										 alert(ioption);
										   htmloption2 = htmloption2 + "<option value=\""+listingvalue+"\">"+listingname +"</option>\n"; 
										 } // end of for loop  
										 } // end of anonymous onComplete function  
										 }); // end of the new Ajax.Request() call 
						
										 
						GEvent.addListener(map, "click", function(overlay, latlng){	
						
						var htmlform = "<form method=\"post\" enctype=\"multipart/form-data\"  action=\"/products/create\" >";
						htmlform = htmlform + "<table border=0 class=\"addlistingtable\"><tr><td width=\"350\" height=\"320\"  valign=\"top\">\n";
						htmlform = htmlform + "<div id=\"gaddproductform1\" style=\"display:block;\">\n";
						//htmlform = htmlform + "<!--lat:"+latlng.lat()+"\n<br>lng:"+latlng.lng()+"-->\n ";
						
						htmlform = htmlform + "<input type=\"hidden\" name=\"product2[lat]\" id=\"product2_lat\" value=\""+latlng.lat()+"\"/>\n";
						htmlform = htmlform + "<input type=\"hidden\" name=\"product2[lng]\" id=\"product2_lng\" value=\""+latlng.lng()+"\"/>\n";
						

						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /></div>";
						
						// set redirect after created
						htmlform = htmlform + "<input type=\"hidden\" name=\"myredirect\" id=\"myredirect\" value=\""+document.getElementById('myredirect').value+"\"/>"
						
						//For sale etc
						htmlform = htmlform + "<table border=0 cellpadding=0 cellspacing=5><tr><td>Property Listing:</td><td><select name=\"product2[salerent]\" id=\"product2_salerent\" onchange=\"return salerentprice(\'product2_salerent\');\">\n";
						htmlform = htmlform + "<option value=\"1\">For Sale</option>\n";
						htmlform = htmlform + "<option value=\"0\">For Rent</option>\n";
						htmlform = htmlform + "<option value=\"2\">For Sale or Rent</option>\n";						
						htmlform = htmlform + "</select></td></tr>\n";
						
						//type of listings
						htmlform = htmlform + "<tr><td>Property Type: </td><td><select name=\"product2[Type]\" id=\"product2_Type\" onchange=\"return listingtype(\'product2_Type\');\">\n";
						htmlform = htmlform + htmloption1;
						htmlform = htmlform + "</select></td></tr>";
						
						
						//SQM
						htmlform = htmlform + "<tr>\n<td>SQM:</td>\n";
						htmlform = htmlform + "<td><input type='text' name='tmpsqm' id='tmpsqm' onblur=\"this.value=formatCurrency(this.value,'product2_SQM');\"/><input type='hidden' name='product2[SQM]' id='product2_SQM'/></td>\n";
						htmlform = htmlform + "</tr>\n";
						
						
						//price	
						htmlform = htmlform + "<tr><td><span id=\"saleprice\" stylel=\"display:block;\">Sale Price: </span></td><td><span id=\"saleprice2\" stylel=\"display:block;\">   	<input type=\"text\" name=\"tempprice1\" id=\"tempprice1\"  	onblur=\"this.value=formatCurrency(this.value,'product2_Price');\"/>    	<input type=\"hidden\" name=\"product2[Price]\" id=\"product2_Price\"  />      	</span></td></tr>\n";
						htmlform = htmlform + "<tr><td><span id=\"rentprice\" style=\"display:none;\">Rent Price: </span></td><td><span id=\"rentprice2\" style=\"display:none;\">			<input type=\"text\" name=\"tempprice2\" id=\"tempprice2\"  	onblur=\"this.value=formatCurrency(this.value,'product2_RPrice');\"/>		<input type=\"hidden\" name=\"product2[RPrice]\" id=\"product2_RPrice\"  />		</span></td></tr>\n";
											
		   				//htmlform = htmlform + "<tr><td><span id=\"saleprice\" stylel=\"display:block;\">Sale Price: </span></td><td><span id=\"saleprice2\" stylel=\"display:block;\"><input type=\"text\" name=\"product2[Price]\" id=\"product2_Price\"  onchange=\"return checknumber(\'product2_Price\');\"/></span></td></tr>\n";
						//htmlform = htmlform + "<tr><td><span id=\"rentprice\" style=\"display:none;\">Rent Price: </span></td><td><span id=\"rentprice2\" style=\"display:none;\"><input type=\"text\" name=\"product2[RPrice]\" id=\"product2_RPrice\"  onchange=\"return checknumber(\'product2_RPrice\');\"/></span></td></tr>\n";
						
						//bedroom
						htmlform = htmlform + "<tr><td><div id=\"showbed\">Bedrooms: </div></td><td><div id=\"showbed2\"><select name=\"product2[Bed]\" id=\"product2_Bed\">\n";
						htmlform = htmlform + "<option value=\"0\">Studio</option>\n";
						htmlform = htmlform + "<option value=\"1\">1</option>\n";
						htmlform = htmlform + "<option value=\"2\">2</option>\n";
						htmlform = htmlform + "<option value=\"3\">3</option>\n";
						htmlform = htmlform + "<option value=\"4\">4</option>\n";
						htmlform = htmlform + "<option value=\"5\">5</option>\n";
						htmlform = htmlform + "<option value=\"6\">6+</option>\n";
						htmlform = htmlform + "</select></div></td></tr>";
						
						//bathroom
						htmlform = htmlform + "<tr><td><div id=\"showbath\">Bathroom: </div></td><td><div id=\"showbath2\"><select name=\"product2[Bath]\" id=\"product2_Bath\">\n";
						htmlform = htmlform + "<option value=\"1\">1</option>\n";
						htmlform = htmlform + "<option value=\"2\">2</option>\n";
						htmlform = htmlform + "<option value=\"3\">3</option>\n";
						htmlform = htmlform + "<option value=\"4\">4</option>\n";
						htmlform = htmlform + "<option value=\"5\">5+</option>\n";
						htmlform = htmlform + "</select></div></td></tr>";

						//parking
						htmlform = htmlform + "<tr><td>Parking: </td><td><select name=\"product2[Parking]\" id=\"product2_Parking\">\n";						
						htmlform = htmlform + htmloption2;
						htmlform = htmlform + "</select></td></tr>\n"
						
						//auto-renew this listing every month
						//htmlform = htmlform + "<tr><td></td><td> <input type=\"checkbox\"name=\"product2[renew]\" id=\"product2_renew\" value=\"1\" checked=\"checked\" /><label for=\"product2_renew\" style=padding-left:15px>Auto-renew this listing every month</label><br><br>";						
						//htmlform = htmlform + "</td></tr>\n"						
											
						htmlform = htmlform + "</table>\n";
						
						
						// next button
						htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'block\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\">Next</a>\n";
						htmlform = htmlform + "</div>\n";
						

						htmlform = htmlform + "<div id=\"gaddproductform2\" style=\" display:none;\">\n";
						// Detail

						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /><br /></div>";
		   				htmlform = htmlform + "<table border=0 cellpadding=0 cellspacing=5 style=margin-top:-10px><tr><td valign=top>Property Details: </td><td><textarea name=\"product2[Detail]\" id=\"product2_Detail\" cols=\"30\" rows=\"3\"></textarea></td></tr>\n";						
						
						//picture
						//htmlform = htmlform + "<tr><td><strong>Images</strong> </td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 1: </td><td><input type=\"file\" name=\"image2[pic1]\" id=\"image2_pic1\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 2: </td><td><input type=\"file\" name=\"image2[pic2]\" id=\"image2_pic2\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 3: </td><td><input type=\"file\" name=\"image2[pic3]\" id=\"image2_pic3\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 4: </td><td><input type=\"file\" name=\"image2[pic4]\" id=\"image2_pic4\" cols=\"30\" ></td></tr>\n";
						htmlform = htmlform + "<tr><td>Image 5: </td><td><input type=\"file\" name=\"image2[pic5]\" id=\"image2_pic5\" cols=\"30\" ></td></tr></table>\n";
						
		   				htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:					document.getElementById(\'gaddproductform1\').style.display=\'block\';		document.getElementById(\'gaddproductform2\').style.display=\'none\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\" style=margin-right:15px>Back</a>\n";
						htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'none\';		document.getElementById(\'gaddproductform3\').style.display=\'block\'; \">Next</a>\n";
						htmlform = htmlform + "</div>\n";	
						
						htmlform = htmlform + "<div id=\"gaddproductform3\" style=\" display:none;\">\n";


						// confirm page
						htmlform = htmlform + "<div align=center class=addlisting123><img src=/images/headers/addlisting-step123.png /></div>";

						htmlform = htmlform + "<p></p>\n";
						htmlform = htmlform + "<p></p>\n";
						htmlform = htmlform+ "<div id=\"confirmdetail\"></div>\n";
						htmlform = htmlform + "<!--Edit me at javascripts/indexpro.js --> ";
						
						
						//auto-renew this listing every month
						htmlform = htmlform + "<input type=\"checkbox\"name=\"product2[renew]\" id=\"product2_renew\" value=\"1\"  checked=\"checked\" /><label for=\"product2_renew\" style=padding-left:10px>Auto-renew this listing every month</label><br><br>";						


						
		   				htmlform = htmlform + "<a href=\"#\" onclick=\"javascript:		document.getElementById(\'gaddproductform1\').style.display=\'none\';		document.getElementById(\'gaddproductform2\').style.display=\'block\';		document.getElementById(\'gaddproductform3\').style.display=\'none\';\" style=margin-right:15px>Back</a>\n";
						htmlform = htmlform + "<input type=\"Submit\" value=\"Add My Listing!\" onclick=\"return validateform(\'product2_Price\',\'product2_Bed\',\'product2_Bath\',\'product2_Detail\',\'product2_RPrice\',\'product2_salerent\');\" />	\n";  
						htmlform = htmlform + "</div>\n";										
		  				htmlform = htmlform + "</td></tr></table></form>\n";
							if (latlng) {   
					            var myHtml = "The GLatLng value is: " + latlng.lat() + ","+latlng.lng()+" at zoom level " + map.getZoom();
//					            map.openInfoWindow(latlng, myHtml);
								map.openInfoWindow(latlng, htmlform);
					          }

						});
		
//		map.removeOverlay(marker[0]);
					}
				});
			//}
    }
	
	
	
	
	
//==========================  end load function   =================================================================================================	
		function genpage3(){
//			alert('yes');
			 str="<br>";
			 str = str + "<b>Type:</b>"+document.getElementById('product2_Type').options[document.getElementById('product2_Type').selectedIndex].text +"<br>\n";
						
			if(document.getElementById('product2_salerent').value =='1'){
//				alert("string1:"+str);
				str = str + "<b>Listing:</b> for Sale<br>\n";
				str = str + "<b>Price for Sale:</b>"+document.getElementById('product2_Price').value+"<br>\n";
			}else if (document.getElementById('product2_salerent').value =='0'){
//				alert("string2:"+str);
				str = str + "<b>Listing:</b> for Rent<br>\n";
				str = str + "<b>Price for Rant:</b>"+document.getElementById('product2_RPrice').value+"<br>\n";
			}else{
//				alert("string3:"+str);
				str = str + "<b>Listing:</b> For Sale or Rent<br>\n";
				str = str + "<b>Price for Sale:</b>"+document.getElementById('product2_Price').value+"<br>\n";
				str = str + "<b>Price for Rant:</b>"+document.getElementById('product2_RPrice').value+"<br>\n";
			}
			
			if (document.getElementById('product2_Bed').value == '0') {
//				alert("string4:"+str);
				str = str + "<b>Bedroom:</b> " + document.getElementById('product2_Bed').options[0].text + "<br>\n";
			}
			else if (document.getElementById('product2_Bed').value == '6') {
//				alert("string5:"+str);
				str = str + "<b>Bedroom:</b> More than 6 Bedrooms<br>\n";
			}else {
//				alert("string6:"+str);
				str = str + "<b>Bedroom:</b> " + document.getElementById('product2_Bed').options[document.getElementById('product2_Bed').selectedIndex].text + " Bedrooms<br>\n";
			}
			
			
			 if (document.getElementById('product2_Bath').value == '5') {
//			 	alert("string7:"+str);
				str = str + "<b>Bathroom:</b> More than 5 Bathrooms<br>\n";
			}else {
//				alert("string8:"+str);
				str = str + "<b>Bathroom:</b> " + document.getElementById('product2_Bath').options[document.getElementById('product2_Bath').selectedIndex].text + " Bathrooms<br>\n";
			}
			
			
			str = str + "<b>Parking:</b>"+document.getElementById('product2_Parking').options[document.getElementById('product2_Parking').selectedIndex].text +"<br>\n";
			str = str + "<!--Detail<br><textarea  name=\"textarea\" id=\"textarea\" cols=\"40\" rows=\"5\" readonly=\"readonly\">"+document.getElementById('product2_Detail').value+"</textarea>-->\n";
//			str = str + "";
//			str = str + "";
//			alert("string:"+str);
//			alert(document.getElementById('product2_Parking').selectedIndex );
			document.getElementById('confirmdetail').innerHTML=str;
			
		}


		function newuserform(){
//			alert("i do");
		if(document.getElementById('user2_FirstName').value ==""){alert('Please Enter FirstName'); document.getElementById('user2_FirstName').focus();return false;}
		else if(document.getElementById('user2_LastName').value ==""){alert('Please Enter LastName'); document.getElementById('user2_LastName').focus();return false;}
//		else if(document.getElementById('user2_UserName').value ==""){alert('Please Enter UserName'); document.getElementById('user2_UserName').focus();return false;}
		else if(document.getElementById('user2_Password').value ==""){alert('Please Enter Password'); document.getElementById('user2_Password').focus();return false;}
		else if(document.getElementById('user3_Password3').value ==""){alert('Please Enter Confirm Password'); document.getElementById('user3_Password3').focus();return false;}
		else if(document.getElementById('user2_Phone').value ==""){alert('Please Enter Phone'); document.getElementById('user2_Phone').focus();return false;}
		else if(document.getElementById('user2_Email').value ==""){alert('Please Enter Email'); document.getElementById('user2_Email').focus();return false;}
//		else if(document.getElementById('user2_Address').value ==""){alert('Please Enter Address'); document.getElementById('user2_Address').focus();return false;}
		else if(document.getElementById('user2_Suburb').value ==""){alert('Please Enter Suburb'); document.getElementById('user2_Suburb').focus();return false;}
//		else if(document.getElementById('user2_Province').value ==""){alert('Please Enter Province'); document.getElementById('user2_Province').focus();return false;}
//		else if(document.getElementById('user2_Postcode').value ==""){alert('Please Enter Postcode'); document.getElementById('user2_Postcode').focus();return false;}
		else if(document.getElementById('user2_Password').value != document.getElementById('user3_Password3').value){alert('Please check Confirm Password'); document.getElementById('user2_Password').focus();return false;}
		else if(document.getElementById('checkemail').value=='0'){alert('Please check Your Email'); document.getElementById('user2_Email').focus();return false;}
//		else if(document.getElementById('checkemail').value=='0'){alert('Please check Your Email'); document.getElementById('user2_Email').focus();return false;}
		else{
//			alert('yes');
//			return false;
//			document.getElementById('newuser').action="/users/create";
//			document.getElementById('newuser').submit();
			document.newuser.action="/users/create";
			document.newuser.submit();
//			return true;
			
//			alert("yes");
		}
	}
	
	
		function newacountform(){
		if(document.getElementById('user_FirstName').value ==""){alert('Please Enter FirstName'); document.getElementById('user_FirstName').focus();return false;}
		else if(document.getElementById('user_LastName').value ==""){alert('Please Enter LastName'); document.getElementById('user_LastName').focus();return false;}
//		else if(document.getElementById('user_UserName').value ==""){alert('Please Enter UserName'); document.getElementById('user_UserName').focus();return false;}
		else if(document.getElementById('user_Password').value ==""){alert('Please Enter Password'); document.getElementById('user_Password').focus();return false;}
		else if(document.getElementById('user_Phone').value ==""){alert('Please Enter Phone'); document.getElementById('user_Phone').focus();return false;}
		else if(document.getElementById('user_Email').value ==""){alert('Please Enter Email'); document.getElementById('user_Email').focus();return false;}
		else if(document.getElementById('user_Address').value ==""){alert('Please Enter Address'); document.getElementById('user_Address').focus();return false;}
		else if(document.getElementById('user_Suburb').value ==""){alert('Please Enter Suburb'); document.getElementById('user_Suburb').focus();return false;}
		else if(document.getElementById('user_Province').value ==""){alert('Please Enter Province'); document.getElementById('user_Province').focus();return false;}
		else if(document.getElementById('user_Postcode').value ==""){alert('Please Enter Postcode'); document.getElementById('user_Postcode').focus();return false;}
		else{
			return true;
		}
	}
	
	
//		function editaccount(){
//		if(document.getElementById('user_FirstName').value ==""){alert('Please Enter FirstName'); document.getElementById('user_FirstName').focus();return false;}
//		else if(document.getElementById('user_LastName').value ==""){alert('Please Enter LastName'); document.getElementById('user_LastName').focus();return false;}
////		else if(document.getElementById('user_UserName').value ==""){alert('Please Enter UserName'); document.getElementById('user_UserName').focus();return false;}
//		else if(document.getElementById('user_Phone').value ==""){alert('Please Enter Phone'); document.getElementById('user_Phone').focus();return false;}
//		else if(document.getElementById('user_Email').value ==""){alert('Please Enter Email'); document.getElementById('user_Email').focus();return false;}
//		else if(document.getElementById('user_Suburb').value ==""){alert('Please Enter Suburb'); document.getElementById('user_Suburb').focus();return false;}
//		else{
//			return true;
//		}
//	}
//	
//	function changepasswordfrom(){
//		if(document.getElementById('user5_oldpass').value ==""){alert('Please Enter Old password'); document.getElementById('user5_oldpass').focus();return false;}
//		else if(document.getElementById('user4_Password').value ==""){alert('Please Enter New password'); document.getElementById('user4_Password').focus();return false;}
//		else if(document.getElementById('user5_confirmpass').value ==""){alert('Please Enter Confirm password'); document.getElementById('user5_confirmpass').focus();return false;}
//		else if(document.getElementById('user4_Password').value != document.getElementById('user5_confirmpass').value){alert('Please check Confirmpassword');return false;}
//		else{return true;}
//	}
	
    //<![CDATA[
	
	
	
	
	function validateform(id1,id2,id3,id4,id6,salerent){
//		alert('CHOME 555');
		if (document.getElementById(id2).value==""){alert('Please Check your Bedroom');document.getElementById(id2).focus();return false;}
		else if (document.getElementById(id3).value==""){alert('Please Check your Bathroom');document.getElementById(id3).focus();return false;}
		else if (document.getElementById(id4).value.replace(/ /g,"")==""){alert('Please Check your Description');document.getElementById(id4).focus();return false;}//area field
//		else if (document.getElementById(id5).value==""){alert('Please Check your Detail');document.getElementById(id4).focus();return false;}
		else if (document.getElementById(salerent).value == '1' && document.getElementById(id1).value.replace(/ /g,"")==""){alert('Please Check your Sale Price');document.getElementById(id1).focus();return false;}
		else if (document.getElementById(salerent).value == '0' && document.getElementById(id6).value.replace(/ /g,"")==""){alert('Please Check your Rent Price');document.getElementById(id6).focus();return false;}
		else if (document.getElementById(salerent).value == '2' && (document.getElementById(id6).value.replace(/ /g,"")=="" || document.getElementById(id1).value.replace(/ /g,"")=="" )){alert('Please Check your Price');document.getElementById(id1).focus();return false;}
		else if(document.getElementById('user_FirstName').value.replace(/ /g,"")==""){alert("Please Enter Name"); return false;}
		else if(document.getElementById('user_Email').value.replace(/ /g,"")==""){alert("Please Enter Email"); return false;}
		else if(document.getElementById('checkemail').value=="0" && document.getElementById('user_Password').vale==""){alert("Please Enter Password"); return false;}
		else if(document.getElementById('confirmlocation').value=="0" ){alert("Please Confirm Location on map"); return false;}
		else{
			document.getElementById('addlisting_form').submit();
			return true;
			}
	}
	
	function changeform(id){
		document.getElementById('shadow').style.display='inline';
		document.getElementById('quickeventmodal').style.display='inline';
		
		
		
			document.getElementById('search').style.display='none';
			document.getElementById('sell').style.display='none';
			document.getElementById('account').style.display='none';
			document.getElementById('register').style.display='none';
			document.getElementById('login').style.display='none';
			document.getElementById('preregister').style.display='none';
			document.getElementById('credits').style.display='none';			
			document.getElementById('favorites').style.display='none';			
			document.getElementById('smssettings').style.display='none';			
			document.getElementById('mylistings').style.display='none';		
			document.getElementById('showbusiness').style.display='none';		
			
			document.getElementById(id).style.display='inline';
			
			

		
	}
	function changeform2(id,id_business,id_listing){
		document.getElementById('shadow').style.display='inline';
		document.getElementById('quickeventmodal').style.display='inline';
		
		
			document.getElementById('search').style.display='none';
			document.getElementById('sell').style.display='none';
			document.getElementById('account').style.display='none';
			document.getElementById('register').style.display='none';
			document.getElementById('login').style.display='none';
			document.getElementById('preregister').style.display='none';
			document.getElementById('credits').style.display='none';			
			document.getElementById('favorites').style.display='none';			
			document.getElementById('smssettings').style.display='none';			
			document.getElementById('mylistings').style.display='none';		
			document.getElementById('showbusiness').style.display='none';		
			

			document.getElementById(id).style.display='inline';
			document.getElementById('businessdetail').innerHTML = '<iframe src="/products/businessdetail/'+ id_business +'" width=450 height=250 border=0 frameborder=0 > </iframe>';

			document.getElementById('imailform').innerHTML = '<iframe src="/products/gemail2user?business='+id_business+'" width=450 height=250 border=0 frameborder=0 > </iframe>';
			document.getElementById('linktoourlisting').innerHTML='<a href="#" onclick="ourlisting(&quot;' + id_listing + '&quot;);"><img src="/images/icons/house.png" alt="View all of our listings" style="margin-left:10px;margin-right:5px" />View all of our listings</span>';
			
			

		
	}
	function changeimage(gdiv,img){
		//alert(gdiv + ":" + img);
		str = "<img src='"+img+"' width='400' height='300'/>"
		document.getElementById(gdiv).innerHTML = str;

	}
//	function myalert(id){
////		alert(document.getElementById(id).value);
//		str  = document.getElementById(id).value;
//		a = 0;
//		while (a > 0) {
//			if (str.search('_')) {
//				//		alert(str.substr(0,str.search('_'))+'\n'+str.substr(str.search('_')+1,str.length));
//				lat = str.substr(0, str.search('_'));
//				str = str.substr(str.search('_') + 1, str.length);
//				lng = str.substr(0, str.search(','));
//				str = str.substr(str.search(',') + 1, str.length);
//				
//				
////				var latlng = new GLatLng(lat,lng);
////
////				var marker = new GMarker(latlng, {
////							icon: icons[2]
////						});
////						map.addOverlay(marker);
//
//					
//					
////						map = new GMap2(document.getElementById("map"));
////						map.addControl(new GOverviewMapControl());
////						map.enableDoubleClickZoom();
////						//        		map.setMapType(G_SATELLITE_MAP);
////						map.setMapType(G_HYBRID_MAP);
////						map.addControl(new GMapTypeControl());
////						map.addControl(new GLargeMapControl());
////						
//						geocoder = new GClientGeocoder();
//						
//						
//						map.setCenter(point, 15);
//						var marker = new GMarker(point, {
//							icon: icons[2]
//						});
//						map.addOverlay(marker);
//						GEvent.addListener(marker, 'click',
//									function() {
//									marker.openInfoWindowHtml('yes');
//									});
//					
//				
//			}else{a = 1;}
//		}
//	}
	
	
	function showfavorite(){
		
		//show sidebar
		document.getElementById('sidebar').style.display ='inline';
		document.getElementById('sidebar').innerHTML = "see me at views/products/gfavorite<br><div id=\"myfav\"></div>"; //<img src=\"/action_back.gif\" onclick=\"hidsidebar();\" style=\"display:inline;\"/><br>";
		// shif command
		document.getElementById('command').style.left ='25%';
		
		// new google map side
		document.getElementById('map').style.position='absolute';
		document.getElementById('map').style.left='20%';
		document.getElementById('map').style.top='0%';	
		document.getElementById('map').style.width='80%';	
		document.getElementById('map').style.height='100%';	
		document.getElementById('map').style.index='1';	
		document.getElementById('map').style.backgroundcolor=' #999999';
	}
	
	
	function showsidebar(idproduct){
		//show sidebar
		document.getElementById('sidebar').style.display ='inline';
		document.getElementById('sidebar').innerHTML = "<img src=\"/action_back.gif\" onclick=\"hidsidebar();\" style=\"display:inline;\"/><br><iframe src=/products/gsidebar/"+idproduct+" width='280' height='95%' border=0 frameborder=0></iframe>";
		// shif command
		document.getElementById('command').style.left ='25%';
		
		// new google map side
		document.getElementById('map').style.position='absolute';
		document.getElementById('map').style.left='20%';
		document.getElementById('map').style.top='0%';	
		document.getElementById('map').style.width='80%';	
		document.getElementById('map').style.height='100%';	
		document.getElementById('map').style.index='1';	
		document.getElementById('map').style.backgroundcolor=' #999999';
		
	}
	
	function hidsidebar(){
		//hidden sidebar
		document.getElementById('sidebar').style.display ='none';
		// ship command
		document.getElementById('command').style.left ='80px';
		// new google map side
		document.getElementById('map').style.position='absolute';
		document.getElementById('map').style.left='0%';
		document.getElementById('map').style.top='0%';	
		document.getElementById('map').style.width='100%';	
		document.getElementById('map').style.height='100%';	
		document.getElementById('map').style.index='1';	
		document.getElementById('map').style.backgroundcolor=' #999999';
		
	}	
	
	
	function quickpage(id){
			var tempquick = document.getElementById('tempquickpage').value;
//		alert(tempquick);
			document.getElementById(tempquick).style.display="none";
			document.getElementById(id).style.display="block";					
			document.getElementById('tempquickpage').value = id;
			
		}
		
		
		
		function mylistings(xmlpath){
			// create the map
			map = new GMap2(document.getElementById("map"));
			map.setCenter(new GLatLng(12.93245881808736, 100.8932876586914), 15);
//			map.addControl(new GOverviewMapControl());
			map.enableDoubleClickZoom();
			map.setMapType(G_HYBRID_MAP);
			map.addControl(new GMapTypeControl());
			map.addControl(new GLargeMapControl());
			geocoder = new GClientGeocoder();
			
			
			changeform('mylistings');
			
			
		document.getElementById('mylistings2').style.display ='inline';
		document.getElementById('mylistings2').innerHTML = "<div id=\"showlistings\"></div>";
		
		
		side_bar_html = "";
		mypage = '<a href="#" onclick="quickpage(&quot;page1&quot;);">1</a> ';
		gmarkers = [];
		
					GDownloadUrl(xmlpath, function(doc){
				var xmlDoc = GXml.parse(doc);
				var markers = xmlDoc.documentElement.getElementsByTagName("marker");
				var tab = 0;
				var a=1;
				side_bar_html += '<div id="page1" >';
				for (var i = 0; i < markers.length; i++) {
					if(tab == 3){
						a = a + 1;
						mypage += '<a href="#" onclick="quickpage(&quot;page' + a + '&quot;);">' + a + ' </a> ';
						side_bar_html += '</div><div id="page'+a+'" style="display:none;">';
						tab = 0;
						
					}
					
					// obtain the attribues of each marker
					var lat = parseFloat(markers[i].getAttribute("lat"));
					var lng = parseFloat(markers[i].getAttribute("lng"));
					var point = new GLatLng(lat, lng);
					var html = markers[i].getAttribute("html");
					var label = markers[i].getAttribute("label");
					var html2 = markers[i].getAttribute("html2");
					// create the marker
					var marker = createMarker(point, label, html,i,html2);
					map.addOverlay(marker);
					
					tab = tab + 1;
				}
				// put the assembled side_bar_html contents into the side_bar div
//				if (tab != 0){
//						a = a + 1;
//						mypage += '<a href="#" onclick="quickpage(&quot;page' + a + '&quot;);">' + a + '</a> ';
//				}
				side_bar_html += '</div>\n<input type="hidden" name="tempquickpage" id="tempquickpage" value="page1"/>\n';
				if (a > 1 ){side_bar_html += '<br>page: ' + mypage + '<br>';}
				document.getElementById("showlistings").innerHTML = side_bar_html;
			});
		
		
		// A function to create the marker and set up the event window
		function createMarker(point, name, html,i,html2){
			var marker = new GMarker(point);
			GEvent.addListener(marker, "click", function(){
				marker.openInfoWindowHtml(html);
			});
			gmarkers2[i] = marker;
			htmls[i] = html;
			
			// save the info we need to use later for the side_bar
			gmarkers.push(marker);
			// add a line to the side_bar html
			side_bar_html += '<a href="javascript:myclick2(' + (gmarkers.length - 1) + ')" >' + html2 + '<\/a> '; 

			
			return marker;
		}
		
	
		
	}
	
	
	
	
	
	
	function ourlisting(id_userlisting) {
 if (GBrowserIsCompatible()) {
		var side_bar_html = "";
		var gmarkers = [];
		var htmls = [];
		var i = 0;
		var mylisting = 0;
		
		
		// A function to create the marker and set up the event window
		function createMarker(point,number,id_product){
//			alert(point+"<br>"+name+"<br>"+html);
//			alert('yes1');	
			var marker = new GMarker(point);
			marker.value = number;
			  var detailform = "";
			  detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";			  
			  detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  
			  var myhtml="";
//				alert(id_product);
			 var myAjax = new Ajax.Request( '/controlpanel/getimages2', {  
			 method: 'get',  
			 parameters: 'id='+id_product,   
			 onComplete: function(request){  
			 myhtml = myhtml + request.responseText;  				 
			 } // end of anonymous onComplete function
			 }); // end of the new Ajax.Request() call
			 
			 GEvent.addListener(marker,"click", function() {
		        map.openInfoWindowHtml(point, myhtml+"<br>"+detailform);
		      });
//			  alert('yes2');	
			  
			// === store the name so that the tooltip function can use it ===
			var mylabel="";
			 var myAjax2 = new Ajax.Request( '/products/getlabel', {  
			 method: 'get',  
			 parameters: 'id='+id_product,   
			 onComplete: function(request){  
			 mylabel = mylabel + request.responseText;
			 marker.tooltip = '<div class="tooltip">' +mylabel+ '<\/div>';
			 } // end of anonymous onComplete function
			 }); // end of the new Ajax.Request() call
			 
			 
//			marker.tooltip = '<div class="tooltip">' + name+"<br>" +mylabel+ '<\/div>';

			map.addOverlay(marker);
			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
			GEvent.addListener(marker, "mouseover", function(){
				showTooltip(marker);
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden"
			});
			 
		}
		
		// ====== This function displays the tooltip ======
		// it can be called from an icon mousover or a side_bar mouseover
		function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
		// It launches the tooltip on the icon      
		function mymouseover(i){
			showTooltip(gmarkers[i])
		}
		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
		// It hides the tooltip      
		function mymouseout(){
			tooltip.style.visibility = "hidden";
		}
		
		// This function picks up the side_bar click and opens the corresponding info window
		function myclick(i){
			gmarkers[i].openInfoWindowHtml(htmls[i]);
		}
		
		
		
		// create the map
		
		
		
////		var p_address = new GLatLng(address);
		 map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		
		

		
		
		
		geocoder = new GClientGeocoder();
//		if ((document.getElementById(location).value).search(/thailand|Thailand/)>0){
//			strlocation = document.getElementById(location).value;
//		}else{
//			strlocation = document.getElementById(location).value + ", thailand";
//		}
			//alert("location in search :"+strlocation);
//		geocoder.getLatLng(strlocation, function(point){
//			if (point) {
//				map.setCenter(point, 14);
//			}else{
//				alert('Please check your Location');
				map.setCenter(new GLatLng(12.9357, 100.889), 14);
//			}
//		});
		// ====== set up marker mouseover tooltip div ======
		var tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		//ajax for create marker
		var html="";
		htmloption2 = new Array();
		
//										var myAjax2 = new Ajax.Request( "/products/getmarker?salerent="+document.getElementById('salerent').value+"&propertytype="+document.getElementById('propertytype').value+"&city="+document.getElementById('code').value+"&fprice="+document.getElementById('fprice').value+"&tprice="+document.getElementById('tprice').value+"&bed="+document.getElementById('bed').value+"&bed2="+document.getElementById('bed2').value+"&bath="+document.getElementById('bath').value +"", {  
//										 method: 'get',  
//										 onComplete: function(request){  
//										 mytypes = eval( "(" + request.responseText + ")" );  
//										
//										 for (var y = 0; y < mytypes.length; y++) {
//										 			var lat = mytypes[y].lat;
//												 	var lng = mytypes[y].lng;
//													var id_product = mytypes[y].id;
//													var point = new GLatLng(lat, lng);	
//
//													createMarker(point,y,id_product);
//
//										 }
//
//										 } // end of anonymous onComplete function  
//										 }); // end of the new Ajax.Request() call 										 
										 
									var myAjax = new Ajax.Request( '/controlpanel/ourlisting', {  
									 method: 'get',    
									 parameters: 'id='+id_userlisting,
									 onComplete: function(request){  
									 markers = eval( "(" + request.responseText + ")" );  
									
									 for (var i = 0 ; i < markers.length ; i++) {  
									 var imarker = markers[i];  
									 var lat = imarker.lat;  
									 var lng = imarker.lng;  
									 var id_product = imarker.id; 
									 var latlng = new GLatLng(lat,lng);
										 if ( lat != 0 & lng != 0) { 			 
											createMarker(latlng, i,id_product);
											
										 } // end of if lat and lng  
									 } // end of for loop  
									   
									   
									 } // end of anonymous onComplete function  
									   
									 }); // end of the new Ajax.Request() call 
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
//		  if (GBrowserIsCompatible()) {
//		  	var side_bar_html = "";
//		var gmarkers = [];
//		var htmls = [];
//		var i = 0;
//		var mylisting = 0;
//		
//		
//		    var map = new GMap2(document.getElementById("map"));
//		    map.setCenter(new GLatLng(12.9357, 100.889), 14);
//		  	map.enableDoubleClickZoom();
//			map.setMapType(G_HYBRID_MAP);
//			map.addControl(new GMapTypeControl());
//			map.addControl(new GLargeMapControl()); 
//		    // Creates a marker at the given point
//		    // Clicking the marker will hide it
//		    function createMarker(latlng, number,id_product) {
//		      var marker = new GMarker(latlng);
//		      marker.value = number;
//			  var detailform = "";
//			  detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//			  detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";			  
//			  detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//			  
//			 var myhtml="";
////				alert(id_product);
//			 var myAjax = new Ajax.Request( '/controlpanel/getimages2', {  
//			 method: 'get',  
//			 parameters: 'id='+id_product,   
//			 onComplete: function(request){  
//			 myhtml = myhtml + request.responseText;  	
//			 
//			 } // end of anonymous onComplete function  			   
//			 }); // end of the new Ajax.Request() call 
//			 
////			 myhtml = myhtml + " ";
////			 alert(myhtml);
//		      GEvent.addListener(marker,"click", function() {
//		        map.openInfoWindowHtml(latlng, myhtml+"<br>"+detailform);
//		      });
//			  
//			  
//			  
//			 var mylabel="";
//			 var myAjax2 = new Ajax.Request( '/products/getlabel', {  
//			 method: 'get',  
//			 parameters: 'id='+id_product,   
//			 onComplete: function(request){  
//			 mylabel = mylabel + request.responseText;
//			 marker.tooltip = '<div class="tooltip">' +mylabel+ '<\/div>';
//			 } // end of anonymous onComplete function
//			 }); // end of the new Ajax.Request() call
//			 
//			 
////			marker.tooltip = '<div class="tooltip">' + name+"<br>" +mylabel+ '<\/div>';
//
//			map.addOverlay(marker);
//			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
//			GEvent.addListener(marker, "mouseover", function(){
//				showTooltip(marker);
//			});
//			GEvent.addListener(marker, "mouseout", function(){
//				tooltip.style.visibility = "hidden"
//			});
////			  gmarkers2[number] = marker;
////			  gmarkers.push(marker);
////		      return marker;
//			}
//		function showTooltip(marker){
//			tooltip.innerHTML = marker.tooltip;
//			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
//			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
//			var anchor = marker.getIcon().iconAnchor;
//			var width = marker.getIcon().iconSize.width;
//			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
//			pos.apply(tooltip);
//			tooltip.style.visibility = "visible";
//		}
//		
//		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
//		// It launches the tooltip on the icon      
//		function mymouseover(i){
//			showTooltip(gmarkers[i])
//		}
//		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
//		// It hides the tooltip      
//		function mymouseout(){
//			tooltip.style.visibility = "hidden";
//		}
//		
//
//		
//		    var myAjax = new Ajax.Request( '/controlpanel/ourlisting', {  
//			 method: 'get',    
//			 parameters: 'id='+id_userlisting,
//			 onComplete: function(request){  
//			 markers = eval( "(" + request.responseText + ")" );  
//			
//			 for (var i = 0 ; i < markers.length ; i++) {  
//			 var imarker = markers[i];  
//			 var lat = imarker.lat;  
//			 var lng = imarker.lng;  
//			 var id_product = imarker.id; 
//			 var latlng = new GLatLng(lat,lng);
//				 if ( lat != 0 & lng != 0) { 			 
//					createMarker(latlng, i,id_product);
//					
//				 } // end of if lat and lng  
//			 } // end of for loop  
//			   
//			   
//			 } // end of anonymous onComplete function  
//			   
//			 }); // end of the new Ajax.Request() call 
//		  }
		 
		 
		 }
		 	var gmarkers = [];	
		  	var gmarkers2 = [];	
    function gfav2(id_user){
			 if (GBrowserIsCompatible()) {
			 	
				var mypage = "";
				var side_bar_html = "";
				
				var htmls = [];
				var i = 0;
				var mylisting = 0;
		
		
		// A function to create the marker and set up the event window
		function createMarker(point,number,id_product,elementpage,id_fav){
//			alert(point+"<br>"+name+"<br>"+html);
//			alert('yes1');	
			var marker = new GMarker(point);
			marker.value = number;
			  var detailform = "";
//			  detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//			  detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";			  
//			  detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  
			  detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";			  
			  detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
			  
			  var myhtml="";
//				alert(id_product);
			 var myAjax = new Ajax.Request( '/controlpanel/getimages2', {  
			 method: 'get',  
			 parameters: 'id='+id_product,   
			 onComplete: function(request){  
			 myhtml = myhtml + request.responseText;  				 
			 } // end of anonymous onComplete function
			 }); // end of the new Ajax.Request() call
			 
			 GEvent.addListener(marker,"click", function() {
		        map.openInfoWindowHtml(point, myhtml+"<br>"+detailform);
		      });
//			  alert('yes2');	
			  
			// === store the name so that the tooltip function can use it ===
			var mylabel="";
			 var myAjax2 = new Ajax.Request( '/products/getfavlabel', {  
			 method: 'get',  
			 parameters: 'id='+id_product+'&id_fav='+id_fav,   
			 onComplete: function(request){  
			 mylabel = mylabel + request.responseText;
			 marker.tooltip = '<div class="tooltip">' +mylabel+ '<\/div>';
			 //delete favorites =><div style='float: right;' class='smallfont'><a href='/controlpanel/deletefavorites/"+id_fav+"' onclick='if (confirm('Are you sure?')) { var f = document.createElement('form'); f.style.display = 'none'; this.parentNode.appendChild(f); f.method = 'POST'; f.action = this.href;var m = document.createElement('input'); m.setAttribute('type', 'hidden'); m.setAttribute('name', '_method'); m.setAttribute('value', 'delete'); f.appendChild(m);f.submit(); };return false;'>Delete</a></div>	
			document.getElementById(elementpage).innerHTML = document.getElementById(elementpage).innerHTML +"<div id='mclistingblockwide'>"+ mylabel.replace(/<img/, "<img onclick='myclick("+number+");' ")+"</div>";
			htmls[number] = html;
			 } // end of anonymous onComplete function
			 }); // end of the new Ajax.Request() call
			 
			 
//			marker.tooltip = '<div class="tooltip">' + name+"<br>" +mylabel+ '<\/div>';
			gmarkers2[number] = marker;
			
			map.addOverlay(marker);
			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
			GEvent.addListener(marker, "mouseover", function(){
				showTooltip(marker);
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden"
			});
			gmarkers2[number] = marker;
			  gmarkers.push(marker);
			 
		}
		
		// ====== This function displays the tooltip ======
		// it can be called from an icon mousover or a side_bar mouseover
		function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
		// It launches the tooltip on the icon      
		function mymouseover(i){
			showTooltip(gmarkers[i])
		}
		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
		// It hides the tooltip      
		function mymouseout(){
			tooltip.style.visibility = "hidden";
		}
		
		// This function picks up the side_bar click and opens the corresponding info window
//		function myclick(i){
//			gmarkers[i].openInfoWindowHtml(htmls[i]);
//		}
//		function myclick2(i){
//			GEvent.trigger(gmarkers[i], "click");
//		}
		function myclick(i){
			GEvent.trigger(gmarkers2[i], "click");
		}
		
		
		// create the map
		
		
		
////		var p_address = new GLatLng(address);
		 map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		
		

		
		
		
		geocoder = new GClientGeocoder();
		changeform('favorites');	
		document.getElementById('quickfavorites').style.display ='inline';
		document.getElementById('quickfavorites').innerHTML = "<div id=\"myfav\"></div>";
		
		
		map.setCenter(new GLatLng(12.9357, 100.889), 14);
		// ====== set up marker mouseover tooltip div ======
		var tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		//ajax for create marker
			mypage = '<a href="#" onclick="quickpage(&quot;page' + 1 + '&quot;);">' + 1 + ' </a>';
				var tab = 0;
				var a=1;
				side_bar_html += '<div id="page1" ></div>';								 
										 
									var myAjax = new Ajax.Request( '/products/myfavorites', {  
									 method: 'get',    
									 parameters: 'id_user='+id_user,
									 onComplete: function(request){  
									 markers = eval( "(" + request.responseText + ")" );  
									
									 for (var i = 0 ; i < markers.length ; i++) {  
									 if(tab == 5){
											a = a + 1;
											mypage += '<a href="#" onclick="quickpage(&quot;page' + a + '&quot;);">' + a + ' </a>';
											if (a > 1){
												side_bar_html += '<div id="page'+a+'" style="display:none;"></div>';
											}										
											
											tab = 0;
											
										}
					
									 var imarker = markers[i];  
									 var lat = imarker.lat;  
									 var lng = imarker.lng;  
									 var id_fav = imarker.id;
									 var id_product = imarker.id_product; 
									 var latlng = new GLatLng(lat,lng);
									 if (i == 0){map.setCenter(new GLatLng(lat, lng), 14);}
										 if ( lat != 0 & lng != 0) { 			 
											createMarker(latlng, i,id_product,'page'+a,id_fav);
											
										 } // end of if lat and lng  
										 tab = tab + 1;
									 } // end of for loop  
									   
									   side_bar_html += '\n<input type="hidden" name="tempquickpage" id="tempquickpage" value="page1"/>\n';
										if (a > 1 ){side_bar_html += '<br>Page: ' + mypage + '<br>';}
										document.getElementById("myfav").innerHTML = side_bar_html;
									 } // end of anonymous onComplete function  
									   
									 }); // end of the new Ajax.Request() call 
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
			
		}
 
 	 function myclick(i){
			GEvent.trigger(gmarkers2[i], "click");
		}	
		 
//		 function mapregister(city){
//		 
//		var map = new GMap2(document.getElementById("mapregister")); 	
//		map.enableDoubleClickZoom();
//		map.setMapType(G_HYBRID_MAP);
//		map.addControl(new GMapTypeControl());
//		map.addControl(new GLargeMapControl());		
//		geocoder = new GClientGeocoder();
//		
//		address = document.getElementById(city).value + ", thailand";
////			if (geocoder) {
//				geocoder.getLatLng(address, function(point){
//					if (!point) {
//						alert(address + " not found\nPlease try again.");
//						document.getElementById('msgreguser').innerHTML="<span style=\"color:red;\">not found</span>";
//						document.getElementById('user2_lat').value="12.93245881808736";
//						document.getElementById('user2_lng').value="100.8932876586914";
//						// for show only 555+
//						document.getElementById('tempuserlat').value = document.getElementById('user2_lat').value;
//						document.getElementById('tempuserlng').value = document.getElementById('user2_lng').value;
//									
//					}
//					else {
//						alert(address + "\nlat: "+point.lat() +"\nlng: "+ point.lng());
//						document.getElementById('msgreguser').innerHTML="";
//						document.getElementById('user2_lat').value= point.lat();
//						document.getElementById('user2_lng').value= point.lng();
//						// for show only 555+
//						document.getElementById('tempuserlat').value = document.getElementById('user2_lat').value;
//						document.getElementById('tempuserlng').value = document.getElementById('user2_lng').value;
//					}
//				});
//				
////			}
//		 	
//		 }
		 
//		function checkfirstpopup(id_product,lat,lng){
//			var myAjax = new Ajax.Request('/controlpanel/getimages2', {
//						method: 'get',
//						parameters: 'id=' + id_product,
//						onComplete: function(request){
//							myhtml = myhtml + request.responseText;
//							var detailform = "";
//							detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//							detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
//							detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//							detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
////							marker.htmlshow = myhtml + "<br>" + detailform;
//						 setTimeout('map.openInfoWindowHtml(marker.point, myhtml + "<br>" + detailform);',1000);	
//						} // end of anonymous onComplete function
//					}); // end of the new Ajax.Request() call
//		}