     


var map, cluster, eventListeners=[], markersArray=[], icon;
var tooltip;
function myOnload() {
	if (GBrowserIsCompatible()) {
		map=new GMap2(document.getElementById('map'));
		map.setCenter(new GLatLng(0, 0), 0, G_NORMAL_MAP);
		
	 /*===============  Set Zoom option  =======================*/	
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 9;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  /*===============  end set Zoom option  =======================*/
	  
	  
		map.addControl(new GMapTypeControl());//, new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 7)));
		map.addControl(new GLargeMapControl());//, new GControlPosition(G_ANCHOR_TOP_RIGHT, new GSize(7, 28)));		
		map.setMapType(G_HYBRID_MAP);
		geocoder = new GClientGeocoder();
		map.setCenter(new GLatLng(12.9357, 100.889),13);		
		
//	      // Create an EWindow
//	      ew = new EWindow(map, E_STYLE_1);      
//	      map.addOverlay(ew);
//		
//		// ========== Close the EWindow if theres a map click ==========
//		      GEvent.addListener(map, "click", function(overlay,point) {
//		        if (!overlay) {
//					alert('hidden Element::. overlay is :'+overlay);
//		          	ew.hide();
//		        }
//		      });
		GEvent.addListener(map, 'zoomend', function() { map.closeInfoWindow(); });
		
		
		// ====== set up marker mouseover tooltip div ======
		tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		
		//	add an HtmlControl containing a couple of form elements
		//	see http://googlemapsapi.martinpearman.co.uk/htmlcontrol for more info on HtmlControl
//		var html='<div class="htmlControl" style="padding:0px 3px 3px 3px">Enable clustering: <input type="checkbox" checked="checked" onclick="toggleClustering()" /><form id="examplesForm" action=""><p>Choose markers:<br /><select name="examplesSelect" onchange="selectExample()"><option value="marker_data_02.php">Tine\'s markers</option><option value="random">Random 850</option></select></p></form></div>';
//		var control=new HtmlControl(html);
//		map.addControl(control, new GControlPosition(G_ANCHOR_TOP_LEFT, new GSize(7,7)));
			
		function myClusterClick(args) {
			cluster.defaultClickAction=function(){
				map.setCenter(args.clusterMarker.getLatLng(), map.getBoundsZoomLevel(args.clusterMarker.clusterGroupBounds))
				delete cluster.defaultClickAction;
			}
			var html='<div style="height:16em; overflow:auto; width:20em" id="clusterlistings"><h4>'+args.clusteredMarkers.length+' Listings:</h4>';//<div id="showclusters"><img src="/images/loadingb.gif"/></div></div>';
			var id_product="";
			var indexlist = "";
			for (i=0; i<args.clusteredMarkers.length; i++) {
				id_product = id_product + "" + args.clusteredMarkers[i].id_product + ":";
				indexlist = indexlist + args.clusteredMarkers[i].index + ":";
//				html+='<a href="javascript:cluster.triggerClick('+args.clusteredMarkers[i].index+')">'+args.clusteredMarkers[i].id_product+'</a><br />';
			}
			map.openInfoWindowHtml(args.clusterMarker.getLatLng(), html);
			var myAjax2 = new Ajax.Request( "/listings/getclusterdetails?id_products="+id_product+"&index="+indexlist,{
			method: 'get',
			onComplete: function(request){									
					var text =  request.responseText;
//					document.getElementById('showclusters').innerHTML=text;
					html+='<br>'+text+'<br /><a href="javascript:void(0)" onclick="cluster.defaultClickAction()">Zoom</a> in to view these listings</div>';
					map.openInfoWindowHtml(args.clusterMarker.getLatLng(), html);
				} // end of anonymous onComplete function			
			}); // end of the new Ajax.Request() call
			

		}
		
		//	create a ClusterMarker
		cluster=new ClusterMarker(map, {clusterMarkerTitle:'Click to see info about %count locations' , clusterMarkerClick:myClusterClick });
		
		icon=new GIcon();
		icon.shadow='http://googlemapsapi.martinpearman.co.uk/maps/clustermarker/images/icon_shadow.png';
		icon.shadowSize=new GSize(37, 34);
		icon.iconSize=new GSize(20, 34);
		icon.iconAnchor=new GPoint(10, 30);
		icon.infoWindowAnchor=new GPoint(10, 8);
		
		document.getElementById('latlng').value = map.getCenter();
		
		setfirstmarkers();
		var ftime;
		var old_latlng;
		GEvent.addListener(map,'moveend',function() {
				tooltip.style.visibility = "hidden";
				ftime = document.getElementById('ftime').value;
				old_latlng = document.getElementById('latlng').value;

				if(ftime == '1' && old_latlng != map.getCenter() ){
					
//					alert('Center '+ map.getCenter() + "Old_Ct " +old_latlng);

		document.getElementById('ftime').value = '0';
		markersArray=[];
//		for(i=eventListeners.length-1; i>=0; i--){
//			GEvent.removeListener(eventListeners[i]);
//		}
		eventListeners=[];
		var json=[], lat, lng;
			var bounds = map.getBounds();
		 	var southWest = bounds.getSouthWest();
		 	var northEast = bounds.getNorthEast();
		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
		var myAjax = new Ajax.Request( "/products/getmarker?clearlisting=0&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value,{//products/getmarker?clearlisting=1
					method: 'get',
					parameters: mapBoundary,
					onComplete: function(request){									
					markers = eval( "(" + request.responseText + ")" );
					if (markers.length > 0) {
						json = markers;
						//				alert("json: " + json.length);
						var marker, newIcon, j = 1, title, lat, lng;
						for (var i = 0; i < json.length; i++) {
						if(json[i].MapFeatured =='0'){
							newIcon=new GIcon(icon, '/icon/markerred.png');
						}else{newIcon=new GIcon(icon, '/icon/markerselected.png');}
//							newIcon = new GIcon(icon, '/icon/markerred.png');
							
							lat = Math.round(json[i].lat * 100) / 100;
							lng = Math.round(json[i].lng * 100) / 100;
							title = 'id: ' + json[i].id + ',(' + lat + ', ' + lng + ')';
							marker = newMarker(new GLatLng(json[i].lat, json[i].lng), title, newIcon,json[i].id);
							markersArray.push(marker);
							j++;
							if (j > 26) {
								j = 1;
							}
						}
						
						cluster.addMarkers(markersArray);
						cluster.fitMapToMarkers();
//						cluster.removeMarkers();
						map.savePosition();
						json = [];
					}						
						
						
						document.getElementById('ftime').value = '1';
						document.getElementById('latlng').value = map.getCenter();
						document.getElementById('markkersArray').value = markersArray.length;
						
		} // end of anonymous onComplete function
		
		}); // end of the new Ajax.Request() call


				}  // end if ftfime
			});  // end Gevent
	}
}
function pretty(a) {
        return '<table border="0" cellpadding="0" cellspacing="0"><tr><td width="100%" class="EWTitle" nowrap>' +
               '<a href="javascript:ew.hide()"><img width="14" height="13" title="Close the EWindow" src="/icon/icon_close.gif" border=0 style="position:absolute;right:4px;top:4px"><\/a>' +
               '<\/td><\/tr>' +
               '<tr><td nowrap>' + a + '<\/td><\/tr><\/table>';
      }

function newMarker(markerLocation, title, markerIcon,id_product) {
//	var marker=new GMarker(markerLocation, {title:title, icon:markerIcon});
	var marker=new GMarker(markerLocation, {icon:markerIcon});	
	marker.id_product = id_product;
//	eventListeners.push(GEvent.addListener(marker, 'click', function() {
//		marker.openInfoWindowHtml('<p>'+title+'</p>');
//	}));
			//=====================   Add Marker Event  ===================================
			GEvent.addListener(marker, "mouseover", function(){	
					if (!marker.tooltip) {
						var mylabel = "loadding...";
						var myAjax2 = new Ajax.Request('/products/getlabel', {
							method: 'get',
							parameters: 'id=' + marker.id_product,
							onComplete: function(request){
							    mylabel = request.responseText;
								marker.tooltip = '<div class="tooltip">' + mylabel + '<\/div>';
								showTooltip(marker);
								
							} // end of anonymous onComplete function
						}); // end of the new Ajax.Request() call			 
					}else{showTooltip(marker);}
				
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden"
			});
			 
			 //---------------------------------------------------------------------------------------------
			 GEvent.addListener(marker,"click", function() {
//			 	alert('clicked even');
			 				  var myhtml="";
//				alert(id_product);
				if (!marker.htmlshow) {
					var myAjax = new Ajax.Request('/controlpanel/getimages2', {
						method: 'get',
						parameters: 'id=' + marker.id_product,
						onComplete: function(request){
							myhtml = myhtml + request.responseText;
							var detailform = "";
//							detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + id_product + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//							detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
//							detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//							detailform = detailform + "<div id='directlink' style='display:none;' >   <iframe src=/products/directlink/" + id_product + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
							marker.htmlshow = myhtml + "<br>" + detailform;
							marker.openInfoWindowHtml(marker.htmlshow);
//							map.openInfoWindowHtml( marker.getLatLng(), marker.htmlshow, {pixelOffset: new GSize(150,330)} ); 

//							var ew = new EWindow(map, E_STYLE_1);      
//					        map.addOverlay(ew);
//					        ew.openOnMarker(marker,pretty(marker.htmlshow));
//							ew.zindex(50);
						} // end of anonymous onComplete function
					}); // end of the new Ajax.Request() call
				}else{
					marker.openInfoWindowHtml(marker.htmlshow);
//					map.openInfoWindowHtml( marker.getLatLng(), marker.htmlshow, {pixelOffset: new GSize(150,330)} ); 
//							var ew = new EWindow(map, E_STYLE_1);      
//					        map.addOverlay(ew);
//					        ew.openOnMarker(marker,pretty(marker.htmlshow));
//							ew.zindex(50);
				}
				
		      });	
			  		 
			  

			 
		  //=====================   Add Marker Event  ===================================	
	return marker;
}

function toggleClustering() {
	cluster.clusteringEnabled=!cluster.clusteringEnabled;
	cluster.refresh(true);
}



function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		// ===== This function is invoked when the mouse goes over an entry in the side_bar =====
		// It launches the tooltip on the icon      
		function mymouseover(i){
			showTooltip(gmarkers[i]);
		}
		// ===== This function is invoked when the mouse leaves an entry in the side_bar =====
		// It hides the tooltip      
		function mymouseout(){
			tooltip.style.visibility = "hidden";
		}
		
	function setadvertise(){
		 var myAjax = new Ajax.Request( "/listings/getmapadvertising" ,{
			method: 'get',
			onComplete: function(request){		
			document.getElementById('mapshowadvertise').innerHTML=request.responseText;
			
			} // end of anonymous onComplete function			
			}); // end of the new Ajax.Request() call
	}	
		
function setfirstmarkers(){
	document.getElementById('ftime').value = '0';
//	map.openInfoWindowHtml(new GLatLng(document.getElementById('mylat').value, document.getElementById('mylng').value),"test");
		if (document.getElementById('firstpopup').value != "") {
			map.setCenter(new GLatLng(document.getElementById('mylat').value, document.getElementById('mylng').value), 13);	
		markersArray=[];
//		alert(markersArray.length);
		for(i=eventListeners.length-1; i>=0; i--){
			GEvent.removeListener(eventListeners[i]);
		}
		eventListeners=[];
		var json=[], lat, lng;
			var bounds = map.getBounds();
		 	var southWest = bounds.getSouthWest();
		 	var northEast = bounds.getNorthEast();
		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
		 var myAjax = new Ajax.Request( "/products/getmarker?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value ,{
			method: 'get',
			parameters: mapBoundary,
			onComplete: function(request){		
			if(request.responseText !='[]' ){		
					markers = eval( "(" + request.responseText+ ")" );
					json=markers;
					var marker, newIcon, j=1, title, lat, lng;
					for (var i=0; i<json.length; i++) {
						if(json[i].MapFeatured =='0'){
							newIcon=new GIcon(icon, '/icon/markerred.png');
						}else{newIcon=new GIcon(icon, '/icon/markerselected.png');}
						
						
						lat=Math.round(json[i].lat*100)/100;
						lng=Math.round(json[i].lng*100)/100;
						title='id: '+json[i].id+', ('+lat+', '+lng+')';
						marker=newMarker(new GLatLng(json[i].lat, json[i].lng), title, newIcon,json[i].id);			 
						markersArray.push(marker);	
						if(document.getElementById('firstpopup').value !="" && json[i].id == document.getElementById('firstpopup').value){
							var myAjax = new Ajax.Request('/controlpanel/getimages2', {
								method: 'get',
								parameters: 'id=' + document.getElementById('firstpopup').value,
								onComplete: function(request){
									var myhtml = request.responseText;
									var detailform = "";
//									detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + document.getElementById('firstpopup').value + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//									detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
//									detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//									detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
									var texthtml = myhtml + "<br>" + detailform;
									map.openInfoWindowHtml(new GLatLng(document.getElementById('mylat').value, document.getElementById('mylng').value),texthtml);
									document.getElementById('mylat').value = '0';
									document.getElementById('mylat').value = '0';
									document.getElementById('firstpopup').value = '';
								} // end of anonymous onComplete function
							}); // end of the new Ajax.Request() call
						}					
					}
					cluster.removeMarkers();
					cluster.addMarkers(markersArray);
					cluster.fitMapToMarkers();
					map.savePosition();
					json=[];
					document.getElementById('ftime').value = '1';
					document.getElementById('markkersArray').value = markersArray.length;
					setadvertise();
	}else{
		cluster.removeMarkers();
		document.getElementById('ftime').value = '1';
		document.getElementById('markkersArray').value = '0';
	}	

} // end of anonymous onComplete function

}); // end of the new Ajax.Request() call
		}
		else {
			if ((document.getElementById('code').value).search(/thailand|Thailand/) > 0) {
				strlocation = document.getElementById('code').value;
			}
			else {
				strlocation = document.getElementById('code').value + ", thailand";
			}
			if (document.getElementById('code').value != "") {
			
				var getlatlng = new Ajax.Request("/listings/getlatlng", {
					method: 'get',
					parameters: "province=" + document.getElementById('code').value,
					onComplete: function(request){
						mylatlng = eval("(" + request.responseText + ")");
						var lat = mylatlng.lat;
						var lng = mylatlng.lng;
						var strlocation = "" + lat + "," + lng;
						
						
//								alert("latlng form server");
								map.setCenter(new GLatLng(lat, lng), 12);
						
						
//						
//					} // end of anonymous onComplete function  
//				}); // end of the new Ajax.Request() call 
//			}
			
			
			geocoder.getLatLng(strlocation, function(point){
//				if (point) {
//					map.setCenter(point, 13);
////					alert('yess have new point');
//				}
//				else {
//					//				alert('Please check your Location');
//					alert('noooooo');
//					map.setCenter(new GLatLng(12.9357, 100.889), 13);
//				}
		markersArray=[];
//		alert(markersArray.length);
		for(i=eventListeners.length-1; i>=0; i--){
			GEvent.removeListener(eventListeners[i]);
		}
		eventListeners=[];
		var json=[], lat, lng;
			var bounds = map.getBounds();
		 	var southWest = bounds.getSouthWest();
		 	var northEast = bounds.getNorthEast();
		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
		 var myAjax = new Ajax.Request( "/products/getmarker?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value ,{
			method: 'get',
			parameters: mapBoundary,
			onComplete: function(request){		
			if(request.responseText !='[]' ){		
					markers = eval( "(" + request.responseText+ ")" );
					json=markers;
					var marker, newIcon, j=1, title, lat, lng;
					for (var i=0; i<json.length; i++) {
						if(json[i].MapFeatured =='0'){
							newIcon=new GIcon(icon, '/icon/markerred.png');
						}else{newIcon=new GIcon(icon, '/icon/markerselected.png');}
//						newIcon=new GIcon(icon, '/icon/markerred.png');
						
						lat=Math.round(json[i].lat*100)/100;
						lng=Math.round(json[i].lng*100)/100;
						title='id: '+json[i].id+', ('+lat+', '+lng+')';
						marker=newMarker(new GLatLng(json[i].lat, json[i].lng), title, newIcon,json[i].id);			 
						markersArray.push(marker);	
						if(document.getElementById('firstpopup').value !="" && json[i].id == document.getElementById('firstpopup').value){
							var myAjax = new Ajax.Request('/controlpanel/getimages2', {
								method: 'get',
								parameters: 'id=' + document.getElementById('firstpopup').value,
								onComplete: function(request){
									var myhtml = request.responseText;
									var detailform = "";
									detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + document.getElementById('firstpopup').value + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
									detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
									detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
									detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
									var texthtml = myhtml + "<br>" + detailform;
									map.openInfoWindowHtml(new GLatLng(document.getElementById('mylat').value, document.getElementById('mylng').value),texthtml);
									document.getElementById('mylat').value = '0';
									document.getElementById('mylat').value = '0';
									document.getElementById('firstpopup').value = '';
								} // end of anonymous onComplete function
							}); // end of the new Ajax.Request() call
						}					
					}
					cluster.removeMarkers();
					cluster.addMarkers(markersArray);
					cluster.fitMapToMarkers();
					map.savePosition();
					json=[];
					document.getElementById('ftime').value = '1';
					document.getElementById('markkersArray').value = markersArray.length;
					setadvertise();
	}else{
		cluster.removeMarkers();
		document.getElementById('ftime').value = '1';
		document.getElementById('markkersArray').value = '0';
	}	

} // end of anonymous onComplete function

}); // end of the new Ajax.Request() call
			});
			
			
				
					} // end of anonymous onComplete function  
				}); // end of the new Ajax.Request() call 
			}
			
		}
//		
//		markersArray=[];
////		alert(markersArray.length);
//		for(i=eventListeners.length-1; i>=0; i--){
//			GEvent.removeListener(eventListeners[i]);
//		}
//		eventListeners=[];
//		var json=[], lat, lng;
//			var bounds = map.getBounds();
//		 	var southWest = bounds.getSouthWest();
//		 	var northEast = bounds.getNorthEast();
//		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
//		 var myAjax = new Ajax.Request( "/products/getmarker?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value ,{
//			method: 'get',
//			parameters: mapBoundary,
//			onComplete: function(request){		
//			if(request.responseText !='[]' ){		
//					markers = eval( "(" + request.responseText+ ")" );
//					json=markers;
//					var marker, newIcon, j=1, title, lat, lng;
//					for (var i=0; i<json.length; i++) {
//						newIcon=new GIcon(icon, '/icon/markerred.png');
//						
//						lat=Math.round(json[i].lat*100)/100;
//						lng=Math.round(json[i].lng*100)/100;
//						title='id: '+json[i].id+', ('+lat+', '+lng+')';
//						marker=newMarker(new GLatLng(json[i].lat, json[i].lng), title, newIcon,json[i].id);			 
//						markersArray.push(marker);	
//						if(document.getElementById('firstpopup').value !="" && json[i].id == document.getElementById('firstpopup').value){
//							var myAjax = new Ajax.Request('/controlpanel/getimages2', {
//								method: 'get',
//								parameters: 'id=' + document.getElementById('firstpopup').value,
//								onComplete: function(request){
//									var myhtml = request.responseText;
//									var detailform = "";
//									detailform = detailform + "<div id='detail' style='display:inline;' >   <iframe src=/products/gdetail/" + document.getElementById('firstpopup').value + " 		width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//									detailform = detailform + "<div id='mailform' style='display:none;' >   <iframe src=/products/gemail2user/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>	</div>";
//									detailform = detailform + "<div id='detail2' 	style='display:none;' >   <iframe src=/products/gfavorite/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//									detailform = detailform + "<div id='directlink' 	style='display:none;' >   <iframe src=/products/directlink/" + document.getElementById('firstpopup').value + " 	width=400 height=250 border=0 frameborder=0></iframe>   </div>";
//									var texthtml = myhtml + "<br>" + detailform;
//									map.openInfoWindowHtml(new GLatLng(document.getElementById('mylat').value, document.getElementById('mylng').value),texthtml);
//									document.getElementById('mylat').value = '0';
//									document.getElementById('mylat').value = '0';
//									document.getElementById('firstpopup').value = '';
//								} // end of anonymous onComplete function
//							}); // end of the new Ajax.Request() call
//						}					
//					}
//					cluster.removeMarkers();
//					cluster.addMarkers(markersArray);
//					cluster.fitMapToMarkers();
//					map.savePosition();
//					json=[];
//					document.getElementById('ftime').value = '1';
//					document.getElementById('markkersArray').value = markersArray.length;
//	}else{
//		cluster.removeMarkers();
//		document.getElementById('ftime').value = '1';
//		document.getElementById('markkersArray').value = '0';
//	}	
//
//} // end of anonymous onComplete function
//
//}); // end of the new Ajax.Request() call



}

function ourlisting(id_userlisting){
	document.getElementById('ftime').value = '0';
		if ((document.getElementById('code').value).search(/thailand|Thailand/)>0){
			strlocation = document.getElementById('code').value;
		}else{
			strlocation = document.getElementById('code').value + ", thailand";
		}
		 	geocoder.getLatLng(strlocation, function(point){
		 		if (point) {
		 			map.setCenter(point, 13);
		 		}
		 		else {
		 			//				alert('Please check your Location');
						map.setCenter(new GLatLng(12.9357, 100.889), 13);
					}
				});
		
		
		markersArray=[];
//		alert(markersArray.length);
		for(i=eventListeners.length-1; i>=0; i--){
			GEvent.removeListener(eventListeners[i]);
		}
		eventListeners=[];
		var json=[], lat, lng;
//			var bounds = map.getBounds();
//		 	var southWest = bounds.getSouthWest();
//		 	var northEast = bounds.getNorthEast();
//		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
//		 var myAjax = new Ajax.Request( "/products/getmarker?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value ,{
		 	var myAjax = new Ajax.Request( '/controlpanel/ourlisting', {  
									 method: 'get',    
									 parameters: 'id='+id_userlisting,
//			method: 'get',
//			parameters: mapBoundary,
			onComplete: function(request){		
			if(request.responseText !='[]' ){		
					markers = eval( "(" + request.responseText+ ")" );
					json=markers;
					var marker, newIcon, j=1, title, lat, lng;
					for (var i=0; i<json.length; i++) {
						newIcon=new GIcon(icon, '/icon/markerred.png');
						
						lat=Math.round(json[i].lat*100)/100;
						lng=Math.round(json[i].lng*100)/100;
						title='id: '+json[i].id+', ('+lat+', '+lng+')';
						marker=newMarker(new GLatLng(json[i].lat, json[i].lng), title, newIcon,json[i].id);			 
						markersArray.push(marker);						
					}
					cluster.removeMarkers();
					cluster.addMarkers(markersArray);
					cluster.fitMapToMarkers();
					map.savePosition();
					json=[];
					document.getElementById('ftime').value = '1';
					document.getElementById('markkersArray').value = markersArray.length;
	}else{
		cluster.removeMarkers();
		document.getElementById('ftime').value = '1';
		document.getElementById('markkersArray').value = '0';
	}	

} // end of anonymous onComplete function

}); // end of the new Ajax.Request() call



}

	function getScreenCenterY() {
	var y = 0;
	 
	y = getScrollOffset()+(getInnerHeight()/2);
	 
	return(y);
	}
	 
	function getScreenCenterX() {
	return(document.body.clientWidth/2);
	}
	function getInnerHeight() {
		var y;
		if (self.innerHeight) // all except Explorer
		{
		y = self.innerHeight;
		}
		else if (document.documentElement && document.documentElement.clientHeight)
		// Explorer 6 Strict Mode
		{
		y = document.documentElement.clientHeight;
		}
		else if (document.body) // other Explorers
		{
		y = document.body.clientHeight;
		}
		return(y);
	}

	function getScrollOffset() {
		var y;
		if (self.pageYOffset) // all except Explorer
		{
		y = self.pageYOffset;
		}
		else if (document.documentElement && document.documentElement.scrollTop)
		// Explorer 6 Strict
		{
		y = document.documentElement.scrollTop;
		}
		else if (document.body) // all other Explorers
		{
		y = document.body.scrollTop;
		}
		return(y);
	}

function changeform2(id,id_business,id_listing){
			posY = getScreenCenterY() - 150;
			posX = getScreenCenterX() - 150;
//			alert("X:"+posX+"\nY:"+posY);
			document.getElementById(id).style.display='inline';
			document.getElementById(id).style.left=posX;
			document.getElementById(id).style.top =posY;
			document.getElementById('businessdetail').innerHTML = '<iframe src="/products/businessdetail/'+ id_business +'" width=450 height=250 border=0 frameborder=0 > </iframe>';

			document.getElementById('imailform').innerHTML = '<iframe src="/products/gemail2user?business='+id_business+'" width=450 height=250 border=0 frameborder=0 > </iframe>';
			document.getElementById('linktoourlisting').innerHTML='<a href="#" onclick="ourlisting(&quot;' + id_listing + '&quot;);"><img src="/images/icons/house.png" alt="View all of our listings" style="margin-left:10px;margin-right:5px" />View all of our listings</span>';
			
			

		
	}
	
function contactuserform(id_listing){
			posY = getScreenCenterY() - 150;
			posX = getScreenCenterX() - 150;
//			alert("X:"+posX+"\nY:"+posY);
			document.getElementById('contactuserform').style.display='inline';
			document.getElementById('contactuserform').style.left=posX;
			document.getElementById('contactuserform').style.top =posY;
			document.getElementById('contactspace').innerHTML = '<iframe src="/listings/contactform/'+ id_listing +'" width=450 height=250 border=0 frameborder=0 > </iframe>';
			

		
	}
	
	
	