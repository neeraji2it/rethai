// check for update status after admin pay for listings
function updatestatus(id,statusid){
//alert(document.getElementById(id).checked);
	if (document.getElementById(id).checked){
		document.getElementById(statusid).innerHTML="<span style=\"color:green;\">Online</span>";
	}
	
}

// when admin cancle pay
function cancelpay(id){
//	alert('no: '+id);
	document.getElementById(id).checked=false;
}

// admin Delist listing
function delist(id,page){
		var str="";
		str = str +"<div style=padding-left:15px><br><h1>Delist</h1>\n<form name=\"f_site\" id=\"f_site\" method=\"post\" action=\"/admin/updatestatus\">\n<table cellspacing=\"10\" cellpadding=\"10\">\n";
		str = str +"<tr><td><input type=\"hidden\" name=\"id\" id=\"id\" value=\""+id+"\"/></td></tr>\n";
		str = str +"<tr><td><input type=\"hidden\" name=\"page\" id=\"page\" value=\""+page+"\"/></td></tr>\n";
		str = str +"<tr><td><input name=\"product[Status]\" type=\"radio\" value=\"d\" id=\"product_Status_1\" /><label for=\"product_Status_1\">&nbsp; Delete listing Permanently</label></td></tr>\n";
		str = str +"<tr><td><input name=\"product[Status]\" type=\"radio\" value=\"s\" id=\"product_Status_2\" /><label for=\"product_Status_2\">&nbsp; Listing has been Sold</label></td></tr>\n";
		str = str +"<tr><td><input name=\"product[Status]\" type=\"radio\" value=\"f\" id=\"product_Status_3\" /><label for=\"product_Status_3\">&nbsp; Take offline Temporarily</label></td></tr>\n";

		str = str +"<tr><td><input type=\"submit\" name=\"Submit\" value=\"Submit\" id=\"Submit\" /></td></tr>\n";
		str = str +"</form></table></div>\n";
		document.getElementById('shadow').style.display="block";
		document.getElementById('delist').innerHTML=str;
		document.getElementById('delist').style.display="block";
	}

function adminpay(idpay,idadminpay){
	if (document.getElementById(idpay).checked == true){
		document.getElementById(idadminpay).value = '1';		
	}else{
		document.getElementById(idadminpay).value = '0';
	}
	
}	

function setnewpoint(lat,lng){
	document.getElementById('product_lat').value=lat;
	document.getElementById('product_lng').value=lng;
}



function loadmap(lat,lng){
	if(lat == '0' && lng == '0'){
		lat="12.9357";
		lng="100.889";
	}
//	alert('location:\n<br>lat:' + lat + '\n<br>lng:'+lng);
	if (GBrowserIsCompatible()) {	
	
	
	function createMarker(point){
//			alert(point);
			var marker = new GMarker(point, {draggable: true, bouncy: true});
			var html="";			
			map.addOverlay(marker);
			GEvent.addListener(marker, "click", function(latlng){
				html="";
				html = html + "Confirm Location!\n<br>";
				html = html + "lat:"+latlng.lat()+'\n<br>';
				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
				marker.openInfoWindowHtml(html);
			});
			GEvent.addListener(marker, "dragstart", function() {
				
			  map.closeInfoWindow();
			});
			
			GEvent.addListener(marker, "dragend", function(latlng) {
				html="";
				html = html + "Confirm Location!\n<br>";
				html = html + "lat:"+latlng.lat()+'\n<br>';
				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
			  marker.openInfoWindowHtml(html);
			});
			 
		}
		
		

		var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		map.setCenter(new GLatLng(lat, lng), 14);
		createMarker(new GLatLng(lat, lng));
		
		geocoder = new GClientGeocoder();
		
		
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
	

}


function admin_searchlocation(){
	//alert('yess:'+GBrowserIsCompatible());
	if (GBrowserIsCompatible()) {	

		

		var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		//map.setCenter(new GLatLng(lat, lng), 14);
		
		var strlocation;
		geocoder = new GClientGeocoder();
		
		if ((document.getElementById('location').value).search(/thailand|Thailand/)>0){
			strlocation = document.getElementById('location').value;
		}else{
			strlocation = document.getElementById('location').value + ", thailand";
		}
		//alert(strlocation);
		 	geocoder.getLatLng(strlocation, function(point){
				
		 		if (point) {
		 			map.setCenter(point, 14);
					lat = point.lat();
					lng = point.lng();
					
		 		}
		 		else {
					map.setCenter(new GLatLng(12.9357, 100.889), 14);
					lat = 12.9357;
					lng = 100.889;
					}				
		point = new GLatLng(lat, lng);
		var marker = new GMarker(point, {draggable: true, bouncy: true});
			var html="";			
			map.addOverlay(marker);
			//alert('yessss: try to create marker');
			GEvent.addListener(marker, "click", function(latlng){
				html="";
				html = html + "Confirm Location!\n<br>";
				html = html + "lat:"+latlng.lat()+'\n<br>';
				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<br><input type=\"submit\" value=\"Confirm Location\" onclick=\"setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
				marker.openInfoWindowHtml(html);
			});
			GEvent.addListener(marker, "dragstart", function() {
				
			  map.closeInfoWindow();
			});
			
			GEvent.addListener(marker, "dragend", function(latlng) {
				html="";
				html = html + "Confirm Location!\n<br>";
				html = html + "lat:"+latlng.lat()+'\n<br>';
				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
			  marker.openInfoWindowHtml(html);
			});
		
			
				});
		
		
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}	
	
}


function switchelement(id){
	if (document.getElementById(id).style.display == 'none') {
		document.getElementById(id).style.display = 'block';
	}else{
		document.getElementById(id).style.display = 'none';
	}
	return false;
}


function resetcompform(){
	document.getElementById("compname").value = "";
	document.getElementById("url").value = "";
	document.getElementById("phone").value = "";
	document.getElementById("email").value = "";
	document.getElementById("location").value = "";
	document.forms["compsearch"].submit();
}
