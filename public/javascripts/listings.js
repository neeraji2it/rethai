var gmarkers = [];
var map;
var firstload;
var deletingMarkers=[];
var deletingMarkersNum=0;

	function deleteoldmarker(){
		if (deletingMarkers.length > 0) {
			for (var i = 0; i < deletingMarkers.length; i++) {				
					map.removeOverlay(deletingMarkers[i]);
			}
		}
		
	}
	
function load(firstload){
	document.getElementById('shadowmap').style.display='none';	
	document.getElementById('tempfirstload').value = firstload;
	if (GBrowserIsCompatible()) {
		
		deletingMarkersNum = 0;
		deletingMarkers=[];
		function createMarker(point, number, id_product){	
			var marker = new GMarker(point);
			marker.value = number;
			marker.id_product = id_product;
			gmarkers[number] = marker;
			deletingMarkers[number] = marker;
			deletingMarkersNum = deletingMarkersNum + 1;
			map.addOverlay(marker);
			
//			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
			GEvent.addListener(marker, "mouseover", function(){	
			if (!marker.tooltip) {
						var mylabel = "loadding...";
						var myAjax2 = new Ajax.Request('/products/getlabel', {
							method: 'get',
							parameters: 'id=' + marker.id_product,
							onComplete: function(request){
								mylabel = request.responseText;
								marker.tooltip = '<div class="tooltip">' + mylabel + '<\/div>';
								showTooltip(marker);
								
							} // end of anonymous onComplete function
						}); // end of the new Ajax.Request() call			 
					}else{showTooltip(marker);}
			marker.setImage("/icon/markerselected.png"); // change graphic		
			});
			GEvent.addListener(marker, "mouseout", function(){
				tooltip.style.visibility = "hidden";
				marker.setImage("/icon/markerred.png"); // change graphic
			});
			GEvent.addListener(marker, "click", function(){
				tooltip.style.visibility = "hidden";
				window.open("/rethai/"+document.getElementById('textlink_'+marker.id_product).value+"-"+marker.id_product, "_blank");
			});
			  
		}
		
		
		function showTooltip(marker){
			tooltip.innerHTML = marker.tooltip;
			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
			var anchor = marker.getIcon().iconAnchor;
			var width = marker.getIcon().iconSize.width;
			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
			pos.apply(tooltip);
			tooltip.style.visibility = "visible";
		}
		
		

		

		
				
	

		 map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		//map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		
		
// ====== set up marker mouseover tooltip div ======
		var tooltip = document.createElement("div");
		document.getElementById("map").appendChild(tooltip);
		tooltip.style.visibility = "hidden";
		
		
		geocoder = new GClientGeocoder();										 
										 
//			var str = "[{'Bed':'5','lng':'100.90069055557251','id':18,'Bath':'2','lat':'12.930702087235707','Price':350000},{'Bed':'16','lng':'100.87083220481873','id':20,'Bath':'8','lat':'12.914896181973738','Price':50000},{'Bed':'0','lng':'100.89268684387207','id':35,'Bath':'1','lat':'12.92534816456214','Price':null},{'Bed':'0','lng':'100.90496063232422','id':37,'Bath':'1','lat':'12.926853966921035','Price':5600000},{'Bed':'2','lng':'100.89414596557617','id':38,'Bath':'2','lat':'12.925766443906237','Price':2000000},{'Bed':'0','lng':'100.88178634643555','id':39,'Bath':'1','lat':'12.92133264720584','Price':123},{'Bed':'0','lng':'100.87715148925781','id':41,'Bath':'1','lat':'12.92024510014311','Price':500000},{'Bed':'3','lng':'100.90959548950195','id':43,'Bath':'1','lat':'12.966587132550504','Price':321},{'Bed':'0','lng':'100.889','id':47,'Bath':'1','lat':'12.9357','Price':33123},{'Bed':'2','lng':'100.88539123535156','id':48,'Bath':'2','lat':'12.947850597383681','Price':900000},{'Bed':'3','lng':'100.889','id':49,'Bath':'3','lat':'12.9357','Price':7500000},{'Bed':'0','lng':'100.889','id':50,'Bath':'1','lat':'12.9357','Price':123},{'Bed':'0','lng':'100.89431762695312','id':51,'Bath':'1','lat':'12.930283816162502','Price':1231233},{'Bed':'2','lng':'100.889','id':53,'Bath':'2','lat':'12.9357','Price':1500000},{'Bed':'0','lng':'100.889','id':55,'Bath':'1','lat':'12.9357','Price':1}]";							 
			
											var str2 = document.getElementById('tempmarker').value;
										 mytypes = eval( "(" + str2 + ")" );  
												
						map.setCenter(new GLatLng(12.9357, 100.889), 12);
											var set_centermap = '1';
//											newIcon=new GIcon(icon, '/icon/markerred.png');
												for (var y = 0; y < mytypes.length; y++) {
													var lat = mytypes[y].lat;
													var lng = mytypes[y].lng;
													var id_product = mytypes[y].id;
													var point = new GLatLng(lat, lng);
													if (set_centermap == '1') {
														map.setCenter(point, 12);
														set_centermap = '0';
													}
													createMarker(point,y,id_product);
												}

		GEvent.addListener(map,'moveend',function() {	
			document.getElementById('shadowmap').style.display='block';	
			deleteoldmarker();	
			var bounds = map.getBounds();
		 	var southWest = bounds.getSouthWest();
		 	var northEast = bounds.getNorthEast();
		 	var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();
		var myAjax = new Ajax.Request( "/listings/getmarker?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value + "&createdate=" + document.getElementById('createdate').value + "&search_from=" + document.getElementById('search_from').value,{//products/getmarker?clearlisting=1
					method: 'get',
					parameters: mapBoundary,
					onLoading:function(request){javascript: document.getElementById('processing2').style.display='block';},
					onComplete: function(request){						
					document.getElementById('processing2').style.display='none';
					
					markers = eval( "(" + request.responseText + ")" );
					var allID = ":";
					if (markers.length > 0) {
					//document.getElementById('tempcountlistings').value=markers.length;
						for (var i = 0; i < markers.length; i++) {							
							var lat = markers[i].lat;//Math.round(markers[i].lat * 100) / 100;
							var lng = markers[i].lng;//Math.round(markers[i].lng * 100) / 100;
							var id_product = markers[i].id;
							allID = allID + id_product + ":";
							var point = new GLatLng(lat, lng);
							createMarker(point,i,id_product);
//							marker = newMarker(new GLatLng(lat,lng), 'title', newIcon,json[i].id);
						}
						
						
						// Generate html
//		=========================================================================================================================================						
						var myAjax = new Ajax.Updater('main', "/listings/search?clearlisting=1&salerent=" + document.getElementById('salerent').value + "&propertytype=" + document.getElementById('propertytype').value + "&code=" + document.getElementById('code').value + "&fprice=" + document.getElementById('fprice').value + "&tprice=" + document.getElementById('tprice').value + "&bed=" + document.getElementById('bed').value + "&bed2=" + document.getElementById('bed2').value + "&bath=" + document.getElementById('bath').value + "&createdate=" + document.getElementById('createdate').value + "&search_from=" + document.getElementById('search_from').value,{ //products/getmarker?clearlisting=1
					method: 'get',
					parameters: mapBoundary + '&allID=' + allID,
					onComplete: function(request){						
						document.getElementById('foundlistings').innerHTML=document.getElementById('tempcountlistings').value ;
						document.getElementById('shadowmap').style.display='none';	
					} // end of anonymous onComplete function					
					}); // end of the new Ajax.Request() call
		
//		=========================================================================================================================================
		
		
					}else{
						document.getElementById('shadowmap').style.display='none';	
						document.getElementById('foundlistings').innerHTML="0";
					document.getElementById('main').innerHTML="<span style='color:red;'>Sorry there was no results, try expanding your search</span>";	
					}						
		} // end of anonymous onComplete function
		
		}); // end of the new Ajax.Request() call

			});  // end Gevent

	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
}


//function load(firstload){
//	document.getElementById('tempfirstload').value = firstload;
////				alert(datasearch[1][1]);
//	if (GBrowserIsCompatible()) {
//		
//		function createMarker(point, number, id_product){	
//			var marker = new GMarker(point);
//			marker.value = number;
//			marker.id_product = id_product;
//			gmarkers[number] = marker;
//			deletingMarkers[number] = marker;
//			deletingMarkersNum = deletingMarkersNum + 1;
//			map.addOverlay(marker);
//			
//			//  ======  The new marker "mouseover" and "mouseout" listeners  ======
//			GEvent.addListener(marker, "mouseover", function(){	
//			if (!marker.tooltip) {
//						var mylabel = "loadding...";
//						var myAjax2 = new Ajax.Request('/products/getlabel', {
//							method: 'get',
//							parameters: 'id=' + marker.id_product,
//							onComplete: function(request){
//								mylabel = request.responseText;
//								marker.tooltip = '<div class="tooltip">' + mylabel + '<\/div>';
//								showTooltip(marker);
//								
//							} // end of anonymous onComplete function
//						}); // end of the new Ajax.Request() call			 
//					}else{showTooltip(marker);}
//					
//			marker.setImage("/icon/markerselected.png"); // change graphic		
//			});
//			GEvent.addListener(marker, "mouseout", function(){
//				tooltip.style.visibility = "hidden";
//				marker.setImage("/icon/markerred.png"); // change graphic
//			});
//			GEvent.addListener(marker, "click", function(){
//				tooltip.style.visibility = "hidden";
////				window.blank.location = "show?id="+marker.id_product;
//				window.open("/rethai/"+document.getElementById('textlink_'+marker.id_product).value+"-"+marker.id_product, "_blank");
//			});
//			  
//		}
//		
//		
//		
//		function showTooltip(marker){
//			tooltip.innerHTML = marker.tooltip;
//			var point = map.getCurrentMapType().getProjection().fromLatLngToPixel(map.getBounds().getSouthWest(), map.getZoom());
//			var offset = map.getCurrentMapType().getProjection().fromLatLngToPixel(marker.getPoint(), map.getZoom());
//			var anchor = marker.getIcon().iconAnchor;
//			var width = marker.getIcon().iconSize.width;
//			var pos = new GControlPosition(G_ANCHOR_BOTTOM_LEFT, new GSize(offset.x - point.x - anchor.x + width, -offset.y + point.y + anchor.y));
//			pos.apply(tooltip);
//			tooltip.style.visibility = "visible";
//		}
//		
//		
//
//		
//
//		
//				
//	
//
//		 map = new GMap2(document.getElementById("map"));
//		var mt = map.getMapTypes();
//      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
//      for (var i=0; i<mt.length; i++) {
//        mt[i].getMinimumResolution = function() {return 8;}
//        mt[i].getMaximumResolution = function() {return 18;}
//      }
//	  
//		map.enableDoubleClickZoom();
//		map.setMapType(G_HYBRID_MAP);
//		map.addControl(new GMapTypeControl());
//		map.addControl(new GLargeMapControl());
//		
//		
//// ====== set up marker mouseover tooltip div ======
//		var tooltip = document.createElement("div");
//		document.getElementById("map").appendChild(tooltip);
//		tooltip.style.visibility = "hidden";
//		
//		
//		geocoder = new GClientGeocoder();
//		
//		 
//										 
//										 
//		if(document.getElementById('code').value !=""){
//			var getlatlng = new Ajax.Request( "/listings/getlatlng", {  
//										 method: 'get',  
//										 parameters: "province="+document.getElementById('code').value,
//										 onComplete: function(request){  
//										 mylatlng = eval( "(" + request.responseText + ")" );  
//										 			var lat = mylatlng.lat;
//												 	var lng = mylatlng.lng;
//												strlocation = ""+lat+","+lng;	
//												
//											
//													map.setCenter(new GLatLng(lat, lng), 12);
//												
//											
//											
//										 } // end of anonymous onComplete function  
//										 }); // end of the new Ajax.Request() call 
//										 
//			strlocation=document.getElementById('code').value + ", thailand";
//		}else{
//			strlocation="pattaya, thailand";
//			geocoder.getLatLng(strlocation, function(point){
//					if (point) {
//						map.setCenter(point, 12);
//					}else{
//						map.setCenter(new GLatLng(12.9357, 100.889), 12);
//					}
//				});
//		}
//		
////		geocoder.getLatLng(strlocation, function(point){
////			if (point) {
////				map.setCenter(point, 12);
////			}else{
////				map.setCenter(new GLatLng(12.9357, 100.889), 12);
////			}
////		});
//
//
//
//
//		// Ok need to get marker here
//		GEvent.addListener(map,'moveend',function() {
//			tooltip.style.visibility = "hidden";
////				alert('Mooooove');
//			var bounds = map.getBounds();
//			var southWest = bounds.getSouthWest();
//			var northEast = bounds.getNorthEast();
//			var mapBoundary = 'ne=' + northEast.toUrlValue() + '&sw=' + southWest.toUrlValue();		
//			//deleted old markers
//			//deleteoldmarker();
//				firstload = document.getElementById('tempfirstload').value;
//			if(firstload =='1'){
////				alert('1');
//				document.getElementById('tempfirstload').value = '0';
//										var myAjax2 = new Ajax.Request( "/listings/initialmarker", {  
//										 method: 'get',  
//										 parameters: mapBoundary,
//										 onComplete: function(request){  
//										 mytypes = eval( "(" + request.responseText + ")" );  
//										 for (var y = 0; y < mytypes.length; y++) {
//										 			var lat = mytypes[y].lat;
//												 	var lng = mytypes[y].lng;
//													var id_product = mytypes[y].id;
//													var point = new GLatLng(lat, lng);	
//													
//													//createMarker(point,y,id_product);
//													createMarker(point,mytypes[y].id,id_product);
//										 }
//											
//										 } // end of anonymous onComplete function  
//										 }); // end of the new Ajax.Request() call  
//			}else{
////				alert('2');
//										var myAjax2 = new Ajax.Request( "/listings/getmarker?salerent="+document.getElementById('salerent').value+"&code="+document.getElementById('code').value+"&fprice="+		document.getElementById('fprice').value+"&tprice="+document.getElementById('tprice').value+"&bath="+document.getElementById('bath').value+"&bed="+document.getElementById('bed').value+"&bed2="+document.getElementById('bed2').value+"&propertytype="+document.getElementById('propertytype').value, {  
//										 method: 'get',  
//										 parameters: mapBoundary,
//										 onComplete: function(request){  
//										 mytypes = eval( "(" + request.responseText + ")" );  
//										 for (var y = 0; y < mytypes.length; y++) {
//										 			var lat = mytypes[y].lat;
//												 	var lng = mytypes[y].lng;
//													var id_product = mytypes[y].id;
//													var point = new GLatLng(lat, lng);	
//													
////													createMarker(point,y,id_product);
//													createMarker(point,mytypes[y].id,id_product);
//										 }
//											
//										 } // end of anonymous onComplete function  
//										 }); // end of the new Ajax.Request() call  
//										 
//				
//										var myAjax2 = new Ajax.Request( "/listings/searchlistings?partner="+document.getElementById('partner').value+"&salerent="+document.getElementById('salerent').value+"&code="+document.getElementById('code').value+"&fprice="+		document.getElementById('fprice').value+"&tprice="+document.getElementById('tprice').value+"&bath="+document.getElementById('bath').value+"&bed="+document.getElementById('bed').value+"&bed2="+document.getElementById('bed2').value+"&propertytype="+document.getElementById('propertytype').value, {  
//										 method: 'get',  
//										 parameters: mapBoundary,	
//										 onLoading:function(request){document.getElementById('loading').style.display='block';},							 
//										 onComplete: function(request){  
//										 document.getElementById('loading').style.display='none';
//										 //mytypes = eval( "(" + request.responseText + ")" );  
//										 document.getElementById('main').innerHTML=request.responseText;	
//										 document.getElementById('foundlistings').innerHTML=document.getElementById('tempcountlistings').value;
//										 } // end of anonymous onComplete function  
//										 }); // end of the new Ajax.Request() call  				
//			}
//				firstload ="0";							 						 
//			});
//
//	}
//	
//	else {
//		alert("Sorry, the Google Maps API is not compatible with this browser");
//	}
//}

  
      function mymouseover(i) {
			gmarkers[i].setImage("/icon/markerselected.png"); // change graphic	
      }  
      function mymouseout(i) {
			gmarkers[i].setImage("/icon/markerred.png"); // change graphic	
      }
	  
	  
	  
function csslisting(id,chk){
	if(chk == '1'){
		document.getElementById(id).style.borderStyle=" 1px solid #ddd;"
//		alert('yesss'+document.getElementById(id).style.borderStyle);
//		document.getElementById(id).style.borderColor="#ddd;"
//		document.getElementById(id).style="border: 1px solid #ddd;";	
	}else{
		document.getElementById(id).style.border='0px';
	}
	
}	  
	  
	  
	  
	  
	  
function loadshowlisting(lat,lng){
	
	var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		//map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		map.setCenter(new GLatLng(lat,lng), 14);
		var point = new GLatLng(lat, lng);	
		var marker = new GMarker(point);
		map.addOverlay(marker);
}	  







function removeElement(parentDiv, childDiv){
     if (childDiv == parentDiv) {
          alert("The parent div cannot be removed.");
     }
     else if (document.getElementById(childDiv)) {     
          var child = document.getElementById(childDiv);
          var parent = document.getElementById(parentDiv);
          parent.removeChild(child);
     }
     else {
          alert("Child div has already been removed or does not exist.");
          return false;
     }
}


function removecompare(id_listing,element_id){
//	var mystr;
//	mystr = document.getElementById('compare_id').value;
//	document.getElementById('compare_id').value =mystr.replace(/+str+/,":")
//alert('yesss');
	removeElement('showcompare', 'compare_' + id_listing);
		//		 var str=document.getElementById('compare_id').value;
		document.getElementById('ccount').value = (document.getElementById('ccount').value * 1) - 1;
		if((document.getElementById('ccount').value) * 1 < 1){document.getElementById('compare_form').style.display = 'none';}
		document.getElementById('compare_id').value = document.getElementById('compare_id').value.replace(":"+id_listing+":", ":");
		document.getElementById(element_id).checked = false;
}

function chb_compare(element_id,img,id_listing,price,bed,bath,sqm,parking,listingtype){
	//alert(document.getElementById(element_id).checked );
	if (document.getElementById(element_id).checked == true) {
		document.getElementById('compare_form').style.display = "block";
//		alert(document.getElementById('compare_form').style.display);
		//put in list
		if ((document.getElementById('ccount').value) * 1 < 5) {
			if (document.getElementById('compare_id').value.search(":" + id_listing + ":") == -1) {
				document.getElementById('ccount').value = (document.getElementById('ccount').value * 1) + 1;
				detail = "";
				detail = detail + "<a href='#compare_form' onclick=\"removecompare('" + id_listing + "','" + element_id + "');\"><small>Remove</small></a>\n<table> \n";
				detail = detail + "		<tr>\n";
				detail = detail + "			<td> <img src='" + img + "' width='50' height='50'/> </td>\n";
				detail = detail + "			<td style=padding-left:5px;>\n";
				
				detail = detail + "				<small>	\n";
				detail = detail + "						" + "&#3647;<strong>" + price + "</strong><br>" + bed + "<br>" + listingtype + "\n";
				detail = detail + "				</small>\n";
				
				
				detail = detail + "			</td>\n";
				detail = detail + "		</tr>\n</table>";
				document.getElementById('showcompare').innerHTML = document.getElementById('showcompare').innerHTML + "<div id='compare_" + id_listing + "'>" + detail + "</div>";
				document.getElementById('compare_id').value = document.getElementById('compare_id').value + id_listing + ":";
			}else{
				document.getElementById(element_id).checked = true;
			}
		}else{
			document.getElementById(element_id).checked = false;
		}
	}
	else {
		// take off from list
		removeElement('showcompare', 'compare_' + id_listing);
		//		 var str=document.getElementById('compare_id').value;
		document.getElementById('ccount').value = (document.getElementById('ccount').value * 1) - 1;
		if((document.getElementById('ccount').value) * 1 < 1){ document.getElementById('compare_form').style.display = 'none';}
		document.getElementById('compare_id').value = document.getElementById('compare_id').value.replace(":"+id_listing+":", ":");
	}
	
}






function setnewpoint(lat,lng){
	document.getElementById('product_lat').value=lat;
	document.getElementById('product_lng').value=lng;
	
	document.getElementById('temp_lat1').value=lat;
	document.getElementById('temp_lng1').value=lng;
	document.getElementById('temp_lat2').value=lat;
	document.getElementById('temp_lng2').value=lng;
	setsubmitform();
}



function loadmap(lat,lng){ 

	if (GBrowserIsCompatible()) {	
	
	if(lat == '0' && lng == '0'){
		lat="12.9357";
		lng="100.889";
	}
	setnewpoint(lat,lng)
//	alert('location:\n<br>lat:' + lat + '\n<br>lng:'+lng);
	
	
		
		
	
	function createMarker(point){
//			alert(point);
			var marker = new GMarker(point, {draggable: true, bouncy: true});
			var html="";			
//			gmarkers.push(marker);
			map.addOverlay(marker);
			GEvent.addListener(marker, "click", function(latlng){
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
				marker.openInfoWindowHtml(html);
			});
			GEvent.addListener(marker, "dragstart", function() {
				
			  map.closeInfoWindow();
			});
			
			GEvent.addListener(marker, "dragend", function(latlng) {
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\" document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
			  marker.openInfoWindowHtml(html);
			});
			 
		}
		
		

	var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		//map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		map.setCenter(new GLatLng(lat, lng), 14);
		createMarker(new GLatLng(lat, lng));
		
		geocoder = new GClientGeocoder();
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
	

}


function loadmap2(lat,lng,lat2,lng2){ 

	if (GBrowserIsCompatible()) {	
	
	if(lat == '0' && lng == '0'){
		lat="12.9357";
		lng="100.889";
	}
	setnewpoint(lat,lng)
//	alert('location:\n<br>lat:' + lat + '\n<br>lng:'+lng);
	
	
		
		
	
	function createMarker(point){
//			alert(point);
			var marker = new GMarker(point, {draggable: true, bouncy: true});
			var html="";			
//			gmarkers.push(marker);
			map.addOverlay(marker);
			GEvent.addListener(marker, "click", function(latlng){
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
				marker.openInfoWindowHtml(html);
			});
			GEvent.addListener(marker, "dragstart", function() {
				
			  map.closeInfoWindow();
			});
			
			GEvent.addListener(marker, "dragend", function(latlng) {
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\" document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
			  marker.openInfoWindowHtml(html);
			});
			 
		}
		
		

	var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		//map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		map.setCenter(new GLatLng(lat2, lng2), 14);
		createMarker(new GLatLng(lat, lng));
		
		geocoder = new GClientGeocoder();
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}
	

}




function searchlocation(){
	//alert('yess:'+GBrowserIsCompatible());
	if (GBrowserIsCompatible()) {	

		

		var map = new GMap2(document.getElementById("map"));
		var mt = map.getMapTypes();
      // Overwrite the getMinimumResolution() and getMaximumResolution() methods
      for (var i=0; i<mt.length; i++) {
        mt[i].getMinimumResolution = function() {return 8;}
        mt[i].getMaximumResolution = function() {return 18;}
      }
	  
		map.enableDoubleClickZoom();
		//map.setMapType(G_HYBRID_MAP);
		map.addControl(new GMapTypeControl());
		map.addControl(new GLargeMapControl());
		//map.setCenter(new GLatLng(lat, lng), 14);
		
		var strlocation;
		geocoder = new GClientGeocoder();
		
		if ((document.getElementById('location').value).search(/thailand|Thailand/)>0){
			strlocation = document.getElementById('location').value;
		}else{
			strlocation = document.getElementById('location').value + ", thailand";
		}
		/******************* NEW Search location System  **********************************************/
		var getlatlng = new Ajax.Request("/listings/getlatlng", {
					method: 'get',
					parameters: "province=" + document.getElementById('location').value,
					onComplete: function(request){
						mylatlng = eval("(" + request.responseText + ")");
						var lat = mylatlng.lat;
						var lng = mylatlng.lng;
						var strlocation = "" + lat + "," + lng;						
						
						
		 	geocoder.getLatLng(strlocation, function(point){
				
		 		if (point) {
		 			map.setCenter(point, 14);
					lat = point.lat();
					lng = point.lng();
					
		 		}
		 		else {
					map.setCenter(new GLatLng(12.9357, 100.889), 14);
					lat = 12.9357;
					lng = 100.889;
					}				
		point = new GLatLng(lat, lng);
		var marker = new GMarker(point, {draggable: true, bouncy: true});
			var html="";			
			map.addOverlay(marker);
			//alert('yessss: try to create marker');
			GEvent.addListener(marker, "click", function(latlng){
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
				marker.openInfoWindowHtml(html);
			});
			GEvent.addListener(marker, "dragstart", function() {
				
			  map.closeInfoWindow();
			});
			
			GEvent.addListener(marker, "dragend", function(latlng) {
				html="";
//				html = html + "Confirm Location!\n<br>";
//				html = html + "lat:"+latlng.lat()+'\n<br>';
//				html = html + "lng:"+latlng.lng()+'\n<br>';
				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
				html = html + "";
			  marker.openInfoWindowHtml(html);
			});
		
			
				});
						
						
					} // end of anonymous onComplete function  
				}); // end of the new Ajax.Request() call 
				
				
		/*******************END  NEW Search location System  **********************************************/
		
		
		
				/****************************  old search location system  *********************************/
//		 	geocoder.getLatLng(strlocation, function(point){
//				
//		 		if (point) {
//		 			map.setCenter(point, 14);
//					lat = point.lat();
//					lng = point.lng();
//					
//		 		}
//		 		else {
//					map.setCenter(new GLatLng(12.9357, 100.889), 14);
//					lat = 12.9357;
//					lng = 100.889;
//					}				
//		point = new GLatLng(lat, lng);
//		var marker = new GMarker(point, {draggable: true, bouncy: true});
//			var html="";			
//			map.addOverlay(marker);
//			//alert('yessss: try to create marker');
//			GEvent.addListener(marker, "click", function(latlng){
//				html="";
////				html = html + "Confirm Location!\n<br>";
////				html = html + "lat:"+latlng.lat()+'\n<br>';
////				html = html + "lng:"+latlng.lng()+'\n<br>';
//				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
//				html = html + "";
//				marker.openInfoWindowHtml(html);
//			});
//			GEvent.addListener(marker, "dragstart", function() {
//				
//			  map.closeInfoWindow();
//			});
//			
//			GEvent.addListener(marker, "dragend", function(latlng) {
//				html="";
////				html = html + "Confirm Location!\n<br>";
////				html = html + "lat:"+latlng.lat()+'\n<br>';
////				html = html + "lng:"+latlng.lng()+'\n<br>';
//				html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"button\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('"+latlng.lat()+"','"+latlng.lng()+"'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
//				html = html + "";
//			  marker.openInfoWindowHtml(html);
//			});
//		
//			
//				});
//		
		
				/**************************** end old search location system  *********************************/
		
	}
	
	else {
		alert("Sorry, the Google Maps API is not compatible with this browser");
	}	
	
}



function condolocation(latlng){
	//alert('yess:'+GBrowserIsCompatible());
	if (latlng != "0") {
		if (GBrowserIsCompatible()) {
		
		
		
			var map = new GMap2(document.getElementById("map"));
			var mt = map.getMapTypes();
			// Overwrite the getMinimumResolution() and getMaximumResolution() methods
			for (var i = 0; i < mt.length; i++) {
				mt[i].getMinimumResolution = function(){
					return 8;
				}
				mt[i].getMaximumResolution = function(){
					return 18;
				}
			}
			
			map.enableDoubleClickZoom();
			//map.setMapType(G_HYBRID_MAP);
			map.addControl(new GMapTypeControl());
			map.addControl(new GLargeMapControl());
			//map.setCenter(new GLatLng(lat, lng), 14);
			
//			alert("lat: " + latlng);
			//alert(strlocation);
			geocoder.getLatLng(latlng, function(point){
			
				if (point) {
					//					alert('YESSS');
					map.setCenter(point, 14);
					lat = point.lat();
					lng = point.lng();
					
				}
				else {
					//					alert('Noooo');
					map.setCenter(new GLatLng(12.9357, 100.889), 14);
					lat = 12.9357;
					lng = 100.889;
				}
				point = new GLatLng(lat, lng);
				var marker = new GMarker(point, {
					draggable: true,
					bouncy: true
				});
				var html = "";
				map.addOverlay(marker);
//				html = "";
//					//				html = html + "Confirm Location!\n<br>";
//					//				html = html + "lat:"+latlng.lat()+'\n<br>';
//					//				html = html + "lng:"+latlng.lng()+'\n<br>';
//					html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('" + latlng.lat() + "','" + latlng.lng() + "'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
//					html = html + "";
//					map.openInfoWindowHtml(html);
				//alert('yessss: try to create marker');
				GEvent.addListener(marker, "click", function(latlng){
					html = "";
					//				html = html + "Confirm Location!\n<br>";
					//				html = html + "lat:"+latlng.lat()+'\n<br>';
					//				html = html + "lng:"+latlng.lng()+'\n<br>';
					html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('" + latlng.lat() + "','" + latlng.lng() + "'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
					html = html + "";
					marker.openInfoWindowHtml(html);
				});
				GEvent.addListener(marker, "dragstart", function(){
				
					map.closeInfoWindow();
				});
				
				GEvent.addListener(marker, "dragend", function(latlng){
					html = "";
					//				html = html + "Confirm Location!\n<br>";
					//				html = html + "lat:"+latlng.lat()+'\n<br>';
					//				html = html + "lng:"+latlng.lng()+'\n<br>';
					html = html + "\n<div id='noticelocation' style='color:green;'></div><br>\n<input type=\"submit\" value=\"Confirm Location\" onclick=\"document.getElementById('confirmlocation').value='1'; setnewpoint('" + latlng.lat() + "','" + latlng.lng() + "'); document.getElementById('noticelocation').innerHTML='Location Saved';\"/>";
					html = html + "";
					marker.openInfoWindowHtml(html);
				});
				
				
			});
			
			
			
		}
		
		else {
			alert("Sorry, the Google Maps API is not compatible with this browser");
		}
	}else{document.getElementById('confirmlocation').value='0';}
}
