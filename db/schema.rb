# This file is auto-generated from the current state of the database. Instead of editing this file, 
# please use the migrations feature of Active Record to incrementally modify your database, and
# then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your database schema. If you need
# to create the application database on another system, you should be using db:schema:load, not running
# all the migrations from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20100519040438) do

  create_table "advertises", :force => true do |t|
    t.string   "city",       :limit => 100, :default => ""
    t.text     "ScriptText"
    t.string   "status",     :limit => 1,   :default => "1"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "advtype",    :limit => 50,  :default => "searchcenter"
  end

  create_table "agentconnects", :force => true do |t|
    t.integer  "id_user",                     :default => 0
    t.string   "salerent",      :limit => 2,  :default => "0"
    t.string   "ttype",         :limit => 10, :default => "0"
    t.string   "location",                    :default => "0"
    t.integer  "sprice",                      :default => 0
    t.integer  "fprice",                      :default => 0
    t.integer  "sbed",                        :default => 0
    t.integer  "fbed",                        :default => 0
    t.integer  "bath",                        :default => 0
    t.text     "details"
    t.text     "agent_history"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "approve",       :limit => 1,  :default => "0"
  end

  create_table "backgroundcontrols", :force => true do |t|
    t.string "run_control", :limit => 1, :default => "1", :null => false
  end

  create_table "bj_config", :primary_key => "bj_config_id", :force => true do |t|
    t.text "hostname"
    t.text "key"
    t.text "value"
    t.text "cast"
  end

  create_table "bj_job", :primary_key => "bj_job_id", :force => true do |t|
    t.text     "command"
    t.text     "state"
    t.integer  "priority"
    t.text     "tag"
    t.integer  "is_restartable"
    t.text     "submitter"
    t.text     "runner"
    t.integer  "pid"
    t.datetime "submitted_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.text     "env"
    t.text     "stdin"
    t.text     "stdout"
    t.text     "stderr"
    t.integer  "exit_status"
  end

  create_table "bj_job_archive", :primary_key => "bj_job_archive_id", :force => true do |t|
    t.text     "command"
    t.text     "state"
    t.integer  "priority"
    t.text     "tag"
    t.integer  "is_restartable"
    t.text     "submitter"
    t.text     "runner"
    t.integer  "pid"
    t.datetime "submitted_at"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "archived_at"
    t.text     "env"
    t.text     "stdin"
    t.text     "stdout"
    t.text     "stderr"
    t.integer  "exit_status"
  end

  create_table "bjqueues", :force => true do |t|
    t.string "apiID",  :limit => 30, :default => "0", :null => false
    t.string "status", :limit => 1,  :default => "1", :null => false
  end

  create_table "businesslogos", :force => true do |t|
    t.string   "filename",     :null => false
    t.string   "content_type", :null => false
    t.integer  "size",         :null => false
    t.integer  "width",        :null => false
    t.integer  "height",       :null => false
    t.integer  "parent_id"
    t.string   "thumbnail"
    t.string   "id_user"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", :force => true do |t|
    t.integer  "id_user",                   :default => 0
    t.string   "logo",       :limit => 50
    t.text     "Detail"
    t.text     "Services"
    t.string   "Name",       :limit => 50
    t.string   "phone",      :limit => 30
    t.string   "Email",      :limit => 50
    t.string   "Url",        :limit => 100
    t.string   "brokerNo",   :limit => 30
    t.integer  "Years",                     :default => 0
    t.string   "Head",       :limit => 30,  :default => "0"
    t.string   "lat",        :limit => 30,  :default => "0"
    t.string   "lng",        :limit => 30,  :default => "0"
    t.datetime "created_on",                                 :null => false
    t.datetime "updated_on",                                 :null => false
    t.string   "Skype",      :limit => 100, :default => ""
    t.text     "Address"
    t.integer  "logo2",                     :default => 0
    t.integer  "logo3",                     :default => 0
    t.string   "city",       :limit => 100, :default => ""
  end

  create_table "condos", :force => true do |t|
    t.string   "name",                       :default => ""
    t.integer  "id_image",                   :default => 0
    t.string   "city",        :limit => 100, :default => ""
    t.string   "lat",         :limit => 50,  :default => "12.9357"
    t.string   "lng",         :limit => 50,  :default => "100.889"
    t.string   "homepage",    :limit => 1,   :default => "0"
    t.integer  "lowprice",                   :default => 0
    t.integer  "heightprice",                :default => 0
    t.string   "status",      :limit => 1,   :default => "1"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "creditdetails", :force => true do |t|
    t.integer  "id_user",                    :default => 0
    t.integer  "id_contact",                 :default => 0
    t.string   "ContactName",  :limit => 20, :default => "0"
    t.integer  "ContactNo",                  :default => 0
    t.string   "SmsDetail",                  :default => "0"
    t.float    "credit",                     :default => 0.0
    t.string   "TXNID",        :limit => 50, :default => "0"
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "id_product",   :limit => 30, :default => "0"
    t.string   "command_type", :limit => 2,  :default => "0"
    t.float    "remains",                    :default => 0.0
  end

  create_table "credits", :force => true do |t|
    t.integer  "id_user",                     :default => 0
    t.integer  "FullCredit",                  :default => 0
    t.integer  "RemainCredit",                :default => 0
    t.integer  "Price",                       :default => 0
    t.string   "TXN_ID",        :limit => 20, :default => "0"
    t.string   "Description",                 :default => "0"
    t.string   "Status",        :limit => 1,  :default => "p"
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "SubscrId",      :limit => 30, :default => "0"
    t.string   "HeadSubscript", :limit => 30, :default => "0"
  end

  create_table "developments", :force => true do |t|
    t.integer  "id_user",                         :default => 0
    t.integer  "id_image",                        :default => 0
    t.integer  "id_Type",                         :default => 0
    t.string   "Type_Name",        :limit => 100, :default => ""
    t.string   "City",             :limit => 100, :default => ""
    t.string   "Name",             :limit => 100, :default => ""
    t.string   "Url",              :limit => 100, :default => ""
    t.string   "HomepageFeatured", :limit => 1,   :default => "1"
    t.integer  "Views",                           :default => 0
    t.text     "DevelopmentHtml"
    t.string   "status",           :limit => 1,   :default => "1"
    t.string   "lat",              :limit => 20,  :default => "12.9357"
    t.string   "lng",              :limit => 20,  :default => "100.889"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "exchange_rates", :force => true do |t|
    t.string   "base_currency"
    t.string   "currency"
    t.float    "rate"
    t.date     "issued_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "favorites", :force => true do |t|
    t.string   "id_user",    :limit => 30, :default => "0"
    t.string   "id_product", :limit => 30, :default => "0"
    t.string   "comment"
    t.string   "link",                     :default => "0"
    t.string   "Deleted",    :limit => 1,  :default => "1"
    t.datetime "created_on",                                :null => false
    t.datetime "updated_on",                                :null => false
    t.string   "lat",        :limit => 30, :default => "0"
    t.string   "lng",        :limit => 30, :default => "0"
  end

  create_table "images", :force => true do |t|
    t.string   "id_user",            :limit => 20, :default => "0",     :null => false
    t.string   "id_product",         :limit => 30, :default => "0",     :null => false
    t.string   "photo_file_name",                  :default => "0",     :null => false
    t.string   "photo_content_type", :limit => 20, :default => "0",     :null => false
    t.string   "photo_file_size",    :limit => 20, :default => "0",     :null => false
    t.string   "file_type",          :limit => 10, :default => "image", :null => false
    t.datetime "created_on",                                            :null => false
    t.datetime "updated_on",                                            :null => false
    t.string   "paperclipfix",       :limit => 1,  :default => "0"
    t.integer  "id_condo",                         :default => 0
  end

  create_table "imagetemps", :force => true do |t|
    t.integer  "id_user",    :default => 0
    t.string   "name",       :default => ""
    t.string   "path",       :default => ""
    t.datetime "created_on",                 :null => false
    t.datetime "updated_on",                 :null => false
  end

  create_table "leadhistories", :force => true do |t|
    t.integer  "id_user"
    t.integer  "id_lead"
    t.string   "checked"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "conect",     :limit => 10, :default => "1"
    t.text     "details"
    t.integer  "id_listing",               :default => 0
  end

  create_table "leadrecords", :force => true do |t|
    t.integer  "id_user",                 :default => 0
    t.string   "leadtype",   :limit => 2, :default => "1"
    t.date     "month",                   :default => '2011-04-24'
    t.string   "lstatus",    :limit => 1, :default => "1"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "leads", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.text     "messages"
    t.string   "ltype"
    t.string   "salerent"
    t.string   "city"
    t.integer  "sprice"
    t.integer  "fprice"
    t.integer  "sbed"
    t.integer  "fbed"
    t.integer  "bath"
    t.text     "user_history"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "id_user",                   :default => 0
    t.integer  "parking",                   :default => 0
    t.string   "approve",      :limit => 1, :default => "0"
  end

  create_table "levelshistories", :force => true do |t|
    t.integer  "id_user",                   :default => 0
    t.string   "UserName",   :limit => 50,  :default => ""
    t.string   "Email",      :limit => 100, :default => ""
    t.string   "Phone",      :limit => 50,  :default => ""
    t.string   "OldLevel",   :limit => 1,   :default => "0"
    t.string   "Level",      :limit => 1,   :default => "0"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "Approve",    :limit => 1,   :default => "0"
    t.string   "LevelName",  :limit => 50,  :default => ""
    t.string   "Listings",   :limit => 1,   :default => "5"
  end

  create_table "listingconects", :force => true do |t|
    t.integer  "id_user"
    t.integer  "id_product"
    t.text     "user_history"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "approve",      :limit => 1, :default => "0"
  end

  create_table "listingcounters", :force => true do |t|
    t.integer  "id_product",   :default => 0
    t.integer  "counter",      :default => 0
    t.date     "viewdate",                    :null => false
    t.datetime "created_on"
    t.datetime "updated_on"
    t.integer  "id_user",      :default => 0
    t.integer  "month1",       :default => 0
    t.integer  "month2",       :default => 0
    t.integer  "month3",       :default => 0
    t.integer  "month4",       :default => 0
    t.integer  "month5",       :default => 0
    t.integer  "month6",       :default => 0
    t.integer  "month7",       :default => 0
    t.integer  "month8",       :default => 0
    t.integer  "month9",       :default => 0
    t.integer  "month10",      :default => 0
    t.integer  "month11",      :default => 0
    t.integer  "month12",      :default => 0
    t.integer  "currentmonth", :default => 0
  end

  create_table "magazineoptions", :force => true do |t|
    t.string  "name",          :limit => 50, :default => ""
    t.integer "photos",                      :default => 1
    t.integer "price",                       :default => 0
    t.integer "listingcredit",               :default => 1
  end

  create_table "magazines", :force => true do |t|
    t.integer  "id_user",                      :default => 0
    t.integer  "id_product",                   :default => 0
    t.integer  "id_template",                  :default => 0
    t.string   "id_photo",      :limit => 50,  :default => ""
    t.text     "details"
    t.string   "subscrptpay",   :limit => 1,   :default => "0"
    t.string   "pay",           :limit => 1,   :default => "0"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "price",                        :default => 0
    t.integer  "rprice",                       :default => 0
    t.string   "bed",           :limit => 2,   :default => "0"
    t.string   "bath",          :limit => 2,   :default => "1"
    t.string   "phone",         :limit => 20,  :default => ""
    t.string   "email",         :limit => 50,  :default => ""
    t.string   "url",           :limit => 100, :default => ""
    t.integer  "logo",                         :default => 0
    t.string   "status",        :limit => 2,   :default => "1"
    t.date     "month"
    t.integer  "listingcredit",                :default => 1
  end

  create_table "mails", :force => true do |t|
    t.string   "id_user",     :limit => 20,  :default => "0", :null => false
    t.integer  "id_product",                 :default => 0,   :null => false
    t.string   "Email",       :limit => 50,                   :null => false
    t.string   "Name",        :limit => 50,                   :null => false
    t.string   "Address",                                     :null => false
    t.string   "Suburb",                                      :null => false
    t.string   "MobilePhone", :limit => 25,                   :null => false
    t.text     "Message",                                     :null => false
    t.date     "ViewDate"
    t.time     "ViewTime"
    t.datetime "created_on",                                  :null => false
    t.datetime "updated_on",                                  :null => false
    t.string   "id_agent",    :limit => 30,  :default => "0"
    t.integer  "customer",                   :default => 0
    t.integer  "master",                     :default => 0
    t.string   "custflag",    :limit => 1,   :default => "0"
    t.string   "userflag",    :limit => 1,   :default => "1"
    t.string   "lat",         :limit => 30,  :default => "0"
    t.string   "lng",         :limit => 30,  :default => "0"
    t.string   "status",      :limit => 3,   :default => "1"
    t.string   "toEmail",     :limit => 150, :default => ""
    t.string   "newsletter",  :limit => 1,   :default => "0"
  end

  create_table "offers", :force => true do |t|
    t.string   "id_product",     :limit => 30
    t.string   "id_productuser", :limit => 30
    t.string   "id_user",        :limit => 30
    t.string   "offerprice",     :limit => 30
    t.text     "comment"
    t.string   "Deleted",        :limit => 1,  :default => "1"
    t.string   "Customer",       :limit => 30, :default => "0"
    t.datetime "created_on",                                    :null => false
    t.datetime "updated_on",                                    :null => false
  end

  create_table "options", :force => true do |t|
    t.string  "option_type", :limit => 50,  :default => "0"
    t.string  "Value",       :limit => 10,  :default => "0"
    t.string  "Name",        :limit => 100, :default => "0"
    t.integer "opt_order",                  :default => 0
  end

  create_table "producthistories", :force => true do |t|
    t.integer  "id_product",              :default => 0,   :null => false
    t.integer  "id_user",                 :default => 0
    t.string   "delist",     :limit => 1, :default => "1", :null => false
    t.date     "startdate"
    t.date     "stopdate"
    t.integer  "remain",                  :default => 0,   :null => false
    t.datetime "created_on",                               :null => false
    t.datetime "updated_on",                               :null => false
  end

  create_table "products", :force => true do |t|
    t.string   "id_user",          :limit => 20,  :default => "0"
    t.string   "City",             :limit => 50,  :default => "0"
    t.integer  "Price",                           :default => 0
    t.string   "Bed",              :limit => 20,  :default => "0"
    t.string   "Bath",             :limit => 20,  :default => "0"
    t.text     "Detail"
    t.string   "Type",             :limit => 20,  :default => "0"
    t.string   "Parking",          :limit => 20,  :default => "0"
    t.string   "Deleted",          :limit => 1,   :default => "1"
    t.string   "lat",              :limit => 20,  :default => "0"
    t.string   "lng",              :limit => 20,  :default => "0"
    t.datetime "created_on"
    t.datetime "updated_on"
    t.string   "salerent",         :limit => 1,   :default => "1"
    t.integer  "RPrice"
    t.string   "Status",           :limit => 1
    t.date     "expiredate"
    t.string   "adminpay",         :limit => 1,   :default => "0"
    t.string   "SQM",              :limit => 30,  :default => "0"
    t.date     "startdate"
    t.integer  "remain",                          :default => 1
    t.string   "renew",            :limit => 1,   :default => "1"
    t.integer  "counter",                         :default => 0
    t.integer  "xmlcommand",                      :default => 0
    t.string   "SearchFeatured",   :limit => 1,   :default => "0"
    t.string   "MapFeatured",      :limit => 1,   :default => "0"
    t.string   "HomepageFeatured", :limit => 1,   :default => "0"
    t.string   "EmailFeatured",    :limit => 1,   :default => "0"
    t.string   "magazine",         :limit => 1,   :default => "0"
    t.date     "magazinedate"
    t.integer  "id_condo",                        :default => 0
    t.string   "condo_history",    :limit => 100, :default => ""
    t.integer  "APIListingID",                    :default => 0
    t.integer  "APIUserID",                       :default => 0
    t.text     "Images_Url"
    t.text     "updateimage"
    t.string   "UploadImages",     :limit => 1,   :default => "0"
    t.string   "Quality",          :limit => 1,   :default => "0"
    t.string   "Ref",              :limit => 50,  :default => ""
    t.string   "Virtual",          :limit => 1,   :default => "0"
    t.string   "youtubecode",                     :default => ""
    t.string   "titlelink",                       :default => ""
    t.string   "priceorder",       :limit => 10,  :default => "1"
    t.string   "imageorder",       :limit => 10,  :default => "1"
    t.string   "homefeatured",     :limit => 10,  :default => "0"
    t.string   "condofeatured",    :limit => 10,  :default => "0"
    t.string   "paypalstatus",     :limit => 20
  end

  create_table "provinces", :force => true do |t|
    t.string  "name", :limit => 50
    t.string  "lat",  :limit => 50, :default => "0"
    t.string  "lng",  :limit => 50, :default => "0"
    t.integer "mt",                 :default => 100
    t.string  "th",   :limit => 50, :default => ""
  end

  create_table "rethaiapihistories", :force => true do |t|
    t.integer  "id_user",                   :default => 0
    t.text     "url",        :limit => 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rethaiapis", :force => true do |t|
    t.integer  "id_user",                  :default => 0
    t.string   "SiteName",                 :default => ""
    t.string   "APItype",    :limit => 20, :default => "addlisting"
    t.string   "APIKey",     :limit => 60, :default => "0"
    t.string   "IP",         :limit => 50, :default => "0"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "Status",     :limit => 1,  :default => "1"
    t.string   "APILink",                  :default => ""
    t.string   "codeURL",                  :default => ""
    t.string   "manage",     :limit => 10, :default => "daily"
    t.string   "DOW",        :limit => 10, :default => ""
    t.integer  "DOM",                      :default => 0
    t.date     "lastupdate",               :default => '2011-04-23'
  end

  create_table "rssfeeds", :force => true do |t|
    t.string   "title",                       :default => ""
    t.string   "url",                         :default => ""
    t.string   "sql",                         :default => ""
    t.string   "fsalerent",                   :default => ""
    t.string   "flistingtype",                :default => ""
    t.string   "fuser",                       :default => ""
    t.string   "flocation",                   :default => ""
    t.integer  "flimit",                      :default => 20
    t.string   "ftypeshow",    :limit => 100, :default => "random"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "link",                        :default => "http://www.rethai.com/rethai"
  end

  create_table "subscriptions", :force => true do |t|
    t.string  "Name",               :limit => 50
    t.integer "Price"
    t.string  "Report",             :limit => 1
    t.string  "PerformanceState",   :limit => 50
    t.integer "Photo"
    t.integer "Listings"
    t.integer "Upgrades"
    t.string  "Enchanced",          :limit => 1
    t.string  "ListingContact",     :limit => 1
    t.string  "Apisites",           :limit => 1
    t.string  "Xmlsites",           :limit => 1
    t.string  "BusinessDetail",     :limit => 1
    t.string  "SpotlightAdvert",    :limit => 1
    t.string  "RealEstateData",     :limit => 50
    t.string  "WebsiteIntegration", :limit => 1
    t.integer "SMS"
    t.string  "PortalPage",         :limit => 1,  :default => "0"
    t.string  "SEOListingPage",     :limit => 1,  :default => "0"
    t.integer "SearchFeatured",                   :default => 0
    t.integer "MapFeatured",                      :default => 0
    t.integer "HomepageFeatured",                 :default => 0
    t.integer "EmailFeatured",                    :default => 0
    t.integer "magazine",                         :default => 0
    t.integer "printdiscount",                    :default => 0
    t.integer "leadconnect",                      :default => 0
    t.integer "listingconnect",                   :default => 0
    t.integer "leadprice",                        :default => 0
    t.integer "HomeFeatured",                     :default => 0
    t.integer "CondoFeatured",                    :default => 0
  end

  create_table "templistings", :force => true do |t|
    t.string   "sessionID",  :default => ""
    t.text     "listingID"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "command",    :default => 1
  end

  create_table "users", :force => true do |t|
    t.string   "FirstName",          :limit => 50
    t.string   "LastName",           :limit => 50
    t.string   "UserName",           :limit => 20
    t.string   "Password",           :limit => 20
    t.string   "Email",              :limit => 50
    t.string   "Phone",              :limit => 20
    t.string   "Address"
    t.string   "Suburb"
    t.string   "Province",           :limit => 20
    t.string   "PostCode",           :limit => 20
    t.string   "UserType",           :limit => 1,   :default => "p"
    t.string   "Deleted",            :limit => 1,   :default => "1"
    t.string   "SMS",                :limit => 1,   :default => "0"
    t.datetime "created_on",                                         :null => false
    t.datetime "updated_on",                                         :null => false
    t.string   "email_newmessage",   :limit => 1,   :default => "1"
    t.string   "sms_newmessage",     :limit => 1,   :default => "0"
    t.string   "sms_partnermedia",   :limit => 1,   :default => "0"
    t.string   "email_newsletter",   :limit => 1,   :default => "1"
    t.string   "email_weeklyalerts", :limit => 1,   :default => "1"
    t.string   "email_partnermedia", :limit => 1,   :default => "1"
    t.string   "SMSPhone",           :limit => 30,  :default => ""
    t.string   "partner",            :limit => 100, :default => ""
    t.string   "level",              :limit => 1,   :default => "1"
    t.string   "leadstatus",         :limit => 1,   :default => "0"
    t.string   "leademail",          :limit => 1,   :default => "0"
    t.integer  "onlinelistings",                    :default => 0
    t.datetime "lastlogin"
    t.datetime "currentlogin"
    t.string   "website",                           :default => ""
    t.string   "activatecode",       :limit => 20,  :default => ""
  end

  create_table "xmllistings", :force => true do |t|
    t.string   "title",                       :default => ""
    t.string   "url",                         :default => ""
    t.string   "sql",                         :default => ""
    t.string   "fsalerent",                   :default => ""
    t.string   "flistingtype",                :default => ""
    t.string   "fuser",                       :default => ""
    t.string   "flocation",                   :default => ""
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "flimit",                      :default => 20
    t.string   "ftypeshow",    :limit => 100, :default => "random"
    t.string   "link",                        :default => "http://www.rethai.com/rethai"
  end

end
