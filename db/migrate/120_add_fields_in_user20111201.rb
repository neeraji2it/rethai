class AddFieldsInUser20111201 < ActiveRecord::Migration
  def self.up
    add_column :users,       :activatecode,     :string,  :limit=>20,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :users,       :activatecode
  end 
end