class AddnewfieldInFavorites20090703< ActiveRecord::Migration
  def self.up
    add_column :favorites,    :lat,        :string,   :limit=>30,      :null => true,   :default=>'0'
    add_column :favorites,    :lng,        :string,   :limit=>30,      :null => true,   :default=>'0'
  end

  def self.down
    remove_column :favorites,    :lat
    remove_column :favorites,    :lng
  end
end