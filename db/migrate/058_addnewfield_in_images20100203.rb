class AddnewfieldInImages20100203< ActiveRecord::Migration
  def self.up
    add_column :images,    :paperclipfix,            :string,      :limit=>1,       :null => true,     :default=> '0'
  end

  def self.down
     remove_column :images,    :paperclipfix
  end
end
