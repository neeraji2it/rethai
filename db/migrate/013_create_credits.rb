class CreateCredits < ActiveRecord::Migration
  def self.up
    create_table :credits do |t|
      t.column :id_user,        :integer,                                     :default=> '0'
      t.column :FullCredit,     :integer,                                     :default=> '0'
      t.column :RemainCredit,   :integer,                                     :default=> '0'
      t.column :Price,          :integer,                                     :default=> '0'
      t.column :TXN_ID,         :string,      :limit=>20,   :null => true,    :default=> '0'
      t.column :Description,    :string,      :limit=>255,  :null => true,    :default=> '0'
      t.column :Status,         :string,      :limit=>1,    :null => true,    :default=> 'p'
      t.column :created_on,     :timestamp,                 :null => true 
      t.column :updated_on,     :timestamp,                 :null => true ,   :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :credits
  end
end
