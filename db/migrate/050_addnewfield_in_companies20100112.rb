class AddnewfieldInCompanies20100112< ActiveRecord::Migration
  def self.up
    add_column :companies,    :Address,           :text,      :null => true,   :default=> ''
  end

  def self.down
    remove_column :companies, :Address
  end

end
