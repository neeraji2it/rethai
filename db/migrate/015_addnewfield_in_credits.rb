class AddnewfieldInCredits< ActiveRecord::Migration
  def self.up
    add_column :credits,    :SubscrId,          :string,   :limit=>30,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :credits,    :SubscrId
  end
end