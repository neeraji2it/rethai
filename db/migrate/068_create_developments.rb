class CreateDevelopments < ActiveRecord::Migration
  def self.up
    create_table :developments  do |t|
      t.column :id_user,                :int,                       :null => true,  :default=>"0"
      t.column :id_image,               :int,                       :null => true,  :default=>"0"
      t.column :id_Type,                :int,                       :null => true,  :default=>"0"
      t.column :Type_Name,              :string,    :limit=>100,    :null => true,  :default=>""
      t.column :City,                   :string,    :limit=>100,    :null => true,  :default=>""
      t.column :Name,                   :string,    :limit=>100,    :null => true,  :default=>""
      t.column :Url,                    :string,    :limit=>100,    :null => true,  :default=>""
      t.column :HomepageFeatured,       :string,    :limit=>1,      :null => true,  :default=>"1"
      t.column :Views,                  :int,                       :null => true,  :default=>"0"
      t.column :DevelopmentHtml,        :text,                      :null => true,  :default=>""
      t.column :status,                 :string,    :limit=>1,      :null => true,  :default=>"1"
      t.column :lat,                    :string,    :limit=>20,     :null => true,  :default=>"12.9357"
      t.column :lng,                    :string,    :limit=>20,     :null => true,  :default=>"100.889"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :developments
  end
end
