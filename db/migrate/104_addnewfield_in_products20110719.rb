class AddnewfieldInProducts20110719 < ActiveRecord::Migration
  def self.up
    add_column :products,       :youtubecode,       :string,    :limit=>255,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :products,       :youtubecode
  end   
end
