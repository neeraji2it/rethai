class CreateRethaiapis < ActiveRecord::Migration
  def self.up
    create_table :rethaiapis  do |t|
      t.column :id_user,         :int,                                :null => true,  :default=>"0"
      t.column :SiteName,        :string,       :limit=>'255',        :null => true,  :default=>""
      t.column :APItype,         :string,       :limit=>'20',         :null => true,  :default=>"addlisting"
      t.column :APIKey,          :string,       :limit=>'60',         :null => true,  :default=>"0"
      t.column :IP,              :string,       :limit=>'50',         :null => true,  :default=>"0"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :rethaiapis
  end
end
