class CreateCondos< ActiveRecord::Migration
  def self.up
    create_table :condos do |t|
      t.column :name,             :string,    :limit=>255,    :null=>true, :default=>""
      t.column :id_image,         :int,                       :null=>true, :default=>"0"
      t.column :city,             :string,    :limit=>100,    :null=>true, :default=>""
      t.column :lat,              :string,    :limit=>50,     :null=>true, :default=>"12.9357"
      t.column :lng,              :string,    :limit=>50,     :null=>true, :default=>"100.889"
      t.column :homepage,         :string,    :limit=>1,      :null=>true, :default=>"0"
      t.column :lowprice,         :int,                       :null=>true, :default=>"0"
      t.column :heightprice,      :int,                       :null=>true, :default=>"0"
      t.column :status,           :string,    :limit=>1,      :null=>true, :default=>"1"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :condos
  end
end