class CreateLeadrecords < ActiveRecord::Migration
  def self.up
    create_table :leadrecords do |t|
      t.column :id_user,         :int,                                :null => true,    :default=>"0"
      t.column :leadtype,        :string,       :limit=>'2',          :null => true,    :default=>"1"
      t.column :month,           :date,                               :null => true,    :default=>DateTime.now
      t.column :lstatus,         :string,       :limit=>'1',          :null => true,    :default=>"1"

      
      t.timestamps
    end
  end

  def self.down
    drop_table :leadrecords
  end   
end
