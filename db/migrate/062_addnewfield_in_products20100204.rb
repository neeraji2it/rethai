class AddnewfieldInProducts20100204< ActiveRecord::Migration
  def self.up
    add_column :products,    :magazine,            :string,      :limit=>1,       :null => true,     :default=> '0'
    add_column :products,    :magazinedate,        :date,        :null => true
  end

  def self.down
     remove_column :products,    :magazine
     remove_column :products,    :magazinedate
  end
end
