class AddWebsiteInUsers < ActiveRecord::Migration
  def self.up
    add_column :users,       :website,     :string,      :limit=>255,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :users,       :website
  end 
end