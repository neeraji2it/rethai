class AddnewfieldInXmllistings20091130< ActiveRecord::Migration
  def self.up
    add_column :xmllistings,    :flimit,           :int,    :null => true, :default=>"20"
  end

  def self.down
    remove_column :xmllistings,    :flimit
  end

end
