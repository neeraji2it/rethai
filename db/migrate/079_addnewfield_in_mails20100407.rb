class AddnewfieldInMails20100407< ActiveRecord::Migration
  def self.up
    add_column :mails,    :toEmail,   :string,        :limit=>150,                   :null => true,     :default=> ''
  end

  def self.down
     remove_column :mails,    :toEmail
  end   
end
