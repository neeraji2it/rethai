class CreateAgentconnects < ActiveRecord::Migration
  def self.up
    create_table :agentconnects do |t|
      t.column :id_user,         :int,                                :null => true,    :default=>"0"
      t.column :salerent,        :string,       :limit=>'2',          :null => true,    :default=>"0"
      t.column :ttype,            :string,       :limit=>'10',         :null => true,    :default=>"0"
      t.column :location,        :string,       :limit=>'255',        :null => true,    :default=>"0"
      t.column :sprice,          :int,          :null => true,  :default=>"0"
      t.column :fprice,          :int,          :null => true,  :default=>"0"
      t.column :sbed,            :int,          :null => true,  :default=>"0"
      t.column :fbed,            :int,          :null => true,  :default=>"0"
      t.column :bath,            :int,          :null => true,  :default=>"0"
      t.column :details,         :text,         :null => true
      t.column :agent_history,   :text,         :null => true
      t.timestamps
    end
  end

  def self.down
    drop_table :agentconnects
  end   
end
