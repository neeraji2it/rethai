class CreateFavorites< ActiveRecord::Migration
  def self.up
    create_table :favorites do |t|
       t.column :id_user,     :string,      :limit=>30,   :null => true,   :default=> '0'
       t.column :id_product,  :string,      :limit=>30,   :null => true,   :default=> '0'
       t.column :comment,     :string,      :limit=>255,  :null => true
       t.column :link,        :string,      :limit=>255,  :null => true,   :default=> '0'
       t.column :Deleted,     :string,      :limit=>1,    :null => true,   :default=> '1'
       t.column :created_on,  :timestamp,                 :null => false 
       t.column :updated_on,  :timestamp,                 :null => false 
    end
  end

  def self.down
    drop_table :favorites
  end
end