class AddnewfieldInCompanies20100318 < ActiveRecord::Migration
  def self.up
    add_column :companies,    :city,           :string,     :limit=>100,      :null => true,      :default=>""
  end

  def self.down
     remove_column :companies,    :city
  end
end
