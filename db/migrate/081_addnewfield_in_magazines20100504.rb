class AddnewfieldInMagazines20100504< ActiveRecord::Migration
  def self.up
    add_column :magazineoptions ,    :listingcredit,     :int,       :null => true,     :default=> '1'
    add_column :magazines ,          :listingcredit,     :int,       :null => true,     :default=> '1'
  end

  def self.down
     remove_column :magazineoptions ,     :listingcredit
     remove_column :magazines ,           :listingcredit
  end   
end
