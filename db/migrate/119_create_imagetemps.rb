class CreateImagetemps < ActiveRecord::Migration
  def self.up
    create_table :imagetemps do |t|
       t.column :id_user,             :int,               :null => true,   :default=> '0'
       t.column :name,      :string,      :limit=>255,    :null => true,   :default=> ''
       t.column :path,      :string,      :limit=>255,    :null => true,   :default=> ''
       
       t.column :created_on,     :timestamp,                 :null => false 
       t.column :updated_on,     :timestamp,                 :null => false ,  :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :imagetemps
  end
end
