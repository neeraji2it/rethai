class AddnewfieldInLevels20091223< ActiveRecord::Migration
  def self.up
    add_column :levelshistories,    :Approve,           :string,      :limit=>1,         :null => true,   :default=> '0'
    add_column :levelshistories,    :LevelName,         :string,      :limit=>50,         :null => true,   :default=> ''
  end

  def self.down
    remove_column :levelshistories,    :Approve
    remove_column :levelshistories,    :LevelName
  end

end
