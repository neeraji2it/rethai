class AddsortfieldInTemplistings20091203< ActiveRecord::Migration
  def self.up
    add_column :templistings,    :command,           :int,    :null => true, :default=>"1"
  end

  def self.down
    remove_column :templistings,    :command
  end

end
