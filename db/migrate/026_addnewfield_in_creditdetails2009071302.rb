class AddnewfieldInCreditdetails2009071302< ActiveRecord::Migration
  def self.up
    add_column :creditdetails,    :command_type,        :string,   :limit=>2,      :null => true,   :default=>'0'
  end

  def self.down
    remove_column :creditdetails,    :command_type
  end
end