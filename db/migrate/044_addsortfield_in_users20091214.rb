class AddsortfieldInUsers20091214< ActiveRecord::Migration
  def self.up
    add_column :users,    :partner,           :string,   :limit=>100, :null => true, :default=>""
  end

  def self.down
    remove_column :users,    :partner
  end

end
