class AddnewfieldInSubscriptions20100204< ActiveRecord::Migration
  def self.up
    add_column :subscriptions,       :magazine,           :int,   :null => true,   :default=> '0'
    
  end

  def self.down
    remove_column :subscriptions,    :magazine
  end

end
