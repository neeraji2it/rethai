class AddnewfieldInSubscriptions20100113< ActiveRecord::Migration
  def self.up
    add_column :subscriptions,    :PortalPage,           :string,   :limit=>1,   :null => true,   :default=> '0'
    add_column :subscriptions,    :SEOListingPage,       :string,   :limit=>1,   :null => true,   :default=> '0'
    
    add_column :subscriptions,    :SearchFeatured,      :int,   :null => true,   :default=> '0'
    add_column :subscriptions,    :MapFeatured,         :int,   :null => true,   :default=> '0'
    add_column :subscriptions,    :HomepageFeatured,    :int,   :null => true,   :default=> '0'
    add_column :subscriptions,    :EmailFeatured,       :int,   :null => true,   :default=> '0'
    
  end

  def self.down
    remove_column :subscriptions,    :PortalPage
    remove_column :subscriptions,    :SEOListingPage
    
    remove_column :subscriptions,    :SearchFeatured
    remove_column :subscriptions,    :MapFeatured
    remove_column :subscriptions,    :HomepageFeatured
    remove_column :subscriptions,    :EmailFeatured
  end

end
