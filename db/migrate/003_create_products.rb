class CreateProducts < ActiveRecord::Migration
  def self.up
    create_table :products do |t|
      t.column :id_user,          :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :City,             :string,      :limit=>50,   :null => true,   :default=> '0'
      t.column :Price,            :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :Bed,              :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :Bath,             :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :Detail,           :text,        :null => true
      t.column :Type,             :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :Parking,          :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :Deleted,          :string,      :limit=>1,   :null => true,   :default=> '1'
      t.column :lat,              :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :lng,              :string,      :limit=>20,   :null => true,   :default=> '0'
      t.column :created_on,     :timestamp,                 :null => true 
      t.column :updated_on,     :timestamp,                 :null => true ,  :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :products
  end
end
