class AddnewfieldInProducts20100113< ActiveRecord::Migration
  def self.up
    
    add_column :products,    :SearchFeatured,      :string,  :limit=>1,    :null => true,   :default=> '0'
    add_column :products,    :MapFeatured,         :string,  :limit=>1,    :null => true,   :default=> '0'
    add_column :products,    :HomepageFeatured,    :string,  :limit=>1,    :null => true,   :default=> '0'
    add_column :products,    :EmailFeatured,       :string,  :limit=>1,    :null => true,   :default=> '0'
    
  end

  def self.down
    remove_column :products,    :SearchFeatured
    remove_column :products,    :MapFeatured
    remove_column :products,    :HomepageFeatured
    remove_column :products,    :EmailFeatured
  end

end
