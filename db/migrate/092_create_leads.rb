class CreateLeads < ActiveRecord::Migration
  def self.up
    create_table :leads do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :messages
      t.string :ltype
      t.string :salerent
      t.string :city
      t.integer :sprice
      t.integer :fprice
      t.integer :sbed
      t.integer :fbed
      t.integer :bath
      t.text :user_history
      t.timestamps
    end
  end

  def self.down
    drop_table :leads
  end
end
