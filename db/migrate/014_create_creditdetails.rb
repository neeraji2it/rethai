class CreateCreditdetails < ActiveRecord::Migration
  def self.up
    create_table :creditdetails do |t|
      t.column :id_user,       :integer,                                     :default=> '0'
      t.column :id_contact,    :integer,                   :null => true,    :default=> '0'
      t.column :ContactName,   :string,      :limit=>20,   :null => true,    :default=> '0'
      t.column :ContactNo,     :integer,     :limit=>20,   :null => true,    :default=> '0'
      t.column :SmsDetail,     :string,      :limit=>255,  :null => true,    :default=> '0'
      t.column :credit,        :float,                     :null => true,    :default=> '0'
      t.column :TXNID,         :string,      :limit=>50,   :null => true,    :default=> '0'
      t.column :created_on,    :timestamp,                 :null => true 
      t.column :updated_on,    :timestamp,                 :null => true ,   :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :creditdetails
  end
end
