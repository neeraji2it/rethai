class AddnewfieldInMails20090703< ActiveRecord::Migration
  def self.up
    add_column :mails,    :lat,        :string,   :limit=>30,      :null => true,   :default=>'0'
    add_column :mails,    :lng,        :string,   :limit=>30,      :null => true,   :default=>'0'
  end

  def self.down
    remove_column :mails,    :lat
    remove_column :mails,    :lng
  end
end