class AddsortfieldInXmllistings20091130< ActiveRecord::Migration
  def self.up
    add_column :xmllistings,    :ftypeshow,           :string,      :limit=>100,    :null => true, :default=>"random"
  end

  def self.down
    remove_column :xmllistings,    :ftypeshow
  end

end
