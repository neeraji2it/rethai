class AddnewfieldInProducts20091111< ActiveRecord::Migration
  def self.up
    add_column :products,    :counter,          :int,         :null => true,   :default=> '0'
  end

  def self.down
    remove_column :products,    :counter
  end

end
