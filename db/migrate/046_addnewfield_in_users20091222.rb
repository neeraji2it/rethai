class AddnewfieldInUsers20091222< ActiveRecord::Migration
  def self.up
    add_column :users,    :level,           :string,      :limit=>1,         :null => true,   :default=> '1'
  end

  def self.down
    remove_column :users,    :level
  end

end
