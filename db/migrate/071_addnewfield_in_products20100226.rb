class AddnewfieldInProducts20100226< ActiveRecord::Migration
  def self.up
    add_column :products,    :APIListingID,     :int,       :null => true,     :default=> '0'
    add_column :products,    :APIUserID,        :int,       :null => true,     :default=> '0'
  end

  def self.down
     remove_column :products,    :APIListingID
     remove_column :products,    :APIUserID
  end
end
