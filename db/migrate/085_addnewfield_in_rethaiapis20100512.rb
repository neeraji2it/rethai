class AddnewfieldInRethaiapis20100512< ActiveRecord::Migration
  def self.up
    add_column :rethaiapis,    :codeURL,    :string,       :limit=>255,       :null => true,     :default=> ''
    add_column :rethaiapis,    :manage,     :string,       :limit=>10,        :null => true,     :default=> 'daily'
    add_column :rethaiapis,    :DOW,        :string,       :limit=>10,        :null => true,     :default=> ''
    add_column :rethaiapis,    :DOM,        :int,                             :null => true,     :default=> '0'
    add_column :rethaiapis,    :lastupdate, :date,                      :null => true,     :default=> DateTime.now - 1
  end

  def self.down
      remove_column :rethaiapis,    :codeURL
      remove_column :rethaiapis,    :manage
      remove_column :rethaiapis,    :DOW
      remove_column :rethaiapis,    :DOM
      remove_column :rethaiapis,    :lastupdate
  end   
end
