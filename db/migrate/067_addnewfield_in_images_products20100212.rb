class AddnewfieldInImagesProducts20100212< ActiveRecord::Migration
  def self.up
    add_column :images,       :id_condo,            :int,       :null => true,     :default=> '0'
    add_column :products,     :id_condo,            :int,       :null => true,     :default=> '0'
    add_column :products,     :condo_history,       :string,      :limit=>100,     :null => true,     :default=> ''
  end

  def self.down
    remove_column :images,        :id_condo
    remove_column :products,      :id_condo
    remove_column :products,      :condo_history
  end
end
