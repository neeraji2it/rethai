class CreateBackgroundcontrols < ActiveRecord::Migration
  def self.up
    create_table :backgroundcontrols do |t|
      t.column :run_control,         :string,      :limit=>1,     :null => false,    :default=> '1'
    end
    
  end

  def self.down
    drop_table :backgroundcontrols
  end
end

