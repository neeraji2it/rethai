class CreateImages < ActiveRecord::Migration
  def self.up
    create_table :images do |t|
       t.column :id_user,             :string,      :limit=>20,   :null => false,   :default=> '0'
       t.column :id_product,          :string,      :limit=>30,   :null => false,   :default=> '0'
       t.column :photo_file_name,     :string,      :limit=>255,  :null => false,   :default=> '0'
       t.column :photo_content_type,  :string,      :limit=>20,   :null => false,   :default=> '0'
       t.column :photo_file_size,     :string,      :limit=>20,   :null => false,   :default=> '0'
       t.column :file_type,           :string,      :limit=>10,   :null => false,   :default=> 'image'
       t.column :created_on,     :timestamp,                 :null => false 
       t.column :updated_on,     :timestamp,                 :null => false ,  :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :images
  end
end
