class CreateCompanies< ActiveRecord::Migration
  def self.up
    create_table :companies do |t|
       t.column :id_user,     :integer,                 :null => true,   :default=> '0'
       t.column :logo,        :string,      :limit=>50, :null => true
       t.column :Detail,      :text,                    :null => true
       t.column :Services,    :text,                    :null => true
       t.column :Name,        :string,      :limit=>50, :null => true
       t.column :phone,       :string,      :limit=>30, :null => true
       t.column :Email,       :string,      :limit=>50, :null => true
       t.column :Url,         :string,      :limit=>100,:null => true
       t.column :brokerNo,    :string,      :limit=>30, :null => true
       t.column :Years,       :integer,                 :null => true,   :default=> '0'
       
       
       t.column :Head,        :string,      :limit=>30, :null => true,   :default=> '0'
       t.column :lat,         :string,      :limit=>30, :null => true,   :default=> '0'
       t.column :lng,         :string,      :limit=>30, :null => true,   :default=> '0'
       t.column :created_on,  :timestamp,               :null => false 
       t.column :updated_on,  :timestamp,               :null => false 
    end
  end

  def self.down
    drop_table :companies
  end
end