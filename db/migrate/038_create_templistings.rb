class CreateTemplistings < ActiveRecord::Migration
  def self.up
    create_table :templistings do |t|
      t.column :sessionID,                  :string,      :limit=>255,    :null => true, :default=>""
      t.column :listingID,                  :text,                        :null => true, :default=>""
      
      t.timestamps
    end
  end

  def self.down
    drop_table :templistings
  end
end
