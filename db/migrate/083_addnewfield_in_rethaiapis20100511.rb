class AddnewfieldInRethaiapis20100511< ActiveRecord::Migration
  def self.up
    add_column :rethaiapis,    :APILink,     :string,       :limit=>255,        :null => true,     :default=> ''
  end

  def self.down
     remove_column :rethaiapis,    :APILink
  end   
end
