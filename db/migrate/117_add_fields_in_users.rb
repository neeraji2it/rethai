class AddFieldsInUsers < ActiveRecord::Migration
  def self.up
    add_column :users,       :onlinelistings,     :int,        :null => true,     :default=> '0'
    add_column :users,       :lastlogin,          :datetime,       :null => true
    add_column :users,       :currentlogin,       :datetime,       :null => true
  end

  def self.down
    remove_column :users,       :onlinelistings
    remove_column :users,       :lastlogin
    remove_column :users,       :currentlogin
  end 
end