class AddnewfieldInLeadhistories20101223 < ActiveRecord::Migration
  def self.up
    add_column :leadhistories,    :id_listing,    :int,        :null => true,     :default=> '0'
  end

  def self.down
      remove_column :leadhistories,    :id_listing
  end   
end
