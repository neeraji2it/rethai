class AddnewfieldInProducts20110830 < ActiveRecord::Migration
  def self.up
    add_column :products,       :priceorder,       :string,    :limit=>10,        :null => true,     :default=> '1'
    add_column :products,       :imageorder,       :string,    :limit=>10,        :null => true,     :default=> '1'
  end

  def self.down
    remove_column :products,       :priceorder
    remove_column :products,       :imageorder
  end   
end
