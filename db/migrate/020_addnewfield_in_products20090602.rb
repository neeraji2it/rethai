class AddnewfieldInProducts20090602< ActiveRecord::Migration
  def self.up
    add_column :products,    :Status,        :string,   :limit=>1,      :null => true
    add_column :products,    :expiredate,    :date,                     :null => true
  end

  def self.down
    remove_column :products,    :Status
    remove_column :products,    :expiredate
  end
end