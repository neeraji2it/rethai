class CreateRethaiapihistories < ActiveRecord::Migration
  def self.up
    create_table :rethaiapihistories  do |t|
      t.column :id_user,                :int,                       :null => true,  :default=>"0"
      t.column :url,                    :text,      :limit=>255,    :null => true,  :default=>""
      
      t.timestamps
    end
  end

  def self.down
    drop_table :rethaiapihistories
  end
end
