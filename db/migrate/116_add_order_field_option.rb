class AddOrderFieldOption < ActiveRecord::Migration
  def self.up
    add_column :options,       :opt_order,       :int,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :options,    :opt_order
  end 
end