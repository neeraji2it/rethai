class AddnewfieldInListingcounters20110202 < ActiveRecord::Migration
  def self.up
    add_column :listingcounters,    :month1,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month2,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month3,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month4,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month5,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month6,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month7,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month8,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month9,    :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month10,   :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month11,   :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :month12,   :int,        :null => true,     :default=> '0'
    add_column :listingcounters,    :currentmonth, :int,     :null => true,     :default=> '0'
  end

  def self.down
    remove_column :listingcounters,    :month1,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month2,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month3,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month4,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month5,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month6,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month7,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month8,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month9,    :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month10,   :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month11,   :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :month12,   :int,        :null => true,     :default=> '0'
    remove_column :listingcounters,    :currentmonth, :int,     :null => true,     :default=> '0'
  end   
end
