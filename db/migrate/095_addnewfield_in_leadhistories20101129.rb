class AddnewfieldInLeadhistories20101129 < ActiveRecord::Migration
  def self.up
    add_column :leadhistories,    :conect,    :string,       :limit=>10,        :null => true,     :default=> '1'
    add_column :leadhistories,    :details,   :text,                            :null => true       
  end

  def self.down
      remove_column :leadhistories,    :conect
      remove_column :leadhistories,    :details
  end   
end
