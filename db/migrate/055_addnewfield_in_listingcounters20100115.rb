class AddnewfieldInListingcounters20100115< ActiveRecord::Migration
  def self.up
    
    add_column :listingcounters,    :id_user,      :int,    :null => true,   :default=> '0'
    
  end

  def self.down
    remove_column :listingcounters,    :user_id
  end

end
