class AddnewfieldInProducts20100507< ActiveRecord::Migration
  def self.up
    add_column :products,    :Virtual,     :string,       :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
     remove_column :products,    :Virtual
  end   
end
