class AddnewfieldInListingconects20110829 < ActiveRecord::Migration
  def self.up
    add_column :listingconects,       :approve,       :string,    :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :listingconects,       :approve
  end   
end
