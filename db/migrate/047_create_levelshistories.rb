class CreateLevelshistories< ActiveRecord::Migration
  def self.up
    create_table :levelshistories do |t|
      t.column :id_user,        :int,                    :null=>true, :default=>"0"
      t.column :UserName,       :string, :limit=>50,     :null=>true, :default=>""
      t.column :Email,          :string, :limit=>100,    :null=>true, :default=>""
      t.column :Phone,          :string, :limit=>50,     :null=>true, :default=>""
      t.column :OldLevel,       :string, :limit=>1,      :null=>true, :default=>"0"
      t.column :Level,          :string, :limit=>1,      :null=>true, :default=>"0"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :levelshistories
  end
end