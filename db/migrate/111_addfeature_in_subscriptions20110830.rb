class AddfeatureInSubscriptions20110830 < ActiveRecord::Migration
  def self.up
    add_column :subscriptions,       :HomeFeatured,       :int,        :null => true,     :default=> '0'
    add_column :subscriptions,       :CondoFeatured,      :int,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :subscriptions,       :HomeFeatured
    remove_column :subscriptions,       :CondoFeatured
  end   
end
