class CreateMagazineoptions < ActiveRecord::Migration
  def self.up
    create_table :magazineoptions  do |t|
      t.column :name,                :string,    :limit=>50,     :null => true,  :default=>""
      t.column :photos,              :int,                       :null => true,  :default=>"1"
      t.column :price,               :int,                       :null => true,  :default=>"0"
      
    end
  end

  def self.down
    drop_table :magazineoptions
  end
end
