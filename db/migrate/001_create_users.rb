class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
  t.column :FirstName,      :string,      :limit=>50,     :null => true
  t.column :LastName,       :string,      :limit=>50,     :null => true
  t.column :UserName,       :string,      :limit=>20,     :null => true
  t.column :Password,       :string,      :limit=>20,     :null => true
  t.column :Email,          :string,      :limit=>50,     :null => true
  t.column :Phone,          :string,      :limit=>20,     :null => true
  t.column :Address,        :string,      :limit=>255,     :null => true
  t.column :Suburb,         :string,      :limit=>255,    :null => true  
  t.column :Province,       :string,      :limit=>20,     :null => true
  t.column :PostCode,       :string,      :limit=>20,     :null => true
  t.column :UserType,       :string,      :limit=>1,      :null => true,     :default=> 'p'#p=personal, d = dealer,a=admin
  t.column :Deleted,        :string,      :limit=>1,      :null => true,     :default=> '1'
  t.column :SMS,            :string,      :limit=>1,      :null => true,     :default=> '0'
  t.column :created_on,     :timestamp ,  :null => false 
  t.column :updated_on,     :timestamp ,  :null => false ,:default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :users
  end
end
