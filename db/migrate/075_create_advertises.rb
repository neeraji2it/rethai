class CreateAdvertises< ActiveRecord::Migration
  def self.up
    create_table :advertises do |t|
      t.column :city,             :string,    :limit=>100,    :null=>true, :default=>""
      t.column :ScriptText,       :text,                      :null=>true
      t.column :status,           :string,    :limit=>1,      :null=>true, :default=>"1"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :advertises
  end
end