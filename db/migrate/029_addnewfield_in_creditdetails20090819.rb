class AddnewfieldInCreditdetails20090819< ActiveRecord::Migration
  def self.up
    add_column :creditdetails,    :remains,          :float,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :creditdetails,    :remains
  end

end
