class AddnewfieldInCompanies20100113< ActiveRecord::Migration
  def self.up
    add_column :companies,    :logo2,           :int,      :null => true,   :default=> '0'
    add_column :companies,    :logo3,           :int,      :null => true,   :default=> '0'
    
  end

  def self.down
    remove_column :companies,     :logo2
    remove_column :companies,     :logo3
  end

end
