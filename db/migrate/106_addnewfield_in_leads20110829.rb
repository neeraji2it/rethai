class AddnewfieldInLeads20110829 < ActiveRecord::Migration
  def self.up
    add_column :leads,       :approve,       :string,    :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :leads,       :approve
  end   
end
