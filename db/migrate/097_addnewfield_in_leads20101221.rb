class AddnewfieldInLeads20101221 < ActiveRecord::Migration
  def self.up
    add_column :leads,    :id_user,    :int,        :null => true,     :default=> '0'
    add_column :leads,    :parking,    :int,        :null => true,     :default=> '0'
  end

  def self.down
      remove_column :leads,    :id_user
      remove_column :leads,    :parking
  end   
end
