class AddnewfieldInUsers20090409< ActiveRecord::Migration
  def self.up
    add_column :users,    :email_newmessage,        :string,   :limit=>1,      :null => true,     :default=> '1'
    add_column :users,    :sms_newmessage,          :string,   :limit=>1,      :null => true,     :default=> '0'
    add_column :users,    :sms_partnermedia,        :string,   :limit=>1,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :users,    :email_newmessage
    remove_column :users,    :sms_newmessage
    remove_column :users,    :sms_partnermedia
  end
end