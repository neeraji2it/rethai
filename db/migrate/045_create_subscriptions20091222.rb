class CreateSubscriptions20091222< ActiveRecord::Migration
  def self.up
    create_table :subscriptions do |t|
      t.column :Name,         :string, :limit=>50,:null=>true
      
      t.column :Price,            :int,               :null=>true
      t.column :Report,           :string, :limit=>1, :null=>true
      t.column :PerformanceState, :string, :limit=>50,:null=>true
      t.column :Photo,            :int,               :null=>true
      t.column :Listings,         :int,               :null=>true
      
      
      t.column :Upgrades,       :int,               :null=>true
      t.column :Enchanced,      :string, :limit=>1, :null=>true
      t.column :ListingContact, :string, :limit=>1, :null=>true
      t.column :Apisites,       :string, :limit=>1, :null=>true
      t.column :Xmlsites,       :string, :limit=>1, :null=>true
      
      
      t.column :BusinessDetail,     :string, :limit=>1, :null=>true
      t.column :SpotlightAdvert,    :string, :limit=>1, :null=>true
      t.column :RealEstateData,     :string, :limit=>50,:null=>true
      t.column :WebsiteIntegration, :string, :limit=>1, :null=>true
      t.column :SMS,                :int,    :limit=>50,   :null=>true
    end
  end

  def self.down
    drop_table :subscriptions
  end
end