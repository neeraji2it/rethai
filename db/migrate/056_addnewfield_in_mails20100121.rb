class AddnewfieldInMails20100121< ActiveRecord::Migration
  def self.up
    add_column :mails,    :status,            :string,      :limit=>3,       :null => true,     :default=> '1'
  end

  def self.down
     remove_column :mails,    :status
  end
end
