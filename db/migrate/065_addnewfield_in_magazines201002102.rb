class AddnewfieldInMagazines201002102< ActiveRecord::Migration
  def self.up
    add_column :magazines,       :month,          :date,   :null => true
  end

  def self.down
    remove_column :magazines,    :month
  end
end
