class AddnewfieldInUsers20090516< ActiveRecord::Migration
  def self.up
    add_column :users,    :email_newsletter,        :string,   :limit=>1,      :null => true,     :default=> '1'
    add_column :users,    :email_weeklyalerts,      :string,   :limit=>1,      :null => true,     :default=> '1'
    add_column :users,    :email_partnermedia,      :string,   :limit=>1,      :null => true,     :default=> '1'
  end

  def self.down
    remove_column :users,    :email_newsletter
    remove_column :users,    :email_weeklyalerts
    remove_column :users,    :email_partnermedia
  end
end