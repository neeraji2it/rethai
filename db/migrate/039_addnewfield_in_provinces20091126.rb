class AddnewfieldInProvinces20091126< ActiveRecord::Migration
  def self.up
    add_column :provinces,    :lat,           :string,      :limit=>50,         :null => true,   :default=> '0'
    add_column :provinces,    :lng,           :string,      :limit=>50,         :null => true,   :default=> '0'
    add_column :provinces,    :mt,            :int,                             :null => true,   :default=> '100'
  end

  def self.down
    remove_column :provinces,    :lat
    remove_column :provinces,    :lng
    remove_column :provinces,    :mt
  end

end
