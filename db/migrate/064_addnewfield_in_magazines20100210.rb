class AddnewfieldInMagazines20100210< ActiveRecord::Migration
  def self.up
    add_column :magazines,       :status,          :string,   :limit=>2,       :null => true,     :default=> '1'
  end

  def self.down
    remove_column :magazines,    :status
  end
end
