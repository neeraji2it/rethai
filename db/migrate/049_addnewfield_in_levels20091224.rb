class AddnewfieldInLevels20091224< ActiveRecord::Migration
  def self.up
    add_column :levelshistories,    :Listings,           :string,      :limit=>1,         :null => true,   :default=> '5'
  end

  def self.down
    remove_column :levelshistories,    :Listings
  end

end
