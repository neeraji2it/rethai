class CreateRssfeeds < ActiveRecord::Migration
  def self.up
    create_table :rssfeeds do |t|
      t.column :title,                  :string,      :limit=>255,    :null => true, :default=>""
      t.column :url,                    :string,      :limit=>255,    :null => true, :default=>""
      t.column :sql,                    :string,      :limit=>255,    :null => true, :default=>""
      t.column :fsalerent,              :string,      :limit=>255,    :null => true, :default=>""
      t.column :flistingtype,           :string,      :limit=>255,    :null => true, :default=>""
      t.column :fuser,                  :string,      :limit=>255,    :null => true, :default=>""
      t.column :flocation,              :string,      :limit=>255,    :null => true, :default=>""
      t.column :flimit,                 :int,                         :null => true, :default=>"20"
      t.column :ftypeshow,              :string,      :limit=>100,    :null => true, :default=>"random"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :rssfeeds
  end
end
