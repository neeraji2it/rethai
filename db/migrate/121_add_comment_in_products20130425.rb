class AddCommentInProductions20130425 < ActiveRecord::Migration
  def self.up
    add_column :products,       :comment,     :text,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :products,       :comment
  end 
end