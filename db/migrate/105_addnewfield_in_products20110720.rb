class AddnewfieldInProducts20110720 < ActiveRecord::Migration
  def self.up
    add_column :products,       :titlelink,       :string,    :limit=>255,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :products,       :titlelink
  end   
end
