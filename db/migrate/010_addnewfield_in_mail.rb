class AddnewfieldInMail< ActiveRecord::Migration
  def self.up
    add_column :mails,    :customer,            :integer,                 :null => true,     :default=> '0'
    add_column :mails,    :master,              :integer,                 :null => true,     :default=> '0'
    add_column :mails,    :custflag,            :string,  :limit=>1,    :null => true,     :default=> '0'
    add_column :mails,    :userflag,            :string,  :limit=>1,    :null => true,     :default=> '1'
  end

  def self.down
    remove_column :mails,    :customer
    remove_column :mails,    :master
    remove_column :mails,    :custflag
    remove_column :mails,    :userflag
  end
end