class CreateBjqueues < ActiveRecord::Migration
  def self.up
    create_table :bjqueues do |t|
      t.column :apiID,         :string,      :limit=>30,     :null => false,    :default=> '0'
      t.column :status,        :string,      :limit=>1,      :null => false,    :default=> '1'
    end
  end

  def self.down
    drop_table :apiID
    drop_table :status
  end
end

