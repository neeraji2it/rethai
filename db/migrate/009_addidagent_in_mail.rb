class AddidagentInMail< ActiveRecord::Migration
  def self.up
    add_column :mails,    :id_agent,            :string,      :limit=>30,       :null => true,     :default=> '0'
  end

  def self.down
     remove_column :mails,    :id_agent
  end
end