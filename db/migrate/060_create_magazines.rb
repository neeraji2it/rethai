class CreateMagazines < ActiveRecord::Migration
  def self.up
    create_table :magazines  do |t|
      t.column :id_user,                :int,                       :null => true,  :default=>"0"
      t.column :id_product,             :int,                       :null => true,  :default=>"0"
      t.column :id_template,            :int,                       :null => true,  :default=>"0"
      t.column :id_photo,               :string,    :limit=>50,     :null => true,  :default=>""
      t.column :details,                :text,                      :null => true,  :default=>""
      t.column :subscrptpay,            :string,    :limit=>1,      :null => true,  :default=>"0"
      t.column :pay,                    :string,    :limit=>1,      :null => true,  :default=>"0"
      
      t.timestamps
    end
  end

  def self.down
    drop_table :magazines
  end
end
