class AddnewfieldInProducts< ActiveRecord::Migration
  def self.up
    add_column :products,    :Images_Url,           :text,      :null => true
  end

  def self.down
     remove_column :products,    :Images_Url
  end
end
