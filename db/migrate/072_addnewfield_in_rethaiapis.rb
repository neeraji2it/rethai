class AddnewfieldInRethaiapis< ActiveRecord::Migration
  def self.up
    add_column :rethaiapis,    :Status,           :string, :limit=>1,      :null => true,     :default=> '1'
  end

  def self.down
     remove_column :rethaiapis,    :Status
  end
end
