class AddnewfieldInAgentconnects20110829 < ActiveRecord::Migration
  def self.up
    add_column :agentconnects,       :approve,       :string,    :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :agentconnects,       :approve
  end   
end
