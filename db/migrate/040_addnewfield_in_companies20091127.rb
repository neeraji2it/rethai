class AddnewfieldInCompanies20091127< ActiveRecord::Migration
  def self.up
    add_column :companies,    :Skype,           :string,      :limit=>100,         :null => true,   :default=> ''
  end

  def self.down
    remove_column :companies,    :Skype
  end

end
