class Addcolumn20090302InProducts< ActiveRecord::Migration
  def self.up    
    add_column :products, :salerent,     :string,      :limit=>1,     :null => true,   :default=> '1' #1=sale,0=rent
    add_column :products, :RPrice,       :integer
    end

  def self.down
    remove_column :products, :salerent,     :string
    remove_column :products, :RPrice,       :integer
  end
end