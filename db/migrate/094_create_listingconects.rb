class CreateListingconects < ActiveRecord::Migration
  def self.up
    create_table :listingconects do |t|
      t.integer :id_user
      t.integer :id_product
      t.text :user_history
      t.timestamps
    end
  end

  def self.down
    drop_table :listingconects
  end
end
