class AddnewfieldInProducts20091016< ActiveRecord::Migration
  def self.up
    add_column :products,    :renew,          :string,      :limit=>1,    :null => true,   :default=> '1'
  end

  def self.down
    remove_column :products,    :renew
  end

end
