class CreateOffers < ActiveRecord::Migration
  def self.up
    create_table :offers do |t|
       t.column :id_product,      :string,      :limit=>30,   :null => true
       t.column :id_productuser,  :string,      :limit=>30,   :null => true
       t.column :id_user,         :string,      :limit=>30,   :null => true
       t.column :offerprice,      :string,      :limit=>30,   :null => true
       t.column :comment,         :text,                      :null => true
       t.column :Deleted,         :string,      :limit=>1,    :null => true,   :default=>'1'
       t.column :Customer,        :string,      :limit=>30,   :null => true,   :default=>'0'
       
       t.column :created_on,      :timestamp,                 :null => false 
       t.column :updated_on,      :timestamp,                 :null => false ,  :default=> '0000-00-00 00:00:00'
       
    end
  end

  def self.down
    drop_table :offers
  end
end
