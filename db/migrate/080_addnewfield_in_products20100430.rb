class AddnewfieldInProducts20100430< ActiveRecord::Migration
  def self.up
    add_column :products,    :Ref,     :string,       :limit=>50,        :null => true,     :default=> ''
  end

  def self.down
     remove_column :products,    :Ref
  end   
end
