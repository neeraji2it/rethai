class AdddefaultCondos < ActiveRecord::Migration
  def self.up
    unless Condo.count > 0
      condo = Condo.new
      condo.name = ""
      condo.city = 'Pattaya'
      condo.status = '0'
      unless condo.save
        puts "Could not create default Condos table:"
            condo.errors.each do |att, m|
              puts "#{att}: #{m}"
            end
      end
    end
  end

  def self.down
  end
end