class CreateBusinesslogos < ActiveRecord::Migration
  def self.up
    create_table :businesslogos do |t|
      t.column :filename,           :string,      :limit=>255,    :null => false
      t.column :content_type,       :string,      :limit=>255,    :null => false
      t.column :size,               :int,                         :null => false
      t.column :width,              :int,                         :null => false
      t.column :height,             :int,                         :null => false
      t.column :parent_id,          :int,                         :null => true
      t.column :thumbnail,          :string,      :limit=>255,    :null => true
      
      t.column :id_user,          :string,      :limit=>255,    :null => true
      t.timestamps
    end
  end

  def self.down
    drop_table :businesslogos
  end
end
