class AdddefaultUser < ActiveRecord::Migration
  def self.up
    unless User.count > 0
      myadmin = User.new
      myadmin.FirstName = 'khanchai'
      myadmin.LastName = 'pannaen'
      myadmin.UserName = 'admin'
      myadmin.Password = 'admin123456'
      myadmin.Email = 'p_khanchai@hotmail.com'
      myadmin.Phone = '0894216857'
      myadmin.UserType = 'a'
      unless myadmin.save
        puts "Could not create default admin user:"
            myadmin.errors.each do |att, m|
              puts "#{att}: #{m}"
            end
      end
    end
  end

  def self.down
  end
end