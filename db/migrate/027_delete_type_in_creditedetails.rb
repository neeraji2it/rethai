class DeleteTypeInCreditedetails< ActiveRecord::Migration
  def self.up
    remove_column :creditdetails, :type
  end
 
  def self.down
    remove_column :creditdetails, :type
  end

end
