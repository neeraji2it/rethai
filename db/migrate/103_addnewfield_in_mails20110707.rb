class AddnewfieldInMails20110707 < ActiveRecord::Migration
  def self.up
    add_column :mails,       :newsletter,       :string,    :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :mails,       :newsletter
  end   
end
