class AddPaypalFeatureInProducts20110916 < ActiveRecord::Migration
  def self.up
    add_column :products,       :paypalstatus,       :string,    :limit=>20,        :null => true,     :default=> ''
  end

  def self.down
    remove_column :products,       :paypalstatus
  end   
end
