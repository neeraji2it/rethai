class AddnewfieldInProducts20091119< ActiveRecord::Migration
  def self.up
    add_column :products,    :xmlcommand,          :int,         :null => true,   :default=> '0'
  end

  def self.down
    remove_column :products,    :xmlcommand
  end

end
