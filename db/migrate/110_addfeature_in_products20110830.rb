class AddfeatureInProducts20110830 < ActiveRecord::Migration
  def self.up
    add_column :products,       :homefeatured,       :string,    :limit=>10,        :null => true,     :default=> '0'
    add_column :products,       :condofeatured,      :string,    :limit=>10,        :null => true,     :default=> '0'
  end

  def self.down
    remove_column :products,       :homefeatured
    remove_column :products,       :condofeatured
  end   
end
