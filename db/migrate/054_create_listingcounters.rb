class CreateListingcounters < ActiveRecord::Migration
  def self.up
    create_table :listingcounters do |t|
      t.column :id_product,     :integer,                                     :default=> '0'
      t.column :counter,        :integer,                                     :default=> '0'
      t.column :viewdate,       :date,                      :null => false 
      t.column :created_on,     :timestamp,                 :null => true 
      t.column :updated_on,     :timestamp,                 :null => true ,   :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :listingcounters
  end
end
