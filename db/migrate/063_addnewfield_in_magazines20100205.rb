class AddnewfieldInMagazines20100205< ActiveRecord::Migration
  def self.up
    add_column :magazines,    :price,           :int,                       :null => true,     :default=> '0'
    add_column :magazines,    :rprice,          :int,                       :null => true,     :default=> '0'
    add_column :magazines,    :bed,             :string,   :limit=>2,       :null => true,     :default=> '0'
    add_column :magazines,    :bath,            :string,   :limit=>2,       :null => true,     :default=> '1'
    add_column :magazines,    :phone,           :string,   :limit=>20,      :null => true,     :default=> ''
    add_column :magazines,    :email,           :string,   :limit=>50,      :null => true,     :default=> ''
    add_column :magazines,    :url,             :string,   :limit=>100,     :null => true,     :default=> ''
    add_column :magazines,    :logo,            :int,                       :null => true,     :default=> '0'
  end

  def self.down
    remove_column :magazines,    :price
    remove_column :magazines,    :rprice
    remove_column :magazines,    :bed
    remove_column :magazines,    :bath
    remove_column :magazines,    :phone
    remove_column :magazines,    :email
    remove_column :magazines,    :url
    remove_column :magazines,    :logo
  end
end
