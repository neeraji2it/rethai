class AddnewfieldInProducts20090710< ActiveRecord::Migration
  def self.up
    add_column :products,    :adminpay,        :string,   :limit=>1,      :null => true,   :default=>'0'
  end

  def self.down
    remove_column :products,    :adminpay
  end
end