class AddnewfieldInProducts20090819< ActiveRecord::Migration
  def self.up
    add_column :products,    :SQM,          :string,   :limit=>30,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :products,    :SQM
  end

end
