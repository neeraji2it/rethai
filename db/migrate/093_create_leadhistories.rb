class CreateLeadhistories < ActiveRecord::Migration
  def self.up
    create_table :leadhistories do |t|
      t.integer :id_user
      t.integer :id_lead
      t.string :checked
      t.timestamps
    end
  end

  def self.down
    drop_table :leadhistories
  end
end
