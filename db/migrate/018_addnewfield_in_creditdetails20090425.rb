class AddnewfieldInCreditdetails20090425< ActiveRecord::Migration
  def self.up
    add_column :creditdetails,    :type,        :string,   :limit=>2,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :creditdetails,    :type
  end
end