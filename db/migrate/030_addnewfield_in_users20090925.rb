class AddnewfieldInUsers20090925< ActiveRecord::Migration
  def self.up
    add_column :users,    :SMSPhone,          :string,   :limit=>30,      :null => true,     :default=> ''
  end

  def self.down
    remove_column :users,    :SMSPhone
  end

end
