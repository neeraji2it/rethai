class AddnewfieldInProducts20091014< ActiveRecord::Migration
  def self.up
    add_column :products,    :startdate,          :date,      :null => true
    add_column :products,    :remain,             :int,       :null => true,  :default=> 1
  end

  def self.down
    remove_column :products,    :startdate
    remove_column :products,    :remain
  end

end
