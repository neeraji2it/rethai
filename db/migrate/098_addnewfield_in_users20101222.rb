class AddnewfieldInUsers20101222 < ActiveRecord::Migration
  def self.up
    add_column :users,    :leadstatus,    :string,    :limit=>1,        :null => true,     :default=> '0'
    add_column :users,    :leademail,     :string,    :limit=>1,        :null => true,     :default=> '0'
    add_column :subscriptions,    :leadconnect,        :int,            :null => true,     :default=> '0'
    add_column :subscriptions,    :listingconnect,     :int,            :null => true,     :default=> '0'
    add_column :subscriptions,    :leadprice,          :int,            :null => true,     :default=> '0'
  end

  def self.down
      remove_column :users,    :leadstatus
      remove_column :users,    :leademail
      remove_column :subscriptions,    :leadconnect
      remove_column :subscriptions,    :listingconnect
      remove_column :subscriptions,    :leadprice
  end   
end
