class AddnewfieldInCredit20090404< ActiveRecord::Migration
  def self.up
    add_column :credits,    :HeadSubscript,          :string,   :limit=>30,      :null => true,     :default=> '0'
  end

  def self.down
    remove_column :credits,    :HeadSubscript
  end
end