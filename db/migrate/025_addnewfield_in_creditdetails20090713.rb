class AddnewfieldInCreditdetails20090713< ActiveRecord::Migration
  def self.up
    add_column :creditdetails,    :id_product,        :string,   :limit=>30,      :null => true,   :default=>'0'
  end

  def self.down
    remove_column :creditdetails,    :id_product
  end
end