class CreateProvinces< ActiveRecord::Migration
  def self.up
    create_table :provinces do |t|
      t.column :name, :string, :limit=>50,:null=>true
    end
  end

  def self.down
    drop_table :provinces
  end
end