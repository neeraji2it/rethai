class AddnewfieldInProvinces20100127< ActiveRecord::Migration
  def self.up
    add_column :provinces,    :th,            :string,      :limit=>50,       :null => true,     :default=> ''
  end

  def self.down
     remove_column :provinces,    :th
  end
end
