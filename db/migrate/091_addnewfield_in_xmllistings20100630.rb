class AddnewfieldInXmllistings20100630< ActiveRecord::Migration
  def self.up
    add_column :xmllistings,    :link,    :string,       :limit=>255,       :null => true,     :default=> 'http://www.rethai.com/rethai'
    add_column :rssfeeds,       :link,    :string,       :limit=>255,       :null => true,     :default=> 'http://www.rethai.com/rethai'
  end

  def self.down
      remove_column :xmllistings,    :link
      remove_column :rssfeeds,    :link
  end   
end
