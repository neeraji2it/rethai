class CreateXmllistings < ActiveRecord::Migration
  def self.up
    create_table :xmllistings do |t|
      t.column :title,                  :string,      :limit=>255,    :null => true, :default=>""
      t.column :url,                    :string,      :limit=>255,    :null => true, :default=>""
      t.column :sql,                    :string,      :limit=>255,    :null => true, :default=>""
      t.column :fsalerent,              :string,      :limit=>255,    :null => true, :default=>""
      t.column :flistingtype,           :string,      :limit=>255,    :null => true, :default=>""
      t.column :fuser,                  :string,      :limit=>255,    :null => true, :default=>""
      t.column :flocation,              :string,      :limit=>255,    :null => true, :default=>""
      
      t.timestamps
    end
  end

  def self.down
    drop_table :xmllistings
  end
end
