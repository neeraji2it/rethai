class AddnewfieldInAdvertises20100323 < ActiveRecord::Migration
  def self.up
    add_column :advertises,    :advtype,           :string,     :limit=>50,      :null => true,      :default=>"searchcenter"
  end

  def self.down
     remove_column :advertises,    :advtype
  end
end
