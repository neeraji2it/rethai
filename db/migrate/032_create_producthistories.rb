class CreateProducthistories < ActiveRecord::Migration
  def self.up
    create_table :producthistories do |t|
       t.column :id_product,          :int,                       :null => false,   :default=> '0'
       t.column :id_user,             :int,                       :null => true,    :default=> '0'
       t.column :delist,              :string,      :limit=>1,    :null => false,   :default=> '1'
       t.column :startdate,           :date,                      :null => true
       t.column :stopdate,            :date,                      :null => true
       t.column :remain,              :int,                       :null => false,   :default=> '0'
       t.column :created_on,          :timestamp,                 :null => false 
       t.column :updated_on,          :timestamp,                 :null => false ,  :default=> '0000-00-00 00:00:00'
    end
  end

  def self.down
    drop_table :producthistories
  end
end
