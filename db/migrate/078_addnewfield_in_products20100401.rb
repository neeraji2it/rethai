class AddnewfieldInProducts20100401< ActiveRecord::Migration
  def self.up
    add_column :products,    :updateimage, :text,                           :null => true,     :default=> ''
    add_column :products,    :UploadImages,:string,       :limit=>1,        :null => true,     :default=> '0'
    add_column :products,    :Quality,     :string,       :limit=>1,        :null => true,     :default=> '0'
  end

  def self.down
     remove_column :products,    :updateimage
     remove_column :products,    :UploadImages
     remove_column :products,    :Quality
  end   
end
