class OfferController < ApplicationController
#    sss.save
include GeoKit::Geocoders
  def checkpostcode(code)
    location = 'NO'
    
#      include GeoKit::Geocoders
     res=MultiGeocoder.geocode("#{code}")
#      if lat != "" and lat != nil and lng !="" and lng !=nil
        location = "#{res.ll}"

    location
  end
  def definesession
    if not session[:location]
      session[:localtion]=""
    end
    if not session[:fprice]
      session[:fprice]=""
    end
    if not session[:tprice]
      session[:tprice]=""
    end
    if not session[:bed]
      session[:bed]=""
    end
    if not session[:bath]
      session[:bath]=""
    end
  end
  
  def index
    @location="pattaya"
    @text = "no"
    @text2=""
    if session[:user_id] and session[:user_id]!="" and session[:user_id]!=nil
      @user = User.find(session[:user_id])
    end#end if session[:user_id] 
      definesession
      sql = ""
      session[:location] = "pattaya"
       if session[:location]!=""
          location = checkpostcode(session[:location])
          if location != 'NO'
                loc = GeoKit::Geocoders::GoogleGeocoder.geocode(location)
                lat = loc.lat()
                lng = loc.lng()
                if lat != nil and lng != nil
                  x1 = lat + (100 * 0.0145)
                  x2 = lat - (100 * 0.0145)
                  y1 = lng + (100 * 0.0145)
                  y2 = lng - (100 * 0.0145)
                  sql = sql + " and (lat between #{x2} and #{x1}) and (lng between #{y2} and #{y1}) "
                end
          end
        end
        
        products = Product.find(:all,:conditions=>"Deleted = '1' #{sql}")
                    code = ""
                    number = 0
                    xmlfile="<markers>\n"
                    for product in products
                    html = ""
#                      code = code + "#{product.lat}_#{product.lng},"
                      
                                            strprice = product.Price
                                              if strprice.to_f >= 1000
                                                #strprice = sprintf("%.2f",strprice)
                                                strprice = strprice.to_s.reverse
                                                strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
                                                strprice = strprice.gsub(/,$/,"").reverse
                                              #else
                                               # strprice = sprintf("%.2f",strprice)
                                             end
                
                                             
                                             
                    #default picture
                default = "/googleimage/01.jpg"
                     images = Images.find(:all,:conditions=>"id_product = '#{product.id}'")           
                      if images.length > 0
                        for img in images
                            strtype = img.photo_file_name
                            strtype = strtype.slice(strtype.index(".").to_i+1,strtype.length)
                          default  = "https://s3.amazonaws.com/hea/photos/#{img.id}/original.#{strtype}"#img.photo.url
                          break
                        end
                         
                       end  
                  # generate xmlfile
                  html = html + "$#{strprice} &lt;img src=&quot;/action_forward.gif&quot; onclick=&quot;showsidebar(&#39;#{product.id}&#39;);&quot; style=&quot;display:inline;&quot;/&gt;&lt;br&gt;&lt;div id=&quot;image"+number.to_s+"&quot;&gt;  &lt;img src=&quot;#{default}&quot;  width=&quot;400&quot; height=&quot;300&quot;/&gt; &lt;/div&gt; &lt;br&gt;"  


                      if images.length > 0  
                         i = 1
                        for img in images
                            strtype = img.photo_file_name
                            strtype = strtype.slice(strtype.index(".").to_i+1,strtype.length)
                          html = html +"&lt;img src=&quot;https://s3.amazonaws.com/hea/photos/#{img.id}/thumb.#{strtype}&quot;  width=&quot;40&quot; height=&quot;40&quot; onclick=&quot;changeimage(&#39;image"+number.to_s+"&#39;,&#39;https://s3.amazonaws.com/hea/photos/#{img.id}/original.#{strtype}&#39;);&quot;/&gt;\n"
                           if i == 5
                             html = html + "&lt;br&gt;"
                             i = 1
                           else
                             i = i + 1
                           end
                           
                        end
                        
                      end
                     
                    
                      html = html + "&lt;br&gt;&lt;iframe src=/products/gdetail/#{product.id} width=400 border=0 frameborder=0&gt;&lt;/iframe&gt;"
                      
                      code  = code + "\t<marker lat='#{product.lat}' lng='#{product.lng}' html='#{html}' label='$#{strprice}'/>\n"
                      number = number + 1
                    end      #end for 
                    xmlfile = "<markers>\n#{code}</markers>\n"
                    
                      #save body location in local path
                          FileUtils.mkdir_p(File.dirname("#{RAILS_ROOT}/public/xml/offer/index.xml"))  # create dirctory by id_user           
                        #----------------------------- save file in new directory -----------------------------------------           
                          File.open("#{RAILS_ROOT}/public/xml/offer/index.xml", "wb") do |f|     
                            f.write(xmlfile) 
                          end
                   
       
  end
   
   def offer
    myoffer = Offer.new(params[:offer])
    if myoffer.save
      
      #do send Email
        if params[:id_productuser]
          user = User.find(params[:id_productuser])
          email = user.Email
          message = ""
          message = message + "<img src='http://www.rethai.com/images/logo.png' /><br><br>"
          message = message + "Hello #{user.FirstName}  #{user.LastName}<br>\n<br>\n"
          message = message + "There has been an offer on your listing.<br>\n"
          message = message + "Please login to http://www.REThai.com  to view the details.<br>\n<br>\n<br>\n"
          message = message + "http://www.REThai.com<br>\nREThai Team.<br>\n<br>\n"
          Notifier.deliver_message(email,message)
        end
      flash[:notice]= "Offer sent"
    else
      flash[:notice]= "Sorry there was a problem, please try again"
    end
    
    
    redirect_to :controller=>'offer',:action=>'manageoffers'
  end
  
  def manageoffers
       @login = "no"
      if session[:user_id]
#        @offer_pages, @offers = paginate :offers,:conditions=>"Deleted = '1' and (id_productuser = '#{session[:user_id]}' or id_user ='#{session[:user_id]}' or customer ='#{session[:user_id]}')",:order =>"id_product,customer,id asc", :per_page => 20
        @offers =  Offer.paginate(:page => params[:page] ,:conditions=>" Deleted = '1' and (id_productuser = '#{session[:user_id]}' or id_user ='#{session[:user_id]}' or customer ='#{session[:user_id]}')",:order=>"id_product,customer,id asc", :per_page =>20)
        @login = "yes"
      else
        flash[:notice]="please login"
        redirect_to "/"
      end
    end
  
    def reply
    if session[:user_id]
      @offer = Offer.find(params[:id])
#      @user = User.find(params[:id_user])
      @product = Product.find(@offer.id_product)
      

    
    else
      flash[:notice]="Please login!"  
      redirect_to :controller=>'login',:action=>'login'
    end
  end
  
  def destroy
    offers = Offer.find(:all,:conditions=>"id_product = '#{params[:id_product]}' and Customer = '#{params[:customer]}'")
    for del_offer in offers
      del_offer.update_attributes("Deleted"=>"0")
    end
    
    redirect_to :action => 'manageoffers'
  end
  
  def approve
#    sss.save
    if params[:id_product]
      product = Product.find(params[:id_product])
      product.update_attributes("Sold_STC"=>"1")

      # need to do send email for sold
        myoffer = Offer.find(params[:id_offer])

         user1 = User.find(myoffer.id_productuser)  # saller owner listing
         user2 = User.find(myoffer.id_user)         #buyer
         
                                strprice = myoffer.offerprice.to_i
                              if strprice.to_f >= 1000
                                #strprice = sprintf("%.2f",strprice)
                                strprice = strprice.to_s.reverse
                                strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
                                strprice = strprice.gsub(/,$/,"").reverse
                              #else
                                #strprice = sprintf("%.2f",strprice)
                              end
        
       if session[:user_id].to_s ==  myoffer.id_productuser  #saller approve ownner listing
          str1=""
          str1 = str1 + "You approved your listing at #{strprice}"
          

            
          str2= ""
          str2 = str2 + "You offer approved at #{strprice}<br>\n<br>\n"
#              if product.pdf !="0" and product.pdf != nil and product.pdf !=""
#                  mypdf = Pdf.find(product.pdf)            
#          str2 = str2 + " Link: #{mypdf.photo.url}<br>\n<br>\n"
#              end
       else  #buyer approve
          str1=""
          str1 = str1 + "You listing approved at #{strprice}"
          

            
          str2= ""
          str2 = str2 + "You approved listing at #{strprice}<br>\n<br>\n"
#              if product.pdf !="0" and product.pdf != nil and product.pdf !=""
#                  mypdf = Pdf.find(product.pdf)            
#          str2 = str2 + " Link: #{mypdf.photo.url}<br>\n<br>\n"
#              end
       end
        

        
         email = user1.Email
          message = ""
          message = message + "Hello #{user1.FirstName}  #{user1.LastName}<br>\n<br>\n"
          message = message + "#{str1}<br>\n"
          message = message + "Listing detail:<br>\n#{Product.productdetail(product.id)}<br>\n<br>\n"
          message = message + ""
          
          message = message + "http://www.REThai.com<br>\nREThai Team.<br>\n<br>\n"
          Notifier.deliver_inforation(email,message)   
        
        
          email2 = user2.Email
          message = ""
          message = message + "Hello #{user2.FirstName}  #{user2.LastName}<br>\n<br>\n"
          message = message + "#{str2}<br>\n"
          message = message + "Listing detail:<br>\n#{Product.productdetail(product.id)}<br>\n<br>\n"
          message = message + ""
          
         message = message + "http://www.REThai.com<br>\nREThai Team.<br>\n<br>\n"
          Notifier.deliver_inforation(email2,message)
        
      flash[:notice]="Update Offer ready"      
    else
      flash[:notice]="Can't update offer,<br>Please check your detail or contact the admin."
    end
    redirect_to :action=>'manageoffers'
  end
  
  
  
end
