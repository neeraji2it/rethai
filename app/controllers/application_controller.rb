# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
#  include ReCaptcha::AppHelper
#include ReCaptcha::ViewHelper


#  helper :all # include all helpers, all the time

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
#  protect_from_forgery # :secret => '2082a1b0a9a2e7e92880536745709ffd'
  
  # See ActionController::Base for details 
  # Uncomment this to filter the contents of submitted sensitive data parameters
  # from your application log (in this case, all fields with names like "password"). 
  # filter_parameter_logging :password

  before_filter :extract_locale_from_subdomain, :sanitize_params
  



  def set_locale
    # update session if passed
    session[:locale] = params[:locale] if params[:locale]

    # set locale based on session or default 
    I18n.locale = session[:locale] || I18n.default_locale
  end

  def extract_locale_from_subdomain
#    aaa.save
      if params[:locale]      
        session[:locale] = params[:locale] if params[:locale]
      else
        session[:locale]  = request.subdomains.first
#        if request.subdomains.first.to_s != "de" and request.subdomains.first.to_s != "en" and request.subdomains.first.to_s != "fr" and request.subdomains.first.to_s != "nl" and request.subdomains.first.to_s != "ru" and request.subdomains.first.to_s != "th" # and request.subdomains.first.to_s != "MORE"
#          session[:locale]  = "en"
#        else
#          session[:locale]  = request.subdomains.first
#        end
        
      end
      if session[:locale]
        if session[:locale].downcase =="www" or session[:locale].downcase =="beta" or session[:locale].downcase =="alpha" or session[:locale].downcase ==""
          session[:locale] = "en"
        else
#          session[:locale] = "en"          
          if session[:locale].to_s != "de" and session[:locale].to_s != "en" and session[:locale].to_s != "kr" and session[:locale].to_s != "jp" and session[:locale].to_s != "it" and session[:locale].to_s != "fr" and session[:locale].to_s != "nl" and session[:locale].to_s != "ru" and session[:locale].to_s != "th" # and session[:locale].to_s != "MORE"
            session[:locale]  = "en"
          end
        
        end
      else
        session[:locale] = "en"
      end
    I18n.locale = session[:locale] || I18n.default_locale
#    (available_locales.include? parsed_locale) ? parsed_locale  : nil

    #check params[:partner]=================================================================================================
    begin
      # fix partner
      if params[:partner]
        if params[:partner]["/"]
        params[:partner]="rethai"
        end 
        if params[:partner].to_s !="rethai" and params[:partner].to_s !="pattayaone" and params[:partner].to_s !="missioncontrol" and params[:partner].to_s !="products" and params[:partner].to_s != "admin-default"
          params[:partner]="rethai"
        end
        
        
      elsif params[:mypartner]
        if params[:mypartner]["/"]
        params[:partner]="rethai"
        end  
        if params[:mypartner].to_s !="rethai" and params[:mypartner].to_s !="pattayaone" and params[:mypartner].to_s !="missioncontrol" and params[:mypartner].to_s !="products" and params[:mypartner].to_s != "admin-default"
          params[:mypartner]="rethai"
        end
        
        
      else
#        params[:partner]="rethai"
        params[:partner]="products"
      end
      
      
    rescue
#        params[:partner]="rethai"
        params[:partner]="products"
    end
    #=======================================  end check params[:partner] ====================================================
    
    if params[:page]
      if params[:page].to_i <= 0
        params[:page] = "1"
      end
    end
    
    
    
    #set params[:page] paginate
    
    
  end

end


