class IndexController < ApplicationController


	# caches_page			:index
	#caches_action   :index => { :ttl => 2.hours}


layout 'rethai'
  
  
  def index
  	
  end
  
  def createsession
    if params[:code]
      session[:location] = params[:code].gsub(/[<'">=]/, '')
      @location=session[:location]
    end
    if params[:fprice]
      session[:fprice] = params[:fprice].gsub(/[<'">=]/, '')
    end
    if params[:tprice]
      session[:tprice] = params[:tprice].gsub(/[<'">=]/, '')
    end
    if params[:bed]
      session[:bed] = params[:bed].gsub(/[<'">=]/, '')
    end
    if params[:bed2]
      session[:bed2] = params[:bed2].gsub(/[<'">=]/, '')
    end
    if params[:bath]
      session[:bath] = params[:bath].gsub(/[<'">=]/, '')
    end
    
    if params[:propertytype]
      session[:propertytype] = params[:propertytype].gsub(/[<'">=]/, '')
    end
    if params[:salerent]
      session[:salerent] = params[:salerent].gsub(/[<'">=]/, '')
    end
#    redirect_to '/listings/rethai/maps'  
    redirect_to "/listings/maps?location=#{session[:location]}"
  end
  
  
  def myaccount
    if session[:user_id]
      @user = User.find(session[:user_id])
    else
      flash[:notice]="Please login"
      redirect_to "/listings/rethai/login"
    end
  end
  
  
  
  def stats
       begin
              sql=""
              lat = ""
              lng = ""
              mt = ""
        if params[:province]      
          strprovince = params[:province].gsub(/[<'">=]/, '')
          while strprovince["/"]
            strprovince["/"]=""
          end
        end
        session[:location] = strprovince
        if session[:location].to_s == ""
          session[:location] = "Pattaya"
        end
        
        provinces = Province.find(:all,:conditions=>"`name` like '%#{session[:location] }%'")
          if provinces.length > 0
            for pv in provinces
              lat = pv.lat
              lng = pv.lng
              mt = pv.mt
              break
            end
          end
          
          #check lat ,lng again 
          
          if lat.to_s =="" or lng.to_s==""            
              lat = "12.9357"
              lng = "100.889"
          end
          if mt.to_s =="" or mt.to_s =="0" or mt == nil           
              mt = 50
          end
          
          if lat != 0 and lng != 0          
              x1 = lat.to_f + (mt * 0.009065)
              x2 = lat.to_f - (mt * 0.009065)
              y1 = lng.to_f + (mt * 0.009065)
              y2 = lng.to_f - (mt * 0.009065)
              sql = sql + " and ((lat between #{x2} and #{x1} )and(lng between #{y2} and #{y1} )) "
          end
  s_month = DateTime.now.strftime("%m")
s_year = DateTime.now.strftime("%Y")

if s_month.to_i - 6 < 0
  s_month = 12 + (s_month.to_i - 6).to_i  
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 6  
end
l6month = DateTime.now - 6.month
s_year = l6month.strftime("%Y")
s_month = l6month.strftime("%m")
strdate = ("#{s_year}-#{s_month}-1").to_date

        @products = Product.find(:all,:conditions=>"Deleted = '1' and (`Status` !='d' and Status != 'f'  and Status != 's' or `Status` is null) and Type = '2' and created_on >='#{strdate}' #{sql}",:order=>"created_on asc")      
        @condos   = Product.find(:all,:conditions=>"Deleted = '1' and (`Status` !='d' and Status != 'f'  and Status != 's' or `Status` is null) and Type = '1' and created_on >='#{strdate}' #{sql}",:order=>"created_on asc")      

            #generate month
s_month = DateTime.now.strftime("%m")
s_year = DateTime.now.strftime("%Y")
@m6 = ("#{s_year}-#{s_month}-1").to_date


if s_month.to_i - 1 == 0
  s_month = 12
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 1 
end
s_month = (DateTime.now - 1.month).strftime("%m")
s_year =  (DateTime.now - 1.month).strftime("%Y")
@m5 = ("#{s_year}-#{s_month}-1").to_date


if s_month.to_i - 1 == 0
  s_month = 12
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 1  
end
s_month = (DateTime.now - 2.month).strftime("%m")
s_year =  (DateTime.now - 2.month).strftime("%Y")
@m4 = ("#{s_year}-#{s_month}-1").to_date
if s_month.to_i - 1 == 0
  s_month = 12
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 1  
end
s_month = (DateTime.now - 3.month).strftime("%m")
s_year =  (DateTime.now - 3.month).strftime("%Y")
@m3 = ("#{s_year}-#{s_month}-1").to_date
if s_month.to_i - 1 == 0
  s_month = 12
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 1  
end
s_month = (DateTime.now - 4.month).strftime("%m")
s_year =  (DateTime.now - 4.month).strftime("%Y")
@m2 = ("#{s_year}-#{s_month}-1").to_date
if s_month.to_i - 1 == 0
  s_month = 12
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 1  
end
s_month = (DateTime.now - 5.month).strftime("%m")
s_year =  (DateTime.now - 5.month).strftime("%Y")
@m1 = ("#{s_year}-#{s_month}-1").to_date







      
if @products.length > 0      
#generate data
s_month = DateTime.now.strftime("%m")
s_year = DateTime.now.strftime("%Y")
if s_month.to_i - 6 < 0
  s_month = 12 + (s_month.to_i - 6).to_i  
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 6  
end

@d1=""
@d2=""
@d3=""
@d4=""
@d5=""
@d6=""
      l6month = DateTime.now - 6.month
      s_year = l6month.strftime("%Y")
      s_month = l6month.strftime("%m")
strdate = ("#{s_year}-#{s_month}-1").to_date


sqmprice = 0
price = 0
sqm = 0
i = 0

x = 1
@listings=Array.new(6)
@prices=Array.new(6)
@sqm=Array.new(6)
for c in @products
if i == 0
  # get first month
  d_month = c.created_on.to_date.strftime("%m").to_i   # next month
  d_year  = c.created_on.to_date.strftime("%Y").to_i
    if d_month.to_i  == 12
      d_month = 1
      d_year = d_year.to_i + 1
    else
      d_month = d_month.to_i + 1  
    end
    
    n1month = c.created_on.to_date + 1.month
  d_year = n1month.strftime("%Y")
  d_month = n1month.strftime("%m")
  mmonth = ("#{d_year}-#{d_month}-1").to_date
  lastdate = c.created_on
i = 1
end
  if c.created_on.to_date < mmonth
    if c.Price.to_s !="" and c.Price.to_i !=0 and c.Price != nil and c.SQM.to_s !="" and c.SQM.to_i !=0 and c.SQM !=nil      
      sqmprice = sqmprice + (c.Price.to_i / c.SQM.to_i )
      price = price + c.Price.to_i
      sqm = sqm + c.SQM.to_i
      x = x + 1
    end
    
  else
    if @d1 == ""  and lastdate.strftime("%y%m")==@m6.strftime("%y%m")
      @d1 = sqmprice.to_f / x.to_i
      @listings[0] = x
      @prices[0] = price.to_f
      @sqm[0] = sqm.to_f
    elsif @d2 ==""  and lastdate.strftime("%y%m")==@m5.strftime("%y%m")
      @d2 = sqmprice.to_f / x.to_i
      @listings[1] = x
      @prices[1] = price.to_f
      @sqm[1] = sqm.to_f
    elsif @d3 ==""  and lastdate.strftime("%y%m")==@m4.strftime("%y%m")
      @d3 = sqmprice.to_f / x.to_i
      @listings[2] = x
      @prices[2] = price.to_f
      @sqm[2] = sqm.to_f
    elsif @d4 ==""  and lastdate.strftime("%y%m")==@m3.strftime("%y%m")
      @d4 = sqmprice.to_f / x.to_i
      @listings[3] = x
      @prices[3] = price.to_f
      @sqm[3] = sqm.to_f
    elsif @d5 ==""  and lastdate.strftime("%y%m")==@m2.strftime("%y%m")
      @d5 = sqmprice.to_f / x.to_i
      @listings[4] = x
      @prices[4] = price.to_f
      @sqm[4] = sqm.to_f
    elsif @d6 ==""  and lastdate.strftime("%y%m")==@m1.strftime("%y%m")
      @d6 = sqmprice.to_f / x.to_i
      @listings[5] = x
      @prices[5] = price.to_f
      @sqm[5] = sqm.to_f
    end
    
    if c.Price.to_s !="" and c.Price.to_s !="0" and c.Price != nil and c.SQM.to_s !="" and c.SQM.to_s !="0" and c.SQM !=nil      
      sqmprice = (c.Price.to_i / c.SQM.to_i )
      price = c.Price.to_f
      sqm = c.SQM.to_f
    else
      sqmprice = 0
      price = 0
      sqm = 0
    end

    if d_month.to_i + 1 == 13
      d_month = 1
      d_year = d_year.to_i + 1
    else
      d_month = d_month.to_i + 1  
    end
      n1month = mmonth.to_date + 1.month
      d_year = n1month.strftime("%Y")
      d_month = n1month.strftime("%m")
    mmonth = ("#{d_year}-#{d_month}-1").to_date
    lastdate = c.created_on
    i = i + 1
    x = 1
  end
  
end #end for

  
    if @d1 == ""  and lastdate.strftime("%y%m")==@m6.strftime("%y%m")
      @d1 = sqmprice.to_f / x.to_i
      @listings[0] = x
      @prices[0] = price.to_f
      @sqm[0] = sqm.to_f
    elsif @d2 ==""  and lastdate.strftime("%y%m")==@m5.strftime("%y%m")
      @d2 = sqmprice.to_f / x.to_i
      @listings[1] = x
      @prices[1] = price.to_f
      @sqm[1] = sqm.to_f
    elsif @d3 ==""  and lastdate.strftime("%y%m")==@m4.strftime("%y%m")
      @d3 = sqmprice.to_f / x.to_i
      @listings[2] = x
      @prices[2] = price.to_f
      @sqm[2] = sqm.to_f
    elsif @d4 ==""  and lastdate.strftime("%y%m")==@m3.strftime("%y%m")
      @d4 = sqmprice.to_f / x.to_i
      @listings[3] = x
      @prices[3] = price.to_f
      @sqm[3] = sqm.to_f
    elsif @d5 ==""  and lastdate.strftime("%y%m")==@m2.strftime("%y%m")
      @d5 = sqmprice.to_f / x.to_i
      @listings[4] = x
      @prices[4] = price.to_f
      @sqm[4] = sqm.to_f
    elsif @d6 ==""  and lastdate.strftime("%y%m")==@m1.strftime("%y%m")
      @d6 = sqmprice.to_f / x.to_i
      @listings[5] = x
      @prices[5] = price.to_f
      @sqm[5] = sqm.to_f
    end


    if @d1 == ""
      @d1 = 0
      @listings[0] = 0
      @prices[0] = 0
      @sqm[0] = 0
    end
    if @d2 ==""
      @d2 = 0
      @listings[1] = 0
      @prices[1] = 0
      @sqm[1] = 0
    end
    if  @d3 =="" 
      @d3 = 0
      @listings[2] = 0
      @prices[2] = 0
      @sqm[2] = 0
    end
    if @d4 ==""
      @d4 = 0
      @listings[3] = 0
      @prices[3] = 0
      @sqm[3] = 0
    end
    if @d5 ==""
      @d5 = 0
      @listings[4] = 0
      @prices[4] = 0
      @sqm[4] = 0
    end
    if @d6 ==""
      @d6 = 0
      @listings[5] = 0
      @prices[5] = 0
      @sqm[5] = 0
    end

      #generate scrol
      #get max value
      @maxvalue=@d1
      if @maxvalue.to_i < @d2.to_i
        @maxvalue = @d2
      end
      if @maxvalue.to_i < @d3.to_i
        @maxvalue = @d3.to_i
      end
      if @maxvalue.to_i < @d4.to_i
        @maxvalue = @d4.to_i
      end
      if @maxvalue.to_i < @d5.to_i
        @maxvalue = @d5.to_i
      end
      if @maxvalue.to_i < @d6.to_i
        @maxvalue = @d6.to_i
      end
      @maxvalue = @maxvalue.to_i + (@maxvalue.to_i * 15 / 100)
      @s1=@maxvalue.to_i
      @s2=((@maxvalue.to_i / 4) * 3).to_i
      @s3=((@maxvalue.to_i / 4) * 2).to_i
      @s4=((@maxvalue.to_i / 4) * 1).to_i
      
      
      else  #else if @products.length
        @products=""
      end  #end if @products.length


#======================================  end Products  =====================================================================================

      
      
if @condos.length > 0      
#generate data
s_month = DateTime.now.strftime("%m")
s_year = DateTime.now.strftime("%Y")
if s_month.to_i - 6 < 0
  s_month = 12 + (s_month.to_i - 6).to_i  
  s_year = s_year.to_i - 1
else
  s_month = s_month.to_i - 6  
end
      l6month = DateTime.now - 6.month
      s_year = l6month.strftime("%Y")
      s_month = l6month.strftime("%m")
@c_d1=""
@c_d2=""
@c_d3=""
@c_d4=""
@c_d5=""
@c_d6=""
strdate = ("#{s_year}-#{s_month}-1").to_date


sqmprice = 0
price = 0
sqm = 0
i = 0

x = 1
@c_listings=Array.new(6)
@c_prices=Array.new(6)
@c_sqm=Array.new(6)
for c in @condos
if i == 0
  # get first month
  d_month = c.created_on.to_date.strftime("%m").to_i   # next month
  d_year  = c.created_on.to_date.strftime("%Y").to_i
    if d_month.to_i  == 12
      d_month = 1
      d_year = d_year.to_i + 1
    else
      d_month = d_month.to_i + 1  
    end
    n1month = c.created_on.to_date + 1.month
    d_month = n1month.strftime("%m").to_i
    d_year  = n1month.strftime("%Y").to_i
  mmonth = ("#{d_year}-#{d_month}-1").to_date
  lastdate = c.created_on
i = 1
end
  if c.created_on.to_date < mmonth
    if c.Price.to_s !="" and c.Price.to_i != 0 and c.Price != nil and c.SQM.to_s !="" and c.SQM.to_i !=0 and c.SQM !=nil      
      sqmprice = sqmprice + (c.Price.to_i / c.SQM.to_i )
      price = price + c.Price.to_i
      sqm = sqm + c.SQM.to_i
      x = x + 1
    end
    
  else
    if @c_d1 == ""  and lastdate.strftime("%y%m")==@m6.strftime("%y%m")
      @c_d1 = sqmprice.to_f / x.to_i
      @c_listings[0] = x
      @c_prices[0] = price.to_f
      @c_sqm[0] = sqm.to_f
    elsif @c_d2 ==""  and lastdate.strftime("%y%m")==@m5.strftime("%y%m")
      @c_d2 = sqmprice.to_f / x.to_i
      @c_listings[1] = x
      @c_prices[1] = price.to_f
      @c_sqm[1] = sqm.to_f
    elsif @c_d3 ==""  and lastdate.strftime("%y%m")==@m4.strftime("%y%m")
      @c_d3 = sqmprice.to_f / x.to_i
      @c_listings[2] = x
      @c_prices[2] = price.to_f
      @c_sqm[2] = sqm.to_f
    elsif @c_d4 ==""  and lastdate.strftime("%y%m")==@m3.strftime("%y%m")
      @c_d4 = sqmprice.to_f / x.to_i
      @c_listings[3] = x
      @c_prices[3] = price.to_f
      @c_sqm[3] = sqm.to_f
    elsif @c_d5 ==""  and lastdate.strftime("%y%m")==@m2.strftime("%y%m")
      @c_d5 = sqmprice.to_f / x.to_i
      @c_listings[4] = x
      @c_prices[4] = price.to_f
      @c_sqm[4] = sqm.to_f
    elsif @c_d6 ==""  and lastdate.strftime("%y%m")==@m1.strftime("%y%m")
      @c_d6 = sqmprice.to_f / x.to_i
      @c_listings[5] = x
      @c_prices[5] = price.to_f
      @c_sqm[5] = sqm.to_f
    end
    
    if c.Price.to_s !="" and c.Price.to_s !="0" and c.Price != nil and c.SQM.to_s !="" and c.SQM.to_s !="0" and c.SQM !=nil      
      sqmprice = (c.Price.to_i / c.SQM.to_i )
      price = c.Price.to_f
      sqm = c.SQM.to_f
    else
      sqmprice = 0
      price = 0
      sqm = 0
    end

    if d_month.to_i + 1 == 13
      d_month = 1
      d_year = d_year.to_i + 1
    else
      d_month = d_month.to_i + 1  
    end
      n1month = mmonth.to_date + 1.month
      d_year = n1month.strftime("%Y")
      d_month = n1month.strftime("%m")
    mmonth = ("#{d_year}-#{d_month}-1").to_date
    lastdate = c.created_on
    i = i + 1
    x = 1
  end
  
end

  
    if @c_d1 == ""  and lastdate.strftime("%y%m")==@m6.strftime("%y%m")
      @c_d1 = sqmprice.to_f / x.to_i
      @c_listings[0] = x
      @c_prices[0] = price.to_f
      @c_sqm[0] = sqm.to_f
    elsif @c_d2 ==""  and lastdate.strftime("%y%m")==@m5.strftime("%y%m")
      @c_d2 = sqmprice.to_f / x.to_i
      @c_listings[1] = x
      @c_prices[1] = price.to_f
      @c_sqm[1] = sqm.to_f
    elsif @c_d3 ==""  and lastdate.strftime("%y%m")==@m4.strftime("%y%m")
      @c_d3 = sqmprice.to_f / x.to_i
      @c_listings[2] = x
      @c_prices[2] = price.to_f
      @c_sqm[2] = sqm.to_f
    elsif @c_d4 ==""  and lastdate.strftime("%y%m")==@m3.strftime("%y%m")
      @c_d4 = sqmprice.to_f / x.to_i
      @c_listings[3] = x
      @c_prices[3] = price.to_f
      @c_sqm[3] = sqm.to_f
    elsif @c_d5 ==""  and lastdate.strftime("%y%m")==@m2.strftime("%y%m")
      @c_d5 = sqmprice.to_f / x.to_i
      @c_listings[4] = x
      @c_prices[4] = price.to_f
      @c_sqm[4] = sqm.to_f
    elsif @c_d6 ==""  and lastdate.strftime("%y%m")==@m1.strftime("%y%m")
      @c_d6 = sqmprice.to_f / x.to_i
      @c_listings[5] = x
      @c_prices[5] = price.to_f
      @c_sqm[5] = sqm.to_f
    end


    if @c_d1 == ""
      @c_d1 = 0
      @c_listings[0] = 0
      @c_prices[0] = 0
      @c_sqm[0] = 0
    end
    if @c_d2 ==""
      @c_d2 = 0
      @c_listings[1] = 0
      @c_prices[1] = 0
      @c_sqm[1] = 0
    end
    if  @c_d3 =="" 
      @c_d3 = 0
      @c_listings[2] = 0
      @c_prices[2] = 0
      @c_sqm[2] = 0
    end
    if @c_d4 ==""
      @c_d4 = 0
      @c_listings[3] = 0
      @c_prices[3] = 0
      @c_sqm[3] = 0
    end
    if @c_d5 ==""
      @c_d5 = 0
      @c_listings[4] = 0
      @c_prices[4] = 0
      @c_sqm[4] = 0
    end
    if @c_d6 ==""
      @c_d6 = 0
      @c_listings[5] = 0
      @c_prices[5] = 0
      @c_sqm[5] = 0
    end

      #generate scrol
      #get max value
      @c_maxvalue=@c_d1
      if @c_maxvalue.to_i < @c_d2.to_i
        @c_maxvalue = @c_d2
      end
      if @c_maxvalue.to_i < @c_d3.to_i
        @c_maxvalue = @c_d3.to_i
      end
      if @c_maxvalue.to_i < @c_d4.to_i
        @c_maxvalue = @c_d4.to_i
      end
      if @c_maxvalue.to_i < @c_d5.to_i
        @c_maxvalue = @c_d5.to_i
      end
      if @c_maxvalue.to_i < @c_d6.to_i
        @c_maxvalue = @c_d6.to_i
      end
      @c_maxvalue = @c_maxvalue.to_i + (@c_maxvalue.to_i * 15 / 100)
      @c_s1=@c_maxvalue.to_i
      @c_s2=((@c_maxvalue.to_i / 4) * 3).to_i
      @c_s3=((@c_maxvalue.to_i / 4) * 2).to_i
      @c_s4=((@c_maxvalue.to_i / 4) * 1).to_i
      
      
      else  #else if @condos.length
        @condos = ""
      end  #end if @condos.length


#======================================  end @condos  =====================================================================================

      
      
      
      
    rescue
      @products=""      
      @condos = ""
    end
    

  end
  
end
