class AdminController < ApplicationController
  
  
  layout :choose_layout,:except=>['popupmagazine','condomap','condoimages']
#  layout  'rethai'
  before_filter :checkuser,:except=>['login','checklogin']
  
  def choose_layout    
    if [ 'index' ].include? action_name
      'admin-default' 
    else
      'admin-default'
    end
  end

  def checkuser
    if session[:Admin_id]
      #
    else
      flash[:notice]="Please login"
      redirect_to :controller=>'admin',:action=>'login'
    end
  end





 
 def messages
   sort = "mails.id desc"
   if params[:field].to_s !="" 
     sort = "mails.#{params[:field]} #{params[:sort]}"
   end
   @messages = Mail.paginate(:page => params[:page] , :per_page =>50,:conditions=>"mails.status = '1'", :order=>"#{sort}")#.find(:all)
   #User.paginate(:page => params[:page] ,:conditions=>"UserType !='a' #{sql}", :per_page =>20)
 end

def deletemessage
  text = ""
  begin
    Mail.find(params[:id]).update_attributes("status"=>"0")
    text = "<span>Deleted !</span>"
  rescue
    text = ""
  end
  render :text=>text
end







#==========  END ADMIN FILTER  ====================================================================
  def login
    
  end
  def checklogin

    begin
      if params[:user][:UserName] and params[:user][:Password]
        while params[:user][:UserName]["'"]
          params[:user][:UserName]["'"] = ""
        end
        while params[:user][:UserName]["="]
          params[:user][:UserName]["="] = ""
        end
        while params[:user][:UserName]['"']
          params[:user][:UserName]['"'] = ""
        end
        while params[:user][:UserName]['/']
          params[:user][:UserName]['/'] = ""
        end
        
        
        while params[:user][:Password]["'"]
          params[:user][:Password]["'"] = ""
        end
        while params[:user][:Password]["="]
          params[:user][:Password]["="] = ""
        end
        while params[:user][:Password]['"']
          params[:user][:Password]['"'] = ""
        end
        while params[:user][:Password]['/']
          params[:user][:Password]['/'] = ""
        end
#        sss.save
        users = User.find(:all,:conditions=>"UserName = '#{params[:user][:UserName]}' and Deleted = '1' and UserType = 'a'",:order=>"id asc")
        if users.length > 0
          for user in users
          
          
            if user.Password.to_s == params[:user][:Password].to_s
#              sss.save
              session[:Admin_id]=user.id
              flash[:notice]="Hello <strong>#{user.FirstName}</strong>"
              redirect_to :controller=>'admin',:action=>'index'
#              sss.save
            else
              flash[:notice]=">Sorry, please try again"
              redirect_to :controller=>'admin',:action=>'login'
            end
            
            
            
            

            
            break
          end #end for user in users
        else
              #not have username
              flash[:notice]="Sorry, please try again"
              redirect_to :controller=>'admin',:action=>'login'
        end #end if users.length
     end    

    rescue => e
      flash[:notice]="Sorry, please try again :#{e.message}"
      redirect_to :controller=>'admin',:action=>'login'
    end

  end
  
  def logout
    reset_session
    flash[:notice]="Logged out"
    redirect_to:controller=>'admin',:action=>'login'
  end
  
  
  
  
 
def listingsexport
  @xmls = Xmllisting.find(:all)
#    @listings = Product.paginate(:page => params[:page] ,:order=>"updated_on desc", :per_page =>30)
  end
 def getusers
        myrange = params[:user]

    find_options = { :conditions=>"FirstName like '%#{myrange}%' and UserType !='a'",:order => "FirstName ASC" ,:limit=>'10'}
    @organizations = User.find(:all, find_options).collect(&:FirstName)
          
    render :inline => "<%= content_tag(:ul, @organizations.map { |org| content_tag(:li, h(org)) }) %>"
  end
  
  def deletexml
    Xmllisting.find(params[:id]).destroy
    redirect_to :controller=>'admin',:action=>'listingsexport'
  end
  
 def savexmlfilter
   sql = ""
   
   # set user
   if params[:user] !=""
     user = User.find_by_Email(params[:user])
     sql = sql + " and id_user ='#{user.id}'"
   end
   
   #set property Type
   if params[:propertytype] !="0"
    sql = sql + " and Type = '#{params[:propertytype]}'"  
  end
   
   # set sale or rent or sale/rent
   if params[:salerent] != "2"
     sql = sql + " and (salerent ='#{params[:salerent]}' or salerent = '2')"
   end
   
   # set location
#   sql = sql + " and "

 xmllisting = {
        :title=>"",
        :url=>"",
        :sql=>sql,
        :fsalerent=>params[:salerent],
        :flistingtype=>params[:propertytype],
        :fuser=>params[:user],
        :flocation=>params[:code],
        :flimit=>params[:limit],
        :ftypeshow=>params[:typeshow]
 
 
 }
xml = Xmllisting.new(xmllisting)
xml.save
@xmls = Xmllisting.find(:all)
redirect_to:controller=>'admin',:action=>'listingsexport'
#@product = Product.find(:all,:conditions=>"Deleted = '1' #{sql}")
#   sss.save
 end
  
 def editxmlfilter
   begin
          sql = ""
       
         # set user
         if params[:Euser] !=""
           user = User.find_by_Email(params[:Euser])
           sql = sql + " and id_user ='#{user.id}'"
         end
         
         #set property Type
         if params[:Epropertytype] !="0"
          sql = sql + " and Type = '#{params[:Epropertytype]}'"  
        end
         
         # set sale or rent or sale/rent
         if params[:Esalerent] != "2"
           sql = sql + " and (salerent ='#{params[:Esalerent]}' or salerent = '2')"
         end
     
   
   
      xml = Xmllisting.find(params[:id]).update_attributes("sql"=>"#{sql}","fsalerent"=>"#{params[:Esalerent]}","flistingtype"=>"#{params[:Epropertytype]}","fuser"=>"#{params[:Euser]}","flocation"=>"#{params[:Ecode]}","flimit"=>"#{params[:Elimit]}","ftypeshow"=>"#{params[:Etypeshow]}")
   rescue  
   end

  redirect_to :controller=>'admin',:action=>'listingsexport'
 end
  
  def xmlcommand
       product = Product.find(params[:id])
   text = ""
   if product.update_attributes("xmlcommand"=>"1")
     text = text + "<span style=\"color:red;\">*</span><input type=\"checkbox\" name=\"xmlcommand[#{params[:id]}]\" id=\"xmlcommand_#{params[:id]}\" value=\"#{params[:id]}\" checked=\"checked\" disabled=\"disabled\" />"
   else
     text = text + "<input name=\"xmlcommand[#{params[:id]}]\" id=\"xmlcommand_#{params[:id]}\" value=\"#{params[:id]}\" type=\"checkbox\">"
     text = text + "       <script type=\"text/javascript\">"
     text = text + "       //<![CDATA["
     text = text + "       new Form.Element.EventObserver('xmlcommand_#{params[:id]}', function(element, value) {if (confirm('Confirm to pay by Admin?')) { new Ajax.Updater('pay#{params[:id]}', '/admin/xmlcommand', {asynchronous:true, evalScripts:true, onComplete:function(request){updatestatus('xmlcommand_#{params[:id]}','status#{params[:id]}');}, parameters:'code='+escape($('xmlcommand_#{params[:id]}').value)+'&id=#{params[:id]}'+'&authenticity_token='+window.AUTH_TOKEN}); }else{cancelpay('xmlcommand_#{params[:id]}');}})"
     text = text + "       //]]>"
     text = text + "       </script>"

   end
   
   render :text=>text
  end
  
  
  
  
  
  
  def index
    @mydate = DateTime.now
    nmonth = (@mydate.strftime("%d-%m-%Y")).to_date
      jobs = User.find(:all,:conditions=>"Deleted = '1' and UserType != 'a' and created_on between '#{@mydate.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date - 1}'")

                              strprice = jobs.length 
                              if strprice.to_f >= 1000
                                strprice = strprice.to_s.reverse
                                strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
                                strprice = strprice.gsub(/,$/,"").reverse
                              end    
      session[:totalusers]=  strprice     
      
      
      listingd = Product.find(:all,:conditions=>"Deleted != '0' and Status !='d' and created_on between '#{@mydate.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date - 1}'")

                              strprice = listingd.length 
                              if strprice.to_f >= 1000
                                strprice = strprice.to_s.reverse
                                strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
                                strprice = strprice.gsub(/,$/,"").reverse
                              end    
      session[:totallistings]=  strprice    
  

  
  @listings = Product.find(:all,:conditions=>"Deleted != '0' and (`Status` !='d' or `Status` is null or `Status` = '') ")
  @messages = Mail.find(:all,:conditions=>"master = '0'")
  @users = User.find(:all,:conditions=>"UserType !='a' and Deleted = '1'")
  @businesses = Companies.find(:all,:conditions=>"Head = '0'")


  @mydate2 = DateTime.now
  nmonth = (@mydate2.strftime("%d-%m-%Y")).to_date

  @listingstoday = Product.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @listingsyesterday = Product.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @listingsthisweek = Product.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @listingsthismonth = Product.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  
  @messagetoday = Mail.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @messageyesterday = Mail.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @messagethisweek = Mail.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @messagethismonth = Mail.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  
  @usertoday = User.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @useryesterday = User.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @userthisweek = User.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @userthismonth = User.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  
  @companytoday = Companies.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @companyyesterday = Companies.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @companythisweek = Companies.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @companythismonth = Companies.find(:all,:select=>"id",:conditions=>"created_on between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  
  
  @listingconnecttoday = Listingconect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @listingconnectyesterday = Listingconect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @listingconnectthisweek = Listingconect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @listingconnectthismonth = Listingconect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @listingconnect = Listingconect.find(:all,:select=>"id")
  
  
  @agentconnecttoday = Agentconnect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @agentconnectyesterday = Agentconnect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @agentconnectthisweek = Agentconnect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @agentconnectthismonth = Agentconnect.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @agentconnect = Agentconnect.find(:all,:select=>"id")
  
  
  @leadconnecttoday = Lead.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @leadconnectyesterday = Lead.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 1}' and '#{@mydate.strftime('%d-%m-%Y').to_date}'")
  @leadconnectthisweek = Lead.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 7}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @leadconnectthismonth = Lead.find(:all,:select=>"id",:conditions=>"created_at between '#{@mydate.strftime('%d-%m-%Y').to_date - 30}' and '#{@mydate.strftime('%d-%m-%Y').to_date + 1}'")
  @leadconnect = Lead.find(:all,:select=>"id")
  
  
  
  
  
  
  
  
  
  end
  
  def users
    
    
    
    mysort="users.id desc"
    if params[:sortonline]
      s =  "#{params[:sortonline]}"
      if s["asc"]
        mysort=" users.onlinelistings asc"
      else
        mysort=" users.onlinelistings desc"
      end   
    elsif params[:sortlevel]
        s =  "#{params[:sortlevel]}"
        if s["asc"]
          mysort="users.level asc"
        else
          mysort="users.level desc"
        end     
    elsif params[:sortname]
        s =  "#{params[:sortname]}"
        if s["asc"]
          mysort="users.FirstName asc"
        else
          mysort="users.FirstName desc"
        end 
    elsif params[:sortemail]
        s =  "#{params[:sortemail]}"
        if s["asc"]
          mysort="users.Email asc"
        else
          mysort="users.Email desc"
        end 
    elsif params[:sortagent]
        s =  "#{params[:sortagent]}"
        if s["asc"]
          mysort="users.UserType asc"
        else
          mysort="users.UserType desc"
        end 
    elsif params[:sortcreatedate]
        s =  "#{params[:sortcreatedate]}"
        if s["asc"]
          mysort="users.created_on asc"
        else
          mysort="users.created_on desc"
        end 
    elsif params[:sortlastlogin]
        s =  "#{params[:sortlastlogin]}"
        if s["asc"]
          mysort="users.lastlogin asc"
        else
          mysort="users.lastlogin desc"
        end 
    elsif params[:sortpartner]
        s =  "#{params[:sortpartner]}"
        if s["asc"]
          mysort="users.partner asc"
        else
          mysort="users.partner desc"
        end 
    elsif params[:sortwebsite]
        s =  "#{params[:sortwebsite]}"
        if s["asc"]
          mysort="users.website asc"
        else
          mysort="users.website desc"
        end 
    end
    
    sql = ""
    if params[:usertype]
      sql = " and users.UserType = '#{params[:usertype]}'"
    end
#                                                                              # and products.Deleted = '1' and (products.Status !='d' and products.Status != 'f'  and products.Status != 's' or products.Status is null) ,:joins=>" INNER JOIN products ON users.id =  products.id_user",:group=>"users.id"
#      @users = User.paginate(:page => params[:page], :select=>"users.*, count( * ) AS online" ,:conditions=>"users.UserType !='a'",:order=>"#{onsort}", :per_page =>50)      
      @users = User.paginate(:page => params[:page] , :select=>"users.*" ,:conditions=>"users.UserType !='a' #{sql}",:order=>"#{mysort}", :per_page =>50)
    
  end
  def adduser
#    @user = User.new
  end
  
  def createuser
    begin
#      sss.save
      user = User.new(params[:user])
      if user.save
        flash[:notice]="user created"
        redirect_to :action=>'users'
      else
        flash[:notice]="Please try again"
        @user = User.new(params[:user])
      end
    rescue
      flash[:notice]="please try again"
      @user = User.new(params[:user])
      render :action=>'adduser'
    end
  end
  
  
  
  
  
  
  
  
  
  def edituser
    begin
      @user = User.find(params[:id])
    rescue
      flash[:notice]="please try again"
      redirect_to :action=>'users'
    end
  end
  def updateuser
#    sss.save
    begin
      user = User.find(params[:user][:id])
      #set alllistings when admin downgrade without user request
        alllistings = 5
      if user.level.to_i > params[:user][:level].to_i       
        x = Subscription.find(params[:user][:level])
        alllistings = x.Listings
      end
            #need to update levelshistories table
            ulevels = Levelshistories.find(:all,:conditions=>"Approve = '0' and id_user ='#{params[:user][:id]}'",:order=>"id asc")
            
            for level in ulevels
              if level.Level.to_s == params[:user][:level].to_s
                  level.update_attributes("Approve"=>"1")
                  alllistings = level.Listings
              else
                  level.update_attributes("Approve"=>"c")
              end
              
              
            end
            
            #check overload (when users upgrade for lower level)
            if user.level.to_i > params[:user][:level].to_i
              mylistings = Product.find(:all,:conditions=>"id_user = '#{params[:user][:id]}' and Deleted = '1' and (Status !='d' and Status !='f' or Status is null)",:order=>"id desc")
              if mylistings.length.to_i > alllistings.to_i
                Product.update_all({:Status=>'f'},"id_user = '#{params[:user][:id]}' and Deleted = '1' and (Status !='d' and Status !='f' or Status is null)",:order=>"id asc",:limit=>(mylistings.length.to_i - alllistings.to_i))
              end
            end
      

      
      
      
      user.update_attributes(params[:user])
      flash[:notice]="user updated."
      redirect_to :action=>'users'
    rescue
      flash[:notice]="please try again"
      redirect_to :action=>'users'
    end
  end
  
  
  
  
  
  
  def reuser
    text=""
    begin
      User.find(params[:id]).update_attributes("Deleted"=>"1")
       flash[:notice]="User Deleted."
        text="<a href='#' onclick='new Ajax.Updater(&#39;command_#{params[:id]}&#39;, &#39;/admin/deleteuser/#{params[:id]}&#39;, {asynchronous:true, evalScripts:true}); return false;'><span style='color: red;'>Delete</span></a>"        
     rescue
      text="<a href='#' onclick='new Ajax.Updater(&#39;command_#{params[:id]}&#39;, &#39;/admin/reuser/#{params[:id]}&#39;, {asynchronous:true, evalScripts:true});  return false;'><span style='color: green;'>ReUser</span></a> "
      flash[:notice]="Please try again."
    end
   render :text=>text
 end
 
 
 def deleteuser
   text=""
    begin
      User.find(params[:id]).update_attributes("Deleted"=>"0")
       flash[:notice]="User Deleted."
       text="<a href='#' onclick='new Ajax.Updater(&#39;command_#{params[:id]}&#39;, &#39;/admin/reuser/#{params[:id]}&#39;, {asynchronous:true, evalScripts:true}); return false;'><span style='color: green;'>ReUser</span></a> "
     rescue
      text="<a href='#' onclick='new Ajax.Updater(&#39;command_#{params[:id]}&#39;, &#39;/admin/deleteuser/#{params[:id]}&#39;, {asynchronous:true, evalScripts:true});  return false;'><span style='color: red;'>Delete</span></a>"        
      flash[:notice]="Please try again."
    end
   render :text=>text
 end
 
 
 
 
 
 def finduser
   begin
     if params[:user]
       sql = ""
       if params[:user][:FirstName] and params[:user][:FirstName] !="" and params[:user][:FirstName] !=nil
         sql = sql + " and FirstName like '%#{params[:user][:FirstName]}%'"
       end
       if params[:user][:LastName] and params[:user][:LastName] !="" and params[:user][:LastName] !=nil
         sql = sql + " and LastName like '%#{params[:user][:LastName]}%'"
       end
       if params[:user][:UserName] and params[:user][:UserName] !="" and params[:user][:UserName] !=nil
         sql = sql + " and UserName like '%#{params[:user][:UserName]}%'"
       end
       if params[:user][:Email] and params[:user][:Email] !="" and params[:user][:Email] !=nil
         sql = sql + " and Email like '%#{params[:user][:Email]}%'"
       end
       if params[:user][:UserType] and params[:user][:UserType] !="" and params[:user][:UserType] !=nil
         sql = sql + " and UserType = '#{params[:user][:UserType]}'"
       end
       @users = User.paginate(:page => params[:page] ,:conditions=>"UserType !='a' #{sql}", :per_page =>50)
     else
       @users = User.paginate(:page => params[:page] ,:conditions=>"UserType !='a'", :per_page =>50)
     end  
   rescue
     flash[:notice]="please try again"
     redirect_to :action=>'finduser'
   end
    
 end
 
 
 
 def credits
   @users = User.paginate(:page => params[:page] ,:conditions=>"UserType !='a'", :per_page =>20)
 end
 def addcredit
   @user = User.find(params[:id])
 end
 
 def savecredit
#   sss.save
   begin
     @user = User.find(params[:id_user])
     params[:credit][:RemainCredit] = params[:credit][:FullCredit]
     params[:credit][:Status] = "1"
     @credit = Credit.new(params[:credit])
     if @credit.save
       flash["notice"]="Credits saved"
      redirect_to :action=>'credits'
    else
       flash["notice"]="Credits saved"
       render :action=>'credits'
     end
   rescue
     @user = User.find(params[:id_user])
     @credit = Credit.new(params[:credit])
     flash["notice"]="Please try again"
     render :action=>'addcredit',:id=>params[:id_user]
   end
 end
 
 
 def editcredit
   @user = User.find(params[:id])
   @credits = Credit.paginate(:page => params[:page] ,:conditions=>"id_user ='#{@user.id}'",:order=>"id desc", :per_page =>20)
 end
 
 def updatecredit
#   sss.save
   begin     
     @credit = Credit.find(params[:id])
     @user = User.find(@credit.id_user)
     if @credit.update_attributes(params[:credit])
        flash["notice"]="Credits updated"
        redirect_to :action=>'editcredit',:id=>@credit.id_user
    else
       flash["notice"]="Please try again"
       render :action=>'editcredit',:id=>@credit.id_user
     end
   rescue
#     @user = User.find(params[:id_user])
#     @credit = Credit.new(params[:credit])
     flash["notice"]="Please try again"
     redirect_to :action=>'credits'
   end
 end


 def creditshistory
#   begin
     @history = Credit.find(:all,:conditions=>"Description like '%Admin%'",:group=>"id_user",:order=>"updated_on desc")
#   rescue
#     flash["notice"]="Please try again"
#     redirect_to :action=>'credits'
#   end
 end
    
 def listings
   
     sql = ""
   begin
     if params[:listingID] and params[:listingID] !=""
       sql = sql + " and products.id = '#{params[:listingID]}'"
     else
       price_sql=""
       rprice_sql=""
         #--------- saleprice -------------------------------------------------------------
            if params[:minprice].to_i != 0 and params[:maxprice].to_i != 0
               price_sql = price_sql + " (products.Price between '#{params[:minprice]}' and '#{params[:maxprice]}') "
            elsif params[:minprice].to_i == 0 and params[:maxprice].to_i != 0
               price_sql = price_sql + " products.Price <= '#{params[:maxprice]}' "
            elsif params[:minprice].to_i != 0 and params[:maxprice].to_i == 0
               price_sql = price_sql + " products.Price >= '#{params[:minprice]}' "
            end
         #--------- saleprice -------------------------------------------------------------
            if params[:minrprice].to_i != 0 and params[:maxrprice].to_i != 0
               rprice_sql = rprice_sql + " (products.RPrice between '#{params[:minrprice]}' and '#{params[:maxrprice]}') "
            elsif params[:minrprice].to_i == 0 and params[:maxrprice].to_i != 0
               rprice_sql = rprice_sql + " products.RPrice <= '#{params[:maxrprice]}' "
            elsif params[:minrprice].to_i != 0 and params[:maxrprice].to_i == 0
               rprice_sql = rprice_sql + " products.RPrice >= '#{params[:minrprice]}' "
           end
         #---------------------------------------------------------------------------------
           
       
       if params[:salerent] and params[:salerent] != ""
         if params[:salerent].to_s !="2"
           sql = sql + " and products.salerent = '#{params[:salerent]}'" 
         end    
         
         if params[:salerent].to_s =="1" and price_sql !=""
           #for sale
            sql = sql + " and (#{price_sql})" 
         elsif params[:salerent].to_s =="0" and rprice_sql !=""
            sql = sql + " and (#{rprice_sql})" 
         else
          if price_sql !="" and rprice_sql !=""
            sql = sql + " and (#{price_sql} or #{rprice_sql})"
          elsif price_sql =="" and rprice_sql !=""
            sql = sql + " and (#{rprice_sql})"
          elsif price_sql !="" and rprice_sql ==""
            sql = sql + " and (#{price_sql})"
          end
          
         end
         
       end
              
       if params[:type] and params[:type] != ""
           sql = sql + " and products.Type = '#{params[:type]}'"  
       end
          
       if params[:ref] and params[:ref] != ""
           sql = sql + " and products.Ref like '%#{params[:ref]}%'"  
       end
       
       if params[:title] and params[:title] != ""
           sql = sql + " and products.titlelink like '%#{params[:title]}%'"  
       end
       
        #--------- Bed  ------------------------------------------------------------
            if params[:bed].to_s != "" and params[:bed2].to_s != ""
               sql = sql + " and (products.Bed between '#{params[:bed]}' and '#{params[:bed2]}') "
            elsif params[:bed].to_s == "" and params[:bed2].to_s != ""
               sql = sql + " and products.Bed <= '#{params[:bed2]}' "
            elsif params[:bed].to_s != "" and params[:bed2].to_s == ""
               sql = sql + " and products.Bed >= '#{params[:bed]}' "
            end
        #--------- end Bed  --------------------------------------------------------
       
       if params[:bath] and params[:bath] != ""
           sql = sql + " and products.Bath <= '#{params[:bath]}'"  
       end
       
        #--------- createdate  ------------------------------------------------------------
            if params[:mincreatedate].to_s != "" and params[:maxcreatedate].to_s != ""
               sql = sql + " and (products.created_on between '#{params[:mincreatedate]}' and '#{params[:maxcreatedate]}') "
            elsif params[:mincreatedate].to_s == "" and params[:maxcreatedate].to_s != ""
               sql = sql + " and products.created_on <= '#{params[:maxcreatedate]}' "
            elsif params[:mincreatedate].to_s != "" and params[:maxcreatedate].to_s == ""
               sql = sql + " and products.created_on >= '#{params[:mincreatedate]}' "
            end
        #--------- end createdate  --------------------------------------------------------
       
     end  #end if if params[:listingID]
     
   rescue
     sql = ""
   end
   
   sort = "products.id asc"
   if params[:field].to_s !="" 
     sort = "products.#{params[:field]} #{params[:sort]}"
   end
   @listings = Product.paginate(:page => params[:page],:select=>"products.*, options.Name as typename",:joins=>"INNER JOIN options ON products.Type = options.Value" ,:conditions=>" products.Deleted != '0' and options.option_type = 'Listing' #{sql}",:order=>"#{sort}", :per_page =>50)
 end
  
 def add2virtual
   text =""
   begin
     Product.find(params[:id]).update_attributes("Virtual"=>"1")
     text ="<span style='color:green;'><img src='/images/icons/tick.png'/></span>"
     text = text + "<a onclick=\"new Ajax.Updater('virtual#{params[:id]}', '/admin/del_virtual/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';}, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\" href=\"#\">Off</a>"
   rescue
     text = "<span style='color:green;'><img src='/images/icons/exclamation.png'/></span>"
     text = text + "<a onclick=\"new Ajax.Updater('virtual#{params[:id]}', '/admin/add2virtual/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';}, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\" href=\"#\">Add to Virtual</a>"
   end
   
   render :text => text
   
 end
  
 def del_virtual
   text =""
   begin
     Product.find(params[:id]).update_attributes("Virtual"=>"0")
     text = text + "<a onclick=\"new Ajax.Updater('virtual#{params[:id]}', '/admin/add2virtual/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';}, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\" href=\"#\">Add to Virtual</a>"
     
   rescue
     text ="<span style='color:green;'><img src='/images/icons/tick.png'/></span>"
     text = text + "<a onclick=\"new Ajax.Updater('virtual#{params[:id]}', '/admin/del_virtual/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';}, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\" href=\"#\">Off</a>"
   end
   
   render :text => text
   
 end


 def adminpay
   product = Product.find(params[:id])
   text = ""
   if product.update_attributes("Deleted"=>"1","Status"=>"1","adminpay"=>"1")
     text = text + "<span style=\"color:red;\">*</span><input type=\"checkbox\" name=\"adminpay[#{params[:id]}]\" id=\"adminpay_#{params[:id]}\" value=\"#{params[:id]}\" checked=\"checked\" disabled=\"disabled\" />"
   else
     text = text + "<input name=\"adminpay[#{params[:id]}]\" id=\"adminpay_#{params[:id]}\" value=\"#{params[:id]}\" type=\"checkbox\">"
     text = text + "       <script type=\"text/javascript\">"
     text = text + "       //<![CDATA["
     text = text + "       new Form.Element.EventObserver('adminpay_#{params[:id]}', function(element, value) {if (confirm('Confirm to pay by Admin?')) { new Ajax.Updater('pay#{params[:id]}', '/admin/adminpay', {asynchronous:true, evalScripts:true, onComplete:function(request){updatestatus('adminpay_#{params[:id]}','status#{params[:id]}');}, parameters:'code='+escape($('adminpay_#{params[:id]}').value)+'&id=#{params[:id]}'}); }else{cancelpay('adminpay_#{params[:id]}');}})"
     text = text + "       //]]>"
     text = text + "       </script>"

   end
   
   render :text=>text
 end
 
 def updatestatus
   myparam = params[:product]
   if myparam[:Status] == 'd'
     myparam[:Deleted] = '0'
   end
   Product.find(params[:id]).update_attributes(myparam)
   if params[:page] == ""
     redirect_to :controller=>'admin',:action=>'listings'
   else
     redirect_to :controller=>'admin',:action=>'listings',:page=>params[:page]
   end
   
 end
 
 
 
 def relist
    begin
        product = Product.find(params[:id])
#        myparam = params[:product]
##        if params[:product][:Status] == "d"
#          myparam["Deleted"]="1"
#        end
        product.update_attributes("Deleted"=>"1","Status"=>"1")
#        Product.generatelistings(session[:user_id])
        flash[:notice]="Status updated"
    rescue
      flash[:notice]="Please try again"
    end   
    if params[:page] == ""
     redirect_to :controller=>'admin',:action=>'listings'
   else
     redirect_to :controller=>'admin',:action=>'listings',:page=>params[:page]
   end
  end   
  
    
  def editlisting
    @listing = Product.find(params[:id])
    @user = User.find(@listing.id_user)
    @images = Images.find(:all,:conditions=>"id_product = '#{@listing.id}'")
  end
  
  def updatelisting
#    sss.save
    @listing = Product.find(params[:id])
#    sss.save
    myparam = params[:product]
    if myparam[:Status].to_s == 'd'
      myparam[:Deleted]='0' 
    end
    @listing.update_attributes(params[:product])
    
          if params[:del]
            delhash = params[:del]
            images = Images.find(:all,:conditions=>"id_product = '#{params[:id]}'")
            for img in images
              if delhash.has_key?("#{img.id}") 
                img.destroy
              end
            end
          end
    
            #update condo images
            if @listing.id_condo.to_s != "0"
                condo = Condo.find(@listing.id_condo)
#              condo.update_attributes("id_image"=>"#{id_image}")
              if @listing.Price.to_i > condo.heightprice.to_i
                #set new loprice
                condo.update_attributes("heightprice"=>"#{@listing.Price.to_i}")
              elsif @listing.Price.to_i < condo.lowprice.to_i or condo.lowprice.to_s == "0"
                #set new height price
                condo.update_attributes("lowprice"=>"#{@listing.Price.to_i}")
              end
            end
    flash[:notice]="Listing Updated."
    if params[:myredirect]
      redirect_to "#{params[:myredirect]}"
    else
      redirect_to :controller=>'admin',:action=>'listings'
    end
    
  end



  
  
  
  
  def genxml
    if params[:mydate]
      @mydate = params[:mydate].to_date
    else
      @mydate = DateTime.now
    end
    filename = "newusers.xml"
    if params[:filename]
      filename = params[:filename]
    end
    goupdate = "graph1"
    if params[:goupdate]
      goupdate = params[:goupdate]
    end
    smonth = @mydate.strftime('%m').to_i - 1
  syear = @mydate.strftime('%Y').to_i
  if smonth == 0
    smonth = 12
    syear = syear.to_i - 1
  end
  
  fmonth = @mydate.strftime('%m').to_i + 1
  fyear = @mydate.strftime('%Y').to_i
  if fmonth == 13 
    fmonth = 1
    fyear = fyear.to_i + 1
  end
  ssmonth = ("01-#{smonth}-#{syear}").to_date
  ffmonth = ("01-#{fmonth}-#{fyear}").to_date - 1
    session[:totalusers]='0'
    self.genxmlfile(@mydate,filename)
    text = ""
    if  filename =="newusers.xml"
                    strprice = session[:totalusers]
                              if strprice.to_f >= 1000
                                #strprice = sprintf("%.2f",strprice)
                                strprice = strprice.to_s.reverse
                                strprice = strprice.gsub!(/([0-9]{3})/,"\\1,")
                                strprice = strprice.gsub(/,$/,"").reverse
                              #else
                                #strprice = sprintf("%.2f",strprice)
                              end
#                              session[:totalwon] = strprice
      text = text + "\n<p>Total Users:#{strprice}<br></p>"  
    end
    
    text = text + "\n<OBJECT classid=\"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
    text = text + "\ncodebase=\"http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0\""
    text = text + "\nWIDTH=\"400\" HEIGHT=\"350\" id=\"#{filename}\" ALIGN=\"\" >"
    text = text + "\n<PARAM NAME=movie VALUE=\"/charts/FC_2_3_MSLine.swf\">"

    text = text + "\n<PARAM NAME=FlashVars VALUE=\"&dataURL=/Charts/#{filename}&chartWidth=400&chartHeight=350\">"
    text = text + "\n<PARAM NAME=quality VALUE=high> <PARAM NAME=bgcolor VALUE=#FFFFFF> "
    text = text + "\n<EMBED src=\"/charts/FC_2_3_MSLine.swf\" FlashVars=\"&dataURL=/charts/#{filename}&chartWidth=400&chartHeight=350\" quality=high bgcolor=#FFFFFF  WIDTH=\"400\" HEIGHT=\"350\" NAME=\"FC_2_3_MSLine\" ALIGN=\"\""
    text = text + "\nTYPE=\"application/x-shockwave-flash\" PLUGINSPAGE=\"http://www.macromedia.com/go/getflashplayer\"></EMBED>"
    text = text + "\n</OBJECT>"

    text = text + "<br><a href=\"#\" onclick=\"new Ajax.Updater('#{goupdate}', '/admin/genxml?mydate=#{ssmonth.strftime('%Y-%m-%d')}&filename=#{filename}&goupdate=#{goupdate}', {asynchronous:true, evalScripts:true}); return false;\">&lt;&lt; #{ssmonth.strftime('%B,%Y')}</a>&nbsp;&nbsp;&nbsp;"
    text = text + "\n#{@mydate.strftime('%B,%Y')}&nbsp;&nbsp;&nbsp;"
    if (ffmonth+1).strftime("%Y%m").to_i > DateTime.now.strftime("%Y%m").to_i
        text = text + "\n#{(ffmonth+1).strftime('%B,%Y')}&nbsp;&nbsp;&nbsp;"
    else
        text = text + "\n<a href=\"#\" onclick=\"new Ajax.Updater('#{goupdate}', '/admin/genxml?mydate=#{(ffmonth+1).strftime('%Y-%m-%d')}&filename=#{filename}&goupdate=#{goupdate}', {asynchronous:true, evalScripts:true}); return false;\">#{(ffmonth+1).strftime('%B,%Y')} &gt;&gt;</a>&nbsp;&nbsp;&nbsp;"
    end

#sss.save
    render :text=> text#:action=>'dashboard',:layout=>false)
  end
  
  def genxmlfile(month,filename)
    nm = month.strftime("%m").to_i + 1
    ny = month.strftime("%Y").to_i
    
    if nm == 13
      nm = 1
      ny = ny + 1
    end
    nmonth = "1-#{nm}-#{ny}"
    if filename == "newusers.xml" or filename == "totalusers.xml"
      jobs = User.find(:all,:conditions=>"Deleted = '1' and UserType != 'a' and created_on between '#{month.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date}'",:order=>"created_on asc")
      if filename == "newusers.xml" 
        body ="<graph caption='New Users' numdivlines='4' subcaption='For #{month.strftime('%B,%Y')}' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>"
        seriesname = "New Users"  
      else
        alljobs = User.find(:all,:conditions=>"Deleted = '1' and UserType != 'a' and created_on < '#{month.strftime('1-%m-%Y').to_date}'")
        body ="<graph caption='Toltal Users' numdivlines='4' subcaption='For #{month.strftime('%B,%Y')}' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>"
        seriesname = "Total Users"  
      end
        
      session[:totalusers]=jobs.length
      
    elsif filename == "newlistings.xml" or filename == "totallistings.xml"
#      jobs = User.find(:all,:conditions=>"Deleted = '1' and UserType != 'a' and created_on between '#{month.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date - 1}'")
      jobs = Product.find(:all,:conditions=>"Deleted != '0' and (`Status` !='d' or `Status` is null or `Status` = '') and created_on between '#{month.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date}'",:order=>"created_on asc")#and (expiredate >= '#{DateTime.now.strftime('%Y-%m-%d')}' or expiredate is null or expiredate ='') 
      if filename == "newlistings.xml" 
        body ="<graph caption='New Listings' numdivlines='4' subcaption='For #{month.strftime('%B,%Y')}' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>"
      seriesname = "New Listings"   
      else
        alljobs = Product.find(:all,:conditions=>"Deleted != '0' and (`Status` !='d' or `Status` is null or `Status` = '') and created_on < '#{month.strftime('1-%m-%Y').to_date}' ")#and (expiredate >= '#{DateTime.now.strftime('%Y-%m-%d')}' or expiredate is null or expiredate ='') 
        body ="<graph caption='Total Listings' numdivlines='4' subcaption='For #{month.strftime('%B,%Y')}' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>"
        seriesname = "Total Listings"    
      end
        
      session[:totallistings]=jobs.length
      
#    elsif filename == "totalusers.xml"
#      jobs = User.find(:all,:conditions=>"Deleted = '1' and UserType != 'a' and created_on between '#{month.strftime('1-%m-%Y').to_date}' and '#{(nmonth).to_date - 1}'")
#      body ="<graph caption='Total Users' numdivlines='4' subcaption='For #{month.strftime('%B,%Y')}' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>"
#      #<graph caption='New Users' numdivlines='4' subcaption='For November,2008' showgridbg='1' showhovercap='1' lineThickness='1' animation='1' hoverCapSepChar=' ' anchorScale='0' showNames='1' showValues='0' numVDivLines='12' anchorscale='0' rotateNames='1'>
#      seriesname = "Total Users"    
##      session[:totalusers]=jobs.length
    end
    
    d1   = 0
    d2   = 0
    d3   = 0
    d4   = 0
    d5   = 0
    d6   = 0
    d7   = 0
    d8   = 0
    d9   = 0
    d10  = 0
    d11  = 0
    d12  = 0
    d13  = 0
    d14  = 0
    d15  = 0
    d16  = 0
    d17  = 0
    d18  = 0
    d19  = 0
    d20  = 0
    d21  = 0
    d22  = 0
    d23  = 0
    d24  = 0
    d25  = 0
    d26  = 0
    d27  = 0
    d28  = 0
    d29  = 0
    d30  = 0
    d31  = 0
    for job in jobs
        
                  if job.created_on.strftime("%d").to_i == 1
                    d1 = d1 + 1  
                  elsif job.created_on.strftime("%d").to_i == 2
                    d2 = d2 + 1  
                  elsif job.created_on.strftime("%d").to_i == 3
                    d3 = d3 + 1  
                  elsif job.created_on.strftime("%d").to_i == 4
                    d4 = d4 + 1  
                  elsif job.created_on.strftime("%d").to_i == 5
                    d5 = d5 + 1  
                  elsif job.created_on.strftime("%d").to_i == 6
                    d6 = d6 + 1  
                  elsif job.created_on.strftime("%d").to_i == 7
                    d7 = d7 + 1  
                  elsif job.created_on.strftime("%d").to_i == 8
                    d8 = d8 + 1  
                  elsif job.created_on.strftime("%d").to_i == 9
                    d9 = d9 + 1  
                  elsif job.created_on.strftime("%d").to_i == 10
                    d10 = d10 + 1  
                  elsif job.created_on.strftime("%d").to_i == 11
                    d11 = d11 + 1  
                  elsif job.created_on.strftime("%d").to_i == 12
                    d12 = d12 + 1  
                  elsif job.created_on.strftime("%d").to_i == 13
                    d13 = d13 + 1  
                  elsif job.created_on.strftime("%d").to_i == 14
                    d14 = d14 + 1  
                  elsif job.created_on.strftime("%d").to_i == 15
                    d15 = d15 + 1  
                  elsif job.created_on.strftime("%d").to_i == 16
                    d16 = d16 + 1  
                  elsif job.created_on.strftime("%d").to_i == 17
                    d17 = d17 + 1  
                  elsif job.created_on.strftime("%d").to_i == 18
                    d18 = d18 + 1  
                  elsif job.created_on.strftime("%d").to_i == 19
                    d19 = d19 + 1  
                  elsif job.created_on.strftime("%d").to_i == 20
                    d20 = d20 + 1  
                  elsif job.created_on.strftime("%d").to_i == 21
                    d21 = d21 + 1  
                  elsif job.created_on.strftime("%d").to_i == 22
                    d22 = d22 + 1  
                  elsif job.created_on.strftime("%d").to_i == 23
                    d23 = d23 + 1  
                  elsif job.created_on.strftime("%d").to_i == 24
                    d24 = d24 + 1  
                  elsif job.created_on.strftime("%d").to_i == 25
                    d25 = d25 + 1  
                  elsif job.created_on.strftime("%d").to_i == 26
                    d26 = d26 + 1  
                  elsif job.created_on.strftime("%d").to_i == 27
                    d27 = d27 + 1  
                  elsif job.created_on.strftime("%d").to_i == 28
                    d28 = d28 + 1  
                  elsif job.created_on.strftime("%d").to_i == 29
                    d29 = d29 + 1  
                  elsif job.created_on.strftime("%d").to_i == 30
                    d30 = d30 + 1  
                  elsif job.created_on.strftime("%d").to_i == 31
                    d31 = d31 + 1  
                  end    
          
    end  #end for
 
 
#  end #end if filename    
        
        body = body + "\n<categories>"
        
        body = body + "\n<category name='#{month.strftime('1/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('2/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('3/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('4/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('5/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('6/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('7/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('8/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('9/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('10/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('11/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('12/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('13/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('14/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('15/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('16/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('17/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('18/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('19/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('20/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('21/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('22/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('23/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('24/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('25/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('26/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('27/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('28/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('29/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('30/%m/%y')}' showName='1'/>"
        body = body + "\n<category name='#{month.strftime('31/%m/%y')}' showName='1'/>"
        
        body = body + "\n</categories>"
        
        body = body + "\n<dataset seriesname='#{seriesname}' color='00A900' showValue='0' lineThickness='2' yaxismaxvalue='100' anchorAlpha='0'>"  
        
        if  filename == "totallistings.xml" or filename == "totalusers.xml"

          body = body + "\n<set value='#{alljobs.length + d1}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 }' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 }' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 }' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 }' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27 + d28}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27 + d28 + d29}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27 + d28 + d29 + d30}' />"
          body = body + "\n<set value='#{alljobs.length + d1 + d2  + d3  + d4  + d5  + d6  + d7  + d8  + d9  + d10 + d11 + d12 + d13 + d14 + d15 + d16 + d17 + d18 + d19 + d20 + d21 + d22 + d23 + d24 + d25 + d26 + d27 + d28 + d29 + d30 + d31}' />"
          
        else
          body = body + "\n<set value='#{d1}' />"
          body = body + "\n<set value='#{d2}' />"
          body = body + "\n<set value='#{d3}' />"
          body = body + "\n<set value='#{d4}' />"
          body = body + "\n<set value='#{d5}' />"
          body = body + "\n<set value='#{d6}' />"
          body = body + "\n<set value='#{d7}' />"
          body = body + "\n<set value='#{d8}' />"
          body = body + "\n<set value='#{d9}' />"
          body = body + "\n<set value='#{d10}' />"
          body = body + "\n<set value='#{d11}' />"
          body = body + "\n<set value='#{d12}' />"
          body = body + "\n<set value='#{d13}' />"
          body = body + "\n<set value='#{d14}' />"
          body = body + "\n<set value='#{d15}' />"
          body = body + "\n<set value='#{d16}' />"
          body = body + "\n<set value='#{d17}' />"
          body = body + "\n<set value='#{d18}' />"
          body = body + "\n<set value='#{d19}' />"
          body = body + "\n<set value='#{d20}' />"
          body = body + "\n<set value='#{d21}' />"
          body = body + "\n<set value='#{d22}' />"
          body = body + "\n<set value='#{d23}' />"
          body = body + "\n<set value='#{d24}' />"
          body = body + "\n<set value='#{d25}' />"
          body = body + "\n<set value='#{d26}' />"
          body = body + "\n<set value='#{d27}' />"
          body = body + "\n<set value='#{d28}' />"
          body = body + "\n<set value='#{d29}' />"
          body = body + "\n<set value='#{d30}' />"
          body = body + "\n<set value='#{d31}' />"
        end
          
        
        
          body = body + "\n</dataset>"  
          body = body + "\n\n</graph>"
          
#         FileUtils.mkdir_p(File.dirname(self.path()))  # create dirctory by id_user    
             
        #----------------------------- save file in new directory -----------------------------------------           
            File.open("#{RAILS_ROOT}/public/charts/#{filename}", "w") do |f|     
            f.write(body)
            end
#        sss.save
        #redirect_to "/upload/#{id_project}/#{option_name}/rss.xml"
      #end
  end
  
  def userlistings
     sql = ""
   begin
     if params[:listingID] and params[:listingID] !=""
       sql = sql + " and products.id = '#{params[:listingID]}'"
     else
       price_sql=""
       rprice_sql=""
         #--------- saleprice -------------------------------------------------------------
            if params[:minprice].to_i != 0 and params[:maxprice].to_i != 0
               price_sql = price_sql + " (products.Price between '#{params[:minprice]}' and '#{params[:maxprice]}') "
            elsif params[:minprice].to_i == 0 and params[:maxprice].to_i != 0
               price_sql = price_sql + " products.Price <= '#{params[:maxprice]}' "
            elsif params[:minprice].to_i != 0 and params[:maxprice].to_i == 0
               price_sql = price_sql + " products.Price >= '#{params[:minprice]}' "
            end
         #--------- saleprice -------------------------------------------------------------
            if params[:minrprice].to_i != 0 and params[:maxrprice].to_i != 0
               rprice_sql = rprice_sql + " (products.RPrice between '#{params[:minrprice]}' and '#{params[:maxrprice]}') "
            elsif params[:minrprice].to_i == 0 and params[:maxrprice].to_i != 0
               rprice_sql = rprice_sql + " products.RPrice <= '#{params[:maxrprice]}' "
            elsif params[:minrprice].to_i != 0 and params[:maxrprice].to_i == 0
               rprice_sql = rprice_sql + " products.RPrice >= '#{params[:minrprice]}' "
           end
         #---------------------------------------------------------------------------------
           
       
       if params[:salerent] and params[:salerent] != ""
         if params[:salerent].to_s !="2"
           sql = sql + " and products.salerent = '#{params[:salerent]}'" 
         end    
         
         if params[:salerent].to_s =="1" and price_sql !=""
           #for sale
            sql = sql + " and (#{price_sql})" 
         elsif params[:salerent].to_s =="0" and rprice_sql !=""
            sql = sql + " and (#{rprice_sql})" 
         else
          if price_sql !="" and rprice_sql !=""
            sql = sql + " and (#{price_sql} or #{rprice_sql})"
          elsif price_sql =="" and rprice_sql !=""
            sql = sql + " and (#{rprice_sql})"
          elsif price_sql !="" and rprice_sql ==""
            sql = sql + " and (#{price_sql})"
          end
          
         end
         
       end
              
       if params[:type] and params[:type] != ""
           sql = sql + " and products.Type = '#{params[:type]}'"  
       end
          
       if params[:ref] and params[:ref] != ""
           sql = sql + " and products.Ref like '%#{params[:ref]}%'"  
       end
       
       if params[:title] and params[:title] != ""
           sql = sql + " and products.titlelink like '%#{params[:title]}%'"  
       end
       
        #--------- Bed  ------------------------------------------------------------
            if params[:bed].to_s != "" and params[:bed2].to_s != ""
               sql = sql + " and (products.Bed between '#{params[:bed]}' and '#{params[:bed2]}') "
            elsif params[:bed].to_s == "" and params[:bed2].to_s != ""
               sql = sql + " and products.Bed <= '#{params[:bed2]}' "
            elsif params[:bed].to_s != "" and params[:bed2].to_s == ""
               sql = sql + " and products.Bed >= '#{params[:bed]}' "
            end
        #--------- end Bed  --------------------------------------------------------
       
       if params[:bath] and params[:bath] != ""
           sql = sql + " and products.Bath <= '#{params[:bath]}'"  
       end
       
        #--------- createdate  ------------------------------------------------------------
            if params[:mincreatedate].to_s != "" and params[:maxcreatedate].to_s != ""
               sql = sql + " and (products.created_on between '#{params[:mincreatedate]}' and '#{params[:maxcreatedate]}') "
            elsif params[:mincreatedate].to_s == "" and params[:maxcreatedate].to_s != ""
               sql = sql + " and products.created_on <= '#{params[:maxcreatedate]}' "
            elsif params[:mincreatedate].to_s != "" and params[:maxcreatedate].to_s == ""
               sql = sql + " and products.created_on >= '#{params[:mincreatedate]}' "
            end
        #--------- end createdate  --------------------------------------------------------
       
     end  #end if if params[:listingID]
     
   rescue
     sql = ""
   end
   
   sort = "products.id asc"
   if params[:field].to_s !="" 
     sort = "products.#{params[:field]} #{params[:sort]}"
   end
   @listings = Product.paginate(:page => params[:page],:select=>"products.*, options.Name as typename",:joins=>"INNER JOIN options ON products.Type = options.Value" ,:conditions=>" products.id_user ='#{params[:id]}' and products.Deleted != '0' and options.option_type = 'Listing' #{sql}",:order=>"#{sort}", :per_page =>100)
#    begin
#      #@listings = Product.paginate(:page => params[:page],:select=>"products.*, options.Name as typename",:joins=>"INNER JOIN options ON products.Type = options.Value" ,:conditions=>" products.Deleted != '0' and options.option_type = 'Listing' #{sql}",:order=>"#{sort}", :per_page =>50)
#      @listings = Product.paginate(:page => params[:page] ,:select=>"products.*, options.Name as typename",:joins=>"INNER JOIN options ON products.Type = options.Value" ,:conditions=>" products.id_user ='#{params[:id]}' and products.Deleted != '0' and options.option_type = 'Listing'", :per_page =>100)
#    rescue
#      flash[:notice]="Please try again"
#      redirect_to :action=>'users'
#    end
  end
  
  def manageprovinces
    @provinces = Province.paginate(:page => params[:page] ,:order=>"name asc", :per_page =>100)
  end
  def updatelatlng
#    sss.save
    begin
      #add or edit province
      if params[:formtype]=="add"
        #add province
        myprovince={
        :name=>params[:name],
        :lat=>params[:lat],
        :lng=>params[:lng],
        :mt=>params[:mt]
        }
        province = Province.new(myprovince)
        if province.save
         text="Add Province Successfull"
        else
         text="Please type again"
        end
      else
        #update province
        myprovince={
        :name=>params[:province],
        :lat=>params[:lat],
        :lng=>params[:lng],
        :mt=>params[:mt]
        }
        province = Province.find(params[:id])
        province.update_attributes(myprovince)
         text="Update Province Successfull"
      end
      
    rescue
      text="Please try again"
    end
    render :text=>text
  end
  
  
  
  def addlanguage
    x = 1
    provinces = Province.find(:all,:order=>"name asc")
    for p in provinces
      if params["province_#{x}"].to_s !=""
        p.update_attributes("th"=>"#{params["province_#{x}"]}")
      end
      
      x = x + 1
    end #end for
    redirect_to :action=>"thaiprovince"
    
  end
  
  def magazines
#    begin
      # selete magazine in this month
      if params[:mmonth]
#        # set sdate and fdate by params[mmonth]
#        fdate = params[:mmonth].to_date
#        mm = fdate.strftime("%m").to_i
#        yy = fdate.strftime("%Y").to_i
#        if mm - 1 == 0
#          mm = 12
#          yy = yy.to_i - 1
#        else
#          mm  = mm.to_i - 1
#        end
#        sdate = ("25-#{mm}-#{yy}").to_date
        mmonth = params[:mmonth].to_date
      else        
#          if DateTime.now > ("25-#{DateTime.now.strftime('%m')}-#{DateTime.now.strftime('%Y')}").to_date
#            sdate = ("25-#{DateTime.now.strftime('%m')}-#{DateTime.now.strftime('%Y')}").to_date
#            if DateTime.now.strftime('%m').to_i + 1 == 13
#              fdate = ("25-1-#{DateTime.now.strftime('%Y').to_i + 1}").to_date
#            else
#              fdate = ("25-#{DateTime.now.strftime('%m').to_i + 1}-#{DateTime.now.strftime('%Y')}").to_date
#            end
#          else
#            fdate = ("25-#{DateTime.now.strftime('%m')}-#{DateTime.now.strftime('%Y')}").to_date
#            if DateTime.now.strftime('%m').to_i - 1 == 0
#              sdate = ("25-12-#{DateTime.now.strftime('%Y').to_i - 1}").to_date
#            else
#              sdate = ("25-#{DateTime.now.strftime('%m').to_i - 1}-#{DateTime.now.strftime('%Y')}").to_date
#            end
#          end
          if DateTime.now >("25-#{DateTime.now.strftime('%m')}-#{DateTime.now.strftime('%Y')}").to_date
            mm = DateTime.now.strftime('%m').to_i + 1
            yy = DateTime.now.strftime('%Y').to_i 
            if mm.to_i == 13
              mm = 1
              yy = yy.to_i + 1
            end
            mmonth = ("25-#{mm}-#{yy}").to_date
          else
            mm = DateTime.now.strftime('%m').to_i 
            yy = DateTime.now.strftime('%Y').to_i 
            mmonth = ("25-#{mm}-#{yy}").to_date
            
          end
      end
        
      @fdate = mmonth  
      session[:mmonth]= mmonth
#      @magazines = Magazine.paginate(:page => params[:page] ,:conditions=>" month = '#{mmonth}'created_at between '#{sdate}' and '#{fdate}'",:order=>"id_user, created_at desc",:per_page=>"50")
       @magazines = Magazine.paginate(:page => params[:page] ,:conditions=>"status != 'n' and month = '#{mmonth}'",:order=>"id_user, month desc",:per_page=>"50")
#    rescue
#      # default error
#      @magazines=""
#      @fdate = ""
#    end
  end
  
  def paymagazine
    text = ""
    begin
      #update paystatus by admin
      magz = Magazine.find(params[:id])
      magz.update_attributes("subscrptpay"=>"a","pay"=>"1")
      #update product
       if DateTime.now > DateTime.now.strftime("25-%m-%Y").to_date
        mm = DateTime.now.strftime("%m").to_i + 1
        yy = DateTime.now.strftime("%Y")
        if mm.to_i == 13
          mm = 1
          yy = yy.to_i + 1
        end
        fdate = ("25-#{mm}-#{yy}").to_date
      else
        fdate = DateTime.now.strftime("25-%m-%Y").to_date
      end
      Product.find(magz.id_product).update_attributes("magazine"=>"1","magazinedate"=>"#{fdate}")
      text = "<span style=\"color:green;\">Paid</span>"
    rescue
      #nothing
#      text = "Pending"
      text="<a href=\"#\" style=\"color:red;\" onclick=\"if (confirm('Are your sure to pay by admin?')) { new Ajax.Updater('pay_#{params[:id]}', '/admin/paymagazine/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';}, onLoading:function(request){ document.getElementById('loading').style.display='inline';}}); }; return false;\">Pending</a>"
    end
    render :text=>text
  end
  
  
  def cancelmagz
    text = ""
    begin
      
      #update magazine
      magazine = Magazine.find(params[:id])
      magazine.update_attributes("status"=>"c")
      
      #update product
      Product.find(magazine.id_product).update_attributes("magazine"=>"0")
      text = "<span style='color:red;'><a href=\"#\" onclick=\"if (confirm('Are your sure roll back cancel?')) { new Ajax.Updater('cancel_#{params[:id]}', '/admin/rollbackmagz/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';  }, onLoading:function(request){ document.getElementById('loading').style.display='inline';}}); }; return false;\">Canceled</a></span>"
    rescue
      text = "<a href=\"#\" onclick=\"if (confirm('Are your sure cancel?')) { new Ajax.Updater('cancel_#{params[:id]}', '/admin/cancelmagz/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';  }, onLoading:function(request){ document.getElementById('loading').style.display='inline';}}); }; return false;\">Cancel</a>"
    end
#    flash[:notice]="listing canceled"
    render :text=>text
  end
  
  
  
  def rollbackmagz
    text = ""
    begin
      
      #update magazine
      magazine = Magazine.find(params[:id])
      magazine.update_attributes("status"=>"1")
      
      #update product
      if magzine.pay.to_s =="1"
        Product.find(magazine.id_product).update_attributes("magazine"=>"1")
      else        
        Product.find(magazine.id_product).update_attributes("magazine"=>"p")
      end
      text = "<a href=\"#\" onclick=\"if (confirm('Are your sure cancel?')) { new Ajax.Updater('cancel_#{params[:id]}', '/admin/cancelmagz/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';  }, onLoading:function(request){ document.getElementById('loading').style.display='inline';}}); }; return false;\">Cancel</a>"
    rescue
      text = "<span style='color:red;'><a href=\"#\" onclick=\"if (confirm('Are your sure roll back cancel?')) { new Ajax.Updater('cancel_#{params[:id]}', '/admin/rollbackmagz/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';  }, onLoading:function(request){ document.getElementById('loading').style.display='inline';}}); }; return false;\">Canceled</a></span>"
    
    end
#    flash[:notice]="listing canceled"
    render :text=>text
  end
  
  
  
  def move2nextmonth
    text = ""
    begin
      #
      magazine = Magazine.find(params[:id])
      if DateTime.now >("25-#{DateTime.now.strftime('%m')}-#{DateTime.now.strftime('%Y')}").to_date
        mm = DateTime.now.strftime('%m').to_i + 2
        yy = DateTime.now.strftime('%Y').to_i 
        if mm.to_i == 13
          mm = 1
          yy = yy.to_i + 1
        elsif mm.to_i == 14
          mm = 2
          yy = yy.to_i + 1
        end
        mmonth = ("25-#{mm}-#{yy}").to_date
      else
        mm = DateTime.now.strftime('%m').to_i + 1
        yy = DateTime.now.strftime('%Y').to_i 
        if mm.to_i == 13
          mm = 1
          yy = yy.to_i + 1
        end
        mmonth = ("25-#{mm}-#{yy}").to_date
        
      end
      magazine = magazine.attributes
      magazine["month"]=mmonth
      magz = Magazine.new(magazine)
      if magz.save
         Magazine.find(params[:id]).update_attributes("status"=>"n")  
         
      end
      text = "#{mmonth.strftime('%b, %y')}"
    rescue
      
    end
#    render :text=>text
    redirect_to :action=>'magazines'
  end
  
  
  def condos
    begin
      sql = ""
      if params[:name]
        if params[:name] != ""
          sql = sql + " and `name` like '%#{params[:name]}%'"
          @condoname = params[:name]
        end
      end
      
      if params[:city]
        if params[:city] != ""
          sql = sql + " and `city` like '%#{params[:city]}%'"
          @city = params[:city]
        end
      end
      
      @condos = Condo.paginate(:page => params[:page],:conditions=>"status = '1' #{sql}",:order=>"id desc" ,:per_page=>"25")
    rescue
      redirect_to :action=>'index'
    end
  end
  
  
  def updatecondo
    text = ""
    begin
      Condo.find(params[:id]).update_attributes(params[:condo])
      text = "Condo Updated"
    rescue
      text = "Please try again"
    end
    render :text=> text
  end
  
  
  def deletecondo
    text=""
    begin 
      #update listings #,:condo_history=>params[:id]
      Product.update_all({:id_condo=>'0',:condo_history=>params[:id]},"id_condo = '#{params[:id]}'",:order=>"id asc")
      Condo.find(params[:id]).update_attributes("status"=>"0")
      flash[:notice]="Deleted"
      text="<img src='/icon/icon_close.gif'/>Deleted"
    rescue
      text="<a href=\"#\" onclick=\"new Ajax.Updater('command_#{params[:id]}', '/admin/deletecondo/#{params[:id]}', {asynchronous:true, evalScripts:true}); return false;\">Delete</a>"
      flash[:notice]="Please try again"
    end
    render :text => text
#    redirect_to :action=>'condos'
  end
  
  def mergecondo
#    sss.save
    begin 
      if params[:mergeID].to_i != params[:mycondo].to_i
          #update listings #,:condo_history=>params[:id]
          Product.update_all({:id_condo=>params[:mycondo],:condo_history=>params[:mergeID]},"id_condo = '#{params[:mergeID]}'",:order=>"id asc")
          Condo.find(params[:mergeID]).update_attributes("status"=>"0")
          flash[:notice]="Deleted"
      end
    rescue
      flash[:notice]="Please try again"
    end
    redirect_to :action=>'condos'
  end  
  
  def upgradecondo
    begin
      Condo.find(params[:id]).update_attributes("homepage"=>"#{params[:homepage]}")
      if params[:homepage].to_s =="1"
        flash[:notice] = "Upgrade Completed"
      else
        flash[:notice] = "Downgrade Completed"
      end      
    rescue
      flash[:notice]="Please try again"
    end
    redirect_to :action=>'condos'
  end
  
  def changecondoimage
    text = ""
    begin
      if params[:condofile] !="" and params[:condofile] != nil
        # do upload condo image and then update condo table
                    kendio = {}
                    kendio["id_user"]='0'
                    kendio["id_product"]='0'
                     kendio["photo"] = params[:condofile]
                     kendio["paperclipfix"] = "1"
                     kendio["id_condo"]=params[:id]
                    begin
                      iimage = Images.new(kendio)
                      iimage.save
                      
                      #update condo
                      Condo.find(params[:id]).update_attributes("id_image"=>"#{iimage.id}")
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
      else
        # Update condo images with listing image
      Condo.find(params[:id]).update_attributes(params[:condo])
        
      end
      text = "Condo image Completed"
    rescue
      text = "Please try again"  
    end
    render :text => text
  end
  
  
  
  
  
  def development
    begin
      @developments = Development.find(:all,:conditions=>"status = '1'")
    rescue
      redirect_to :action=>'index'         
    end
  end
  
  
  def addcondos
    begin
      if session[:Admin_id]
        @condo = Condo.new()
      else
        redirect_to :action=>'login'
      end
    rescue
      redirect_to :action=>'index'
    end
  end
  
  
  def createcondo
    begin
      #save condo
      if session[:Admin_id]
        @condo = Condo.new(params[:condo])
        if @condo.save          
          #save condo
          
              if params[:image] !="" and params[:image] != nil
                      kendio = {}
                      kendio["id_user"]='0'
                      kendio["id_product"]='0'
                      kendio["photo"] = params[:image]
                      kendio["paperclipfix"] = "1"
                      kendio["id_condo"]=@condo.id
                    begin
                      iimage = Images.new(kendio)
                      iimage.save
                      @condo.update_attributes("id_image"=>"#{iimage.id}")
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end#end if params[:image]
              redirect_to :action=>'condos'
        else
          render :action=>'addcondos'
        end  #end if @condo.save
        
      else
        redirect_to :action=>'login'
      end
    rescue
      render :action=>'addcondos'
    end
  end
  
  
  def editcondos
    begin
      if session[:Admin_id]
        @condo = Condo.find(params[:id])
      else
        redirect_to :action=>'login'
      end
    rescue
      redirect_to :action=>'index'
    end
  end
  
  
  def updatecondo
    begin
      if session[:Admin_id]
        @condo = Condo.find(params[:id])
        mycondo = params[:condo]
        if not mycondo[:homepage]
          mycondo[:homepage] = '0'
        end
        if @condo.update_attributes(mycondo)
            #upload image
          
              if params[:image] !="" and params[:image] != nil
                      kendio = {}
                      kendio["id_user"]='0'
                      kendio["id_product"]='0'
                      kendio["photo"] = params[:image]
                      kendio["paperclipfix"] = "1"
                      kendio["id_condo"]=@condo.id
                    begin
                      #delete old image
                      if @condo.id_image.to_i > 0
                        image = Images.find(@condo.id_image)
                        if image.id_product.to_i == 0
                          image.destroy
                        end
                      end
                      
                      iimage = Images.new(kendio)
                      iimage.save
                      @condo.update_attributes("id_image"=>"#{iimage.id}")
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end#end if params[:image]
            redirect_to :action=>'condos'
          else
            render :action=>'editcondos',:id=>@condo.id
        end
      else
        redirect_to :action=>'login'
      end
    rescue
      redirect_to :action=>'index'
    end
  end
  
  
#  def updatehtmldev(id,html)
#    dev = Development.find(id)
#    dev.update_attributes("Url"=>"#{html}")
#  end
  
  def createdevelopment
    begin
      # create development
      if session[:Admin_id]
        dev = Development.new(params[:dev])
        if dev.save
#          updatehtmldev(dev.id,params[:dev][:DevelopmentHtml])
#          logger.info("On controler=>#{params[:dev][:DevelopmentHtml]}")
          #update image
              if params[:image] !="" and params[:image] != nil
                      kendio = {}
                      kendio["id_user"]='0'
                      kendio["id_product"]='0'
                      kendio["photo"] = params[:image]
                      kendio["paperclipfix"] = "1"
                      kendio["id_condo"]='0'
                    begin
                      iimage = Images.new(kendio)
                      iimage.save
                      dev.update_attributes("id_image"=>"#{iimage.id}")
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end#end if params[:image]
          
          
          redirect_to :action=>"development"
        else
          redirect_to :action=>"index"
        end
        
      else
        redirect_to :action=>"index"
      end
    rescue
      redirect_to :action=>"index"
    end
  end
  
  def deletedevelopment
    begin
      # delete development
      if session[:Admin_id]
        dev = Development.find(params[:id])
        dev.update_attributes("status"=>"0")
        redirect_to :action=>"development"
      else
        redirect_to :action=>"index"
      end
    rescue
      redirect_to :action=>'index'
    end
  end
  
 def validateurl
    text = ""
    params[:code] = params[:code].gsub(/[<'">=]/, '')
      users = Development.find(:all,:conditions=>"Url = '#{params[:code]}'")
      if users.length > 0
        text = "<img src=\"/images/icons/exclamation.png\"/> Try orther Url Please"
      else
        text = "<img src=\"/images/icons/accept.png\"/>"
      end
    render :text => text   
 end
  
 def editdevelopment
   begin
     @dev = Development.find(params[:id])
   rescue
     redirect_to :action=>'index'
   end
 end
  
 def updatedevelopment
#    begin
      # create development
      if session[:Admin_id]
        dev = Development.find(params[:id])
        if dev.update_attributes(params[:dev])
#          updatehtmldev(dev.id,params[:dev][:DevelopmentHtml])
#          logger.info("On controler=>#{params[:dev][:DevelopmentHtml]}")
          #update image
              if params[:image] !="" and params[:image] != nil
                      kendio = {}
                      kendio["id_user"]='0'
                      kendio["id_product"]='0'
                      kendio["photo"] = params[:image]
                      kendio["paperclipfix"] = "1"
                      kendio["id_condo"]='0'
                    begin
                      iimage = Images.new(kendio)
                      iimage.save
                      dev.update_attributes("id_image"=>"#{iimage.id}")
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end#end if params[:image]
          
          
          redirect_to :action=>"development"
        else
          redirect_to :action=>"index"
        end
        
      else
        redirect_to :action=>"index"
      end
#    rescue
#      redirect_to :action=>"index"
#    end
   
 end
  
  
 def apilistings
   begin
     @listings = Product.paginate(:page => params[:page] ,:conditions=>" Deleted != '0' and APIListingID != '0'",:order=>"APIListingID asc", :per_page =>25)
   rescue
     redirect_to "/"
   end
 end
  
 def onlineapilisting
   text = ""
   begin
     
     listing = Product.find(params[:id])
     listing.update_attributes("Status"=>"1")
     text = "<a href=\"#\" onclick=\"if (confirm('Are you sure to Offline?')) { new Ajax.Updater('apilisting#{params[:id]}', '/admin/offlineapilisting/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';}, onLoading:function(request){document.getElementById('loading').style.display='inline';}}); }; return false;\">Online</a> "
   rescue
     text = ""
   end
   render :text => text
 end
  
 def offlineapilisting
   text = ""
   begin
     
     listing = Product.find(params[:id])
     listing.update_attributes("Status"=>"f")
     text = "<a href=\"#\" onclick=\"if (confirm('Are you sure to Online?')) { new Ajax.Updater('apilisting#{params[:id]}', '/admin/onlineapilisting/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading').style.display='none';}, onLoading:function(request){document.getElementById('loading').style.display='inline';}}); }; return false;\">Offline</a> "
   rescue
     text = ""
   end
   render :text => text
 end
  
 
# def advertising
#   begin
#     
#   rescue
#     redirect_to :action=>'index'
#   end
# end
 def qualitylistings
   begin
     @listings = Product.paginate(:page => params[:page] ,:conditions=>" Deleted != '0' and Quality = '0'",:order=>"APIListingID asc", :per_page =>25)
   rescue
     redirect_to :action=> 'index'
   end
   
 end
 def deletelisting
   text = ""
   begin
     Product.find(params[:id]).update_attributes("Deleted"=>"0")
      text = "<img src='/images/icons/tick.png'/>"
   rescue
    text = "<a href=\"#\" onclick=\"new Ajax.Updater('delete#{params[:id]}', '/admin/deletelisting/#{params[:id]}', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('delete#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Delete</a>"
    end
   render :text =>text
 end
 
  def approve
    text = ""
    begin
      Product.find(params[:id]).update_attributes("Quality"=>"1")
      text = "<img src='/images/icons/tick.png'/>"
    rescue
      text = "<a href=\"#\" onclick=\"new Ajax.Updater('apilisting#{params[:id]}', '/admin/approve/#{params[:id]}', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('apilisting#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Yes</a>"
      
    end
   render :text =>text
    
  end
  
  def manageapi
    @rethaiapis =Rethaiapi.paginate(:order=>"Status desc, id asc",:page => params[:page] , :per_page =>20)
  end
  def deleteapi
    text = ""
    begin
      r_api = Rethaiapi.find(params[:id])
      r_api.update_attributes("Status"=>"0")
      text = "<img src='/icon/icon_close.gif' /><a href=\"#\" onclick=\"new Ajax.Updater('delete_#{params[:id]}', '/admin/reopenapi/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';  }, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\">Reopen</a>"
    rescue
      text = "<img src='/images/icons/accept.png' /><a href=\"#\" onclick=\"new Ajax.Updater('delete_#{params[:id]}', '/admin/deleteapi/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';  }, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\">Delete</a>"
    end
   render :text =>text
  end
  
  
  def reopenapi
    text = ""
    begin
      r_api = Rethaiapi.find(params[:id])
      r_api.update_attributes("Status"=>"1")
      text = "<img src='/images/icons/accept.png' /><a href=\"#\" onclick=\"new Ajax.Updater('delete_#{params[:id]}', '/admin/deleteapi/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';  }, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\">Delete</a>"      
    rescue
      text = "<img src='/icon/icon_close.gif' /><a href=\"#\" onclick=\"new Ajax.Updater('delete_#{params[:id]}', '/admin/reopenapi/#{params[:id]}', {asynchronous:true, evalScripts:true, onComplete:function(request){document.getElementById('loading#{params[:id]}').style.display='none';  }, onLoading:function(request){ document.getElementById('loading#{params[:id]}').style.display='inline';}}); return false;\">Reopen</a>"
    end
   render :text =>text
  end
  
  
  
  def editapi
    begin
      if params[:id]
        @api = Rethaiapi.find(params[:id])
      else
       redirect_to :action=>'index'
      end
    rescue
       redirect_to :action=>'index'
    end
  end
  
  def updateapi
#    sss.save
    begin
      api = Rethaiapi.find(params[:id])
      myparam = params[:api]
      strdow = ""
      if params[:chk]        
        if params[:chk][:DOW0]
          strdow = strdow + "0"
        end       
        if params[:chk][:DOW1]
          strdow = strdow + "1"
        end       
        if params[:chk][:DOW2]
          strdow = strdow + "2"
        end       
        if params[:chk][:DOW3]
          strdow = strdow + "3"
        end       
        if params[:chk][:DOW4]
          strdow = strdow + "4"
        end       
        if params[:chk][:DOW5]
          strdow = strdow + "5"
        end       
        if params[:chk][:DOW6]
          strdow = strdow + "6"
        end
      end
      myparam["DOW"]=strdow
      api.update_attributes(myparam)
    rescue
    end
      redirect_to :action=>'manageapi'
  end
  
  
  def rss
  @xmls = Rssfeed.find(:all)
#    @listings = Product.paginate(:page => params[:page] ,:order=>"updated_on desc", :per_page =>30)
  end
  def saverss
   sql = ""
   
   # set user
   if params[:user] !=""
     user = User.find_by_Email(params[:user])
     sql = sql + " and id_user ='#{user.id}'"
   end
   
   #set property Type
   if params[:propertytype] !="0"
    sql = sql + " and Type = '#{params[:propertytype]}'"  
  end
   
   # set sale or rent or sale/rent
   if params[:salerent] != "2"
     sql = sql + " and (salerent ='#{params[:salerent]}' or salerent = '2')"
   end
   
   # set location
#   sql = sql + " and "

 xmllisting = {
        :title=>"#{params[:title]}",
        :url=>"",
        :sql=>sql,
        :fsalerent=>params[:salerent],
        :flistingtype=>params[:propertytype],
        :fuser=>params[:user],
        :flocation=>params[:code],
        :flimit=>params[:limit],
        :ftypeshow=>params[:typeshow],
        :link=>params[:link]
 
 
 }
xml = Rssfeed.new(xmllisting)
xml.save
@xmls = Rssfeed.find(:all)
redirect_to:controller=>'admin',:action=>'rss'
#@product = Product.find(:all,:conditions=>"Deleted = '1' #{sql}")
#   sss.save
end

  
 def editrss
   begin
          sql = ""
       
         # set user
         if params[:Euser] !=""
           user = User.find_by_Email(params[:Euser])
           sql = sql + " and id_user ='#{user.id}'"
         end
         
         #set property Type
         if params[:Epropertytype] !="0"
          sql = sql + " and Type = '#{params[:Epropertytype]}'"  
        end
         
         # set sale or rent or sale/rent
         if params[:Esalerent] != "2"
           sql = sql + " and (salerent ='#{params[:Esalerent]}' or salerent = '2')"
         end
     
   
   
      xml = Rssfeed.find(params[:id]).update_attributes("sql"=>"#{sql}","fsalerent"=>"#{params[:Esalerent]}","flistingtype"=>"#{params[:Epropertytype]}","fuser"=>"#{params[:Euser]}","flocation"=>"#{params[:Ecode]}","flimit"=>"#{params[:Elimit]}","ftypeshow"=>"#{params[:Etypeshow]}","link"=>"#{params[:Elink]}","title"=>"#{params[:Etitle]}")
   rescue  
   end

  redirect_to :controller=>'admin',:action=>'rss'
 end
  
  def deleterss
    Rssfeed.find(params[:id]).destroy
    redirect_to :controller=>'admin',:action=>'rss'
  end
  
  
  def savecontrol
    bcontrol = Backgroundcontrol.find(1)
    bcontrol.update_attributes("run_control"=>"#{params[:bcontrol].to_i}")
    
    flash[:notice]="Background control updated"
    redirect_to :action=>'backgroundcontrol'
  end

 def getemail
        myrange = params[:user]

    find_options = { :conditions=>"Email like '%#{myrange}%' and UserType !='a'",:order => "Email ASC" ,:limit=>'10'}
    @organizations = User.find(:all, find_options).collect(&:Email)
          
    render :inline => "<%= content_tag(:ul, @organizations.map { |org| content_tag(:li, h(org)) }) %>"
  end

 
 def companies
   begin
       sql = ""
       sql_email = ""
       sql_phone = ""
       mysort = "id desc"
     begin
       if params[:field] and params[:field] !=""
          mysort = "#{params[:field]} #{params[:sort]}"
       end
     rescue
       mysort = "id desc"
     end
     
     begin
       if params[:compname] and params[:compname]!=""
         sql = sql + " and companies.Name like '%#{params[:compname]}%'"
       end
       if params[:url] and params[:url]!=""
         sql = sql + " and companies.Url like '%#{params[:url]}%'"
       end
#       if params[:phone] and params[:phone]!=""
#         sql = sql + " and companies.phone like '%#{params[:phone]}%'"
#       end
       if params[:location] and params[:location]!=""
         sql = sql + " and companies.city like '%#{params[:location]}%'"
       end
       
       
       #--------------------------------- Email ------------------------------------------
       if params[:email] and params[:email]!=""
        comps = Companies.find(:all,:select=>"Head",:conditions=>"Email like'%#{params[:email]}%' and Head != '0'")
        i = 1
        for com in comps
          sql_email = sql_email + " companies.id = '#{com.Head}'"
          if i < comps.length
            sql_email = sql_email + " or "
          end
          i = i + 1
        end
        
        if sql_email != ""
          sql_email = " and (#{sql_email}) "
        end
      end
      
       #-------------------------------- Phone -------------------------------------------
       
       if params[:phone] and params[:phone]!=""
        phone_comps = Companies.find(:all,:select=>"Head",:conditions=>"phone like'%#{params[:phone]}%' and Head != '0'")
        i = 1
        for pcom in phone_comps
          sql_phone = sql_phone + " companies.id = '#{pcom.Head}'"
          if i < phone_comps.length
            sql_phone = sql_phone + " or "
          end
          i = i + 1
        end
        
        if sql_phone != ""
          sql_phone = " and (#{sql_phone}) "
        end
       end
       #---------------------------------------------------------------------------
       
       
       
       
     rescue
       sql = ""
     end
     
     
    @companies = Companies.paginate(:page => params[:page] ,:select=>"users.onlinelistings,companies.*",:joins=>"left JOIN users ON companies.id_user = users.id",:conditions=>"companies.Head ='0' #{sql} #{sql_email} #{sql_phone}",:order=>mysort, :per_page =>50)
   rescue
    redirect_to :action=>'index'
   end
 end
 
 def viewcompany
   begin
#     @company = Companies.find(params[:id])
    @companies = Companies.find(:all,:conditions=>"id = '#{params[:id]}' and Head = '0'")
#    @branchs = Companies.find(:all,:conditions=>"id_user = '#{session[:user_id]}' and Head != '0'",:order=>"id asc")
    if @companies.length < 1
      redirect_to :action=>'companies'
    end
   rescue
     redirect_to :action=>'companies'
   end
 
 end

  
  
  def updatebusiness
#    begin
      # update company
        mycomp = Companies.find(params[:id_comp])
        if mycomp.update_attributes(params[:comp])
              if params[:image]
              if params[:image][:uploaded_data] and params[:image][:uploaded_data] !="" and params[:image][:uploaded_data] !=nil
                    begin
                      iimage = Businesslogo.new(params[:image])
                      iimage.save
                      oldimg = mycomp.logo
                      mycomp.update_attributes("logo"=>"#{iimage.id}")
                      #delete old images  
                      if oldimg !="" and oldimg !=nil and oldimg !='0'
                        Businesslogo.find(oldimg).destroy                          
                      end
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end
            end
            
            
            if params[:image2]
              if params[:image2][:uploaded_data] and params[:image2][:uploaded_data] !="" and params[:image2][:uploaded_data] !=nil
                    begin
                      iimage = Businesslogo.new(params[:image2])
                      iimage.save
                      oldimg = mycomp.logo2
                      mycomp.update_attributes("logo2"=>"#{iimage.id}")
                      #delete old images  
                      if oldimg !="" and oldimg !=nil and oldimg !='0'
                        Businesslogo.find(oldimg).destroy                          
                      end
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end
            end
            
            
            if params[:image3]
              if params[:image3][:uploaded_data] and params[:image3][:uploaded_data] !="" and params[:image3][:uploaded_data] !=nil
                    begin
                      iimage = Businesslogo.new(params[:image3])
                      iimage.save
                      oldimg = mycomp.logo3
                      mycomp.update_attributes("logo3"=>"#{iimage.id}")
                      #delete old images  
                      if oldimg !="" and oldimg !=nil and oldimg !='0'
                        Businesslogo.find(oldimg).destroy                          
                      end
                    rescue Exception => e
                      logger.info "can not upload file: #{e.message}"
                    end
              end
            end
          flash[:notice]="Update OK"
#          redirect_to:action=>'business'
        else
          flash[:notice]="Please try again"
#          redirect_to:action=>'business'
        end
        
    
    
      #update branch
        mybranch = params[:branch]
        mybranch[:city]= mycomp.city
      if params[:id_branch].to_i > 0
        branch = Companies.find(params[:id_branch])
        branch.update_attributes(mybranch)
      else
        mybranch[:Head]= mycomp.id
        mybranch[:id_user]= mycomp.id_user
        branch = Companies.new(mybranch)
        branch.save
        
      end
#    rescue
#      flash[:notice]="Please try again"
#    end
    
     
     redirect_to :action=>'companies'
     
  end
  
 
 def deletecompany
#   sss.save
   begin
     comp = Companies.find(params[:id])
     branchs = Companies.find(:all,:conditions=>"id_user = '#{comp.id_user}' and Head = '#{comp.id}'")
     for b in branchs
        b.destroy
      end
      #delete logo
      begin
        if comp.logo.to_i > 0
          Businesslogo.find(comp.logo).destroy
        end
      rescue
      end
      
      begin
        if comp.logo2.to_i > 0
          Businesslogo.find(comp.logo2).destroy
        end
      rescue
      end
      
      begin
        if comp.logo3.to_i > 0
          Businesslogo.find(comp.logo3).destroy
        end
      rescue
      end
      
      
     comp.destroy
     if params[:page]
       redirect_to :action=>'companies',:page=>params[:page]
     else
       redirect_to :action=>'companies'
     end
     
   rescue
     redirect_to :action=>'companies'
   end
 end
 
 
 
 def deletespam
   text = ""
   err = ""
   begin
     product = Product.find(params[:id])
     begin
       #delete user
       User.find(product.id_user).destroy
     rescue
#      err = err + "|U:#{product.id_user} "
     end
     
     begin
       #delete condo
         if product.id_condo.to_i > 0
           condo = Condo.find(product.id_condo)
           if condo.created_at.strftime("%Y%m%d").to_i == product.created_on.strftime("%Y%m%d").to_i
             condo.destroy
#             err = err + "delete condo:#{condo.id}"
           else
             err = err + "|not delete condo:#{condo.id}"             
           end
         end
     rescue          
#        err = err + "|Condo:#{product.id_condo} "
     end
     
     begin
       #delete Images
       images = Images.find(:all,:conditions=>"id_product = '#{product.id}'")
       for img in images
          img.destroy
       end
     rescue          
#        err = err + "|Images: "
     end
     
     begin
       #delete listings
        product.destroy
     rescue
#        err = err + "|can not delete listing:#{product.id} "
     end
      text = "<img src='/images/icons/tick.png'/>#{err}"
   rescue
    text = "<a href=\"#\" onclick=\"new Ajax.Updater('spam#{params[:id]}', '/admin/deletespam/#{params[:id]}', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('spam#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Spam!</a>"
    end
   render :text =>text
 end
 
 
 def leadusers
   
      @leads = User.paginate(:page => params[:page], :select=>"id,FirstName,LastName,Email,Phone,leadstatus,leademail,level" ,:conditions=>"Deleted = '1' and leadstatus !='0'",:order=>"leadstatus", :per_page =>20)
      
 end
 
 def updateleaduser
   user = User.find(params[:id])
   user.update_attributes("leadstatus"=>"#{params[:approve]}")
   
   if params[:approve].to_s == "1" 
     lstatus = "<span style=\"color:green;\"> Approved </span>"
   elsif params[:approve].to_s == "n" 
    lstatus = "<span style=\"color:red;\"> Not Interest </span>"
   elsif params[:approve].to_s == "p" 
     lstatus = "<span style=\"color:green;\"> Pending </span>"
   elsif params[:approve].to_s == "c" 
    lstatus = "<span style=\"color:red;\"> Canceled </span>"
   elsif params[:approve].to_s == "d" 
     lstatus = "<span style=\"color:red;\"> Deleted </span>"
   end
   
   script="<script type='text/javascript'>document.getElementById('status#{params[:id]}').innerHTML='#{lstatus}';</script>"
   render :text =>"#{lstatus}#{script}"
 end
 
  def leadinv
    begin
      @user = User.find(params[:id])
      @level = Subscription.find(@user.level)
      if params[:month]
        sdate = ("01-#{params[:month].to_date.strftime('%m-%Y')}").to_date
        fdate = ("01-#{params[:month].to_date.strftime('%m-%Y')}").to_date + 1.month
      else
        sdate = ("01-#{DateTime.now.strftime('%m-%Y')}").to_date
        fdate = ("01-#{DateTime.now.strftime('%m-%Y')}").to_date + 1.month      
      end
                                                                                        # and conect != '3'
      @leadinvs = Leadhistories.find(:all,:conditions=>"id_user = '#{params[:id]}' and checked = '1' and created_at >= '#{sdate}' and created_at < '#{fdate}' ",:order =>"conect asc, created_at desc")
    rescue
      redirect_to :action=>'leadusers'
    end
  end
 
 
 
 
  def leadapprove
    text = ""
    if session[:Admin_id]
      begin
        if params[:approve].to_s =="yes"
          User.find(params[:id]).update_attributes("leadstatus"=>"1")
          text = "Approved"
        elsif params[:approve].to_s =="no"
          User.find(params[:id]).update_attributes("leadstatus"=>"0")
          text = "denied"
        else
          text = "Try again!"
        end
      rescue
        text = "Try again!"
      end
    end
    render :text => text
  end
 
 
 def leadqc
   begin
      @leads = Lead.paginate(:page => params[:page],
                          :conditions=>" `approve` = '0' ",
                          :order=>"id desc", 
                          :per_page =>50
                          )
   rescue
     redirect_to :action=> 'index'
   end
   
 end
 
 
 def leadagentqc
   begin
      @leads = Agentconnect.paginate(:page => params[:page],
                          :conditions=>" `approve` = '0' ",
                          :order=>"id desc", 
                          :per_page =>50
                          )
   rescue
     redirect_to :action=> 'index'
   end
   
 end
 
 
 def leadquality
   begin
      @leads = Listingconect.paginate(:page => params[:page],
                          :select=>"products.Type,products.salerent,products.City,products.Price,products.RPrice,products.Bed,products.Bath,products.created_on as p_createdon,products.Detail,products.Ref,listingconects.*,users.Email,users.Phone,users.FirstName,users.LastName",
                          :joins=>"INNER JOIN products ON listingconects.id_product = products.id INNER JOIN users ON products.id_user = users.id",
                          :conditions=>" listingconects.`approve` = '0' ",
                          :order=>"listingconects.id desc", 
                          :per_page =>50
                          )
   rescue
     redirect_to :action=> 'index'
   end
   
 end
 
 
 
  def approvelead
    text = ""
    begin
      if params[:leadtype].to_s == "1"   #leads
        if Lead.find(params[:id]).update_attributes("approve"=>"1")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('apilisting#{params[:id]}', '/admin/approvelead/#{params[:id]}?leadtype=1', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('apilisting#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Yes</a>"
        end
      elsif  params[:leadtype].to_s == "2"  
        if Agentconnect.find(params[:id]).update_attributes("approve"=>"1")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('apilisting#{params[:id]}', '/admin/approvelead/#{params[:id]}?leadtype=2', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('apilisting#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Yes</a>"
        end
      elsif  params[:leadtype].to_s == "3"   #listing connects
        if Listingconect.find(params[:id]).update_attributes("approve"=>"1")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('apilisting#{params[:id]}', '/admin/approvelead/#{params[:id]}?leadtype=3', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('apilisting#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Yes</a>"
        end
      end
      
    rescue
      text = "<span style='color:red;'>try again</span>"
      
    end
   render :text =>text
    
  end
 
 
  def deletelead
    text = ""
    begin
      if params[:leadtype].to_s == "1"   #leads
        lead = Lead.find(params[:id])
        if lead.update_attributes("approve"=>"d")
          # delete user too(update Delete status)
          begin
            User.find(lead.id_user).update_attributes("Deleted"=>"0")
          rescue            
          end
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('spam#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=1', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('spam#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Spam</a>"
        end
      elsif  params[:leadtype].to_s == "2"   #Aentconnect
        if Agentconnect.find(params[:id]).update_attributes("approve"=>"d")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('spam#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=2', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('spam#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Spam</a>"
        end
      elsif  params[:leadtype].to_s == "3"  
        if Listingconect.find(params[:id]).update_attributes("approve"=>"d")#listing connects
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('spam#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=3', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('spam#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Spam</a>"
        end
      elsif params[:leadtype].to_s == "41"   #leads
        lead = Lead.find(params[:id])
        if lead.update_attributes("approve"=>"d")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('delete#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=41', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('delete#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Delete</a>"
        end
      elsif  params[:leadtype].to_s == "42"   #Aentconnect
        if Agentconnect.find(params[:id]).update_attributes("approve"=>"d")
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('delete#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=42', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('delete#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Delete</a>"
        end
      elsif  params[:leadtype].to_s == "43"  
        if Listingconect.find(params[:id]).update_attributes("approve"=>"d")#listing connects
          text = "<img src='/images/icons/tick.png'/>"
        else
          text = "<a href=\"#\" onclick=\"new Ajax.Updater('delete#{params[:id]}', '/admin/deletelead/#{params[:id]}?leadtype=43', {asynchronous:true, evalScripts:true, onLoading:function(request){document.getElementById('delete#{params[:id]}').innerHTML = <img src='/images/loading.gif'/>;}}); return false;\">Delete</a>"
        end
      end
      
    rescue
      text = "<span style='color:red;'>try again</span>"
      
    end
   render :text =>text
    
  end
 
 
 def checkimages
   begin
     sql = ""
     if params[:date]  # => YYYY-mm-dd
       sql = " and created_on <='#{params[:date]} 00:00:00'"
     end
     products = Product.find(:all,:select=>"id",:conditions=>"Deleted = '1' #{sql}")
     for p in products
       images = Images.find(:all,:conditions=>"id_product ='#{p.id}'",:limit=>1) 
       if images.length <= 0
         p.update_attributes("imageorder"=>"0")
       end
     end
     flash[:notice]="Completed"
     
     render :text=>"Completed"
   rescue
     render :text=>"Please Try again"
   end
#   redirect_to :action=>'index'
end

 def setpaypalprice
   begin
     Option.find(params[:id]).update_attributes(params[:option])
     flash[:notice]="Completed set 'Paypal Price' "
   rescue
     flash[:notice]="Please try again"
   end
   redirect_to :action=>'index'
 end
 
# 
# def checklocation
#   begin
#     #
##     SELECT * FROM `products`WHERE `City` = 'pattaya'AND (`lat` <= '12.7')
#sql = ""
#if params[:date] #YYYY-mm-dd
#  sql = sql + " AND `created_on` >= '#{params[:date]} 00:00:00'"
#end
#products = Product.find(:all,:conditions=>"`City` = 'pattaya' AND (`lat` <= '12.7') #{sql} ")
#for p in products
#  if p.lng.to_f < 100 and p.lng.to_s !=""
#    # it wrong location need to update
#    p.update_attributes("lat"=>"","lng"=>"")
#  end
#end
#   rescue
#     
#   end
# end
 
 def fixthumb

  require 'net/http'
  images = Images.find(:all,:conditions=>"paperclipfix = '0'",:limit=>200,:order=>"id asc")
  for image in images
    #update images
            url = "http://rethai.com#{image.photo.url()}"
            while url[" "]
              url[" "] = "%20"
            end
            while url["["]
              url["["] = "%5B"
            end
            while url["]"]
              url["]"] = "%5D"
            end
            url1 = URI.parse(url)
            url2 = URI.parse(url).host
            url3 = url.slice!(url.index(url2)..url.length)
            
#            begin
            chkimg= url3.downcase
            if chkimg[".jpg"] or chkimg[".png"] or chkimg[".gif"]
            if url3["#{url2.to_s}"] 
              url3["#{url2.to_s}"] = ""
            end
              Net::HTTP.start(url2) { |http|
                resp = http.get("#{url3}")
                File.open("#{RAILS_ROOT}/public/letfun.jpg", "wb") { |file|
                  file.write(resp.body)              
                 }
              }
#              begin
              File.open("#{RAILS_ROOT}/public/letfun.jpg", "r") { |file|
                                  kendio = {}
                                  kendio["photo"] = file
                                  kendio["paperclipfix"] = "1"
#                                  begin
                                    image.update_attributes(kendio)
#                                  rescue
#                                  end
                 } 
#                 rescue
#                 end 
            end #end if chkimg[".jpg"]
#            rescue
#            end
  end #end for image in images
    
 end #end def 
 
 
 def setonlinelistings
  begin
    users = User.find(:all,:conditions=>"UserType !='a' and Deleted = '1' ")
    for user in users
      listings = Product.find(:all,:select=>"count(id) as count",:conditions=>"id_user = '#{user.id}' and Deleted = '1' and (Status ='1' or Status is null) ")
      user.update_attributes("onlinelistings"=>listings[0].count.to_i)
    end
  rescue
  end
  redirect_to :action=>'users'
 end
 
 
 def allleadqualities
   begin
      @leads = Listingconect.paginate(:page => params[:page],
                          :select=>"products.Type,products.salerent,products.City,products.Price,products.RPrice,products.Bed,products.Bath,products.created_on as p_createdon,products.Detail,products.Ref,listingconects.*,users.Email,users.Phone,users.FirstName,users.LastName",
                          :joins=>"INNER JOIN products ON listingconects.id_product = products.id INNER JOIN users ON products.id_user = users.id",
                          :conditions=>" listingconects.`approve` = '1' ",
                          :order=>"listingconects.id desc", 
                          :per_page =>50
                          )
   rescue
     redirect_to :action=> 'index'
   end
   
 end
  
 def allleadqc
   begin
      @leads = Lead.paginate(:page => params[:page],
                          :conditions=>" `approve` = '1' ",
                          :order=>"id desc", 
                          :per_page =>50
                          )
   rescue
     redirect_to :action=> 'index'
   end
   
 end
 
 
 def markoffline
   
 end
 def makeoffline
   begin
     mydate = params[:offlindate].to_date
     sql = "UPDATE `products` SET `Status` = 'f',`comment` = 'offline by admin' WHERE `Status` != 'f' and `updated_on` <= '#{mydate}';"
     # sql = "UPDATE `products` SET `Status` = 'f',`comment` = 'offline by admin' WHERE `Status` != 'f' and `created_on` <= '#{mydate}';"
     Product.connection.execute(sql)
     flash[:notice]="Finished Offline listings"
   rescue
     flash[:notice]="Error try again!"
   end
   redirect_to :action=>'markoffline'
 end
 
 end



