class AgencyController < ApplicationController
  layout 'rethai'
  def index
    begin
      p_id = params[:id]
      if p_id["-"]
        #get ID at last -
        p_id = p_id.split('-').collect{|e|e.to_i} 
        p_id = p_id[(p_id.length - 1)]
      end
      
      @agency = Companies.find(p_id)
      @listings = Product.paginate :conditions=> "Deleted = '1' and (Status !='d' and Status != 'f'  and Status != 's' or Status is null) and id_user = '#{@agency.id_user}' ",:page => params[:page],:order =>"`SearchFeatured` desc", :per_page => 20
      @alllistings = Product.find(:all, :conditions=> "Deleted = '1' and (Status !='d' and Status != 'f'  and Status != 's' or Status is null) and id_user = '#{@agency.id_user}'")
    rescue
      redirect_to "/"
    end
  end
  
  def allagencies
    #begin
      # selecte only real estate users
      users = User.find(:all,:conditions=>"UserType = 'g'")
      allagency = ""
      i = 1
      for u in users
        allagency = allagency + " id_user = '#{u.id}' "
        if i < users.length
          allagency = allagency + " or "
        end
        i = i + 1
      end
      if params[:location] and params[:location].to_s !="0"
#        @location = params[:location]
        
        sql = ""
        if params[:location].to_s !=""
          sql = " and companies.city = '#{params[:location]}' "
        end
        @agencies = Companies.paginate  :conditions=> "companies.Head = '0'  and users.UserType ='g' #{sql} ",
                                        :joins=>" INNER JOIN users ON users.id = companies.id_user",
                                        :order=>"users.level desc",
                                        :page => params[:page], 
                                        :per_page => 20
#        @agencies = Companies.paginate :conditions=> "Head = '0'  #{sql}",:page => params[:page], :per_page => 5
#        @agencies = Companies.find(:all, :conditions=> "Head = '0' #{sql}")
      else
#        @location = ""
        @agencies = Companies.paginate :conditions=> "companies.Head = '0' and users.UserType ='g'",
                                      :joins=>" INNER JOIN users ON users.id = companies.id_user",
                                      :order=>"users.level desc",
                                      :page => params[:page], 
                                      :per_page => 20
#        @agencies = Companies.paginate :conditions=> "Head = '0'",:page => params[:page], :per_page => 5
#        @agencies = Companies.find(:all, :conditions=> "Head = '0' and city = '#{params[:location]}'")
      end
      
    #rescue
      #redirect_to "/"
#    end
  end
  
  
  def linksite
    begin
      @agency = Companies.find(params[:id])
    rescue
      redirect_to "/"
    end
  end
end
