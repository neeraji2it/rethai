class Credit < ActiveRecord::Base
  def self.discredit(id_user,x)
    ccredit = Credit.find(:all,:conditions=>"id_user = '#{id_user}' and RemainCredit > '0' and (Status = 'c' or Status = 'C')",:order=>"created_on desc",:limit=>"3")
    ccredit.reverse
    
    for mycredit in ccredit
        mycredit.update_attributes("RemainCredit"=>"#{mycredit.RemainCredit.to_i - x}")
    end
    
  end
  
  
  def self.takecredits(id_user,credits,ctype,id_product)
    ccredits = Credit.find(:all,:conditions=>"id_user = '#{id_user}' and RemainCredit > '0' and Status = '1'",:order=>"created_on desc",:limit=>"3")
    x=0
    for cc in ccredits
                x = x + cc.RemainCredit.to_f
                if x.to_f >= credits.to_f
                  cc.update_attributes("RemainCredit"=>"#{x - 10}")
                  #update creditdetail
                  if credits.to_s == '10'
                    cddetail ={
                      :id_user =>id_user,
                      :command_type =>ctype,
                      :id_product => id_product,
                      :credit => 10                   
                    } 
                  end
                    creditdetail = Creditdetail.new(cddetail)
                    creditdetail.save
                  break
                else
                  cc.update_attributes("RemainCredit"=>"0")
                end
                
              end
  end
  
  def self.checkcredit(id_user)
    credits = Credit.find(:all,:conditions=>"id_user = '#{id_user}' and RemainCredit > '0' and Status ='1'")
    x = 0
    for credit in credits
      x = x + credit.RemainCredit
    end
    
    #return x (credit)
    x
  end
  
  def self.freecredit(id_user,credits)
    kendio = {
    :id_user=>id_user,
    :FullCredit=>credits,
    :RemainCredit=>credits,
    :Price=>'0',
    :TXN_ID=>'0',
    :Description=>'Add by Admin',
    :Status=>'1',
    :SubscrId=>'0',
    :HeadSubscript=>'0'
    }
    freecredit = Credit.new(kendio)
    freecredit.save
  end
  
end
