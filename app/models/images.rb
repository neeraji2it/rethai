class Images < ActiveRecord::Base
	
   #acts_as_cached
   
   
        has_attached_file :photo,
                    :storage => :filesystem,
                    :styles => {:original => "640x480>",
                                :thumb  => "120x120#",
                                :medium => "350x300>",
                                :large  => "900x600>"
                                },   #if need change dimensions here not work maybe try to edit at vendor/plugins/paperclip/lip/paperclip/attachment.rb(31) again
                    :path => "#{RAILS_ROOT}/public/listingdata/:attachment/:id_partition/:style.:extension",
#                    :url => "http://files.rethai.com/listingdata/:attachment/:id/:style.:extension"
                    :url => "/listingdata/:attachment/:id_partition/:style.:extension"
                            #http://files.rethai.com
                    
#                    validates_attachment_content_type :photo, :content_type => ['image/jpeg', 'image/png', 'image/gif']
                    
#                    :bucket => 'hea'
#       old for s3                     
#                    :s3_credentials => "#{RAILS_ROOT}/config/s3.yml",           
#                     :path => ":attachment/:id/:style.:extension",
#                    :path => "/var/app/attachments/:class/:id/:style/:filename"




            # for fleximages
            #acts_as_fleximage :image_directory => 'public/images/uploaded_photos'
#            acts_as_fleximage :image_directory =>"#{RAILS_ROOT}/public/listingdata/:attachment/:id/:style.:extension"
end
