class Producthistory < ActiveRecord::Base
  
  
  def self.createlisting(id_user,id_product)
    myhistory = {
    :id_user    => id_user,
    :id_product =>id_product,
    :delist     =>"0",
    :startdate  =>DateTime.now,
    :stopdate   =>DateTime.now + 30,
    :remain     =>30
    }
    prodhist = Producthistory.new(myhistory)
    prodhist.save
  end
  
  
  def self.delist(id_user,id_product,remain)
    myhistory = {
    :id_user    => id_user,
    :id_product =>id_product,
#    :startdate  =>DateTime.now,
#    :stopdate   =>DateTime.now + 30,
    :remain     =>remain
    }
    prodhist = Producthistory.new(myhistory)
    prodhist.save
  end
  
  
  def self.relist(id_user,id_product,remain)
    myhistory = {
    :id_user    => id_user,
    :id_product =>id_product,
    :delist     =>"0",
    :startdate  =>DateTime.now,
    :stopdate   =>DateTime.now + remain,
    :remain     =>remain
    }
    prodhist = Producthistory.new(myhistory)
    prodhist.save
  end
  
  
end
